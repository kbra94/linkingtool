#### Version: 0.1

Table of Contents
=================

  - [Introduction](#introduction)
  - [Conceptual Overview](#conceptual-overview)
  	- [Entities](#entities)
  	- [Activities](#activities)
  	- [Metadata](#metadata)
  - [Structure](#structure)
  	- [Tables](#tables)
  	- [Activity Stream](#activity-stream)
 
 

# Introduction

The AI Configurator is an extension to the official box configurator. The main usage of the box configuratior is to generate at the beginning of the project all function block and hardware IOs. The AI configurator should extend by adding data structures after the project was created. 
test

# Requirements
The AI configurator aims for these design goals:

- **linking io hardware** - link io hardware






# Conceptual Overview

The AI configurator is having different features for automatically generating/changing structures inside a TwinCAT project. The AI Configurator entities are terminals


## Terminals

A Terminal is a EtherCAT device, that is inside the IO tree.

There are multiple Terminals, which should be linked.


**Type**|**Description**
-----|-----|-----
EL1004|Digital Input
EL1008|Digital Input
EL2004|Digital Output
EL2008|Digital Output
EL3403|Analog Inout
EL6695|Bridge
MOVIDRIVE+DFE24|Movidrive
MOVIMOT+MFE72A | Movimot
AS-i EtherCAT Single Gateway|ASI Gateways

patterns:

// terminals

TIID^EtherCAT Master CU2508 - X5^=16+EHB_27#000.0700-K1 (EK1100)^=16+EHB_27#000.0700-K51x (EL2008)^Channel 1^Output

TIID^EtherCAT Master CU2508 - X4^=16+1141#000.0700-K1 (EK1100)^=16+1141#000.0700-K52 (EL2008)^Channel 6^Output

TIID^=17+111#000.0705-X2 (EtherCAT)^=17+11#000.0700-K1 (EK1100)^=17+11#000.0700-K3 (EL1004)^Channel 1^Input

TIID^EtherCAT Master CU2508 - X1^=16+111#000.0700-K1 (EK1100)^=16+111#000.0700-K48 (EL1008)^Channel 3^Input


// ASI

TIID^=17+111#000.0705-X1 (EtherCAT)^=17+111#000.0700-K201 (EK1100) (AS-i EtherCAT Single Gateway)^TxPDO 1^Ec-Flags, Inputs Single/A Slaves 1..15

TIID^=17+113#000.0705-X1 (EtherCAT)^=17+113#000.0700-K201 (EK1100) (AS-i EtherCAT Single Gateway)^TxPDO 2^Inputs Single/A Slaves 16..31

TIID^=17+111#000.0705-X1 (EtherCAT)^=17+111#000.0700-K201 (EK1100) (AS-i EtherCAT Single Gateway)^RxPDO 1^Hi-Flags, Outputs Single/A Slaves 1..15

TIID^=17+113#000.0705-X1 (EtherCAT)^=17+113#000.0700-K201 (EK1100) (AS-i EtherCAT Single Gateway)^InfoData^State

TIID^=17+111#000.0705-X1 (EtherCAT)^=17+111#000.0700-K201 (EK1100) (AS-i EtherCAT Single Gateway)^TxPDO 3^Ec-Flags, Inputs B Slaves 1..15

TIID^=17+113#000.0705-X1 (EtherCAT)^=17+113#000.0700-K201 (EK1100) (AS-i EtherCAT Single Gateway)^RxPDO 3^Outputs B Slaves 1..15

TIID^=17+113#000.0705-X1 (EtherCAT)^=17+113#000.0700-K201 (EK1100) (AS-i EtherCAT Single Gateway)^WcState^WcState

TIID^=17+111#000.0705-X1 (EtherCAT)^=17+111#000.0700-K202 (EK1100) (AS-i EtherCAT Single Gateway)^TxPDO 1^Ec-Flags, Inputs Single/A Slaves 1..15

TIID^=17+113#000.0705-X1 (EtherCAT)^=17+113#000.0700-K202 (EK1100) (AS-i EtherCAT Single Gateway)^TxPDO 2^Inputs Single/A Slaves 16..31

TIID^=17+111#000.0705-X1 (EtherCAT)^=17+111#000.0700-K202 (EK1100) (AS-i EtherCAT Single Gateway)^RxPDO 1^Hi-Flags, Outputs Single/A Slaves 1..15

TIID^=17+113#000.0705-X1 (EtherCAT)^=17+113#000.0700-K202 (EK1100) (AS-i EtherCAT Single Gateway)^InfoData^State

TIID^=17+111#000.0705-X1 (EtherCAT)^=17+111#000.0700-K202 (EK1100) (AS-i EtherCAT Single Gateway)^TxPDO 3^Ec-Flags, Inputs B Slaves 1..15

TIID^=17+113#000.0705-X1 (EtherCAT)^=17+113#000.0700-K202 (EK1100) (AS-i EtherCAT Single Gateway)^RxPDO 3^Outputs B Slaves 1..15

TIID^=17+113#000.0705-X1 (EtherCAT)^=17+113#000.0700-K202 (EK1100) (AS-i EtherCAT Single Gateway)^WcState^WcState


// Movimot

TIID^=17+113#000.0705-X5 (EtherCAT)^=17+113#003.0010-T4-K1 (MOVIMOT+MFE72A)^InputData1 (Standard 10 PI)^PI 0001

TIID^=17+113#000.0705-X5 (EtherCAT)^=17+113#003.0010-T4-K1 (MOVIMOT+MFE72A)^OutputData1 (Standard 10 PO)^PO 0001

TIID^=17+113#000.0705-X5 (EtherCAT)^=17+113#003.0010-T4-K1 (MOVIMOT+MFE72A)^InfoData^AdsAddr

TIID^=17+113#000.0705-X7 (EtherCAT)^=17+113#003.0060-T3-K1 (MOVIMOT+MFE72A)^InputData1 (Standard 10 PI)^PI 0001

// Movidrive

TIID^=17+113#000.0705-X4 (EtherCAT)^=17+113#003.0010-T2-K1 (MOVIDRIVE+DFE24)^InputData1 (Standard 10 PI)^PI1
TIID^=17+113#000.0705-X4 (EtherCAT)^=17+113#003.0010-T2-K1 (MOVIDRIVE+DFE24)^InputData1 (Standard 10 PI)^PI6

// Generating

// Terminal
TIPC^PLC_1^PLC_1 Instance^PlcTask Inputs^MappingIO.IO_17_111_K42.DI_1

plc = TIPC^PLC_1^PLC_1 Instance^PlcTask Inputs^
termMapping = MappingIO
io = IO
subsystem = 17
cabinet = 111
bmk = K42
direction = DI | DO
channel = 1

// ASI
TIPC^PLC_1^PLC_1 Instance^PlcTask Inputs^MappingASI.IO_AE111_A10000_10.EcFlags_Inputs_Single_A_Slaves_1_15_Circ_1
TIPC^PLC_1^PLC_1 Instance^PlcTask Inputs^MappingASI.IO_AE111_A10000_10.Inputs_Single_A_Slaves_16_31_Circle_1
TIPC^PLC_1^PLC_1 Instance^PlcTask Inputs^MappingASI.IO_AE111_A10000_10.State
TIPC^PLC_1^PLC_1 Instance^PlcTask Outputs^MappingASI.IO_AE111_A10000_10.HiFlags_Outputs_Single_A_Slaves_1_15_Circle_1
TIPC^PLC_1^PLC_1 Instance^PlcTask Outputs^MappingASI.IO_AE113_A10000_10.Outputs_B_Slaves_1_15_Circle_2
TIPC^PLC_1^PLC_1 Instance^PlcTask Outputs^MappingASI.IO_AE111_A10000_10.Outputs_Single_A_Slaves_16_31_Circle_1
TIPC^PLC_1^PLC_1 Instance^PlcTask Outputs^MappingASI.IO_AE111_A10000_10.Outputs_B_Slaves_1_15_Circle_1



plc = TIPC^PLC_1^PLC_1 Instance^PlcTask Inputs
asiMapping = MappingASI
io = IO
cabinet = AE111
cabinetA = A10000
cabinetAA = 10
ec = ec
flags = Flags
direction = Inputs
single = Single
port = A | B
slaves = Slaves
slaveA = 1
slaveAA = 15
circ = Circ
circNumber = 1 | 2


// Movimot
TIPC^PLC_1^PLC_1 Instance^PlcTask Inputs^MappingIO.IO_113_003_0010_01_1_T4_K1.PI_0001
TIPC^PLC_1^PLC_1 Instance^PlcTask Outputs^MappingIO.IO_113_003_0060_01_1_T4_K1.PO_0001

// Movidrive
TIPC^PLC_1^PLC_1 Instance^PlcTask Inputs^MappingIO.IO_111_001_0820_01_1_T1.PI2



# Appendix: Revision History

**Version**|**Date**|**Notes**
-----|-----|-----

1.0|2021-04-30|Initial Spec