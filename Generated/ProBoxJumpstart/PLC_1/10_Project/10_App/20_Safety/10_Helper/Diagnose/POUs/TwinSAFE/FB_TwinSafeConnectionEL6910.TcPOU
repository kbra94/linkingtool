﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_TwinSafeConnectionEL6910" Id="{2b9405de-9613-051c-2b66-ab3424709e65}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_TwinSafeConnectionEL6910
(**************************************************************************************************
Description			:	Evaluate a TwinSAFE connection status and diagnosis to an EtherCAT terminal
Supported terminals : 	EL6910
Documentation 		:	EL6910 TwinSAFE PLC
Version/Date 		:	V1.0.0 / 22.07.2020
***************************************************************************************************)
VAR_INPUT
	tCoEReadCycle				: TIME 	:= T#0MS;			(* Read cycle CoE-Data, e.g. temperature *)
END_VAR

VAR_OUTPUT
	stInfoData			AT %I*	: ST_TsConnectionInfo1Byte;	(* EL6910 \ Connection Info Data \ Message_X Info Data*)
	nEcSlaveState		AT %I*	: UINT;						(* EL6910 \ InfoData \ State *)
	stEcAdsAddr			AT %I*	: ST_AdsAddr;				(* EL6910 \ InfoData \ AdsAddr *)
	
	(* Wc State *)
	bWcState			AT %I*	: BOOL;
	
	(* Input Toggle *)
	bInputToggle		AT %I*	: BOOL;
	
	(* act_ConnectionState *)
	sState						: STRING;
	
	(* act_ConnectionDiag *)          
	sDiag						: STRING;
	sFault						: STRING;					(* FailSafe Error (Hardware)*)
	bError						: BOOL;						(* TwinSAFE-Error *)	                              
END_VAR

VAR	  
	SafeLogicState		AT %I*	: USINT; 
	CycleCounter    	AT %I*	: USINT;                                
	(* act_ConnectionDiag *)        
	nDiag0_3					: BYTE;

	SubsystemErrorData			: ST_ErrorDataSet;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* Function Block TwinSAFE connection status and diagnosis for EL2904 Terminals *)

// Evaluate the connection diagnosis
	act_ConnectionDiag();

// (* Evaluate the connection state *)
	act_ConnectionState();

SubsystemErrorData := F_GetSubSystemExternErrorData();
	
// Check EtherCAT slave state
IF SubsystemErrorData.ErrorType = E_ErrorType.NO_ERROR_PENDING AND nEcSlaveState <> 8 THEN
	// Set External Sybsystem Error
	F_SetExternalSubsystemError(E_ErrorType.INFO, 'TwinSAFE PLC EL6910 state change', E_ErrorConv.ETHERCAT_SLAVE_ERROR);
ELSIF SubsystemErrorData.ErrorParam = 'TwinSAFE PLC EL6910 state change' AND nEcSlaveState = 8 THEN
	F_ResetExternalSubsystemError(Reset := TRUE);
END_IF

IF SubsystemErrorData.ErrorType = E_ErrorType.NO_ERROR_PENDING AND bWcState THEN
	F_SetExternalSubsystemError(E_ErrorType.INFO, 'TwinSAFE PLC EL6910 WcState invaild', E_ErrorConv.ETHERCAT_SLAVE_ERROR);
ELSIF SubsystemErrorData.ErrorParam = 'TwinSAFE PLC EL6910 WcState invaild' AND NOT bWcState THEN
	F_ResetExternalSubsystemError(Reset := TRUE);
END_IF
]]></ST>
    </Implementation>
    <Action Name="act_ConnectionDiag" Id="{6001fc9e-e675-0a27-1425-7466e0b95fbd}">
      <Implementation>
        <ST><![CDATA[(* Evaluate the connection diagnosis *)

	(* Evaluate Bit 0..3 *)
	nDiag0_3 := USINT_TO_BYTE(stInfoData.nDiag) AND 16#0F;

	CASE nDiag0_3 OF
		0:		sDiag := 'No error';
		1:		sDiag := 'Invalid command';
		2:		sDiag := 'Unknown command';
		3:		sDiag := 'Invalid connection ID';
		4:		sDiag := 'Invalid CRC';
		5:		sDiag := 'Watchdog time elapsed';
		6:		sDiag := 'Invalid FSOE address';
		7:		sDiag := 'Invalid data';
		8:		sDiag := 'Invalid communication parameter length';
		9:		sDiag := 'Invalid communication parameters';
		10:		sDiag := 'Invalid user parameter length';
		11:		sDiag := 'Invalid user parameter';
		12:		sDiag := 'FSoE master reset';
		13:		sDiag := 'Module err detected on slave, with option "Module error is ComError" activated';
		14:		sDiag := 'Module err detected on EL290x, with option "Error acknowledge active" activated';
		15:		sDiag := 'Slave not yet started, or unexpected error argument';
	END_CASE

	(* Evaluate Bit 4..7 *)
	sFault:='';

	IF stInfoData.nDiag.4 
	THEN
		sFault	:= 'FSoE slave error detected - '; 
	END_IF

	IF stInfoData.nDiag.5 
	THEN
		sFault	:= concat(sFault,'FSoE slave reports Failsafe Value active - ');
	END_IF

	IF stInfoData.nDiag.6 
	THEN
		sFault	:= concat(sFault,'StartUp - ');
	END_IF

	IF stInfoData.nDiag.7 
	THEN
		sFault	:= concat(sFault, 'FSoE master reports Failsafe Value active');
	END_IF
		
	(* Error *)
	bError := stInfoData.nDiag <> 0;

(* End *)]]></ST>
      </Implementation>
    </Action>
    <Action Name="act_ConnectionState" Id="{a3cc7ffe-dc78-0839-0457-706cb5d8e4f9}">
      <Implementation>
        <ST><![CDATA[(* Evaluate the connection state *)

	CASE stInfoData.nState OF
		100:	sState := 'Reset state';
		101:	sState := 'Session state';
		102:	sState := 'Connection state';
		103:	sState := 'Parameter state';
		104:	sState := 'Data state';
		105:	sState := 'Shutdown state';
	ELSE
		sState := 'unknown';
	END_CASE
	
(* End *)]]></ST>
      </Implementation>
    </Action>
  </POU>
</TcPlcObject>