﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_ElementEmulation" Id="{5eab26b6-e497-40d2-8ec4-3d0375ef9851}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_ElementEmulation EXTENDS FB_ElementConvEmulation
(**************************************************************************************
 * 	Application		:	
 *	Revision		:	$Revision: $
 *	Revision date	:	$Date: 2015-07-13 10:06:47 $
 *	Last changed by	:	$Author:  $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Emulation Element without Hardware IO's
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR
	pElement					: POINTER TO FB_Element; // Address to element
	rElement					: REFERENCE TO FB_Element; // Reference to element
	ElementReg					: ST_ElementRegistryConv; // Registry
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[
(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();


(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();


(**************************************************************************************************************
   Emulation
***************************************************************************************************************)
// If emulation element is initialized
IF ElementData.Init THEN
	
	// Update real data
	M_UpdateRealObjectData();	

	// Overwrite inputs
	A_OverWriteInputs();

END_IF]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{369fa39c-5de3-40f5-a908-31d2e467afa6}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated dummy element
 **************************************************************************************)

// If element is not initialized
IF NOT ElementData.Init THEN

	// Check that linked element number is valid
	IF eElementNumber > E_ElementNumber.E_BEGIN_ELEMENT AND
	   eElementNumber < E_ElementNumber.E_END_ELEMENT THEN
		
			// Check that the real module is initialized
			IF F_IsElementInitialized(eElementNumber := eElementNumber) THEN
				// Module is initialized
				ElementData.Init := TRUE;
				
				// Send debug message
				DebugMsg := CONCAT('Initialization done. Element: ', ElementRegistry[eElementNumber].ElementName);
				fbDebugMsg.M_SendInfoMsg(DebugMsg);
				
				// Initialize real module
				F_InitializeElement(eElementNumber := eElementNumber);
			END_IF
	END_IF
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs" Id="{4f6f3721-4690-446f-b885-c7e37817ab94}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
* 	NAME		A_OverWriteInputs
* 	FUNCTION	Overwrite hardware inputs for emulation
**************************************************************************************)]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{e5dc23e5-2898-45af-81f2-c2a0072717d8}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid element number
IF eElementNumber > E_ElementNumber.E_BEGIN_ELEMENT AND
   eElementNumber < E_ElementNumber.E_END_ELEMENT THEN

	// Is reference valid  
	IF __ISVALIDREF(ElementRegistry[eElementNumber].rElement) THEN
		// Build address to reference
		pElement := ADR(ElementRegistry[eElementNumber].rElement);

		// If address is possible
		IF pElement <> 0 THEN
			// Build reference to memory
			rElement REF= pElement^;
		ELSE
			// Reinitialize module
			ElementData.Init := FALSE;
		END_IF
	ELSE
		// Reinitialize module
		ElementData.Init := FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
  </POU>
</TcPlcObject>