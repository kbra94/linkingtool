﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_Element" Id="{6bb572bc-0e1f-41f2-8b59-da7fb6c3653d}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_Element EXTENDS FB_ElementConv
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: $
 *	Revision date	:	$Date: $
 *	Last changed by	:	$Author:  $
 *	URL				:	$URL: $
 *
 *	Purpose			:	ProBox element without drive.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *
 **************************************************************************************)
VAR
	Idx						: INT; 								// Iterator
	MemberState				: ST_PositionState; 				// Member state
	eFunctionNumber			: E_FunctionNumber; 				// Function number
	FunctionStopped			: BOOL; 							// Function are stopped
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[
(**************************************************************************************************************
   Input mapping
***************************************************************************************************************)
A_InputMapping();


(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();


(**************************************************************************************************************
   State control
***************************************************************************************************************)
A_StateControl();


(**************************************************************************************************************
   Error handler
***************************************************************************************************************)
A_ErrorHandler();


(**************************************************************************************************************
   Handle element functions
***************************************************************************************************************)
A_HandleFunctions();


(**********************************************************************************************************
   Drive control
***********************************************************************************************************)
// Contains no drive

(**************************************************************************************************************
   Interface handler out
***************************************************************************************************************)
A_InterfaceHandlerOut();


(**************************************************************************************************************
   Output mapping
***************************************************************************************************************)
A_OutputMapping();

]]></ST>
    </Implementation>
    <Action Name="A_ErrorHandler" Id="{a0658c9c-c20f-42fa-a25d-97f7870abe02}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_ErrorHandling
 * 	FUNCTION	Handles element 
 **************************************************************************************)
 

(**************************************************************************************************************
  Monitor element errors
***************************************************************************************************************)
M_MonitorElementErrors();]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_HandleFunctions" Id="{65424bd6-ea33-4ddb-86be-2ebac6108adc}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_HandleElementFunctions
 * 	FUNCTION	Handle interface between element and function layer
 **************************************************************************************)

// Initialize
ElementData.FunctionOrder.ReqDriveRun := FALSE;
ElementData.FunctionOrder.Enable := TRUE;
 
// Go trough all functions and handle states
FOR Idx := 1 TO NUMBER_OF_FUNCTIONS_PER_ELEMENT DO
	// Get next function number
	eFunctionNumber := ElementData.MemberList.FunctionList[Idx].eFunctionNumber;

	// Is number valid 
	IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
	   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN
	   
	   		// Summarize function enables 
		IF NOT ElementInterface.In.FunctionOrders[Idx].Enable THEN
			ElementData.FunctionOrder.Enable := FALSE;
		END_IF
	
		// Summarize requests
		ElementData.FunctionOrder.ReqDriveRun := ElementData.FunctionOrder.ReqDriveRun OR
													ElementInterface.In.FunctionOrders[Idx].ReqDriveRun;
	ELSE
		// No more functions in list
		EXIT;
	END_IF
END_FOR]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_Init" Id="{0953875a-b70f-4bf1-adfd-242623ab5bc2}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize element
 **************************************************************************************)

// Update Registry
M_UpdateRegistry();
 
// Set init flag after one cycle
IF NOT ElementData.Init AND ElementData.InitRunning THEN
	// If the system is initialized and its allowed to initialize
	IF ElementData.OperationState.Info.SystemReady AND
	   SettingsElement.eElementNumber < ElementData.OperationState.InitRelease.eLimitElementNumber THEN
		// Initialization started when base element is initialized
		IF M_InitElement(FALSE) THEN
			// Function initialized - and initialization done
			ElementData.Init := TRUE;
			ElementData.InitRunning := FALSE;
			
			// Set reset cmd
			ElementData.ErrorData.Error.ResetError := TRUE;
			
			DebugMsg := CONCAT('Initialization done. Element: ', ConfigElement.ElementName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
		END_IF
	END_IF
// If the function is not initialized
ELSIF NOT ElementData.Init THEN
	// Reset values
	ElementData.OperationState := ElementBase.OperationStateEmpty;

	// Load function specific configuration to settings
	//Settings := Config;
	
	// Reset values and load config
	M_PreInit();
	ElementData.InitRunning := SettingsElement.eElementNumber > 0;
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InputMapping" Id="{f3331ee7-06b0-4a16-9d8d-d2e2d93c4f21}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InputMapping
 * 	FUNCTION	Input mapping
 **************************************************************************************)
 
 
(**************************************************************************************
   Map HW Inputs
***************************************************************************************)
;


(**************************************************************************************
   Map external inputs
***************************************************************************************)
// Get Master master states - Stored in ElementInterface.In.ElementStates.Master
M_UpdateMasterSlaveStates();

// Get element orders from all functions 
M_GetElementOrders();
 
// Get commanded group state and mode belonging to this element
ElementInterface.In.GroupState := fbGroupManager.M_GetGroupCmd(
											meElementNumber := SettingsElement.eElementNumber);
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InterfaceHandlerOut" Id="{6e383958-3caf-4b4b-9f0f-f621695fab06}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InterfaceHandlerOut
 * 	FUNCTION	Handles outgoing interfaces
 **************************************************************************************)
 
 
(**************************************************************************************************************
   ITC Handler Out
***************************************************************************************************************)
M_ITC_ProcessOut();]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OutputMapping" Id="{d8e1cf9e-eb5f-4ac9-a4c8-fb3234269612}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OutputMapping
 * 	FUNCTION	Handles element specific outpus
 **************************************************************************************)

(**************************************************************************************
   Map HW Outputs
***************************************************************************************)
 ;
 
 
(**************************************************************************************
   Map element interface
***************************************************************************************)
ElementInterface.Out.State.DriveReady := ElementData.FunctionOrder.Enable;

ElementInterface.Out.State.DriveRunning := FALSE;

ElementInterface.Out.State.Speed := 0;

ElementInterface.Out.State.EncoderValue := 0;

ElementInterface.Out.State.Error := ElementData.ErrorData.Error.ErrorType = E_ErrorType.ERROR;


]]></ST>
      </Implementation>
    </Action>
  </POU>
</TcPlcObject>