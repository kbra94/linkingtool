﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_Element_BLT_INT1_Emulation" Id="{14109183-f197-0e36-3043-4a2a1fe04a6c}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_Element_BLT_INT1_Emulation EXTENDS FB_ElementConvEmulation
(**************************************************************************************
 * 	Application		:	QuickBox2.0
 *	Revision		:	$Revision: 488065 $
 *	Revision date	:	$Date: 2021-10-13 13:42:49 +0200 (Mi, 13 Okt 2021) $
 *	Last changed by	:	$Author: g7nowan $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/QuickMove/02_Controls/QuickBox/20_Release/trunk/01_Software/QB2Jumpstart/JumpStart/20_Product/20_QuickBox/22_InterRoll/13_Elements/ELEMENT_BLT/FB_Element_BLT_INT1_Emulation.TcPOU $
 *
 *	Purpose			:	Emulation Belt without Tracking Element
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				27.06.2018		dlu					Typical defined
 *
 **************************************************************************************)
VAR
	pElement					: POINTER TO FB_Element_BLT_INT1; // Address to element
	rElement					: REFERENCE TO FB_Element_BLT_INT1; // Reference to element
	ElementReg					: ST_ElementRegistryConv; // Registry
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[
(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();


(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();


(**************************************************************************************************************
   Emulation
***************************************************************************************************************)
// If emulation element is initialized
IF ElementData.Init THEN
	
	// Update real data
	M_UpdateRealObjectData();	

	// Overwrite inputs
	A_OverWriteInputs();

END_IF]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{c235383f-9181-0839-297d-c3d59dfc58e8}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated dummy element
 **************************************************************************************)

// If element is not initialized
IF NOT ElementData.Init THEN

	// Check that linked element number is valid
	IF eElementNumber > E_ElementNumber.E_BEGIN_ELEMENT AND
	   eElementNumber < E_ElementNumber.E_END_ELEMENT THEN
		
			// Check that the real module is initialized
			IF F_IsElementInitialized(eElementNumber := eElementNumber) THEN
				// Module is initialized
				ElementData.Init := TRUE;
				
				// Send debug message
				DebugMsg := CONCAT('Initialization done. Element: ', ElementRegistry[eElementNumber].ElementName);
				fbDebugMsg.M_SendInfoMsg(DebugMsg);
				
				// Initialize real module
				F_InitializeElement(eElementNumber := eElementNumber);
			END_IF
	END_IF
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs" Id="{fdf59dc8-ef21-01a5-0ced-917ebbe93905}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
* 	NAME		A_OverWriteInputs
* 	FUNCTION	Overwrite hardware inputs for emulation
**************************************************************************************)

rElement.HW_Inputs.State:=8;
rElement.HW_Inputs.PI01_StatusWord1.2:=TRUE;
rElement.HW_Inputs.PI01_StatusWord1.1:=TRUE;
rElement.HW_Inputs.PI03_StatusWord2.9 := TRUE;

rElement.HW_Inputs.PI01_StatusWord1.0:= rElement.HW_Outputs.PO1_ControlWord.1;]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{1659e497-5562-07e9-00e5-884abb5e31c0}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid element number
IF eElementNumber > E_ElementNumber.E_BEGIN_ELEMENT AND
   eElementNumber < E_ElementNumber.E_END_ELEMENT THEN

	// Is reference valid  
	IF __ISVALIDREF(ElementRegistry[eElementNumber].rElement) THEN
		// Build address to reference
		pElement := ADR(ElementRegistry[eElementNumber].rElement);

		// If address is possible
		IF pElement <> 0 THEN
			// Build reference to memory
			rElement REF= pElement^;
		ELSE
			// Reinitialize module
			ElementData.Init := FALSE;
		END_IF
	ELSE
		// Reinitialize module
		ElementData.Init := FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
  </POU>
</TcPlcObject>