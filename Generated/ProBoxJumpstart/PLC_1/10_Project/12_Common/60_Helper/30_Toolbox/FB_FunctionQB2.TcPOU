﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_FunctionQB2" Id="{b0c1561f-709d-418c-a997-5e0802023d16}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_FunctionQB2 EXTENDS FB_FunctionConv
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 488065 $
 *	Revision date	:	$Date: 2021-10-13 13:42:49 +0200 (Mi, 13 Okt 2021) $
 *	Last changed by	:	$Author: g7nowan $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/QuickMove/02_Controls/QuickBox/20_Release/trunk/01_Software/QB2Jumpstart/JumpStart/20_Product/20_QuickBox/22_InterRoll/12_E_Functions/FB_FunctionQB2.TcPOU $
 *
 *	Purpose			:	QB2 Function base for all elementfunctions.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
 VAR_INPUT	
	ManualOrder				: BOOL;	// Manual Drive Button	 
 END_VAR
 VAR
	ZoneIdx 				: INT; // Index
	Idx						: INT; // Iterator
	AllZonesStopped         : BOOL;
	EmptyManualCommands		: ST_ITC_ManualMotion; // Empty manual commands
	ManualPartsAktiv		: ST_OrderManualMotions; // Manual Part Altive	
	
	
	FunctionStartRequestCycleCount: UDINT;	// to avoid starting all Functions on the same Powersupply at the same time
	fbGetCurTaskIdx  	: GETCURTASKINDEX;
END_VAR
VAR_OUTPUT
	ManualMovmentSelected	: booL;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[]]></ST>
    </Implementation>
    <Action Name="A_AssignTransportParam" Id="{8490b74a-b987-4dfe-909a-18a8a1c9d584}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_AssignTransportParam
 * 	FUNCTION	Assign the variable transport parameter based to the current side
 **************************************************************************************)

(**********************************************************************************************************
	 Get transport control data (Previous, Next, Own)
***********************************************************************************************************)
IF ZoneIdx <= 0 OR ZoneIdx > SettingsFunction.NumberOfZones THEN
	ZoneIdx:=1;
END_IF

// Init var 
//ZoneData[1].TransportControlData.TransportParam.ZoneEndLength := 0;
//ZoneData[1].TransportControlData.TransportParam.StopDelayDistance := 0;

ZoneData[ZoneIdx].TransportControlData.TransportParam.ZoneLength := SettingsFunction.Zone[ZoneIdx].Length[1];

	
// Process all 4 sides 
FOR Idx := 1 TO 4 DO
	// If a previous function number is existing
	IF FunctionInterface.In.ePrevFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND FunctionInterface.In.ePrevFunctionNumber < E_FunctionNumber.F_END_FUNCTION OR
		FunctionInterface.In.ePrevFunctionNumber > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION AND FunctionInterface.In.ePrevFunctionNumber < E_FunctionNumber.F_END_EXTERN_FUNCTION THEN
		// Update stop delay distance based on previous function number
		IF FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[Idx] THEN
			ZoneData[ZoneIdx].TransportControlData.TransportParam.StopDelayDistance := SettingsFunction.Zone[ZoneIdx].StopDelayDistanceFromSide[Idx];
		END_IF
	END_IF

	// If the zone is occupied	
	IF ZoneData[ZoneIdx].TransportControlData.Occupied THEN
		// If a next function number is existing
		IF FunctionInterface.In.eNextFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND FunctionInterface.In.eNextFunctionNumber < E_FunctionNumber.F_END_FUNCTION OR
			FunctionInterface.In.eNextFunctionNumber > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION AND FunctionInterface.In.eNextFunctionNumber < E_FunctionNumber.F_END_EXTERN_FUNCTION THEN
			// Update zone end length based on next function number
			IF FunctionInterface.In.eNextFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[Idx] THEN
				ZoneData[ZoneIdx].TransportControlData.TransportParam.ZoneEndLength := SettingsFunction.Zone[ZoneIdx].ZoneEndLengthToSide[Idx];
			END_IF
		END_IF
	END_IF
END_FOR

// If the zone is not occupied	
IF NOT ZoneData[ZoneIdx].TransportControlData.Occupied THEN
	// If a previous function number is existing
	IF FunctionInterface.In.ePrevFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND FunctionInterface.In.ePrevFunctionNumber < E_FunctionNumber.F_END_FUNCTION OR
		FunctionInterface.In.ePrevFunctionNumber > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION AND FunctionInterface.In.ePrevFunctionNumber < E_FunctionNumber.F_END_EXTERN_FUNCTION THEN
		// Update zone end length based on previous function number
		IF FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[1] THEN
			ZoneData[ZoneIdx].TransportControlData.TransportParam.ZoneEndLength := SettingsFunction.Zone[ZoneIdx].ZoneEndLengthToSide[2];
		ELSIF FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[2] THEN
			ZoneData[ZoneIdx].TransportControlData.TransportParam.ZoneEndLength := SettingsFunction.Zone[ZoneIdx].ZoneEndLengthToSide[1];
		ELSIF FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[3] THEN
			ZoneData[ZoneIdx].TransportControlData.TransportParam.ZoneEndLength := SettingsFunction.Zone[ZoneIdx].ZoneEndLengthToSide[4];
		ELSIF FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[4] THEN
			ZoneData[ZoneIdx].TransportControlData.TransportParam.ZoneEndLength := SettingsFunction.Zone[ZoneIdx].ZoneEndLengthToSide[3];
		END_IF
	END_IF
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_ExternalSTI" Id="{9ee50b7a-54d8-4ba6-96b0-7c32d334d804}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_ExternalSTI
 * 	FUNCTION	External Subsystem transport interface
 **************************************************************************************)


(**************************************************************************************
   External Subsystem transport interfaces
***************************************************************************************)
FOR Idx := 1 TO NUMBER_OF_STI DO
	// Is an external Subsystem transport interface configured
	IF SettingsFunction.eExternalSTI_FunctionNumber[Idx] > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION AND
	   SettingsFunction.eExternalSTI_FunctionNumber[Idx] < E_FunctionNumber.F_END_EXTERN_FUNCTION THEN
	   
		// When interface is valid
		IF __QUERYINTERFACE(fbSTI_Channels[SettingsFunction.eExternalSTI_FunctionNumber[Idx]].Instance,FunctionBase.STI_Instance) THEN
			// Call external inbound interface
			fbSTI_Channels[SettingsFunction.eExternalSTI_FunctionNumber[Idx]].Instance.M_CallInterface(
				mErrorData				:= FunctionData.ErrorData.ErrorDataSet[1]);
		END_IF
	END_IF
END_FOR
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InterfaceHandlerOut" Id="{ffca7d61-8bc2-45d8-bc68-410e4a748617}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InterfaceHandlerOut
 * 	FUNCTION	Handles outgoing interfaces
 **************************************************************************************)
 
 
(**************************************************************************************************************
   ITC Handler Out
***************************************************************************************************************)
M_ITC_ProcessOut();]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_ManualControl" Id="{d042b88d-d360-4148-8a0d-1a0e1489f287}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_ManualControl
 * 	FUNCTION	Control manual movement
 **************************************************************************************)

 
(**************************************************************************************
   Reset manual commands
***************************************************************************************)

// If function is not in manual mode
IF FunctionData.OperationState.eMode <> E_PositionMode.MANUAL_MODE THEN
	// Reset manual commands
	ITC.ManualMotion := EmptyManualCommands;

(**************************************************************************************
   Manual Mode Active
***************************************************************************************)
ELSE

	// Buttons are Aktivatet
	ITC.ManualMotion.Speed.Enable 				:= TRUE;	// FALSE is Fast Speed / TRUE is Slow Speed
	ITC.ManualMotion.Axis[1].Forward.Enable 	:= TRUE;	// Roller Drive Direction 1-2

END_IF

// Activate Motor
ManualPartsAktiv.FastSpeed		:= ITC.ManualMotion.Speed.Fast AND ManualOrder;
ManualPartsAktiv.Direction12 	:= ITC.ManualMotion.Axis[1].Forward.Selected AND ManualOrder;

ManualMovmentSelected:=F_ManualSelectionOnSPOC(ITC.ManualMotion);]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_StateControl" Id="{59860751-3e17-4b26-948f-b11a435bf06b}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_StateControl
 * 	FUNCTION	Handles internal states and mode
 **************************************************************************************)
 
(**************************************************************************************
   System state
***************************************************************************************)
FunctionData.OperationState.Info := FunctionInterface.In.Element.OperationState.Info;

FunctionData.OperationState.InitRelease := FunctionInterface.In.Element.OperationState.InitRelease;

 (**************************************************************************************
   Function mode
***************************************************************************************)
FunctionData.OperationState.eMode := FunctionInterface.In.Element.OperationState.eMode;

(**************************************************************************************
   Function states
***************************************************************************************)
CASE FunctionData.OperationState.eState OF
	
	(**************************************************************************************
   		UNKNOWN state
	***************************************************************************************)
	E_PositionState.UNKNOWN:
		// Update internal state
		FunctionData.OperationState.eLastState := FunctionData.OperationState.eState;
		FunctionData.OperationState.eState := E_PositionState.STOPPED;	

		
   	(**************************************************************************************
   		STOPPED state
	***************************************************************************************)
	E_PositionState.STOPPED:

		// When element is not stopping
		IF FunctionInterface.In.Element.OperationState.eState = E_PositionState.STARTED AND
		   FunctionInterface.In.Element.OperationState.eMode = E_PositionMode.AUTO_MODE AND
           FunctionData.Init THEN
			// Update internal state
			FunctionData.OperationState.eLastState := FunctionData.OperationState.eState;
			FunctionData.OperationState.eState := E_PositionState.STARTING;	
			fbGetCurTaskIdx();
			FunctionStartRequestCycleCount:=_TaskInfo[fbGetCurTaskIdx.index].CycleCount;
		END_IF
		
	(**************************************************************************************
   		STOPPING state
	***************************************************************************************)
	E_PositionState.STOPPING:
		// Wait until transport is stopped
		FunctionBase.AllZonesStopped  := TRUE;
		FunctionBase.LinkedFunctionStopped := TRUE;
        FOR FunctionBase.Idx := 1 TO SettingsFunction.NumberOfZones DO
			IF NOT ZoneData[FunctionBase.Idx].TransportControlData.Stopped THEN
				FunctionBase.AllZonesStopped  := FALSE;
			END_IF
		END_FOR
		
		// Check if linked function is already stopped
		IF SettingsFunction.FunctionLink.eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION THEN
			FunctionBase.OperatingState := F_GetFunctionOperationState(eFunctionNumber := SettingsFunction.FunctionLink.eFunctionNumber);

			IF FunctionBase.OperatingState.eState <> E_PositionState.STOPPED THEN
				FunctionBase.LinkedFunctionStopped := FALSE;
			END_IF
		END_IF
		
		// Check that function and own element is stopped
		IF FunctionInterface.In.Element.OperationState.eState = E_PositionState.STOPPED OR 
		   FunctionInterface.In.Element.OperationState.eMode <> E_PositionMode.AUTO_MODE OR 
		   (FunctionBase.AllZonesStopped AND FunctionBase.LinkedFunctionStopped AND NOT InhibitStopping) THEN
			// Goto Stopped
			FunctionData.OperationState.eLastState := FunctionData.OperationState.eState;
			FunctionData.OperationState.eState := E_PositionState.STOPPED;
		END_IF

	(**************************************************************************************
   		STARTING state
	***************************************************************************************)
	E_PositionState.STARTING:
		fbGetCurTaskIdx();
		IF FunctionInterface.In.Element.OperationState.eState <> E_PositionState.STARTED OR 
           FunctionInterface.In.Element.OperationState.eMode <> E_PositionMode.AUTO_MODE THEN
		   	// Update internal state
			FunctionData.OperationState.eLastState := FunctionData.OperationState.eState;
			FunctionData.OperationState.eState := E_PositionState.STOPPED;	
		// Functions are paired in groups of 10, each 10 cycle (100ms) one function of that pair is started, total startup takes 1 secondes
		// this avoids starting all Drives on one power supply at the same time
		ELSIF (UDINT_TO_DINT(_TaskInfo[fbGetCurTaskIdx.index].CycleCount - FunctionStartRequestCycleCount)) MOD 100 >= (SettingsFunction.eFunctionNumber MOD 10)*10 THEN
			// Update internal state
			FunctionData.OperationState.eLastState := FunctionData.OperationState.eState;
			FunctionData.OperationState.eState := E_PositionState.STARTED;	
		END_IF
		
	(**************************************************************************************
   		STARTED state
	***************************************************************************************)
	E_PositionState.STARTED:
		IF FunctionInterface.In.Element.OperationState.eState = E_PositionState.STOPPING AND
           FunctionInterface.In.Element.OperationState.eMode = E_PositionMode.AUTO_MODE THEN
			// Update internal state
			FunctionData.OperationState.eLastState := FunctionData.OperationState.eState;
			FunctionData.OperationState.eState := E_PositionState.STOPPING;	
		ELSIF FunctionInterface.In.Element.OperationState.eState <> E_PositionState.STARTED OR 
              FunctionInterface.In.Element.OperationState.eMode <> E_PositionMode.AUTO_MODE THEN
			// Update internal state
			FunctionData.OperationState.eLastState := FunctionData.OperationState.eState;
			FunctionData.OperationState.eState := E_PositionState.STOPPED;	
		END_IF	
ELSE
	// Programming error
	DebugMsg := CONCAT(	'Programming error - Invalid state: ', INT_TO_STRING(FunctionData.OperationState.eState));
	fbDebugMsg.M_SendErrorMsg(DebugMsg);
END_CASE


]]></ST>
      </Implementation>
    </Action>
  </POU>
</TcPlcObject>