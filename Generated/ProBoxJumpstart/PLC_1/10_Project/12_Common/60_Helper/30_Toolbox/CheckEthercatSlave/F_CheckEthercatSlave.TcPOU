﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="F_CheckEthercatSlave" Id="{8c88e356-dfce-44fe-96d3-65ef6e5b9bc8}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION F_CheckEthercatSlave : BOOL
(**************************************************************************************
 * 	Application		:	QuickBox2.0
 *	Revision		:	$Revision: 488065 $
 *	Revision date	:	$Date: 2021-10-13 13:42:49 +0200 (Mi, 13 Okt 2021) $
 *	Last changed by	:	$Author: g7nowan $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/QuickMove/02_Controls/QuickBox/20_Release/trunk/01_Software/QB2Jumpstart/JumpStart/20_Product/20_QuickBox/30_Common/13_Fieldbus/F_CheckEthercatSlave.TcPOU $
 *
 *	Purpose			:	Checks EthercatSlave-State
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR_INPUT
	EthercatState	: UINT;
END_VAR
VAR_IN_OUT
	ErrorDataSet	: ST_ErrorDataSet;
END_VAR
VAR
	ConnectionState		:E_EC_ConnectionState;
	ConnectionError		:E_EtherCatSlaveError; (* EtherCat slave node connection error *)
	SubySystemOperationState	: ST_PositionState;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* Filter connection state from SlaveDevicestate *)
ConnectionState := WORD_TO_INT(UINT_TO_WORD(EthercatState) AND 16#F);
(* Connectionstate = 0 -->> Slave is switchted OFF *)

// Filter connection error from SlaveDevicestate 
ConnectionError := WORD_TO_INT(UINT_TO_WORD(EthercatState) AND 16#FFF0);

//get subsystemstate
SubySystemOperationState:=F_GetSubsystemOperationState();

// Set errormag
IF (ConnectionError = NO_ERROR AND ConnectionState = OP) THEN
	F_CheckEthercatSlave:=TRUE;
	IF ErrorDataSet.ErrorCode.Conv = E_ErrorConv.ETHERCAT_SLAVE_ERROR THEN
		F_ResetError(Reset:=TRUE,ErrorData:=ErrorDataSet);
	END_IF
ELSIF SubySystemOperationState.eState=E_PositionState.STARTED THEN
	F_SetError(         
		ErrorType	:= E_ErrorType.ERROR,
		ErrorParam	:= 'EtherCAT Slave Error',
		ErrorMsg	:= E_ErrorConv.ETHERCAT_SLAVE_ERROR,
		ErrorData	:= ErrorDataSet);
END_IF]]></ST>
    </Implementation>
  </POU>
</TcPlcObject>