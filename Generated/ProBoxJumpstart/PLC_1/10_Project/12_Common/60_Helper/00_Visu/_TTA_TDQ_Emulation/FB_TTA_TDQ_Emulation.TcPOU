﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_TTA_TDQ_Emulation" Id="{4a254552-2c0d-4b08-bb45-e74da7fb7bac}" SpecialFunc="None">
    <Declaration><![CDATA[// emulates/sends TTA
FUNCTION_BLOCK PUBLIC FB_TTA_TDQ_Emulation

(********************************************************************************************************************************)
VAR
  _Enable                : BOOL                             := FALSE;    // enables the FB
  _Count_TTA             : ULINT                            := 0;        // counts all send TTAs
  _Count_TDQ             : ULINT                            := 0;        // counts all send TTAs
  //-- button TDQ ----------------------------------------------------------------------------------------------------------------
  _Button_TDQ            : BOOL                             := FALSE;    // touch field
  _Button_TDQLast        : BOOL                             := FALSE;    // edge detection
  _Button_TDQAllow       : BOOL                             := FALSE;    // enable button
  //-- button TTA ----------------------------------------------------------------------------------------------------------------
  _Button_TTA            : BOOL                             := FALSE;    // touch field
  _Button_TTALast        : BOOL                             := FALSE;    // edge detection
  _Button_TTAAllow       : BOOL                             := FALSE;    // enable button
  //-- button to predefined ------------------------------------------------------------------------------------------------------
  _Button_Predefined     : BOOL                             := FALSE;    // touch field
  _Button_PredefinedLast : BOOL                             := FALSE;    // edge detection
  _Button_PredefinedAllow: BOOL                             := FALSE;    // enable button
  //-- button swap----------------------------------------------------------------------------------------------------------------
  _Button_Swap           : BOOL                             := FALSE;    // touch field
  _Button_SwapLast       : BOOL                             := FALSE;    // edge detection
  _Button_SwapAllow      : BOOL                             := FALSE;    // enable button
  //-- TU ID & AssignmentID ------------------------------------------------------------------------------------------------------
  _InputType             : INT                              := _INPUTTYPE_FROMDATA;
  _TuIdUnknownLength     : BYTE                             := 0;        // length of unknown label
  _TuIdDataLength        : BYTE                             := 0;        // length of label from tu data
  _TuIdManualLength      : BYTE                             := 0;        // length of manually typed in label
  _TuIdManual            : STRING[Conveyor.TU_ID_STRINGLENGTH];          // Label manually typed in
  _AssignmentManual      : UDINT                            := 0;        // assignment id manually typed in
  //-- TU dimensions -------------------------------------------------------------------------------------------------------------
  _Length                : INT                              := 600;      // 4 digit dimension
  _Width                 : INT                              := 600;      // 4 digit dimension
  _Height                : INT                              := 600;      // 4 digit dimension
  _Weight                : DINT                             := 1_000_000;// 8 digit weight
  //-- TU Type -------------------------------------------------------------------------------------------------------------------
  _TuType                : INT                              := 0;        // 4 digit dimension
  //-- source & destination ------------------------------------------------------------------------------------------------------
  _Predefined            : FB_SisNumber;
  _Source                : FB_SisNumber;
  _Destination           : FB_SisNumber;
  //------------------------------------------------------------------------------------------------------------------------------
  _TuDataValid           : BOOL                             := FALSE;
  _TU_Data               : ST_TU_Data;
END_VAR

(********************************************************************************************************************************)
VAR_STAT CONSTANT
  _INPUTTYPE_UNKNOWN          : BYTE   := 0;
  _INPUTTYPE_FROMDATA         : BYTE   := 1;
  _INPUTTYPE_MANU             : BYTE   := 2;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
M_TuData();       // retrieve TU data at source position

M_Button_Predefined();
M_Button_TTA();
M_Button_TDQ();
M_Button_Swap();




(********************************************************************************************************************************)
(** label length ****************************************************************************************************************)
_TuIdUnknownLength:= INT_TO_BYTE( LEN(Conveyor.TU_ID_UNKNOWN)  );
_TuIdDataLength   := INT_TO_BYTE( LEN(_TU_Data.TU_ID.ASCII  ) );
_TuIdManualLength := INT_TO_BYTE( LEN(_TuIdManual           ) );




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
    </Implementation>
    <Folder Name="Methods" Id="{194e115c-507d-48fb-be9e-75d205e68198}" />
    <Folder Name="Properties" Id="{d816a933-979a-47a7-8c4f-7d87b4eb3bf7}" />
    <Property Name="Enable" Id="{297a5d94-c236-4e7f-af0e-341287a3a81f}" FolderPath="Properties\">
      <Declaration><![CDATA[// enables the FB
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Enable : BOOL]]></Declaration>
      <Get Name="Get" Id="{469ba3fb-e52b-4d27-aaaf-cb157e8f6085}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Enable:= _Enable;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{391de15f-ad62-4fed-ba33-880ccaa867f4}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[_Enable:= Enable;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Method Name="FB_init" Id="{3b2eaec8-4cef-4a77-822a-8499e87ce313}" FolderPath="Methods\">
      <Declaration><![CDATA[METHOD FB_init : BOOL

(********************************************************************************************************************************)
VAR_INPUT
	bInitRetains          : BOOL; // if TRUE, the retain variables are initialized (warm start / cold start)
	bInCopyCode           : BOOL;  // if TRUE, the instance afterwards gets moved into the copy code (online change)
  //------------------------------------------------------------------------------------------------------------------------------
  InitialSubsystem      : WORD                             := 0;
  InitialSwitchgroup    : WORD                             := 0;
  InitialElement        : WORD                             := 0;
  InitialFunc           : WORD                             := 0;
  InitialZone           : WORD                             := 0;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
_Predefined.Subsystem  := InitialSubsystem;
_Predefined.Switchgroup:= InitialSwitchgroup;
_Predefined.Element    := InitialElement;
_Predefined.Func       := InitialFunc;
_Predefined.Zone       := InitialZone;

_Source.Subsystem  := InitialSubsystem;
_Source.Switchgroup:= InitialSwitchgroup;
_Source.Element    := InitialElement;
_Source.Func       := InitialFunc;
_Source.Zone       := InitialZone;

_Destination.Subsystem  := InitialSubsystem;
_Destination.Switchgroup:= InitialSwitchgroup;
_Destination.Element    := InitialElement;
_Destination.Func       := InitialFunc;
_Destination.Zone       := InitialZone;




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Button_Predefined" Id="{f6dd757b-cf6c-4827-87f7-d27ec6b920a5}" FolderPath="Methods\">
      <Declaration><![CDATA[METHOD PROTECTED M_Button_Predefined

(********************************************************************************************************************************)
VAR
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF   (_Enable                           = FALSE)  THEN  _Button_PredefinedAllow:= FALSE;
ELSIF(_Destination.M_Equals(_Predefined) = TRUE)  THEN  _Button_PredefinedAllow:= FALSE;
ELSE                                                    _Button_PredefinedAllow:= TRUE;
END_IF




(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF (    (_Button_Predefined     = TRUE)
     AND(_Button_PredefinedLast = FALSE)  )
THEN
  _Destination.Subsystem  := _Predefined.Subsystem;
  _Destination.Switchgroup:= _Predefined.Switchgroup;
  _Destination.Element    := _Predefined.Element;
  _Destination.Func       := _Predefined.Func;
  _Destination.Zone       := _Predefined.Zone;
END_IF

_Button_PredefinedLast:= _Button_Predefined;




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Button_Swap" Id="{d2c427b1-88d3-473e-ba2a-74286812c2d9}" FolderPath="Methods\">
      <Declaration><![CDATA[METHOD PROTECTED M_Button_Swap

(********************************************************************************************************************************)
VAR
  Subsystem             : WORD                             := 0;
  Switchgroup           : WORD                             := 0;
  Element               : WORD                             := 0;
  Func                  : WORD                             := 0;
  Zone                  : WORD                             := 0;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF   (_Enable                       = FALSE)  THEN  _Button_SwapAllow:= FALSE;
ELSIF(_Destination.M_Equals(_Source) = TRUE)  THEN  _Button_SwapAllow:= FALSE;
ELSE                                                _Button_SwapAllow:= TRUE;
END_IF




(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF (    (_Button_Swap     = TRUE)
     AND(_Button_SwapLast = FALSE)  )
THEN
  Subsystem  := _Source.Subsystem;
  Switchgroup:= _Source.Switchgroup;
  Element    := _Source.Element;
  Func       := _Source.Func;
  Zone       := _Source.Zone;
  //------------------------------------------------
  _Source.Subsystem  := _Destination.Subsystem;
  _Source.Switchgroup:= _Destination.Switchgroup;
  _Source.Element    := _Destination.Element;
  _Source.Func       := _Destination.Func;
  _Source.Zone       := _Destination.Zone;
  //------------------------------------------------
  _Destination.Subsystem  := Subsystem;
  _Destination.Switchgroup:= Switchgroup;
  _Destination.Element    := Element;
  _Destination.Func       := Func;
  _Destination.Zone       := Zone;
END_IF

_Button_SwapLast:= _Button_Swap;




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Button_TDQ" Id="{ca4f823f-40e0-456c-8baf-59a9fb810bae}" FolderPath="Methods\">
      <Declaration><![CDATA[METHOD PROTECTED M_Button_TDQ

(********************************************************************************************************************************)
VAR
  TDQ                   : ST_ITC_TDQ;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF(_Enable                           = FALSE)  THEN  _Button_TDQAllow:= FALSE;
ELSIF(_Source.IsValidZone            = FALSE)  THEN  _Button_TDQAllow:= FALSE;
//ELSIF(_TuDataValid                 = FALSE)  THEN  _Button_TDQAllow:= FALSE;
ELSE                                                 _Button_TDQAllow:= TRUE;
END_IF




(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF (    (_Button_TDQ     = TRUE)
     AND(_Button_TDQLast = FALSE)  )
THEN
	TDQ.Position	:= _Source.Number; 

	CASE(_InputType)OF
	_INPUTTYPE_UNKNOWN:   TDQ.TU_ID:= TU_ID_UNKNOWN;         TDQ.AssignmentID:= ASSIGNMENT_ID_UNKNOWN;
	_INPUTTYPE_FROMDATA:  TDQ.TU_ID:= _TU_Data.TU_ID.ASCII;  TDQ.AssignmentID:= _TU_Data.AssignmentID;
	_INPUTTYPE_MANU:      TDQ.TU_ID:= _TuIdManual;           TDQ.AssignmentID:= _AssignmentManual;
	ELSE                  TDQ.TU_ID:= TU_ID_UNKNOWN;         TDQ.AssignmentID:= ASSIGNMENT_ID_UNKNOWN;
	END_CASE

	TDQ.MsgSequence	:= 0;
	TDQ.Code		:= 0;

	//------------------------------------------------
	fbITC_Manager.M_AddData( meChannelSrc:= 0,
						  mMsgType    := E_ITC_MsgTypeConv.TASK_DELETE_REQUEST,
						  mpData      := ADR   (TDQ),
						  mLen        := SIZEOF(TDQ)  );
	//------------------------------------------------
	_Count_TDQ:= _Count_TDQ + 1;
END_IF

_Button_TDQLast:= _Button_TDQ;

]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Button_TTA" Id="{f6edc75c-c85b-4bdf-a5e2-41d28f6f188f}" FolderPath="Methods\">
      <Declaration><![CDATA[METHOD PROTECTED M_Button_TTA

(********************************************************************************************************************************)
VAR
  TTA                   : ST_ITC_TTA;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF(_Enable                           = FALSE)  THEN  _Button_TTAAllow:= FALSE;
ELSIF(_Source.IsValidZone            = FALSE)  THEN  _Button_TTAAllow:= FALSE;
ELSIF(_Destination.IsValidZone       = FALSE)  THEN  _Button_TTAAllow:= FALSE;
ELSIF(_Destination.M_Equals(_Source) = TRUE)   THEN  _Button_TTAAllow:= FALSE;
//ELSIF(_TuDataValid                 = FALSE)  THEN  _Button_TTAAllow:= FALSE;
ELSE                                                 _Button_TTAAllow:= TRUE;
END_IF




(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF (    (_Button_TTA     = TRUE)
     AND(_Button_TTALast = FALSE)  )
THEN
	TTA.Position              := _Source.Number; 
	TTA.TU_DataIndex          := 0;
	TTA.MsgSequence           := 0;

	CASE(_InputType)OF
	_INPUTTYPE_UNKNOWN:   TTA.TU_ID:= TU_ID_UNKNOWN;         TTA.AssignmentID:= ASSIGNMENT_ID_UNKNOWN;
	_INPUTTYPE_FROMDATA:  TTA.TU_ID:= _TU_Data.TU_ID.ASCII;  TTA.AssignmentID:= _TU_Data.AssignmentID;
	_INPUTTYPE_MANU:      TTA.TU_ID:= _TuIdManual;           TTA.AssignmentID:= _AssignmentManual;
	ELSE                  TTA.TU_ID:= TU_ID_UNKNOWN;         TTA.AssignmentID:= ASSIGNMENT_ID_UNKNOWN;
	END_CASE

	TTA.TU_Type             := _TuType; 
	TTA.Length      	    := _Length;
	TTA.Width       	    := _Width;
	TTA.Height      	    := _Height;
	TTA.Weight             := _Weight;
	TTA.Orientation         := 'LSL';
	TTA.TU_Parameter        := 0;
	TTA.IO_Data             := 0;
	TTA.LowerLimit	        := 0;
	TTA.UpperLimit          := 0;
	TTA.Sequence	  	    := 0;
	TTA.SubSequence		    := 0;
	TTA.SequenceSize        := 0;
	TTA.GroupID	    	    := 0;
	TTA.GroupSize           := 0;
	TTA.DestinationSlot	    := 0;
	TTA.NumberOfDestinations:= 1;
	TTA.Destination[1]      := _Destination.Number;
	//------------------------------------------------
	fbITC_Manager.M_AddData( meChannelSrc:= 0,
						  mMsgType    := E_ITC_MsgTypeConv.TRANSPORT_TASK_ASSIGNMENT,
						  mpData      := ADR   (TTA),
						  mLen        := SIZEOF(TTA)  );
	//------------------------------------------------
	_Count_TTA:= _Count_TTA + 1;
END_IF

_Button_TTALast:= _Button_TTA;




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_TuData" Id="{51473f35-1fcc-4fce-8625-4af5e9a82aba}" FolderPath="Methods\">
      <Declaration><![CDATA[METHOD PROTECTED M_TuData

(********************************************************************************************************************************)
VAR
  SourceIdent           : ST_PositionIdent;
  TransportControlData  : ST_TransportControlData;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
SourceIdent:= F_SearchFunctionPosition(Position:= _Source.Number );

IF (SourceIdent.eFunctionNumber = E_FunctionNumber.F_BEGIN_FUNCTION)
THEN
  _TuDataValid:= FALSE;
  memset(ADR(_TU_Data), 0, SIZEOF(_TU_Data)  );
  RETURN;
END_IF




(********************************************************************************************************************************)
(********************************************************************************************************************************)
TransportControlData:= F_GetTransportControlData(	eFunctionNumber:= SourceIdent.eFunctionNumber,
                                                  ZoneNumber	   := 1  );

IF   (TransportControlData.TU_DataIndex = 0)                           THEN  _TuDataValid:= FALSE;
ELSIF(TransportControlData.TU_DataIndex > Conveyor.NUMBER_OF_TU_DATA)  THEN  _TuDataValid:= FALSE;
ELSIF(TransportControlData.Occupied     = FALSE)                       THEN  _TuDataValid:= FALSE;
ELSE                                                                         _TuDataValid:= TRUE;
END_IF

IF (_TuDataValid = TRUE )  THEN  _TU_Data:= TU_DataTable[TransportControlData.TU_DataIndex];
ELSE                             memset(ADR(_TU_Data), 0, SIZEOF(_TU_Data)  );
END_IF




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
      </Implementation>
    </Method>
    <Property Name="Predefined" Id="{8202104b-2590-42dd-af00-b69e34273e56}" FolderPath="Properties\">
      <Declaration><![CDATA[// predefined destination
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Predefined : I_SisNumber]]></Declaration>
      <Get Name="Get" Id="{d69d290b-bf5a-4b30-b331-6817f3149b7d}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Predefined:= _Predefined;]]></ST>
        </Implementation>
      </Get>
    </Property>
  </POU>
</TcPlcObject>