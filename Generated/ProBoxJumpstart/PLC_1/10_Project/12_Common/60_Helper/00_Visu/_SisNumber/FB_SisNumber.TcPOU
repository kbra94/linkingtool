﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_SisNumber" Id="{20934eaf-e450-49f8-8731-0dd442a9c303}" SpecialFunc="None">
    <Declaration><![CDATA[// represents a SIS number
FUNCTION_BLOCK PUBLIC FB_SisNumber
  IMPLEMENTS I_SisNumber

(********************************************************************************************************************************)
VAR
  _ChangeCounter        : WORD                             := 0;
  //------------------------------------------------------------------------------------------------------------------------------
  _Subsystem            : WORD                             := 0;
  _Switchgroup          : WORD                             := 0;
  _Element              : WORD                             := 0;
  _Func                 : WORD                             := 0;
  _Zone                 : WORD                             := 0;
  //------------------------------------------------------------------------------------------------------------------------------
  _Text                 : A_SisNbrText                     := '';     // format SS-GGG-EEEE-FF-Z
  _Number               : A_SisNbrText                     := '';     // format SSGGGEEEEFFZ
  //------------------------------------------------------------------------------------------------------------------------------
  _IsValidSubsystem     : BOOL                             := FALSE;  // SS-GGG-EEEE-FF-Z with SS <> 0
  _IsValidSwitchgroup   : BOOL                             := FALSE;  // SS-GGG-EEEE-FF-Z with SS & GGG <> 0
  _IsValidElement       : BOOL                             := FALSE;  // SS-GGG-EEEE-FF-Z with SS & GGG & EEEE <> 0
  _IsValidFunc          : BOOL                             := FALSE;  // SS-GGG-EEEE-FF-Z with SS & GGG & EEEE & FF <> 0
  _IsValidZone          : BOOL                             := FALSE;  // SS-GGG-EEEE-FF-Z with SS & GGG & EEEE & FF & Z <> 0
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[]]></ST>
    </Implementation>
    <Folder Name="_internal" Id="{9b32df20-c0d9-4f15-ad12-d5065e64e48b}" />
    <Folder Name="Checks" Id="{3cd48dc4-959b-4af7-b967-65382b298cde}" />
    <Folder Name="Numbers" Id="{26b4a5d2-b03b-4e7b-bff1-04f755c7c527}" />
    <Folder Name="Statistics" Id="{5b8e0ae4-6483-403e-a7bd-6434290f569b}" />
    <Folder Name="Strings" Id="{e56ccd10-b0c2-4aaa-ab05-a4f315a87900}" />
    <Folder Name="Validations" Id="{14b14085-fa2d-43f0-b398-27b5d0c02e21}" />
    <Property Name="ChangeCounter" Id="{d9ce06cd-3838-4571-91aa-1019ff59b6a5}" FolderPath="Statistics\">
      <Declaration><![CDATA[// change counter
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC ChangeCounter : WORD]]></Declaration>
      <Get Name="Get" Id="{753845fe-429e-4073-81d0-bd74bd3c202d}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[ChangeCounter:= _ChangeCounter;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="Element" Id="{d9e229a1-6947-4c51-8ff5-1e1f23d742c7}" FolderPath="Numbers\">
      <Declaration><![CDATA[// element
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Element : WORD]]></Declaration>
      <Get Name="Get" Id="{b4eb6202-c214-44c8-8663-cd358c323d68}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Element:= _Element;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{181bc914-1aba-491f-af98-aa421aac49d6}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[
(********************************************************************************************************************************)
(** value to change? ************************************************************************************************************)
IF (_Element = Element)
THEN
  RETURN;
END_IF




(********************************************************************************************************************************)
(** check, save and update ******************************************************************************************************)
IF (Element > 9999)
THEN
  RETURN;
END_IF

_Element:= Element;
M_Update();




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Method Name="FB_init" Id="{03101829-c37b-4a79-b94b-f8ee3e080e81}" FolderPath="_internal\">
      <Declaration><![CDATA[METHOD FB_init : BOOL

(********************************************************************************************************************************)
VAR_INPUT
	bInitRetains          : BOOL; // if TRUE, the retain variables are initialized (warm start / cold start)
	bInCopyCode           : BOOL;  // if TRUE, the instance afterwards gets moved into the copy code (online change)
  //------------------------------------------------------------------------------------------------------------------------------
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
M_Update();




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
      </Implementation>
    </Method>
    <Property Name="Func" Id="{3fd963be-99d9-4d29-8795-92e1a16cefd0}" FolderPath="Numbers\">
      <Declaration><![CDATA[// function
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Func : WORD]]></Declaration>
      <Get Name="Get" Id="{02a8bed8-39b6-4baf-9538-1735a434a6dd}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Func:= _Func;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{2e78e3b4-434a-408c-aea6-230cf5d0e3e4}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[
(********************************************************************************************************************************)
(** value to change? ************************************************************************************************************)
IF (_Func = Func)
THEN
  RETURN;
END_IF




(********************************************************************************************************************************)
(** check, save and update ******************************************************************************************************)
IF (Func > 99)
THEN
  RETURN;
END_IF

_Func:= Func;
M_Update();




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="IsValidElement" Id="{4103e51f-883b-4be9-b9a9-c42c618b3aa0}" FolderPath="Validations\">
      <Declaration><![CDATA[// number is valid element -> SS-GGG-EEEE-FF-Z with SS & GGG & EEEE <> 0
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC IsValidElement : BOOL]]></Declaration>
      <Get Name="Get" Id="{91d79121-79f2-4258-b7d5-847943813716}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IsValidElement:= _IsValidElement;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="IsValidFunc" Id="{5500f318-d690-4db0-82b0-8a507e2ed7c3}" FolderPath="Validations\">
      <Declaration><![CDATA[// number is valid function -> SS-GGG-EEEE-FF-Z with SS & GGG & EEEE & FF <> 0
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC IsValidFunc : BOOL]]></Declaration>
      <Get Name="Get" Id="{a66e8759-9428-4871-84d8-28bd3a2925be}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IsValidFunc:= _IsValidFunc;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="IsValidSubsystem" Id="{8dbfe8d3-7538-426d-a25e-57fda7f3aef0}" FolderPath="Validations\">
      <Declaration><![CDATA[// number is valid subsystem -> SS-GGG-EEEE-FF-Z with SS <> 0
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC IsValidSubsystem : BOOL]]></Declaration>
      <Get Name="Get" Id="{dbed6830-8ffe-4024-8cd4-4d3679c73dd0}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IsValidSubsystem:= _IsValidSubsystem;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="IsValidSwitchgroup" Id="{7db277db-8815-48d5-8def-573db27f7ef6}" FolderPath="Validations\">
      <Declaration><![CDATA[// number is valid switch group -> SS-GGG-EEEE-FF-Z with SS & GGG <> 0
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC IsValidSwitchgroup : BOOL]]></Declaration>
      <Get Name="Get" Id="{da570890-496b-4975-b14d-dea6cb7d1654}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IsValidSwitchgroup:= _IsValidSwitchgroup;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="IsValidZone" Id="{f8afdb08-417b-44ea-ade0-c5036e68c6bb}" FolderPath="Validations\">
      <Declaration><![CDATA[// number is valid zone -> SS-GGG-EEEE-FF-Z with SS & GGG & EEEE & FF & Z <> 0
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC IsValidZone : BOOL]]></Declaration>
      <Get Name="Get" Id="{2d3b9538-570a-4a6e-bf30-917b95d5c54b}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IsValidZone:= _IsValidZone;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Method Name="M_Equals" Id="{7f8707ff-0687-4b37-9f89-66186b2cd4a0}" FolderPath="Checks\">
      <Declaration><![CDATA[// check for equality
METHOD PUBLIC M_Equals : BOOL

(********************************************************************************************************************************)
VAR_INPUT
  SIS                   : I_SisNumber                      := 0;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF (SIS = 0)
THEN
  M_Equals:= FALSE;
  RETURN;
END_IF




(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF   (_Subsystem   <> SIS.Subsystem)    THEN  M_Equals:= FALSE;
ELSIF(_Switchgroup <> SIS.Switchgroup)  THEN  M_Equals:= FALSE;
ELSIF(_Element     <> SIS.Element)      THEN  M_Equals:= FALSE;
ELSIF(_Func        <> SIS.Func)         THEN  M_Equals:= FALSE;
ELSIF(_Zone        <> SIS.Zone)         THEN  M_Equals:= FALSE;
ELSE                                          M_Equals:= TRUE;
END_IF




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Update" Id="{d70266af-05cb-4679-9273-c66fd2aaf702}" FolderPath="_internal\">
      <Declaration><![CDATA[METHOD PROTECTED M_Update

(********************************************************************************************************************************)
VAR
  Temp                  : STRING[7]                        := '';
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[(************************************************************************************************************)
(************************************************************************************************************)
memset( destAddr:= ADR(_Text),   fillByte:= 16#00, n:= SIZEOF(_Text) );
memset( destAddr:= ADR(_Number), fillByte:= 16#00, n:= SIZEOF(_Number) );




(************************************************************************************************************)
(** sub system **********************************************************************************************)
Temp:= WORD_TO_STRING(_SubSystem);
WHILE( LEN(Temp) < 2)
DO
  Temp  := CONCAT( '0',   Temp  );
END_WHILE

_Text  := CONCAT( _Text,   Temp  );
_Number:= CONCAT( _Number, Temp  );




(************************************************************************************************************)
(** switching group *****************************************************************************************)
_Text:= CONCAT( _Text, '-'  );

Temp:= WORD_TO_STRING(_SwitchGroup);
WHILE( LEN(Temp) < 3)
DO
  Temp  := CONCAT( '0',   Temp  );
END_WHILE

_Text  := CONCAT( _Text,   Temp  );
_Number:= CONCAT( _Number, Temp  );




(************************************************************************************************************)
(** elements ************************************************************************************************)
_Text:= CONCAT( _Text, '-'  );

Temp:= WORD_TO_STRING(_Element);
WHILE( LEN(Temp) < 4)
DO
  Temp  := CONCAT( '0',   Temp  );
END_WHILE

_Text  := CONCAT( _Text,   Temp  );
_Number:= CONCAT( _Number, Temp  );




(************************************************************************************************************)
(** function ************************************************************************************************)
_Text:= CONCAT( _Text, '-'  );

Temp:= WORD_TO_STRING(_Func);
WHILE( LEN(Temp) < 2)
DO
  Temp  := CONCAT( '0',   Temp  );
END_WHILE

_Text  := CONCAT( _Text,   Temp  );
_Number:= CONCAT( _Number, Temp  );




(************************************************************************************************************)
(** zone ****************************************************************************************************)
_Text:= CONCAT( _Text, '-'  );

Temp:= WORD_TO_STRING(_Zone);

_Text  := CONCAT( _Text,   Temp  );
_Number:= CONCAT( _Number, Temp  );




(************************************************************************************************************)
(** valid? **************************************************************************************************)
IF   (_Subsystem   = 0)  THEN  _IsValidSubsystem:= FALSE;  _IsValidSwitchgroup:= FALSE;  _IsValidElement:= FALSE;  _IsValidFunc:= FALSE;  _IsValidZone:= FALSE;
ELSIF(_Switchgroup = 0)  THEN  _IsValidSubsystem:= TRUE;   _IsValidSwitchgroup:= FALSE;  _IsValidElement:= FALSE;  _IsValidFunc:= FALSE;  _IsValidZone:= FALSE;
ELSIF(_Element     = 0)  THEN  _IsValidSubsystem:= TRUE;   _IsValidSwitchgroup:= TRUE;   _IsValidElement:= FALSE;  _IsValidFunc:= FALSE;  _IsValidZone:= FALSE;
ELSIF(_Func        = 0)  THEN  _IsValidSubsystem:= TRUE;   _IsValidSwitchgroup:= TRUE;   _IsValidElement:= TRUE;   _IsValidFunc:= FALSE;  _IsValidZone:= FALSE;
ELSIF(_Zone        = 0)  THEN  _IsValidSubsystem:= TRUE;   _IsValidSwitchgroup:= TRUE;   _IsValidElement:= TRUE;   _IsValidFunc:= TRUE;   _IsValidZone:= FALSE;
ELSE                           _IsValidSubsystem:= TRUE;   _IsValidSwitchgroup:= TRUE;   _IsValidElement:= TRUE;   _IsValidFunc:= TRUE;   _IsValidZone:= TRUE;
END_IF




(************************************************************************************************************)
(** done ****************************************************************************************************)
_ChangeCounter:= _ChangeCounter + 1;

]]></ST>
      </Implementation>
    </Method>
    <Property Name="Number" Id="{4159a76c-0c3d-42bb-8edf-718ddafcb064}" FolderPath="Strings\">
      <Declaration><![CDATA[// format SSGGGEEEEFFZ
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Number : A_SisNbrText]]></Declaration>
      <Get Name="Get" Id="{69461047-84db-484f-a1af-5848ae351a3c}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Number:= _Number;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="Subsystem" Id="{ece11e2a-ad60-46f1-bf34-7768db497762}" FolderPath="Numbers\">
      <Declaration><![CDATA[// subsystem
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Subsystem : WORD]]></Declaration>
      <Get Name="Get" Id="{36be7c81-5b2e-4b9e-96a6-880fb1875cbd}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Subsystem:= _Subsystem;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{53a2d8ee-ea20-4aa1-81cb-486987d93ee8}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[
(********************************************************************************************************************************)
(** value to change? ************************************************************************************************************)
IF (_Subsystem = Subsystem)
THEN
  RETURN;
END_IF




(********************************************************************************************************************************)
(** check, save and update ******************************************************************************************************)
IF (Subsystem > 99)
THEN
  RETURN;
END_IF

_Subsystem:= Subsystem;
M_Update();




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="Switchgroup" Id="{26bb5dce-77aa-456d-a5fc-31db53d588b0}" FolderPath="Numbers\">
      <Declaration><![CDATA[// switch group
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Switchgroup : WORD]]></Declaration>
      <Get Name="Get" Id="{847feeb6-4fa4-4fe8-8eeb-ece9212f301e}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Switchgroup:= _Switchgroup;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{087707a3-d928-459a-aa51-2b3a6244b6ca}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[
(********************************************************************************************************************************)
(** value to change? ************************************************************************************************************)
IF (_Switchgroup = Switchgroup)
THEN
  RETURN;
END_IF




(********************************************************************************************************************************)
(** check, save and update ******************************************************************************************************)
IF (Switchgroup > 999)
THEN
  RETURN;
END_IF

_Switchgroup:= Switchgroup;
M_Update();




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="Text" Id="{84ed4a73-43b3-40bb-9926-e0f42ae13f90}" FolderPath="Strings\">
      <Declaration><![CDATA[// format SS-GGG-EEEE-FF-Z
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Text : A_SisNbrText]]></Declaration>
      <Get Name="Get" Id="{6d04cb7b-1650-46c8-a3db-a22b2b915b5b}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Text:= _Text;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="Zone" Id="{1303af97-bd3b-4969-8213-3212569a3b09}" FolderPath="Numbers\">
      <Declaration><![CDATA[// zone
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Zone : WORD]]></Declaration>
      <Get Name="Get" Id="{118afaff-7736-4fe3-9657-d0cb088ed164}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Zone:= _Zone;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{e121cf48-6441-4dac-b9f2-1b859bd5b931}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[
(********************************************************************************************************************************)
(** value to change? ************************************************************************************************************)
IF (_Zone = Zone)
THEN
  RETURN;
END_IF




(********************************************************************************************************************************)
(** check, save and update ******************************************************************************************************)
IF (Zone > 9)
THEN
  RETURN;
END_IF

_Zone:= Zone;
M_Update();




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
        </Implementation>
      </Set>
    </Property>
  </POU>
</TcPlcObject>