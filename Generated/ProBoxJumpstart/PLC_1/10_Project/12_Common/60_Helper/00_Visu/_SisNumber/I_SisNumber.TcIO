﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <Itf Name="I_SisNumber" Id="{50212d7f-fc27-48f6-8dd2-9e3310ad3045}">
    <Declaration><![CDATA[// represents a SIS number
{attribute 'monitoring' := 'call'}
INTERFACE PUBLIC I_SisNumber
]]></Declaration>
    <Folder Name="Checks" Id="{6c3fdc3d-47c4-44ed-b5d3-65c28e4546c1}" />
    <Folder Name="Numbers" Id="{2a93c699-e756-4236-9df9-3dbbf1750e68}" />
    <Folder Name="Statistics" Id="{a25e14e9-1e5a-458e-a2de-4a62e37ae8f2}" />
    <Folder Name="Strings" Id="{9fd0b620-57a8-4a04-a943-8d638afd6c04}" />
    <Folder Name="Validations" Id="{ff3a4911-d5ec-4de9-a50d-fca919150396}" />
    <Property Name="ChangeCounter" Id="{541f6854-8f2b-4ce5-b809-05bf5086a62e}" FolderPath="Statistics\">
      <Declaration><![CDATA[// change counter
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC ChangeCounter : WORD]]></Declaration>
      <Get Name="Get" Id="{e61d15e0-c528-4854-a660-18d7d3ded19c}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
    </Property>
    <Property Name="Element" Id="{a7656544-bab6-4a0d-9edf-ce33a6aeb362}" FolderPath="Numbers\">
      <Declaration><![CDATA[// element
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Element : WORD]]></Declaration>
      <Get Name="Get" Id="{30e394d7-a034-4ab5-b428-4fda5f3e0658}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
      <Set Name="Set" Id="{8e987447-f07d-4ec6-a1e2-54933d257d77}">
        <Declaration><![CDATA[]]></Declaration>
      </Set>
    </Property>
    <Property Name="Func" Id="{90ced479-69ba-4a4c-afd2-bc83379d62ac}" FolderPath="Numbers\">
      <Declaration><![CDATA[// function
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Func : WORD]]></Declaration>
      <Get Name="Get" Id="{38948c05-8bda-496e-bd88-c7d3de854516}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
      <Set Name="Set" Id="{017595f1-6a30-4322-b023-f8aa3c02952d}">
        <Declaration><![CDATA[]]></Declaration>
      </Set>
    </Property>
    <Property Name="IsValidElement" Id="{c055f6c6-b2cd-4abe-86a6-d33f6263430c}" FolderPath="Validations\">
      <Declaration><![CDATA[// number is valid element -> SS-GGG-EEEE-FF-Z with SS & GGG & EEEE <> 0
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC IsValidElement : BOOL]]></Declaration>
      <Get Name="Get" Id="{4745a371-4ff8-49f3-aa33-fe7e058b2355}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
    </Property>
    <Property Name="IsValidFunc" Id="{636ee2e1-d934-459e-ab55-741b8c4e6857}" FolderPath="Validations\">
      <Declaration><![CDATA[// number is valid function -> SS-GGG-EEEE-FF-Z with SS & GGG & EEEE & FF <> 0
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC IsValidFunc : BOOL]]></Declaration>
      <Get Name="Get" Id="{7996b5a9-39cd-4ec5-8811-4584caf8d350}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
    </Property>
    <Property Name="IsValidSubsystem" Id="{60afc49e-84a1-4ac5-80fb-9998043f0343}" FolderPath="Validations\">
      <Declaration><![CDATA[// number is valid subsystem -> SS-GGG-EEEE-FF-Z with SS <> 0
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC IsValidSubsystem : BOOL]]></Declaration>
      <Get Name="Get" Id="{d1c9326f-85bc-4871-a0ac-28b8ebe003e6}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
    </Property>
    <Property Name="IsValidSwitchgroup" Id="{227b1efa-287a-4fb9-b1b7-98c866132608}" FolderPath="Validations\">
      <Declaration><![CDATA[// number is valid switch group -> SS-GGG-EEEE-FF-Z with SS & GGG <> 0
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC IsValidSwitchgroup : BOOL]]></Declaration>
      <Get Name="Get" Id="{a953c90c-a892-4add-9a91-65de38059f5f}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
    </Property>
    <Property Name="IsValidZone" Id="{91ab5efd-0609-41df-b0ed-35fd170caa06}" FolderPath="Validations\">
      <Declaration><![CDATA[// number is valid zone -> SS-GGG-EEEE-FF-Z with SS & GGG & EEEE & FF & Z <> 0
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC IsValidZone : BOOL]]></Declaration>
      <Get Name="Get" Id="{a0e7abae-9f89-407e-9478-41f8f2c02338}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
    </Property>
    <Method Name="M_Equals" Id="{1104bc72-93df-4654-bdf7-8d4a34777167}" FolderPath="Checks\">
      <Declaration><![CDATA[// check for equality
METHOD PUBLIC M_Equals : BOOL

(********************************************************************************************************************************)
VAR_INPUT
  SIS                   : I_SisNumber                      := 0;
END_VAR
]]></Declaration>
    </Method>
    <Property Name="Number" Id="{6ac36b5a-610c-4c5d-a832-6c9ac76cc4be}" FolderPath="Strings\">
      <Declaration><![CDATA[// format SSGGGEEEEFFZ
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Number : A_SisNbrText]]></Declaration>
      <Get Name="Get" Id="{016cb2f7-e920-4bec-b418-562c96ab2079}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
    </Property>
    <Property Name="Subsystem" Id="{bff24e91-ce7c-40ae-b253-6cee7a94e911}" FolderPath="Numbers\">
      <Declaration><![CDATA[// subsystem
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Subsystem : WORD]]></Declaration>
      <Get Name="Get" Id="{ca14b7e8-91f9-4330-aed5-0377fa862812}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
      <Set Name="Set" Id="{92cac171-1c13-45cf-89c9-da9dcf3e05c3}">
        <Declaration><![CDATA[]]></Declaration>
      </Set>
    </Property>
    <Property Name="Switchgroup" Id="{4f643a57-0faa-4a90-bdc6-3d32c02d1073}" FolderPath="Numbers\">
      <Declaration><![CDATA[// switch group
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Switchgroup : WORD]]></Declaration>
      <Get Name="Get" Id="{200cee2c-a0d6-4c82-b80b-1f73013ba47a}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
      <Set Name="Set" Id="{a3c5b66f-d2d6-49d7-a97c-11efb7ea4e89}">
        <Declaration><![CDATA[]]></Declaration>
      </Set>
    </Property>
    <Property Name="Text" Id="{aa7a65b0-5080-4ef1-9c64-60527ed20806}" FolderPath="Strings\">
      <Declaration><![CDATA[// format SS-GGG-EEEE-FF-Z
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Text : A_SisNbrText]]></Declaration>
      <Get Name="Get" Id="{0e5ffd0a-7e27-49fd-ae94-fb43b100b5ef}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
    </Property>
    <Property Name="Zone" Id="{2db58795-e839-469f-88bf-8a753c689294}" FolderPath="Numbers\">
      <Declaration><![CDATA[// zone
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Zone : WORD]]></Declaration>
      <Get Name="Get" Id="{c0bfba42-f739-4c15-9caf-6f4b04e3a608}">
        <Declaration><![CDATA[]]></Declaration>
      </Get>
      <Set Name="Set" Id="{24e98367-1771-4673-9676-5a2d919c6ffa}">
        <Declaration><![CDATA[]]></Declaration>
      </Set>
    </Property>
  </Itf>
</TcPlcObject>