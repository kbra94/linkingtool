﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_OpenClose" Id="{e30cedea-a23d-4090-b257-0c91b08a9846}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_OpenClose IMPLEMENTS I_StateMachineTextCallback
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date:$
 *	Last changed by	:	$Author:  $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Open Close control.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *  0.1				04.01.2022		dlu					First draft
 *
 **************************************************************************************)
VAR_INPUT
	Config					: ST_OpenClose_Config;
	AutomaticModeStarted	: BOOL;
	AutomaticEnable			: BOOL;
END_VAR
VAR_IN_OUT 
	ErrorDataSet 			: ST_ErrorDataSet;
END_VAR
VAR
	fbStateMachine			: FB_StateMachine(mName:= 'OpenClose state', mStateTextCallback:= THIS^);
	eState					: E_OpenCloseStates	:= E_OpenCloseStates.INIT;
	eLastState				: E_OpenCloseStates	:= E_OpenCloseStates.INIT;
	
	fbPosOverrideTimer  	: FB_Timer; // Timer to let position a little bit ovveride
	
	command					: E_OpenClosePositions;
	RunMotorExtern			: BOOL;
	
	MotorDir				: BOOL;
	
	SensorOpen				: BOOL;
	SensorClose				: BOOL;
	
	PosReachedInSemiMode 	: BOOL;
	RTrigPosReached			: R_TRIG;	
	RTrigCmdRunMotorExtern	: R_TRIG;
	
	Init					: BOOL;
	ManualMode				: BOOL;
	SemiAutoMode			: BOOL;
	AutomaticMode			: BOOL;
	Error					: BOOL;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[M_RunStates(_ErrorDataSet:= ErrorDataSet);]]></ST>
    </Implementation>
    <Method Name="M_GetStateText" Id="{c36bb109-6c57-4e70-b292-8b5e93afa198}">
      <Declaration><![CDATA[(* forwards state text to FB_StateMachine*)
METHOD M_GetStateText : A_StateText
VAR_INPUT
	(* requesting state machine instance*)
	StateMachine	: POINTER TO FB_StateMachine;
	(* state number which text is requested*)
	StateNbr	: UINT;
END_VAR
VAR
	eStateEnum	: E_OpenCloseStates := E_OpenCloseStates.INIT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[eStateEnum:= StateNbr;
M_GetStateText  := TO_STRING(eStateEnum);
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_RunStates" Id="{31b9a1bf-87af-4559-a6c1-6c664ea088e4}">
      <Declaration><![CDATA[METHOD PRIVATE M_RunStates
VAR_IN_OUT 
	_ErrorDataSet : ST_ErrorDataSet;
END_VAR
VAR
	Reason	: STRING[127];
    i       : INT;
	
	SensorGotOccupied : BOOL;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
IF Error THEN 
	F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'set error by extern',
					   ErrorMsg		:= E_ErrorConv.DEVICE_ERROR ,
					   ErrorData	:= _ErrorDataSet);
					   
	fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Error,
									mReason:= 'set error by extern');
									
END_IF

// Run state maschine
eState		:= fbStateMachine.P_State;

CASE eState OF
	E_OpenCloseStates.INIT:
		IF fbStateMachine.P_Entering THEN 
			MotorDir		:= FALSE;
			Error			:= FALSE;
			command 		:= E_OpenClosePositions.Undefined;
			RunMotorExtern	:= FALSE;
			F_ResetError(Reset:= TRUE, ErrorData := _ErrorDataSet);
		END_IF

		// validate inputs
		IF SensorOpen AND SensorClose THEN
			
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'both open/close sensors active',
					   ErrorMsg		:= E_ErrorConv.POSITIONING_ERROR ,
					   ErrorData	:= _ErrorDataSet);
					   
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Error,
									  mReason:= 'both open/close sensors active');
			
		ELSE
			Init := TRUE;
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Idle,
									mReason:= 'initialisation done');
		END_IF
	E_OpenCloseStates.Idle:
		IF fbStateMachine.P_Entering THEN 
			RunMotorExtern := FALSE;
			IF _ErrorDataSet.ErrorType = E_ErrorType.ERROR THEN 
				fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Error,
						mReason:= 'idle with error pending');
			END_IF

		ELSIF command = E_OpenClosePositions.Open AND AutomaticModeStarted THEN 
				fbStateMachine.M_SetState(mState:= E_OpenCloseStates.CmdOpen,
								mReason:= 'by command');
		ELSIF command = E_OpenClosePositions.Close AND AutomaticModeStarted THEN 
				fbStateMachine.M_SetState(mState:= E_OpenCloseStates.CmdClose,
								mReason:= 'by command');
		ELSIF SensorOpen THEN 
				fbStateMachine.M_SetState(mState:= E_OpenCloseStates.IsOpen,
								mReason:= 'SensorOpen active');
		ELSIF SensorClose THEN 
				fbStateMachine.M_SetState(mState:= E_OpenCloseStates.IsClose,
								mReason:= 'SensorClose active');
		END_IF 
		
	E_OpenCloseStates.IsOpen:
		IF fbStateMachine.P_Entering THEN 
			command 		:= E_OpenClosePositions.Undefined;
		END_IF

		IF command = E_OpenClosePositions.Close AND AutomaticModeStarted THEN 
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.CmdClose,
							mReason:= 'by command');
		ELSIF NOT SensorOpen AND Config.CreatesErrorOnPosLost THEN 
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'sensor signal open lost unexpected',
					   ErrorMsg		:= E_ErrorConv.INCORRECT_SENSOR_SIGNAL,
					   ErrorData	:= _ErrorDataSet);
			
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Error,
							mReason:= 'sensor signal lost');			
		END_IF 

	E_OpenCloseStates.IsClose:
		IF fbStateMachine.P_Entering THEN 
			command 		:= E_OpenClosePositions.Undefined;
		END_IF

		IF command = E_OpenClosePositions.Open AND AutomaticModeStarted THEN 
				fbStateMachine.M_SetState(mState:= E_OpenCloseStates.CmdOpen,
								mReason:= 'by command');
		ELSIF NOT SensorClose AND Config.CreatesErrorOnPosLost THEN  
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'sensor signal close lost unexpected',
					   ErrorMsg		:= E_ErrorConv.INCORRECT_SENSOR_SIGNAL,
					   ErrorData	:= _ErrorDataSet);
			
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Error,
							mReason:= 'sensor signal lost');			
		END_IF 
		
	E_OpenCloseStates.CmdOpen:
	
		IF fbStateMachine.P_Entering THEN 
			command := E_OpenClosePositions.Undefined;
			fbPosOverrideTimer.IN := FALSE;
			MotorDir := TRUE;
		END_IF 
		
		fbPosOverrideTimer(IN := SensorOpen, PT := Config.TimePosOverride);		
		
		// Automatic mode was forced to stop
		IF NOT AutomaticEnable THEN
			
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.AutomaticModeStopped,
									  mReason:= 'Automatic mode was forced to stopped');
		
		ELSIF SensorOpen AND fbPosOverrideTimer.Q THEN
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.IsOpen,
						mReason:= 'sensor active');	
		ELSIF fbStateMachine.P_TimeInState > Config.Movement_Timeout THEN 
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'Timeout Movement open',
					   ErrorMsg		:= E_ErrorConv.TIMEOUT,
					   ErrorData	:= _ErrorDataSet);
					   
					 
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Error,
						mReason:= 'timeout');	
						
		END_IF
		
	E_OpenCloseStates.CmdClose:		
		IF fbStateMachine.P_Entering THEN 
			command := E_OpenClosePositions.Undefined; 
			fbPosOverrideTimer.IN := FALSE;
			MotorDir := FALSE;
		END_IF 
		
		fbPosOverrideTimer(IN := SensorClose, PT := Config.TimePosOverride);
		
		// Automatic mode was forced to stop
		IF NOT AutomaticEnable THEN
			
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.AutomaticModeStopped,
									  mReason:= 'Automatic mode was forced to stopped');
		
		ELSIF SensorClose AND fbPosOverrideTimer.Q THEN
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.IsClose,
						mReason:= 'sensor active');	
		ELSIF fbStateMachine.P_TimeInState > Config.Movement_Timeout THEN 
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'Timeout Movement close',
					   ErrorMsg		:= E_ErrorConv.TIMEOUT,
					   ErrorData	:= _ErrorDataSet);
			
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Error,
						mReason:= 'timeout');	
		END_IF
		
	E_OpenCloseStates.AutomaticModeStopped:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		
		// Automatic mode is started
		IF AutomaticModeStarted AND AutomaticEnable THEN
			
			fbStateMachine.M_SetState(mState:= eLastState,
									  mReason:= 'Automatic mode started again');
	  	END_IF
		
	E_OpenCloseStates.Manual:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		
	E_OpenCloseStates.SemiAuto:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		
		// If sensors has to be override a little bit
		fbPosOverrideTimer(IN := SensorOpen OR SensorClose, PT := Config.TimePosOverride);

		RTrigPosReached(CLK := fbPosOverrideTimer.Q);
		RTrigCmdRunMotorExtern(CLK := RunMotorExtern);
		
		IF RTrigPosReached.Q AND NOT PosReachedInSemiMode THEN
			PosReachedInSemiMode := TRUE;
		END_IF
		
		IF RTrigCmdRunMotorExtern.Q THEN
			PosReachedInSemiMode := FALSE;
		END_IF
		
	E_OpenCloseStates.Error:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		IF _ErrorDataSet.ResetError OR _ErrorDataSet.ErrorType = E_ErrorType.NO_ERROR_PENDING THEN 
			fbStateMachine.M_SetState(mState:= E_OpenCloseStates.INIT,
						mReason:= 'reset by extern');	
		END_IF
		
		
	(********************************************************************************************************************************)
	(** invalid *********************************************************************************************************************)
	ELSE
		
		fbStateMachine.M_SetState1(mMsgType:= E_DebugMsgType.ERROR,
								mState := E_OpenCloseStates.Init,
								mReason:= 'invalid internal state');	
		
END_CASE 

eLastState	:= fbStateMachine.P_LastState;

]]></ST>
      </Implementation>
    </Method>
    <Property Name="P_AutomaticMode" Id="{ec449155-a13a-42a6-ba3a-fc87ede00d56}">
      <Declaration><![CDATA[PROPERTY P_AutomaticMode : BOOL]]></Declaration>
      <Get Name="Get" Id="{57e1520d-f9e7-4d6c-b77e-8253475ba816}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_AutomaticMode := NOT (eState = E_OpenCloseStates.Manual) AND NOT (eState = E_OpenCloseStates.SemiAuto);
]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{05bfa07f-44bf-443c-80af-108b0f0bd439}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF AutomaticMode <> P_AutomaticMode THEN
	AutomaticMode := P_AutomaticMode;
	
	IF AutomaticMode THEN 
		ManualMode := FALSE;
		SemiAutoMode := FALSE;
		fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Idle,
										mReason:= 'set automatic mode by extern');	
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_Command" Id="{43959ea7-3d33-46ce-9d00-9039c82e3a79}">
      <Declaration><![CDATA[PROPERTY P_Command : E_OpenClosePositions]]></Declaration>
      <Set Name="Set" Id="{82ea334c-6e2b-4ade-8a80-e38786964235}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF (P_Command = E_OpenClosePositions.Open AND eState <> E_OpenCloseStates.IsOpen)
OR (P_Command = E_OpenClosePositions.Close AND eState <> E_OpenCloseStates.IsClose) THEN 
	command := P_Command;
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_CurrentPosition" Id="{e548957b-733d-4ebd-895e-58a90eabe193}">
      <Declaration><![CDATA[PROPERTY P_CurrentPosition : E_OpenClosePositions]]></Declaration>
      <Get Name="Get" Id="{42d357f7-6aa7-4903-b9e1-e56f0aac3939}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF eState = E_OpenCloseStates.IsOpen THEN 
	P_CurrentPosition := E_OpenClosePositions.Open;	
ELSIF eState = E_OpenCloseStates.IsClose THEN
	P_CurrentPosition := E_OpenClosePositions.Close;
ELSE
	P_CurrentPosition := E_OpenClosePositions.Undefined;
END_IF

 ]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="P_Error" Id="{01556f35-6821-436f-aa93-1195eadf29ee}">
      <Declaration><![CDATA[PROPERTY P_Error : BOOL]]></Declaration>
      <Get Name="Get" Id="{804461ce-f04e-4b72-807e-9e15c6ae01ec}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_Error := (eState = E_OpenCloseStates.Error);]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{b1c62851-170e-4136-977d-9b921da589b7}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Error := P_Error;
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_Init" Id="{e596d1e1-79ec-485a-a9b4-58c06452b27c}">
      <Declaration><![CDATA[PROPERTY P_Init : BOOL]]></Declaration>
      <Get Name="Get" Id="{2bb5756c-1b10-4887-8615-bc31e231be4c}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_Init := Init;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{b8f73764-c6cb-4b77-8da3-1d743a7ecc7d}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF AutomaticMode THEN
	
	IF NOT P_Init THEN
		Init := P_Init;
		fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Init,
								mReason:= 'set init by extern');
	END_IF
END_IF
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_ManualMode" Id="{83e6862f-6fc3-4d05-8cb2-0b4885f56b36}">
      <Declaration><![CDATA[PROPERTY P_ManualMode : BOOL]]></Declaration>
      <Get Name="Get" Id="{0efcdd69-43f1-4a0f-9174-37dc573b21cb}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_ManualMode := (eState = E_OpenCloseStates.Manual);
]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{743d18b1-e5ae-4d33-ba99-4523dfd0a55b}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF ManualMode <> P_ManualMode THEN
	ManualMode := P_ManualMode;
	
	IF ManualMode THEN 
		AutomaticMode 	:= FALSE;
		SemiAutoMode 	:= FALSE;
		fbStateMachine.M_SetState(mState:= E_OpenCloseStates.Manual,
									mReason:= 'set manual mode by extern');
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_MotorDir" Id="{68548d59-46e8-4947-aa0d-ee885c07db4a}">
      <Declaration><![CDATA[PROPERTY P_MotorDir : BOOL]]></Declaration>
      <Get Name="Get" Id="{3051fc59-373f-4fa0-a907-92c5aaf0aaf7}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_MotorDir := MotorDir XOR Config.DefaultDirectionACW;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{0d71c9a8-2cad-4b8c-8945-429ee27c5695}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF eState = E_OpenCloseStates.Manual OR eState = E_OpenCloseStates.SemiAuto THEN 
	MotorDir := P_MotorDir;
END_IF 
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_MotorRun" Id="{c602c764-4b97-467a-95d4-6ca4f5371c85}">
      <Declaration><![CDATA[PROPERTY P_MotorRun : BOOL]]></Declaration>
      <Get Name="Get" Id="{2915ebb9-d4ad-46cd-b66b-8a25c6087ef5}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_MotorRun := (eState = E_OpenCloseStates.CmdOpen OR eState = E_OpenCloseStates.CmdClose)
				OR (eState = E_OpenCloseStates.Manual AND RunMotorExtern)
				OR (eState = E_OpenCloseStates.SemiAuto AND RunMotorExtern AND NOT PosReachedInSemiMode);]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="P_RunMotorExtern" Id="{a0ef00d8-97fe-45f4-bd7e-d5480742bb0e}">
      <Declaration><![CDATA[PROPERTY P_RunMotorExtern : BOOL]]></Declaration>
      <Set Name="Set" Id="{6056d731-aff2-4ae2-ae88-fc11f7c1f338}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF (eState = E_OpenCloseStates.Manual OR eState = E_OpenCloseStates.SemiAuto) THEN 
	RunMotorExtern := P_RunMotorExtern;
END_IF
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_SemiAutoMode" Id="{2a3363ab-1ced-4a60-a10a-ddc519501e1e}">
      <Declaration><![CDATA[PROPERTY P_SemiAutoMode : BOOL]]></Declaration>
      <Get Name="Get" Id="{768c5732-30e2-4824-9b14-1cc58bad8515}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_SemiAutoMode := (eState = E_OpenCloseStates.SemiAuto);
]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{1967deb0-cecd-4729-8c69-61e5006305fb}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF SemiAutoMode <> P_SemiAutoMode THEN
	SemiAutoMode := P_SemiAutoMode;
	
	IF SemiAutoMode THEN 
		AutomaticMode 	:= FALSE;
		ManualMode 		:= FALSE;
		fbStateMachine.M_SetState(mState:= E_OpenCloseStates.SemiAuto,
									mReason:= 'set semi-auto mode by extern');
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_SensorClosed" Id="{b8cee930-d2db-43d2-834e-3acb462ff490}">
      <Declaration><![CDATA[PROPERTY P_SensorClosed : BOOL]]></Declaration>
      <Set Name="Set" Id="{7b210668-3b69-4f52-bab8-e246fa1f19be}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[SensorClose := P_SensorClosed;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_SensorOpened" Id="{c435af8a-431b-4148-a3ff-5b0d97def185}">
      <Declaration><![CDATA[PROPERTY P_SensorOpened : BOOL]]></Declaration>
      <Set Name="Set" Id="{4e058dfc-c428-43e6-947f-4a88669eb5b0}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[SensorOpen := P_SensorOpened;]]></ST>
        </Implementation>
      </Set>
    </Property>
  </POU>
</TcPlcObject>