﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_Hoist" Id="{f33a6add-a75d-444c-a3d0-ed044493a9e1}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_Hoist IMPLEMENTS I_StateMachineTextCallback
VAR_INPUT
	Config					: ST_Hoist_Config;
	AutomaticModeStarted	: BOOL;
	AutomaticEnable			: BOOL;
END_VAR
VAR_IN_OUT 
	ErrorDataSet 	: ST_ErrorDataSet;
END_VAR
VAR
	fbStateMachine	: FB_StateMachine(mName:= 'Hoist state', mStateTextCallback:= THIS^);
	eState			: E_HoistStates	:= E_HoistStates.INIT;
	eLastState		: E_HoistStates	:= E_HoistStates.INIT;
	
	fbPosOverrideTimer  : FB_Timer; // Timer to let position a little bit ovveride
	
	command				: E_HoistPositions;
	RunMotorExtern		: BOOL;
	
	MotorDir		: BOOL;
	
	SensorUp		: BOOL;
	SensorMiddle	: BOOL;
	SensorDown		: BOOL;
	
	PosReachedInSemiMode : BOOL;
	RTrigPosReached				: R_TRIG;	
	RTrigCmdRunMotorExtern		: R_TRIG;
	
	Init			: BOOL;
	ManualMode		: BOOL;
	SemiAutoMode	: BOOL;
	AutomaticMode	: BOOL;
	Error			: BOOL;
	
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[M_RunStates(_ErrorDataSet:= ErrorDataSet);]]></ST>
    </Implementation>
    <Method Name="M_GetStateText" Id="{c99a02f8-0a84-4c88-a5ff-2a090ddcfc62}">
      <Declaration><![CDATA[(* forwards state text to FB_StateMachine*)
METHOD M_GetStateText : A_StateText
VAR_INPUT
	(* requesting state machine instance*)
	StateMachine	: POINTER TO FB_StateMachine;
	(* state number which text is requested*)
	StateNbr	: UINT;
END_VAR
VAR
	eStateEnum	: E_HoistStates := E_HoistStates.INIT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[eStateEnum:= StateNbr;
M_GetStateText  := TO_STRING(eStateEnum);
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_RunStates" Id="{b6141f19-317c-45c2-b4eb-a18d5c27dc05}">
      <Declaration><![CDATA[METHOD PRIVATE M_RunStates
VAR_IN_OUT 
	_ErrorDataSet : ST_ErrorDataSet;
END_VAR
VAR
	Reason	: STRING[127];
    i       : INT;
	
	SensorGotOccupied : BOOL;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
IF Error THEN 
	F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'set error by extern',
					   ErrorMsg		:= E_ErrorConv.DEVICE_ERROR ,
					   ErrorData	:= _ErrorDataSet);
					   
	fbStateMachine.M_SetState(mState:= E_HoistStates.Error,
									mReason:= 'set error by extern');
									
END_IF

// Run state maschine
eState		:= fbStateMachine.P_State;

CASE eState OF
	E_HoistStates.INIT:
		IF fbStateMachine.P_Entering THEN 
			MotorDir		:= FALSE;
			Error			:= FALSE;
			command 		:= E_HoistPositions.Undefined;
			RunMotorExtern	:= FALSE;
			F_ResetError(Reset:= TRUE, ErrorData := _ErrorDataSet);
		END_IF

		// validate inputs
		IF (SensorUp AND SensorMiddle) 
			OR (SensorDown AND SensorMiddle) 
			OR (SensorUp AND SensorDown) THEN
			
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'sensors up/down/middle sim. active',
					   ErrorMsg		:= E_ErrorConv.POSITIONING_ERROR ,
					   ErrorData	:= _ErrorDataSet);
					   
			fbStateMachine.M_SetState(mState:= E_HoistStates.Error,
									  mReason:= 'sensors up/down/middle sim. active');
			
		ELSE
			Init := TRUE;
			fbStateMachine.M_SetState(mState:= E_HoistStates.Idle,
									mReason:= 'initialisation done');
		END_IF
	E_HoistStates.Idle:
		IF fbStateMachine.P_Entering THEN 
			RunMotorExtern := FALSE;
			IF _ErrorDataSet.ErrorType = E_ErrorType.ERROR THEN 
				fbStateMachine.M_SetState(mState:= E_HoistStates.Error,
						mReason:= 'idle with error pending');
			END_IF

		ELSIF command = E_HoistPositions.Up AND AutomaticModeStarted THEN 
				fbStateMachine.M_SetState(mState:= E_HoistStates.MoveUp,
								mReason:= 'by command');
		ELSIF command = E_HoistPositions.Down AND AutomaticModeStarted THEN 
				fbStateMachine.M_SetState(mState:= E_HoistStates.MoveDown,
								mReason:= 'by command');
		ELSIF command = E_HoistPositions.Middle AND AutomaticModeStarted THEN 
				fbStateMachine.M_SetState(mState:= E_HoistStates.MoveMiddle,
								mReason:= 'by command');
		ELSIF SensorDown THEN 
				fbStateMachine.M_SetState(mState:= E_HoistStates.IsDown,
								mReason:= 'SensorDown active');
		ELSIF SensorUp THEN 
				fbStateMachine.M_SetState(mState:= E_HoistStates.IsUp,
								mReason:= 'SensorUp active');
		ELSIF SensorMiddle THEN 
				fbStateMachine.M_SetState(mState:= E_HoistStates.IsMiddle,
								mReason:= 'SensorMiddle active');
		END_IF 
		
	E_HoistStates.IsUp:
		IF fbStateMachine.P_Entering THEN 
			command := E_HoistPositions.Undefined;
		END_IF
		IF NOT Config.EccentricDrive THEN 
			MotorDir := FALSE;
		END_IF
		IF command = E_HoistPositions.Down AND AutomaticModeStarted THEN 
			fbStateMachine.M_SetState(mState:= E_HoistStates.MoveDown,
							mReason:= 'by command');
		ELSIF command = E_HoistPositions.Middle AND AutomaticModeStarted THEN 
			fbStateMachine.M_SetState(mState:= E_HoistStates.MoveMiddle,
							mReason:= 'by command');
		ELSIF NOT SensorUp AND Config.CreatesErrorOnPosLost THEN 
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'sensor signal up lost unexpected',
					   ErrorMsg		:= E_ErrorConv.INCORRECT_SENSOR_SIGNAL,
					   ErrorData	:= _ErrorDataSet);
			
			
			fbStateMachine.M_SetState(mState:= E_HoistStates.Error,
							mReason:= 'sensor signal lost');			
		END_IF 

		
		
	E_HoistStates.IsMiddle:
		IF fbStateMachine.P_Entering THEN 
			command := E_HoistPositions.Undefined;
		END_IF
		IF command = E_HoistPositions.Down AND AutomaticModeStarted THEN 
				IF NOT Config.EccentricDrive THEN 
					MotorDir := FALSE;
				END_IF
				fbStateMachine.M_SetState(mState:= E_HoistStates.MoveDown,
								mReason:= 'by command');
		ELSIF command = E_HoistPositions.Up AND AutomaticModeStarted THEN
				IF NOT Config.EccentricDrive THEN 
					MotorDir := TRUE;
				END_IF 
				fbStateMachine.M_SetState(mState:= E_HoistStates.MoveUp,
								mReason:= 'by command');
		ELSIF NOT SensorMiddle AND Config.CreatesErrorOnPosLost THEN  
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'sensor signal middle lost unexpected',
					   ErrorMsg		:= E_ErrorConv.INCORRECT_SENSOR_SIGNAL,
					   ErrorData	:= _ErrorDataSet);
			
			fbStateMachine.M_SetState(mState:= E_HoistStates.Error,
							mReason:= 'sensor signal lost');			
		END_IF 

		;
	E_HoistStates.IsDown:
		IF fbStateMachine.P_Entering THEN 
			command := E_HoistPositions.Undefined;
		END_IF
		IF NOT Config.EccentricDrive THEN 
			MotorDir := TRUE;
		END_IF
		IF command = E_HoistPositions.Up AND AutomaticModeStarted THEN 
				fbStateMachine.M_SetState(mState:= E_HoistStates.MoveUp,
								mReason:= 'by command');
		ELSIF command = E_HoistPositions.Middle AND AutomaticModeStarted THEN 
				fbStateMachine.M_SetState(mState:= E_HoistStates.MoveMiddle,
								mReason:= 'by command');
		ELSIF NOT SensorDown AND Config.CreatesErrorOnPosLost THEN  
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'sensor signal down lost unexpected',
					   ErrorMsg		:= E_ErrorConv.INCORRECT_SENSOR_SIGNAL,
					   ErrorData	:= _ErrorDataSet);
			
			fbStateMachine.M_SetState(mState:= E_HoistStates.Error,
							mReason:= 'sensor signal lost');			
		END_IF 
		
	E_HoistStates.MoveUp:
	
		IF fbStateMachine.P_Entering THEN 
			command := E_HoistPositions.Undefined;
			fbPosOverrideTimer.IN := FALSE;
			IF NOT Config.EccentricDrive THEN
				MotorDir := TRUE;
			END_IF
		END_IF 
		
		fbPosOverrideTimer(IN := SensorUp, PT := Config.TimePosOverride);
		
		// Automatic mode was forced to stop
		IF NOT AutomaticEnable THEN
			
			fbStateMachine.M_SetState(mState:= E_HoistStates.AutomaticModeStopped,
									  mReason:= 'Automatic mode was forced to stopped');
		
		ELSIF SensorUp AND fbPosOverrideTimer.Q THEN
			fbStateMachine.M_SetState(mState:= E_HoistStates.IsUp,
						mReason:= 'sensor active');	
		ELSIF fbStateMachine.P_TimeInState > Config.Movement_Timeout THEN 
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'Timeout Movement to up',
					   ErrorMsg		:= E_ErrorConv.TIMEOUT,
					   ErrorData	:= _ErrorDataSet);
					   
					 
			fbStateMachine.M_SetState(mState:= E_HoistStates.Error,
						mReason:= 'timeout');	
						
		END_IF
		
	E_HoistStates.MoveMiddle:
		IF fbStateMachine.P_Entering THEN 
			command := E_HoistPositions.Undefined;
			fbPosOverrideTimer.IN := FALSE;
		END_IF 
	
		fbPosOverrideTimer(IN := SensorMiddle, PT := Config.TimePosOverride);
		
		// Automatic mode was forced to stop
		IF NOT AutomaticEnable THEN
			
			fbStateMachine.M_SetState(mState:= E_HoistStates.AutomaticModeStopped,
									  mReason:= 'Automatic mode was forced to stopped');
		
		ELSIF SensorMiddle AND fbPosOverrideTimer.Q THEN
			fbStateMachine.M_SetState(mState:= E_HoistStates.IsMiddle,
						mReason:= 'sensor active');	
		ELSIF fbStateMachine.P_TimeInState > Config.Movement_Timeout THEN 
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'Timeout Movement to middle',
					   ErrorMsg		:= E_ErrorConv.TIMEOUT,
					   ErrorData	:= _ErrorDataSet);
			
			fbStateMachine.M_SetState(mState:= E_HoistStates.Error,
						mReason:= 'timeout');	
		END_IF
		
	E_HoistStates.MoveDown:		
		IF fbStateMachine.P_Entering THEN 
			command := E_HoistPositions.Undefined; 
			fbPosOverrideTimer.IN := FALSE;
			IF NOT Config.EccentricDrive THEN
				MotorDir := FALSE;
			END_IF
		END_IF 
		
		fbPosOverrideTimer(IN := SensorDown, PT := Config.TimePosOverride);
		
		// Automatic mode was forced to stop
		IF NOT AutomaticEnable THEN
			
			fbStateMachine.M_SetState(mState:= E_HoistStates.AutomaticModeStopped,
									  mReason:= 'Automatic mode was forced to stopped');
		
		ELSIF SensorDown AND fbPosOverrideTimer.Q THEN
			fbStateMachine.M_SetState(mState:= E_HoistStates.IsDown,
						mReason:= 'sensor active');	
		ELSIF fbStateMachine.P_TimeInState > Config.Movement_Timeout THEN 
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'Timeout Movement to down',
					   ErrorMsg		:= E_ErrorConv.TIMEOUT,
					   ErrorData	:= _ErrorDataSet);
			
			fbStateMachine.M_SetState(mState:= E_HoistStates.Error,
						mReason:= 'timeout');	
		END_IF
		
	E_HoistStates.AutomaticModeStopped:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		
		// Automatic mode is started
		IF AutomaticModeStarted AND AutomaticEnable THEN
			
			fbStateMachine.M_SetState(mState:= eLastState,
									  mReason:= 'Automatic mode started again');
	  	END_IF
		
	E_HoistStates.Manual:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		
	E_HoistStates.SemiAuto:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		
		// If sensors has to be override a little bit
		fbPosOverrideTimer(IN := SensorDown OR SensorMiddle OR SensorUp, PT := Config.TimePosOverride);

		RTrigPosReached(CLK := fbPosOverrideTimer.Q);
		RTrigCmdRunMotorExtern(CLK := RunMotorExtern);
		
		IF RTrigPosReached.Q AND NOT PosReachedInSemiMode THEN
			PosReachedInSemiMode := TRUE;
		END_IF
		
		IF RTrigCmdRunMotorExtern.Q THEN
			PosReachedInSemiMode := FALSE;
		END_IF
		
	E_HoistStates.Error:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		IF _ErrorDataSet.ResetError OR _ErrorDataSet.ErrorType = E_ErrorType.NO_ERROR_PENDING THEN 
			fbStateMachine.M_SetState(mState:= E_HoistStates.INIT,
						mReason:= 'reset by extern');	
		END_IF
		
	(********************************************************************************************************************************)
	(** invalid *********************************************************************************************************************)
	ELSE
		
		fbStateMachine.M_SetState1(mMsgType:= E_DebugMsgType.ERROR,
								mState := E_HoistStates.Init,
								mReason:= 'invalid internal state');	
END_CASE 

eLastState	:= fbStateMachine.P_LastState;

]]></ST>
      </Implementation>
    </Method>
    <Property Name="P_AutomaticMode" Id="{f9f8f2c9-493a-4cba-96f5-3f9846e40246}">
      <Declaration><![CDATA[PROPERTY P_AutomaticMode : BOOL]]></Declaration>
      <Get Name="Get" Id="{d8639054-b0b0-483d-ac6e-17ba65498be9}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_AutomaticMode := NOT (eState = E_HoistStates.Manual) AND NOT (eState = E_HoistStates.SemiAuto);
]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{e9eb7967-f115-4355-967f-c6de1a94328e}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF AutomaticMode <> P_AutomaticMode THEN
	AutomaticMode := P_AutomaticMode;
	
	IF AutomaticMode THEN 
		ManualMode := FALSE;
		SemiAutoMode := FALSE;
		fbStateMachine.M_SetState(mState:= E_HoistStates.Idle,
										mReason:= 'set automatic mode by extern');	
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_Command" Id="{80004b7c-07de-4ae3-a233-d2e763dd00d5}">
      <Declaration><![CDATA[PROPERTY P_Command : E_HoistPositions]]></Declaration>
      <Set Name="Set" Id="{981509bc-6594-4aea-b2b3-75c5dd3ce6b0}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF (P_Command = E_HoistPositions.Down AND eState <> E_HoistStates.IsDown)
OR (P_Command = E_HoistPositions.Middle AND eState <> E_HoistStates.IsMiddle) 
OR (P_Command = E_HoistPositions.Up AND eState <> E_HoistStates.IsUp) THEN 
	command := P_Command;
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_CurrentPosition" Id="{7fe19c9d-04b2-4c8b-989a-63dc77e5d460}">
      <Declaration><![CDATA[PROPERTY P_CurrentPosition : E_HoistPositions]]></Declaration>
      <Get Name="Get" Id="{88e0754d-ecea-4c9e-ba4a-8f3e61b4ea19}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF eState = E_HoistStates.IsDown THEN 
	P_CurrentPosition := E_HoistPositions.Down;	
ELSIF eState = E_HoistStates.IsMiddle THEN 
	P_CurrentPosition := E_HoistPositions.Middle;	
ELSIF eState = E_HoistStates.IsUp THEN
	P_CurrentPosition := E_HoistPositions.Up;
ELSE
	P_CurrentPosition := E_HoistPositions.Undefined;
END_IF

 ]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="P_Error" Id="{d16e5622-3f62-4651-abd3-d43bddd557a6}">
      <Declaration><![CDATA[PROPERTY P_Error : BOOL]]></Declaration>
      <Get Name="Get" Id="{998815ad-0926-46e7-a55e-eeb15319ebdc}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_Error := (eState = E_HoistStates.Error);]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{2dfc46c2-f6d5-480c-91ac-b5d44b8e2ced}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Error := P_Error;
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_Init" Id="{0e2e56e6-6cff-4fd2-9c28-7d847d894fd1}">
      <Declaration><![CDATA[PROPERTY P_Init : BOOL]]></Declaration>
      <Get Name="Get" Id="{845ff44b-0635-4c17-802c-5c70f335a3eb}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_Init := Init;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{3258889c-acd3-4612-ae25-8d5db5b38997}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF AutomaticMode THEN
	
	IF NOT P_Init THEN
		Init := P_Init;
		fbStateMachine.M_SetState(mState:= E_HoistStates.Init,
								mReason:= 'set init by extern');
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_ManualMode" Id="{491e834e-d07b-4146-ab24-f545f02a3967}">
      <Declaration><![CDATA[PROPERTY P_ManualMode : BOOL]]></Declaration>
      <Get Name="Get" Id="{dbcf9f15-e709-4125-81f5-be9ebf60cd13}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_ManualMode := (eState = E_HoistStates.Manual);
]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{2de9c674-8abc-4b97-a2c3-7d4e28efc316}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF ManualMode <> P_ManualMode THEN
	ManualMode := P_ManualMode;
	
	IF ManualMode THEN 
		AutomaticMode 	:= FALSE;
		SemiAutoMode 	:= FALSE;
		fbStateMachine.M_SetState(mState:= E_HoistStates.Manual,
									mReason:= 'set manual mode by extern');
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_MotorDir" Id="{5c407178-bff0-433a-9e81-dfe384809511}">
      <Declaration><![CDATA[PROPERTY P_MotorDir : BOOL]]></Declaration>
      <Get Name="Get" Id="{1d901052-7ebf-42fc-8c8a-30fb3dbf100b}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_MotorDir := MotorDir XOR Config.DefaultDirectionACW;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{af6f4305-29a3-4535-bc35-167db213173b}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF eState = E_HoistStates.Manual OR eState = E_HoistStates.SemiAuto THEN 
	MotorDir := P_MotorDir;
END_IF 
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_MotorRun" Id="{9b1c33bb-f0f2-4dcb-afd7-d77dd6bd74a1}">
      <Declaration><![CDATA[PROPERTY P_MotorRun : BOOL]]></Declaration>
      <Get Name="Get" Id="{6e21f357-d242-43be-ad5c-7ce74382915d}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_MotorRun := (eState = E_HoistStates.MoveDown OR eState = E_HoistStates.MoveUp OR eState = E_HoistStates.MoveMiddle)
				OR (eState = E_HoistStates.Manual AND RunMotorExtern)
				OR (eState = E_HoistStates.SemiAuto AND RunMotorExtern AND NOT PosReachedInSemiMode);]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="P_RunMotorExtern" Id="{67441f1c-cec8-4ee9-ae8d-433e9c972ba9}">
      <Declaration><![CDATA[PROPERTY P_RunMotorExtern : BOOL]]></Declaration>
      <Set Name="Set" Id="{bffd43ac-23bc-4328-9037-0c8f21c062e7}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF (eState = E_HoistStates.Manual OR eState = E_HoistStates.SemiAuto) THEN 
	RunMotorExtern := P_RunMotorExtern;
END_IF
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_SemiAutoMode" Id="{a810e7f9-bbda-4229-b33c-707bad8e411b}">
      <Declaration><![CDATA[PROPERTY P_SemiAutoMode : BOOL]]></Declaration>
      <Get Name="Get" Id="{40959bdc-1efa-4067-97b2-ad5de2424bd9}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_SemiAutoMode := (eState = E_HoistStates.SemiAuto);
]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{8c36d8cf-78e9-4625-a6a5-592f15f9fe4e}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF SemiAutoMode <> P_SemiAutoMode THEN
	SemiAutoMode := P_SemiAutoMode;
	
	IF SemiAutoMode THEN 
		AutomaticMode 	:= FALSE;
		ManualMode 		:= FALSE;
		fbStateMachine.M_SetState(mState:= E_HoistStates.SemiAuto,
									mReason:= 'set semi-auto mode by extern');
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_SensorDown" Id="{8adb1c98-f032-4da3-b879-41b6f08121b0}">
      <Declaration><![CDATA[PROPERTY P_SensorDown : BOOL]]></Declaration>
      <Set Name="Set" Id="{3175455c-58bb-466b-b94c-914412690c1e}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[SensorDown := P_SensorDown;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_SensorMiddle" Id="{ad2f56b9-d073-451e-a1ae-731bcee35b82}">
      <Declaration><![CDATA[PROPERTY P_SensorMiddle : BOOL]]></Declaration>
      <Set Name="Set" Id="{87860747-b0b0-42fa-9dc9-bdc9d4029cea}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[SensorMiddle := P_SensorMiddle;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_SensorUp" Id="{8c9cfd2f-8447-41ed-877b-ac1f8f0ba61c}">
      <Declaration><![CDATA[PROPERTY P_SensorUp : BOOL]]></Declaration>
      <Set Name="Set" Id="{4a8d99d4-5d92-479a-a8fe-e8ba1046135c}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[SensorUp := P_SensorUp;]]></ST>
        </Implementation>
      </Set>
    </Property>
  </POU>
</TcPlcObject>