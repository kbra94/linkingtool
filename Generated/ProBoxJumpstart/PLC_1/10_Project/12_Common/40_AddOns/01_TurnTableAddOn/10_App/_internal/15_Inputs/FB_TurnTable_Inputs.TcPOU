﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_TurnTable_Inputs" Id="{f856b0c0-1e23-4d59-be3f-32eeb5909213}" SpecialFunc="None">
    <Declaration><![CDATA[// converts the hw input signals into correct logic regarding configuration
FUNCTION_BLOCK INTERNAL FB_TurnTable_Inputs

(********************************************************************************************************************************)
VAR
	_Config				: I_TurnTable_Configuration		:= 0;		// configuration
	_HW_Inputs			: I_TurnTable_HwInputs			:= 0;		// hardware input signals
	//----------------------------------------------------------------------------------------------------------------------------
	_Position_2			: FB_TurnTable_Inputs_Position();	// logic signals position 2
	_Position_3			: FB_TurnTable_Inputs_Position();	// logic signals position 3
	_Position_4			: FB_TurnTable_Inputs_Position();	// logic signals position 4
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[]]></ST>
    </Implementation>
    <Folder Name="_internal" Id="{be099184-d4d1-4003-aa0b-c029b8974cb0}" />
    <Method Name="FB_init" Id="{5d59e3bc-8279-452a-8a3b-95d7a3ed3577}" FolderPath="_internal\">
      <Declaration><![CDATA[METHOD FB_init : BOOL

(********************************************************************************************************************************)
VAR_INPUT
	bInitRetains	: BOOL;		// if TRUE, the retain variables are initialized (warm start / cold start)
	bInCopyCode		: BOOL;		// if TRUE, the instance afterwards gets moved into the copy code (online change)
	//----------------------------------------------------------------------------------------------------------------------------
	Config								: I_TurnTable_Configuration		:= 0;		// configuration
	HW_Inputs							: I_TurnTable_HwInputs			:= 0;			// hardware input signals
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
_Config   := Config;
_HW_Inputs:= HW_Inputs;

_Position_2.Position:= _HW_Inputs.Position_2;	_Position_2.Config:= _Config;
_Position_3.Position:= _HW_Inputs.Position_3;	_Position_3.Config:= _Config;
_Position_4.Position:= _HW_Inputs.Position_4;	_Position_4.Config:= _Config;




(********************************************************************************************************************************)
(** done ************************************************************************************************************************)
]]></ST>
      </Implementation>
    </Method>
    <Property Name="MotorUnitOK" Id="{98c84cfa-ace9-41d2-9766-8f2f3daf3dbf}">
      <Declaration><![CDATA[// HI = motor unit ok
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC MotorUnitOK : BOOL]]></Declaration>
      <Get Name="Get" Id="{711e3ffb-3803-47b4-abb8-88c8f9d8b9b6}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[MotorUnitOK:= _HW_Inputs.MotorUnitOK;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="Overhang_Front" Id="{5a590a06-1aaa-4e4c-95f3-6ee1b48446a1}">
      <Declaration><![CDATA[// HI = overhang front free
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Overhang_Front : BOOL]]></Declaration>
      <Get Name="Get" Id="{d819267f-32ff-4a67-935e-c3bb9b6cd7c6}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Overhang_Front:= ( _HW_Inputs.Overhang_Front = _Config.ActiveSignalOverhangSensor );]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="Overhang_Rear" Id="{6cda8a2c-eccc-4d6f-ba89-30440f83b45b}">
      <Declaration><![CDATA[// HI = overhang rear free
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Overhang_Rear : BOOL]]></Declaration>
      <Get Name="Get" Id="{f0668dcc-56ec-4767-998f-f2a0e7657050}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Overhang_Rear:= ( _HW_Inputs.Overhang_Rear = _Config.ActiveSignalOverhangSensor );]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="Position_2" Id="{e22d4c8d-df3e-402c-8759-f608446e0610}">
      <Declaration><![CDATA[// logic signals position 2
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Position_2 : I_TurnTable_Inputs_Position]]></Declaration>
      <Get Name="Get" Id="{d7663c25-a126-4fc6-a0c1-3aad50febc84}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Position_2:= _Position_2;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="Position_3" Id="{1a2f5295-229a-4448-81f0-300b5cd2ffeb}">
      <Declaration><![CDATA[// logic signals position 3
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Position_3 : I_TurnTable_Inputs_Position]]></Declaration>
      <Get Name="Get" Id="{ba998eb3-ff92-48ea-85f4-53a0650f2c26}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Position_3:= _Position_3;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="Position_4" Id="{80f2470a-f44b-4b0f-bfec-428218753878}">
      <Declaration><![CDATA[// logic signals position 4
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Position_4 : I_TurnTable_Inputs_Position]]></Declaration>
      <Get Name="Get" Id="{c965c437-cae5-40c3-8943-c4aa1e525438}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Position_4:= _Position_4;]]></ST>
        </Implementation>
      </Get>
    </Property>
  </POU>
</TcPlcObject>