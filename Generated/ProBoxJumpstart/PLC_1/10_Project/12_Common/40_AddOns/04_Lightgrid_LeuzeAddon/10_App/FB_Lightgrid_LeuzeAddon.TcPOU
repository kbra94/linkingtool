﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_Lightgrid_LeuzeAddon" Id="{91c5b2ce-fe56-4ced-bee9-dbb9e680db17}" SpecialFunc="None">
    <Declaration><![CDATA[// Addon for a conveyor with Lightgrid for enabling muting
FUNCTION_BLOCK PUBLIC FB_Lightgrid_LeuzeAddon EXTENDS FB_FunctionConv
(**************************************************************************************
 * 	Application		:	QB-EC1
 *	Revision		:	$Revision: 0.1 $
 *	Revision date	:	$Date: 2021-09-29 15:34:47 $
 *	Last changed by	:	$Author: rot $
 *	URL				:	$URL: 
 *
 *	Purpose			:	Enable the Muting function for a Lightgrid
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				29.09.2021		rot					First version


 **************************************************************************************)

(********************************************************************************************************************************)
VAR_INPUT
	Config					: ST_CFG_Lightgrid_LeuzeAddon; // Configuration
	HW_Inputs		 		: ST_HW_InputsLightgrid_LeuzeAddon; // Hardware inputs
END_VAR

(********************************************************************************************************************************)
VAR_OUTPUT
	HW_Outputs				: ST_HW_OutputsLightgrid_LeuzeAddon;
END_VAR

(********************************************************************************************************************************)
VAR
	Inputs		 			: ST_HW_InputsLightgrid_LeuzeAddon; // Hardware inputs
	TransportState			: ST_TransportControlData;	//Transportation State
	DelayTimer				: TON; // Timer for Delay of MutingEnable
	StartDelay				: BOOL; //Starts the Timer for Delay
END_VAR

(********************************************************************************************************************************)
VAR PERSISTENT
	Settings				: ST_CFG_Lightgrid_LeuzeAddon;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[

(**************************************************************************************************************
   Input mapping
***************************************************************************************************************)
A_InputMapping();

(**************************************************************************************************************
   Initialize
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   State control
***************************************************************************************************************)
A_StateControl();

(**************************************************************************************************************
   Error handler
***************************************************************************************************************)
A_ErrorHandler();

(**************************************************************************************************************
   Interface handler out
***************************************************************************************************************)
M_ITC_ProcessOut();

(**************************************************************************************************************
   Output mapping
***************************************************************************************************************)
M_OutputMapping();]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{aee1e146-6459-47c8-bba0-033745c44595}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize function
 **************************************************************************************)
 
// Update Registry
M_UpdateRegistry();
 

// Init ongoing - Wait until init is released (Sequencial startup)
IF NOT FunctionData.Init AND FunctionData.InitRunning THEN
	// When the subsystem is started, 
	// When its released to initialize
	IF FunctionData.OperationState.Info.SystemReady AND
	   SettingsFunction.eFunctionNumber < FunctionData.OperationState.InitRelease.eLimitFunctionNumber THEN
	   
	   IF M_InitFunction(FALSE) THEN
			// Function initialized - and initialization done
			FunctionData.Init := TRUE;
			FunctionData.InitRunning := FALSE;
			
			// Set reset cmd
			FunctionData.ErrorData.Error.ResetError := TRUE;
			
			DebugMsg := CONCAT('Initialization done. Function: ', ConfigFunction.FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
		END_IF
	END_IF
(*
If the function is not initialized,
clean all values and wait until init is released
*)
ELSIF NOT FunctionData.Init THEN
	// Reset internal variables
	FunctionData.OperationState := FunctionBase.PositionStateEmpty;

	// Load function specific configuration to settings
	Settings := Config;
	
	FunctionInterface.Out.FunctionOrders.Enable := TRUE;
	
	// Reset values and load config
	M_PreInit();
	
	// To start init a valid functionnumber and element link is needed
	FunctionData.InitRunning := SettingsFunction.eFunctionNumber > 0 AND
								SettingsFunction.eElementLink > 0;
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InputMapping" Id="{2de80649-c8e7-4cf1-b7e7-de26ef8a65d3}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InputMapping
 * 	FUNCTION	Input mapping
 **************************************************************************************)
 
 (**************************************************************************************
   Hardware Inputs
***************************************************************************************)
// Adapt input logic state high/low
Inputs.MuteEnableAtHandover		:= Settings.InvertInputs.MuteEnableAtHandover XOR HW_Inputs.MuteEnableAtHandover;			      
Inputs.MuteEnableAtTakeover		:= Settings.InvertInputs.MuteEnableAtTakeover XOR HW_Inputs.MuteEnableAtTakeover;		 

(**************************************************************************************
   Element states
***************************************************************************************)
// Get status of the corresponding element
M_GetElementStates();


(**************************************************************************************************************
   fill variables
***************************************************************************************************************)
TransportState := F_GetTransportControlData(SettingsFunction.FunctionLink.eFunctionNumber, SettingsFunction.FunctionLink.ZoneNumber);

]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_OutputMapping" Id="{9011e6c7-305c-4eb8-9566-421b92ac33aa}">
      <Declaration><![CDATA[METHOD PROTECTED M_OutputMapping

(********************************************************************************************************************************)
VAR
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		M_OutputMapping
 * 	FUNCTION	Output mapping
 **************************************************************************************)
 
 DelayTimer(IN:= StartDelay, PT:= Settings.MutingStartDelay);
(**************************************************************************************
   Hardware outputs
***************************************************************************************)

//If the Muting is configured for Takeover
IF Settings.MutingTakeoverOnly OR Inputs.MuteEnableAtTakeover THEN
	IF TransportState.eTakeOverState = E_TakeoverState.ACTIVE THEN // Handoverstate is active
		StartDelay := TRUE;
		IF DelayTimer.Q THEN
			HW_Outputs.CSMutingEnable := TRUE;
		END_IF;
	ELSE
		StartDelay := FALSE;
		HW_Outputs.CSMutingEnable := FALSE;
	END_IF;
//the muting is configured for Handover
ELSIF Settings.MutingHandoverOnly OR Inputs.MuteEnableAtHandover THEN
	IF TransportState.eHandOverState = E_HandoverState.ACTIVE THEN // Handoverstate is active
		StartDelay := TRUE;
		IF DelayTimer.Q THEN
			HW_Outputs.CSMutingEnable := TRUE;
		END_IF
	ELSE
		StartDelay := FALSE;
		HW_Outputs.CSMutingEnable := FALSE;
	END_IF;
END_IF;
(**************************************************************************************
   AddOn outputs
***************************************************************************************)
FunctionInterface.Out.AddOnOrders.Enable := TRUE;
FunctionInterface.Out.AddOnOrders.EnableHandover := TRUE;
FunctionInterface.Out.AddOnOrders.EnableTakeover := TRUE;

]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>