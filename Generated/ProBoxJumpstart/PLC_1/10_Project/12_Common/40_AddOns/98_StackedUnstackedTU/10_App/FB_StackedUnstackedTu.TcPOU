﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_StackedUnstackedTu" Id="{e7134fde-004c-4179-9761-1cfd7e1a4883}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_StackedUnstackedTu EXTENDS FB_FunctionConv

VAR_INPUT
	Config : ST_CFG_StackedUnstacked;
	HW_Inputs : ST_HW_InputsStackedUnstackedTU; // Hardware inputs
	
END_VAR

VAR_OUTPUT
END_VAR

VAR
	Inputs : ST_InputsStackedUnstackedTU;
	
	StackedUnstackedStates : ST_StackedUnstackedStates;
	sensorOccupiedError : BOOL;			//Sensor Occupied befor the expected step
	measurementResult : BOOL;			//Result of the Measurement, TU Stacked or Unstacked
	
	TU_DataIndex			: DINT; // Searched TU index
END_VAR

VAR PERSISTENT
	Settings : ST_CFG_StackedUnstacked; // Settings
END_VAR

]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Input mapping
***************************************************************************************************************)
M_InputMapping();

(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();


(**************************************************************************************************************
   State control 
***************************************************************************************************************)
A_StateControl();


(**************************************************************************************************************
   Error handler
***************************************************************************************************************)
A_ErrorHandler();


(**************************************************************************************************************
   Profile control states
***************************************************************************************************************)
M_StackedUnstackedStates();		


(**************************************************************************************************************
   Interface handler out
***************************************************************************************************************)
M_ITC_ProcessOut();


(**************************************************************************************************************
   Output mapping
***************************************************************************************************************)
A_OutputMapping();]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{e6c24824-8c17-4a2a-9a56-829726b57133}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize function
 **************************************************************************************)

// Update Registry
M_UpdateRegistry();
 
// Init ongoing - Wait until init is released (Sequencial startup)
IF NOT FunctionData.Init AND FunctionData.InitRunning THEN
	// When the subsystem is started, 
	// and its released to initialize
	IF FunctionData.OperationState.Info.SystemReady AND
	   SettingsFunction.eFunctionNumber < FunctionData.OperationState.InitRelease.eLimitFunctionNumber THEN
	   
	   IF M_InitFunction(FALSE) THEN
			// Function initialized - and initialization done
			FunctionData.Init := TRUE;
			FunctionData.InitRunning := FALSE;
			
			// Set reset cmd
			FunctionData.ErrorData.Error.ResetError := TRUE;
			
			DebugMsg := CONCAT('Initialization done. Function: ', ConfigFunction.FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
		END_IF
	END_IF
(*
If the function is not initialized,
clean all values and wait until init is released
*)
ELSIF NOT FunctionData.Init THEN
	// Reset internal values
	StackedUnstackedStates.eLastState := StackedUnstackedStates.eState;
	StackedUnstackedStates.eState := E_StackedUnstackedState.INIT;
	FunctionData.OperationState := FunctionBase.PositionStateEmpty;

	// Load function specific configuration to settings
	Settings := Config;
	
	// Reset values and load config
	M_PreInit();
	
	// To start init a valid functionnumber and element link is needed
	FunctionData.InitRunning := SettingsFunction.eFunctionNumber > 0 AND
								SettingsFunction.eElementLink > 0;
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OutputMapping" Id="{c333c581-fb96-439c-8615-1178fbc733c2}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OutputMapping
 * 	FUNCTION	Output mapping
 **************************************************************************************)


(**************************************************************************************
   Order outputs (Commands to corresponding element)
***************************************************************************************)
// Drive run not needed
FunctionInterface.Out.FunctionOrders.ReqDriveRun := FALSE;

// Allow element to drive
FunctionInterface.Out.FunctionOrders.Enable := TRUE;
//FunctionInterface.Out.FunctionOrders.Enable_12 := TRUE;
FunctionInterface.Out.FunctionOrders.Enable_34 := TRUE;

  
(**************************************************************************************
   AddOn outputs
***************************************************************************************)
FunctionInterface.Out.AddOnOrders.Enable := TRUE;		// FunctionData.ErrorData.Error.ErrorType <> E_ErrorType.ERROR;
FunctionInterface.Out.AddOnOrders.EnableHandover := FunctionData.ErrorData.Error.ErrorType = E_ErrorType.NO_ERROR_PENDING AND FunctionInterface.Out.AddOnOrders.DataReady;
FunctionInterface.Out.AddOnOrders.EnableTakeover := TRUE;]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_getFunctionLinkTransportData" Id="{94abe05c-817a-499e-8e8c-b51df468f82f}">
      <Declaration><![CDATA[METHOD PRIVATE M_getFunctionLinkTransportData

]]></Declaration>
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
   Transport data of corresponding zone
***************************************************************************************)
// Get transport state of corresponding conveyor to generate a proper trigger
Inputs.FunctionLink_TransportData := F_GetTransportControlData(
							eFunctionNumber	:= SettingsFunction.FunctionLink.eFunctionNumber,
							ZoneNumber 		:= SettingsFunction.FunctionLink.ZoneNumber);]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_InputMapping" Id="{ca4a8875-a8ef-4ee3-b44e-e2b5a31a52ec}">
      <Declaration><![CDATA[METHOD PRIVATE M_InputMapping

]]></Declaration>
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InputMapping
 * 	FUNCTION	Input mapping
 **************************************************************************************)
 
(**************************************************************************************
   Hardware Inputs
***************************************************************************************)
// Adapt input logic state high/low
// Since no feeddback singnal from the drive is available the HW output signal for the drive is used at fbTransportControl instead.
//Inputs.DriveRunning := Settings.HW_InputInverted.DriveRunning XOR HW_Inputs.DriveRunning;
HW_Inputs.SensorStackedOccupied 	:= Settings.HW_InputInverted.SensorStackedOccupied XOR HW_Inputs.SensorStackedOccupied;


//HW Inputs auf Interne Variabel schreiben
Inputs.SensorStackedOccupied := HW_Inputs.SensorStackedOccupied;


(**************************************************************************************
   Element states
***************************************************************************************)
// Get status of the corresponding element
M_GetElementStates(); 

(**************************************************************************************
   AddOn states
***************************************************************************************)
// Get status of all corresponding AddOn functions and store it in FunctionInterface.In.AddOnOrder
M_GetAddOnOrder();


(**************************************************************************************
   Transport data of corresponding zone
***************************************************************************************)
// Get transport state of corresponding conveyor to generate a proper trigger
M_getFunctionLinkTransportData();
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_StackedUnstackedStates" Id="{76d0e727-0989-481f-b083-fd62f537aee9}">
      <Declaration><![CDATA[METHOD PRIVATE M_StackedUnstackedStates

]]></Declaration>
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
   Error reset
***************************************************************************************)
	
// Reset Incorrect sensor signal
F_ResetError(
	Reset 	  := FALSE,
	ErrorData := FunctionData.ErrorData.ErrorDataSet[5]);	

// Reset No free TU data set 
F_ResetError(
	Reset 	  := FALSE,
	ErrorData := FunctionData.ErrorData.ErrorDataSet[6]);
	

	
(**************************************************************************************
   StackedUnstacked states
***************************************************************************************)
CASE StackedUnstackedStates.eState OF
	
	(**************************************************************************************
   		INIT
	***************************************************************************************)
	E_StackedUnstackedState.INIT:
		
		(* 
		Data ready needs to be set all the time until 
		a new process will be started.
		*)
		FunctionInterface.Out.AddOnOrders.DataReady := TRUE;
		FunctionInterface.Out.AddOnOrders.TU_DataIndex := 0;
		
		
		// Change state
		StackedUnstackedStates.eLastState := StackedUnstackedStates.eState;
		StackedUnstackedStates.eState := E_StackedUnstackedState.IDLE;
		

	(**************************************************************************************
   		IDLE
	***************************************************************************************)
	E_StackedUnstackedState.IDLE:
		// Report data ready
		FunctionInterface.Out.AddOnOrders.DataReady := TRUE;
		
		// Function has to be started and TakeOverState ACTIVE
		IF FunctionData.OperationState.eState = E_PositionState.STARTED AND Inputs.FunctionLink_TransportData.eTakeOverState = E_TakeOverState.ACTIVE  THEN
			

				//Prepare sensorOccupiedError
				sensorOccupiedError := FALSE;
		
				// Go to "CHECK_OCCUPATION_SENSOR"
				StackedUnstackedStates.eLastState := StackedUnstackedStates.eState;
				StackedUnstackedStates.eState := E_StackedUnstackedState.CHECK_OCCUPATION_SENSOR;
			
		END_IF
		
	(**************************************************************************************
   		CHECK_OCCUPATION_SENSOR
	***************************************************************************************)
	E_StackedUnstackedState.CHECK_OCCUPATION_SENSOR:
	
		FunctionInterface.Out.AddOnOrders.DataReady := FALSE;
	
		IF Inputs.SensorStackedOccupied THEN
			sensorOccupiedError := TRUE;
		ELSE
			sensorOccupiedError := FALSE;
		END_IF
	
		//Prepare measurementResult
		measurementResult := FALSE;
	
		// Go to "MEASUREMENT"
		StackedUnstackedStates.eLastState := StackedUnstackedStates.eState;
		StackedUnstackedStates.eState := E_StackedUnstackedState.MEASUREMENT;
	

	(**************************************************************************************
   		MEASUREMENT
	***************************************************************************************)
	E_StackedUnstackedState.MEASUREMENT:
	
	
		IF Inputs.SensorStackedOccupied THEN
			measurementResult := TRUE;
		END_IF
	
		 
		IF Inputs.FunctionLink_TransportData.Occupied THEN
		
		// Go to "EVALUATE_MEASURED_DATA"
		StackedUnstackedStates.eLastState := StackedUnstackedStates.eState;
		StackedUnstackedStates.eState := E_StackedUnstackedState.EVALUATE_MEASURED_DATA;
		
		END_IF

	
	(**************************************************************************************
   		EVALUATE_MEASURED_DATA
	***************************************************************************************)
	E_StackedUnstackedState.EVALUATE_MEASURED_DATA:
	
	
		IF Inputs.FunctionLink_TransportData.TU_DataIndex = 0 THEN
			TU_DataIndex := F_InsertTU_Data(SettingsFunction.eFunctionNumber, 0);			
		ELSE
			TU_DataIndex := Inputs.FunctionLink_TransportData.TU_DataIndex;
		END_IF
		
	
		IF TU_DataIndex > 0 THEN
			IF sensorOccupiedError THEN
			
				TU_DataTable[TU_DataIndex].DataSet.TU_Type := Settings.TU_Type.UNKNOWN;
				
				// Error, Incorrect sensor signal
				F_SetError(
					ErrorType 	:= E_ErrorType.ERROR,
					ErrorParam 	:= 'Incorrect sensor signal',                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
					ErrorMsg 	:= E_ErrorConv.INCORRECT_SENSOR_SIGNAL,
					ErrorData 	:= FunctionData.ErrorData.ErrorDataSet[5]);
					
				//DebugsMsg
				IF Settings.EnableDebugMsg THEN
					
					DebugMsg := CONCAT ('TU_DataIndex = ', DINT_TO_STRING (TU_DataIndex));
					DebugMsg := CONCAT (DebugMsg, ', TU_Type = '); 
					DebugMsg := CONCAT (DebugMsg, INT_TO_STRING (TU_DataTable[TU_DataIndex].DataSet.TU_Type)); 
					
					fbDebugMsg.M_SendInfoMsg(DebugMsg);
				END_IF
			
				StackedUnstackedStates.eLastState := StackedUnstackedStates.eState;
				StackedUnstackedStates.eState := E_StackedUnstackedState.ERROR;
			ELSE
		
				
				IF measurementResult THEN
				
					TU_DataTable[TU_DataIndex].DataSet.TU_Type := Settings.TU_Type.STACKED;				//TU_TYPE 1 Unstacked ; 2 Stacked 
				ELSE
				
					TU_DataTable[TU_DataIndex].DataSet.TU_Type := Settings.TU_Type.UNSTACKED;
				END_IF
				
		
				//DebugsMsg
				IF Settings.EnableDebugMsg THEN
					
					DebugMsg := CONCAT ('TU_DataIndex = ', DINT_TO_STRING (TU_DataIndex));
					DebugMsg := CONCAT (DebugMsg, ', TU_Type = '); 
					DebugMsg := CONCAT (DebugMsg, INT_TO_STRING (TU_DataTable[TU_DataIndex].DataSet.TU_Type)); 
					
					fbDebugMsg.M_SendInfoMsg(DebugMsg);
				END_IF
					
				
				
				StackedUnstackedStates.eLastState := StackedUnstackedStates.eState;
				StackedUnstackedStates.eState := E_StackedUnstackedState.END;
			END_IF
			
		ELSE
			
			// Error, No free TU data set
			F_SetError(
				ErrorType 	:= E_ErrorType.ERROR,
				ErrorParam 	:= 'No free TU data set',                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
				ErrorMsg 	:= E_ErrorConv.NO_FREE_TU_DATA_SET,
				ErrorData 	:= FunctionData.ErrorData.ErrorDataSet[6]);
				 
								
			// Go to "ERROR"
			StackedUnstackedStates.eLastState := StackedUnstackedStates.eState;
			StackedUnstackedStates.eState := E_StackedUnstackedState.ERROR;
		END_IF
		
		
	(**************************************************************************************
   		END
	***************************************************************************************)
	E_StackedUnstackedState.END:
	
	FunctionInterface.Out.AddOnOrders.DataReady := TRUE;

	IF Inputs.FunctionLink_TransportData.NumberOfPresentTUs = 0 THEN
		
		StackedUnstackedStates.eLastState := StackedUnstackedStates.eState;
		StackedUnstackedStates.eState := E_StackedUnstackedState.IDLE;
	END_IF


	
	(**************************************************************************************
   		ERROR
	***************************************************************************************)
	E_StackedUnstackedState.ERROR:
	
		// Set data ready
		FunctionInterface.Out.AddOnOrders.DataReady := TRUE;
		

		// Wait until error is resetted
		IF FunctionData.ErrorData.Error.ErrorType <> E_ErrorType.ERROR THEN
			// Go back to idle
			StackedUnstackedStates.eLastState := StackedUnstackedStates.eState;
			StackedUnstackedStates.eState := E_StackedUnstackedState.IDLE;
		END_IF

ELSE
	// Programming error
	DebugMsg := CONCAT(	'Programming error - Invalid StackedUnstacked state: ',	INT_TO_STRING(StackedUnstackedStates.eState));
	fbDebugMsg.M_SendErrorMsg(DebugMsg);
END_CASE]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>