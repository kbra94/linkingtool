﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_LightgridAddon" Id="{6d3a410f-ecfd-40a9-97a7-17422e9b349b}" SpecialFunc="None">
    <Declaration><![CDATA[// Addon for a conveyor with Lightgrid for enabling muting
FUNCTION_BLOCK PUBLIC FB_LightgridAddon EXTENDS FB_FunctionConv
(**************************************************************************************
 * 	Application		:	QB-EC1
 *	Revision		:	$Revision: 0.1 $
 *	Revision date	:	$Date: 2021-09-29 15:34:47 $
 *	Last changed by	:	$Author: rot $
 *	URL				:	$URL: 
 *
 *	Purpose			:	Enable the Muting function for a Lightgrid
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				29.09.2021		rot					First version


 **************************************************************************************)

(********************************************************************************************************************************)
VAR_INPUT
	Config					: ST_CFG_LightgridAddon; // Configuration
END_VAR

(********************************************************************************************************************************)
VAR_OUTPUT
	HW_Outputs				: ST_HW_OutputsLightgridAddon;
END_VAR

(********************************************************************************************************************************)
VAR
	TransportState			: ST_TransportControlData;	//Transportation State
	DelayTimerHandover		: TOF; // Timer for Delay of MutingEnable
	DelayTimerTakeover		: TOF; // Timer for Delay of MutingEnable
	InHandover				: BOOL; // Handoverstate is active
	InTakeover				: BOOL; // Takeoverstate is active
END_VAR

(********************************************************************************************************************************)
VAR PERSISTENT
	Settings				: ST_CFG_LightgridAddon;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[

(**************************************************************************************************************
   Input mapping
***************************************************************************************************************)
A_InputMapping();

(**************************************************************************************************************
   Initialize
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   State control
***************************************************************************************************************)
A_StateControl();

(**************************************************************************************************************
   Error handler
***************************************************************************************************************)
A_ErrorHandler();

(**************************************************************************************************************
   Interface handler out
***************************************************************************************************************)
M_ITC_ProcessOut();

(**************************************************************************************************************
   Output mapping
***************************************************************************************************************)
M_OutputMapping();]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{7f6eedd8-edda-4ec5-bd57-87e43e6e7acf}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize function
 **************************************************************************************)
 
// Update Registry
M_UpdateRegistry();
 

// Init ongoing - Wait until init is released (Sequencial startup)
IF NOT FunctionData.Init AND FunctionData.InitRunning THEN
	// When the subsystem is started, 
	// When its released to initialize
	IF FunctionData.OperationState.Info.SystemReady AND
	   SettingsFunction.eFunctionNumber < FunctionData.OperationState.InitRelease.eLimitFunctionNumber THEN
	   
	   IF M_InitFunction(FALSE) THEN
			// Function initialized - and initialization done
			FunctionData.Init := TRUE;
			FunctionData.InitRunning := FALSE;
			
			// Set reset cmd
			FunctionData.ErrorData.Error.ResetError := TRUE;
			
			DebugMsg := CONCAT('Initialization done. Function: ', ConfigFunction.FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
		END_IF
	END_IF
(*
If the function is not initialized,
clean all values and wait until init is released
*)
ELSIF NOT FunctionData.Init THEN
	// Reset internal variables
	FunctionData.OperationState := FunctionBase.PositionStateEmpty;

	// Load function specific configuration to settings
	Settings := Config;
	
	FunctionInterface.Out.FunctionOrders.Enable := TRUE;
	
	// Reset values and load config
	M_PreInit();
	
	// To start init a valid functionnumber and element link is needed
	FunctionData.InitRunning := SettingsFunction.eFunctionNumber > 0 AND
								SettingsFunction.eElementLink > 0;
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InputMapping" Id="{3ce0c98e-35d7-45ab-beb6-a48d8e8e0326}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InputMapping
 * 	FUNCTION	Input mapping
 **************************************************************************************)
  

(**************************************************************************************
   Element states
***************************************************************************************)
// Get status of the corresponding element
M_GetElementStates();


(**************************************************************************************************************
   fill variables
***************************************************************************************************************)
TransportState := F_GetTransportControlData(SettingsFunction.FunctionLink.eFunctionNumber, SettingsFunction.FunctionLink.ZoneNumber);

]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_OutputMapping" Id="{96c9d932-539e-4964-96a2-77bc6f6405f4}">
      <Declaration><![CDATA[METHOD PROTECTED M_OutputMapping

(********************************************************************************************************************************)
VAR
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		M_OutputMapping
 * 	FUNCTION	Output mapping
 **************************************************************************************)
 
 
(**************************************************************************************
   Hardware outputs
***************************************************************************************)
HW_Outputs.MutingEnabled := FALSE;

//If the Muting is configured for Takeover
IF Settings.MuteEnableAtTakeover THEN
	InTakeover := TransportState.eTakeOverState = E_TakeoverState.ACTIVE; // Handoverstate is active
	DelayTimerTakeover(PT := Settings.MutingTimeDelay, IN := InTakeover); //TOF 
	//if the transportstate is in takeover
	IF TransportState.eTakeOverState = E_TakeoverState.ACTIVE OR DelayTimerTakeover.Q THEN
		HW_Outputs.MutingEnabled := TRUE;
	END_IF
END_IF
	

//the muting is configured for Handover
IF Settings.MuteEnableAtHandover THEN
	InHandover := TransportState.eHandOverState = E_HandoverState.ACTIVE; // Handoverstate is active
	DelayTimerHandover(PT := Settings.MutingTimeDelay, IN := InHandover); //TOF 

	//if the transportstate is in handover or in Delay
	IF TransportState.eHandOverState = E_HandoverState.ACTIVE OR DelayTimerHandover.Q THEN
		HW_Outputs.MutingEnabled := TRUE;
	END_IF
END_IF

  
(**************************************************************************************
   AddOn outputs
***************************************************************************************)
FunctionInterface.Out.AddOnOrders.Enable := TRUE;
FunctionInterface.Out.AddOnOrders.EnableHandover := TRUE;
FunctionInterface.Out.AddOnOrders.EnableTakeover := TRUE;

]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>