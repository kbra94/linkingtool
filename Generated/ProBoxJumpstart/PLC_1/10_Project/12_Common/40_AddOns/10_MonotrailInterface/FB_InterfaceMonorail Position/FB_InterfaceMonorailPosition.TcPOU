﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_InterfaceMonorailPosition" Id="{7441508e-b56b-0ca5-165d-f1735fc12951}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK INTERNAL FB_InterfaceMonorailPosition 
(********************************************************************************************************************************)
VAR
	_eElementNumber				: E_ElementNumber				:= 0;			// element number of the position
	_eFunctionNumber			: E_FunctionNumber				:= 0;			// function number of the position
	_ZoneNumber					: INT							:= 0;			// zone number
	//----------------------------------------------------------------------------------------------------------------------------
	_Enable						: BOOL							:= TRUE; 		// General enable
	_EnableTakeover				: BOOL							:= TRUE; 		// Enable takeover
	_EnableHandover				: BOOL							:= TRUE; 		// Enable Handover
	_ReqDriveRun				: BOOL;											// Run linked drive
	//----------------------------------------------------------------------------------------------------------------------------
	_TransportControlData		: REFERENCE TO ST_TransportControlData;			// would prefer interface
	_FunctionInterface			: REFERENCE TO ST_FunctionInterface;			// would prefer interface
	_FunctionData				: REFERENCE TO ST_FunctionData;					// would prefer interface

	//----------------------------------------------------------------------------------------------------------------------------
	_EmptyTransportControl		: ST_TransportControlData;						// Empty structure for Reference
	_EmptyFunctionInterface		: ST_FunctionInterface;							// Empty structure for Reference
	_EmptyFunctionData			: ST_FunctionData;								// Empty structure for Reference

END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[// Body is never called]]></ST>
    </Implementation>
    <Folder Name="Methods" Id="{1b630186-9dd5-0a3e-0fad-044a8a3cdeaf}" />
    <Folder Name="Properties" Id="{e1d3731d-68f6-0e17-27de-b5dfd781c787}" />
    <Property Name="Enable" Id="{b28c7c2b-5d11-02e9-26bc-13a497d98d0c}" FolderPath="Properties\">
      <Declaration><![CDATA[// General enable
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC Enable : BOOL]]></Declaration>
      <Get Name="Get" Id="{29daf102-01e6-0743-08fa-2efa4418d7c5}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Enable:= _Enable;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{2b59262b-cdd0-0c02-0642-6af62c46ab61}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[_Enable:= Enable;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="EnableHandover" Id="{3cc7f911-14f7-0003-0c07-145f9677eb47}" FolderPath="Properties\">
      <Declaration><![CDATA[// Enable Handover
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC EnableHandover : BOOL]]></Declaration>
      <Get Name="Get" Id="{d1d0f4e2-1fb2-0f62-1901-730029ac5eb5}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[EnableHandover:= _EnableHandover;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{e28399b9-9216-0062-0dd6-2c0c7c0b472f}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[_EnableHandover:= EnableHandover;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="EnableTakeover" Id="{861f1f03-b9ab-07ff-1323-8728c48a9b8e}" FolderPath="Properties\">
      <Declaration><![CDATA[// Enable takeover
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC EnableTakeover : BOOL]]></Declaration>
      <Get Name="Get" Id="{5eab8654-195f-0935-2dc0-55bae8cba67f}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[EnableTakeover:= _EnableTakeover;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{7d363cdf-6136-0fbb-21a3-bc02f972b21c}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[_EnableTakeover:= EnableTakeover;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="FunctionNumber" Id="{4f68d84a-497d-0404-35c1-d7c59f1b415b}" FolderPath="Properties\">
      <Declaration><![CDATA[// function number of the position
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC FunctionNumber : E_FunctionNumber]]></Declaration>
      <Get Name="Get" Id="{5f0ab4ff-a17c-06d8-1b3b-8f29aff7bb8b}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[FunctionNumber:= _eFunctionNumber;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Method Name="M_CloseLink" Id="{e5474bf2-673e-0ad5-30e1-46fa1eb343d3}" FolderPath="Methods\">
      <Declaration><![CDATA[// updates links to linked position
METHOD PUBLIC M_CloseLink

(********************************************************************************************************************************)
VAR_INPUT
	MyFunctionNumber	: E_FunctionNumber			:= 0;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF (_eFunctionNumber = 0)
THEN
	RETURN;
END_IF




(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF (__ISVALIDREF(FunctionRegistry[_eFunctionNumber].rFunction) = FALSE)
THEN
	RETURN;
END_IF

FunctionRegistry[_eFunctionNumber].rFunction.M_CloseAddOnLink(eAddOnNumber := MyFunctionNumber);




(********************************************************************************************************************************)
(** done ************************************************************************************************************************)
_eFunctionNumber:= 0;
_ZoneNumber     := 0;

]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_OpenEmptyLink" Id="{a40e3832-5c6b-0ec3-1186-f502cbfdfa9a}" FolderPath="Methods\">
      <Declaration><![CDATA[// HI = updates links to linked position successful
METHOD PUBLIC M_OpenEmptyLink : BOOL

(********************************************************************************************************************************)
VAR_INPUT
	LinkedFunctionNumber	: E_FunctionNumber	:= 0;
	LinkedZoneNumber		: INT				:= 0;
	MyFunctionNumber		: E_FunctionNumber	:= 0;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF(LinkedFunctionNumber <> 0)
OR (LinkedZoneNumber <> 0)
THEN
	M_OpenEmptyLink:= FALSE;
	RETURN;
END_IF


(********************************************************************************************************************************)
(********************************************************************************************************************************)
_TransportControlData	REF= _EmptyTransportControl;
_FunctionInterface		REF= _EmptyFunctionInterface;
_FunctionData			REF= _EmptyFunctionData;

_FunctionData.Init		:= TRUE;

(********************************************************************************************************************************)
(** done ************************************************************************************************************************)
M_OpenEmptyLink:= TRUE;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_OpenLink" Id="{76b667de-301e-0473-0d42-59e3826140b7}" FolderPath="Methods\">
      <Declaration><![CDATA[// HI = updates links to linked position successful
METHOD PUBLIC M_OpenLink : BOOL

(********************************************************************************************************************************)
VAR_INPUT
	LinkedElementNumber		: E_ElementNumber	:= 0;
	LinkedFunctionNumber	: E_FunctionNumber	:= 0;
	LinkedZoneNumber		: INT				:= 0;
	MyFunctionNumber		: E_FunctionNumber	:= 0;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF(LinkedFunctionNumber = 0)
THEN
	M_OpenLink:= FALSE;
	RETURN;
END_IF

_eElementNumber	:= LinkedElementNumber;
_eFunctionNumber:= LinkedFunctionNumber;
_ZoneNumber     := LinkedZoneNumber;




(********************************************************************************************************************************)
(** references valid? ***********************************************************************************************************)
IF NOT __ISVALIDREF(FunctionRegistry[_eFunctionNumber].rFunction)
THEN
	M_OpenLink:= FALSE;
	RETURN;

ELSIF NOT __ISVALIDREF(FunctionRegistry[_eFunctionNumber].Zone[_ZoneNumber].refZoneData)
THEN
	M_OpenLink:= FALSE;
	RETURN;
	
ELSIF NOT __ISVALIDREF(FunctionRegistry[_eFunctionNumber].rFunctionData)
THEN
	M_OpenLink:= FALSE;
	RETURN;
END_IF

IF NOT FunctionRegistry[_eFunctionNumber].Zone[_ZoneNumber].refZoneData.Init
THEN
	M_OpenLink:= FALSE;
	RETURN;
END_IF

IF NOT __ISVALIDREF(ElementRegistry[_eElementNumber].rElement)
THEN
	M_OpenLink:= FALSE;
	RETURN;
END_IF

(********************************************************************************************************************************)
(********************************************************************************************************************************)
_TransportControlData	REF= FunctionRegistry[_eFunctionNumber].Zone[_ZoneNumber].refZoneData.TransportControlData;
_FunctionInterface		REF= FunctionRegistry[_eFunctionNumber].rFunction.FunctionInterface;
_FunctionData			REF= FunctionRegistry[_eFunctionNumber].rFunctionData;


FunctionRegistry[_eFunctionNumber].rFunction.M_CreateAddOnLink(	eAddOnNumber	:= MyFunctionNumber,	// myself as controller
															  	ZoneNumber  	:= _ZoneNumber );		// destination zone to be controlled
															  
ElementRegistry[_eElementNumber].rElement.M_CreateElementLink(meFunctionNumber := MyFunctionNumber);															  

(********************************************************************************************************************************)
(** done ************************************************************************************************************************)
M_OpenLink:= TRUE;
]]></ST>
      </Implementation>
    </Method>
    <Property Name="Ready" Id="{716d031c-10c4-068c-1281-d26170aa796b}" FolderPath="Properties\">
      <Declaration><![CDATA[// function error
{attribute 'monitoring' := 'call'}
PROPERTY Ready : BOOL]]></Declaration>
      <Get Name="Get" Id="{61ffd258-ec76-0e1f-0467-5d327d4c3953}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[
IF NOT __ISVALIDREF(_FunctionData)
THEN
	RETURN;
END_IF


Ready	:= (_FunctionData.Init = TRUE)
			AND (_FunctionData.ErrorData.ZoneErrorPending = FALSE)
			AND (_FunctionData.ErrorData.Error.ErrorType <> E_ErrorType.ERROR);]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="ReqDriveRun" Id="{f2fac5a1-f384-0d08-19df-a73d59083798}" FolderPath="Properties\">
      <Declaration><![CDATA[// Drive Run Request
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC ReqDriveRun : BOOL]]></Declaration>
      <Get Name="Get" Id="{75bc2db4-6e00-065c-2e2c-0fab37ab4459}">
        <Declaration><![CDATA[
VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[ReqDriveRun := _ReqDriveRun;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{57536ec9-78de-0899-0718-602d4e3b786b}">
        <Declaration><![CDATA[
VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[_ReqDriveRun := ReqDriveRun;
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="TransportControlData" Id="{e3be0c1e-b0fb-0ece-03f6-f172d64e00a0}" FolderPath="Properties\">
      <Declaration><![CDATA[// transport control data
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC TransportControlData : REFERENCE TO ST_TransportControlData]]></Declaration>
      <Get Name="Get" Id="{b70b2a14-0f25-0320-140d-e12ff820423f}">
        <Declaration><![CDATA[VAR	
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[TransportControlData REF= _TransportControlData;



]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="ZoneNumber" Id="{611cb79f-3872-0f82-0d98-392fd9107fbc}" FolderPath="Properties\">
      <Declaration><![CDATA[// function number of the position
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC ZoneNumber : INT]]></Declaration>
      <Get Name="Get" Id="{0c953967-c19a-0ffe-2a64-32999c9737bb}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[ZoneNumber:= _ZoneNumber;]]></ST>
        </Implementation>
      </Get>
    </Property>
  </POU>
</TcPlcObject>