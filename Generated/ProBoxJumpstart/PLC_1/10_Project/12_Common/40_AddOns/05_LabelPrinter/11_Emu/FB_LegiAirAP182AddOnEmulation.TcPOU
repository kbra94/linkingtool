﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_LegiAirAP182AddOnEmulation" Id="{ca4e5523-69e1-4580-ba79-f0e25b1978e8}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_LegiAirAP182AddOnEmulation EXTENDS FB_FunctionConvEmulation
(**************************************************************************************
 * 	Application		:	QuickBox2.0
 *	Revision		:	$Revision: 352308 $
 *	Revision date	:	$Date: 2021-11-29 $
 *	Last changed by	:	$Author: jag $
 *	URL				:	
 *	Purpose			:	Function label printer Emulation
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *	0.2				16.11.2021		jag					change inputs and timer
 *
 **************************************************************************************)
VAR_INPUT
	Config					: ST_CFG_LegiAirAP182Emulation;
END_VAR
 
VAR
	pFunction				: POINTER TO FB_LegiAirAP182AddOn; // Address to function
	rFunction				: REFERENCE TO FB_LegiAirAP182AddOn; // Reference to function

	tLabelerPrint			:TON;   //wait until label is advanced
	tLabelerDonate			:TON;	//wait until label is applied
	tLabelerDataReady		:TON;	//wait until data was received
	tLabelerNOTDataReady	:TON;	//wait until data was received
	tLabelerWaitForPrinter	:TON;	//wait until Print Start signal
	tLabelerLabelOnPad		:Ton;	//simulate the time of the label on the pad
		
	myStates				:INT;		//states for emulation
	

	bDataReady 				:BOOL;
	bReady		            :BOOL;
	bSync_Busy 	            :BOOL;
	bHome 		            :BOOL;
	bLabelOnPad 	        :BOOL;
	bPrint		            :BOOL;
	bWarning		        :BOOL;
	bCarriageHome           :BOOL;
	
	
	bMode					:BOOL := TRUE;
	myMode					:E_LabelPrinterSimulationsMode;
END_VAR

VAR PERSISTENT
	Settings				: ST_CFG_LegiAirAP182Emulation;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();


(**************************************************************************************************************
   Emulation
***************************************************************************************************************)

// If emulation function is initialized
IF FunctionData.Init THEN
	
	// Update data of real object
	M_UpdateRealObjectData();
	
	//Overwrite inputs
	A_OverWriteInputs();
	
END_IF 
]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{1e7f51cb-d44f-4067-a12e-cced1992e4e7}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated dummy function
 **************************************************************************************)

// If dummy function is not initialized
IF NOT FunctionData.Init Then
	// Check that linked dummy function number is valid
	IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
	   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN   
		
		// Check if the real module is initialized
		IF F_IsFunctionInitialized(eFunctionNumber := eFunctionNumber) THEN
			// Module is initialized
			FunctionData.Init:= TRUE;
			
			Settings := Config;
			
			// Send debug message
			DebugMsg := CONCAT('Initialization done. Element Function: ', FunctionRegistry[eFunctionNumber].FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
			
			// Initialize real module
			F_InitializeFunction(eFunctionNumber := eFunctionNumber);
		END_IF
	END_IF
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs" Id="{9465c9d2-603c-4129-8174-31870e8085a4}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OverWriteInputs
 * 	FUNCTION	Overwrite inputs for emulation
 **************************************************************************************)

 CASE myMode OF

 // Test Manuel setting
 E_LabelPrinterSimulationsMode.Manual:		
 
	rFunction.HW_Inputs.DataReady 		:= bDataReady; 		 
	rFunction.HW_Inputs.Ready 			:= bReady;  
	rFunction.HW_Inputs.Sync_Busy 		:= bSync_Busy; 	  
	rFunction.HW_Inputs.Home 			:= bHome; 		         
	rFunction.HW_Inputs.LabelOnPad 		:= bLabelOnPad; 	 
	rFunction.HW_Inputs.Print			:= bPrint;		         
	rFunction.HW_Inputs.Warning			:= bWarning;		     
	rFunction.HW_Inputs.CarriageHome	:= bCarriageHome;
		
			
 
 	IF bMode THEN
		myMode := E_LabelPrinterSimulationsMode.Auto;
		
	ELSE
		myMode := E_LabelPrinterSimulationsMode.Manual;
	END_IF
 
 // Test automaticly with timers
 E_LabelPrinterSimulationsMode.Auto:
 
	IF bMode THEN
		myMode := E_LabelPrinterSimulationsMode.Auto;
			
	ELSE
		myMode := E_LabelPrinterSimulationsMode.Manual;
	END_IF
	
	CASE myStates OF
	 
	//initialization of timer and so on...
		0://init
			tLabelerLabelOnPad(IN := FALSE, PT:=T#2S);		//simulate the time of the label on the pad
			tLabelerWaitForPrinter(IN := FALSE, PT:=T#2S);	//simulate the time of the labelp wait for printing
			tLabelerPrint(IN := FALSE, PT:=T#3S);			//simulate the time of the labelprinting
			tLabelerDonate(IN := FALSE, PT:=T#1S); 			//simulate the time of the label donation, will be signalized by busy signal
			tLabelerDataReady(IN := FALSE, PT:=T#2S); 		//simulate the time of data reveicement	
			tLabelerNOTDataReady(IN := FALSE, PT:=T#2S); 	//simulate the time of data reveicement											
			rFunction.HW_Inputs.DataReady 		:= FALSE;
			rFunction.HW_Inputs.Ready 			:= TRUE; 
			rFunction.HW_Inputs.Sync_Busy 		:= FALSE;
			rFunction.HW_Inputs.Home 			:= TRUE;
			rFunction.HW_Inputs.LabelOnPad 		:= FALSE;
			rFunction.HW_Inputs.Print			:= FALSE;
			rFunction.HW_Inputs.Warning			:= FALSE;
			rFunction.HW_Inputs.CarriageHome	:= TRUE;

			IF rFunction.TransportData.Occupied AND rFunction.TransportData.DataPresent  THEN
				myStates :=1;
							
			END_IF
				
	
		1://Waiting for occupied zone
	
			tLabelerDataReady(IN := TRUE);
			
			IF tLabelerDataReady.Q THEN
				rFunction.HW_Inputs.DataReady := TRUE;
				myStates :=2;		
			END_IF
			
		2://Wait For Printer start signal
			
			tLabelerWaitForPrinter(IN := TRUE);
			
			IF tLabelerWaitForPrinter.Q THEN
				rFunction.HW_Inputs.Print 		:= TRUE;
				rFunction.HW_Inputs.LabelOnPad 	:= TRUE;
				rFunction.HW_Inputs.Sync_Busy 	:= TRUE;
				
				myStates :=3;
			END_IF
			
		3://Print label
			
			tLabelerPrint(IN := TRUE);
			
			IF tLabelerPrint.Q THEN
				
				rFunction.HW_Inputs.Home 		:= FALSE;
				rFunction.HW_Inputs.CarriageHome:= FALSE;
								
				myStates :=4;
			END_IF
					
		4://Donate label
			
			tLabelerDonate(IN:=TRUE);
				
			IF tLabelerDonate.Q THEN
				rFunction.HW_Inputs.LabelOnPad 	:= FALSE;
				
				myStates :=5;
			END_IF
		
		5://Donate label
			
			tLabelerLabelOnPad(IN:=TRUE);
				
			IF tLabelerLabelOnPad.Q THEN
				rFunction.HW_Inputs.Sync_Busy 		:= FALSE;
				rFunction.HW_Inputs.LabelOnPad 		:= FALSE;
				rFunction.HW_Inputs.Print			:= FALSE;
				rFunction.HW_Inputs.CarriageHome	:= TRUE;
				rFunction.HW_Inputs.Home 			:= TRUE;
				
				myStates :=6;
			END_IF
	
		6://Waiting for occupied zone
	
			tLabelerNOTDataReady(IN:=TRUE);
				
			IF tLabelerNOTDataReady.Q THEN
				rFunction.HW_Inputs.DataReady := FALSE;
				
				myStates :=7;		
			END_IF
			
		7:// Outfeed
		
			IF NOT rFunction.TransportData.Occupied THEN
							
				myStates :=0;
			END_IF
	
		 
	 END_CASE
 
 END_CASE]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{d22a4c57-31dc-4a9b-8248-811f2e439b25}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid function number
IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN

	// Is reference valid  
	IF __ISVALIDREF(FunctionRegistry[eFunctionNumber].rFunction) THEN
		// Build address to reference
		pFunction := ADR(FunctionRegistry[eFunctionNumber].rFunction);

		// If address is possible
		IF pFunction <> 0 THEN
			// Build reference to memory
			rFunction REF= pFunction^;
		ELSE
			// Reinitialize module
			FunctionData.Init:= FALSE;
		END_IF
	ELSE
		// Reinitialize module
		FunctionData.Init:= FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
  </POU>
</TcPlcObject>