﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_RcManualInfeedAddOn" Id="{1558d704-1d92-47ff-9d83-afc7ea90a103}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_RcManualInfeedAddOn EXTENDS FB_FunctionConv
	IMPLEMENTS I_StateMachineTextCallback
VAR_INPUT
	Config	: ST_RcManualInfeed_CFG;
	HW_Inputs : ST_RcManualInfeed_HW_Inputs;
END_VAR
VAR_OUTPUT
	HW_Outputs : ST_RcManualInfeed_HW_Outputs;
END_VAR
VAR
	_Inputs								: ST_RcManualInfeed_HW_Inputs;
	_InfeedEnum							: E_RcManualInfeed_States;
	_InfeedState						: FB_StateMachine(mName:= 'Infeed state',  mStateTextCallback:= THIS^);
	_TransContrData						: REFERENCE TO ST_TransportControlData;
	_TimeRemaining						: TIME := T#0S;
	_TuCounter							: ULINT := 0; // Counter of reveiced TUs since last SPS start
	
	_ReqDriveRun						: BOOL;
	_fb_ProBoxToFunctionOrders 			: FB_ProBoxToFunctionOrders;
END_VAR

VAR PERSISTENT
	_Settings				: ST_RcManualInfeed_CFG; // Settings - activated configuration 
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Input mapping
***************************************************************************************************************)
M_InputMapping();


(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
M_Init();


(**************************************************************************************************************
   State control
***************************************************************************************************************)
A_StateControl();


(**************************************************************************************************************
   Error handler
***************************************************************************************************************)
A_ErrorHandler();


(**************************************************************************************************************
   Process AddOns
***************************************************************************************************************)
M_ProcessAddOns();


(**************************************************************************************************************
   Manual control
***************************************************************************************************************)
M_ManualControl();


(**************************************************************************************************************
   run rc manual infeed
***************************************************************************************************************)
M_RunStates();	// run statemachine


(**************************************************************************************************************
   Interface handler out
***************************************************************************************************************)
M_ITC_ProcessOut();


(**************************************************************************************************************
   Output mapping
***************************************************************************************************************)
M_OutputMapping();	


(**************************************************************************************************************
***************************************************************************************************************)
]]></ST>
    </Implementation>
    <Method Name="M_GetStateText" Id="{52306102-c022-42f8-b179-e5f906a1728c}">
      <Declaration><![CDATA[// forwards state text to FB_StateMachine
METHOD PUBLIC M_GetStateText : A_StateText

(********************************************************************************************************************************)
VAR_INPUT
	StateMachine    : POINTER TO FB_StateMachine;		// requesting state machine instance
	StateNbr		: UINT;		// state number which text is requested
END_VAR

(********************************************************************************************************************************)
VAR
	Enum			: E_RcManualInfeed_States		:= E_RcManualInfeed_States.Init;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF (StateMachine = ADR(_InfeedState))
THEN
	Enum := StateNbr;
	M_GetStateText:= TO_STRING(Enum);
	RETURN;
END_IF


(********************************************************************************************************************************)
(********************************************************************************************************************************)
M_GetStateText:= 'unknown state machine';]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Init" Id="{aa538b17-1e2b-427f-9a80-140ac0246272}">
      <Declaration><![CDATA[METHOD M_Init
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize function
 **************************************************************************************)
 
// Update Registry
M_UpdateRegistry();
 

// Init ongoing - Wait until init is released (Sequencial startup)
IF NOT FunctionData.Init AND FunctionData.InitRunning THEN
	// When the subsystem is started, 
	// When its released to initialize
	IF FunctionData.OperationState.Info.SystemReady AND
	   SettingsFunction.eFunctionNumber < FunctionData.OperationState.InitRelease.eLimitFunctionNumber THEN
	   
	   IF M_InitFunction(FALSE) THEN
			// Check reference is valid
			IF NOT __ISVALIDREF(FunctionRegistry[SettingsFunction.FunctionLink.eFunctionNumber].Zone[SettingsFunction.FunctionLink.ZoneNumber].refZoneData) THEN
				RETURN;
   			END_IF
			
	    	// Linked function transport control data
   			_TransContrData REF= FunctionRegistry[SettingsFunction.FunctionLink.eFunctionNumber].Zone[SettingsFunction.FunctionLink.ZoneNumber].refZoneData.TransportControlData;
			
	   		// Function initialized - and initialization done
			FunctionData.Init := TRUE;
			FunctionData.InitRunning := FALSE;
			
			// Set reset cmd
			FunctionData.ErrorData.Error.ResetError := TRUE;
			
			DebugMsg := CONCAT('Initialization done. Function: ', ConfigFunction.FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
		END_IF
	END_IF
(*
If the function is not initialized,
clean all values and wait until init is released
*)
ELSIF NOT FunctionData.Init THEN
	// Reset internal variables
	FunctionData.OperationState := FunctionBase.PositionStateEmpty;

	// Load function specific configuration to settings
	_Settings := Config;

	_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.Init,
									mReason := 'function initialize');
	
	// Reset values and load config
	M_PreInit();
	
	// To start init a valid functionnumber and element link is needed
	FunctionData.InitRunning := SettingsFunction.eFunctionNumber > 0 AND
								SettingsFunction.eElementLink > 0;
END_IF
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_InputMapping" Id="{07870c6c-f12c-4ca0-b478-e8b85f731e1e}">
      <Declaration><![CDATA[METHOD M_InputMapping
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(**************************************************************************************
   Hardware Inputs
***************************************************************************************)
// Adapt input logic state high/low
_Inputs.TuRecognition	:= HW_Inputs.TuRecognition XOR _Settings.InvertTuRecognition;
 

(**************************************************************************************
   Element states
***************************************************************************************)
// Get status of the corresponding element
M_GetElementStates(); 



(**************************************************************************************
   AddOn states
***************************************************************************************)
// Get status of all corresponding AddOn functions and store it in FunctionInterface.In.AddOnOrder
M_GetAddOnOrder();



(**************************************************************************************
***************************************************************************************)
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_ManualControl" Id="{c23121ec-4d60-4bf0-a92a-d1840657d98c}">
      <Declaration><![CDATA[METHOD M_ManualControl
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[ 
(**************************************************************************************
   Reset manual commands
***************************************************************************************)

// If function is not in manual mode
IF FunctionData.OperationState.eMode <> E_PositionMode.MANUAL_MODE THEN
	// Reset manual commands
//	ITC.ManualMotion. := EmptyManualCommands;
END_IF

(**************************************************************************************
   Enable available axis, buttons, features
***************************************************************************************)
// Example:
//	ITC.ManualMotion.Axis[1].Forward.Enable := TRUE;


]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_OutputMapping" Id="{6e890dab-da67-4328-8348-303bcb1f4457}">
      <Declaration><![CDATA[METHOD M_OutputMapping
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(**************************************************************************************
   Hardware outputs
***************************************************************************************)
// HW_Outputs.LedReady is set in M_RunStates



(**************************************************************************************
   Order outputs (Commands to corresponding element)
***************************************************************************************)
_fb_ProBoxToFunctionOrders(
	CreepSpeed_12	:= FALSE,
	LowSpeed_12		:= FALSE,
	Forward_12		:= _ReqDriveRun, 
	Reverse_12		:= FALSE,
	Enable_12		:= TRUE,	 
	CreepSpeed_34	:= FALSE, 
	LowSpeed_34		:= FALSE, 
	Forward_34		:= FALSE, 
	Reverse_34		:= FALSE,
	Enable_34 		:= FALSE);



// Initialize
FunctionInterface.Out.FunctionOrders.Enable     := TRUE;
FunctionInterface.Out.FunctionOrders.ReqDriveRun:= FALSE;
FunctionInterface.Out.FunctionOrders.ReqSpeed   := 0;

FunctionInterface.Out.FunctionOrders.Enable 			:= _fb_ProBoxToFunctionOrders.FunctionOrders.Enable;
FunctionInterface.Out.FunctionOrders.ReqDriveRun 		:= _fb_ProBoxToFunctionOrders.FunctionOrders.ReqDriveRun;
FunctionInterface.Out.FunctionOrders.ReqSpeed 			:= _fb_ProBoxToFunctionOrders.FunctionOrders.ReqSpeed;
FunctionInterface.Out.FunctionOrders.ReqDriveDirection 	:= _fb_ProBoxToFunctionOrders.FunctionOrders.ReqDriveDirection;

IF _InfeedState.P_State = E_RcManualInfeed_States.FeedInDebounce OR
_InfeedState.P_State = E_RcManualInfeed_States.FeedForward OR  
_InfeedState.P_State = E_RcManualInfeed_States.ForwardDelay THEN
	InhibitStopping := TRUE;
ELSE
	InhibitStopping := FALSE;
END_IF

(**************************************************************************************
   AddOn outputs
***************************************************************************************)
// This function provide no AddOn functionality
;]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_RunStates" Id="{4fbe026a-649e-44da-b81e-3736ae3dabc3}">
      <Declaration><![CDATA[METHOD M_RunStates
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
_InfeedEnum := _InfeedState.P_State;
CASE _InfeedEnum OF

(********************************************************************************************************************************)
(** initialisation **************************************************************************************************************)
E_RcManualInfeed_States.Init:
	//-- ENTRY ------------------------------------------------------------------------------------------------------------------
	_InfeedState.P_SisNbr:= F_AddDividerToSisString(ConfigFunction.FunctionName);

	//-- lock buttons ----------------------------------------------------------------------
	

	//-- set hardware outputs --------------------------------------------------------------
	HW_Outputs.LedReady := FALSE;

	//-- set transport movement ------------------------------------------------------------
	FunctionInterface.Out.AddOnOrders.Enable        	:= TRUE;
	FunctionInterface.Out.AddOnOrders.EnableTakeover	:= FALSE;
	FunctionInterface.Out.AddOnOrders.EnableHandover	:= FALSE;
	FunctionInterface.Out.AddOnOrders.DataReady     	:= TRUE;
	_ReqDriveRun										:= FALSE;

	//-- ACTION -----------------------------------------------------------------------------------------------------------------

	//-- TRANSITION -------------------------------------------------------------------------------------------------------------
	IF FunctionData.Init
	AND (FunctionInterface.In.Element.OperationState.eState = E_PositionState.STARTED)
	AND (FunctionData.ErrorData.Error.ErrorType <> E_ErrorType.ERROR)
	THEN
		//-- linked function is occupied
		IF _TransContrData.Occupied
		THEN
			_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.Occupied,
									mReason := 'function started and Zone is occupied');

		//-- linked function is not occupied
		ELSE
			_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.IdleFeedIn,
									mReason := 'function started and zone is not occupied');
		END_IF
	END_IF

(********************************************************************************************************************************)
(** Wait for feed in ************************************************************************************************************)
E_RcManualInfeed_States.IdleFeedIn:
	//-- ENTRY ------------------------------------------------------------------------------------------------------------------
	IF _InfeedState.P_Entering
	THEN
		FunctionInterface.Out.AddOnOrders.Enable        	:= TRUE;
		FunctionInterface.Out.AddOnOrders.EnableTakeover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.EnableHandover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.DataReady     	:= TRUE;
		_ReqDriveRun										:= FALSE;
		
		// Rollcontainer handover allowed
		HW_Outputs.LedReady := TRUE;
	END_IF
	
	//-- ACTION -----------------------------------------------------------------------------------------------------------------
	
	
	//-- TRANSITION -------------------------------------------------------------------------------------------------------------
	// Function was stopped
	IF (FunctionInterface.In.Element.OperationState.eState <> E_PositionState.STARTED)
	THEN
		_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.INIT,
								mReason := 'Function was stopped');
	//-- linked function is occupied
	ELSIF _TransContrData.Occupied
	THEN
		_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.Occupied,
								mReason := 'Zone is occupied');
								
	//-- TuRecognition sensor is occupied						
	ELSIF _Inputs.TuRecognition
	THEN
		_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.FeedInDebounce,
								mReason := 'function started and Zone is occupied');
	END_IF
	
	
(********************************************************************************************************************************)
(** Feed in sensor debounce *****************************************************************************************************)
E_RcManualInfeed_States.FeedInDebounce:
	//-- ENTRY ------------------------------------------------------------------------------------------------------------------
	IF _InfeedState.P_Entering
	THEN
		FunctionInterface.Out.AddOnOrders.Enable        	:= TRUE;
		FunctionInterface.Out.AddOnOrders.EnableTakeover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.EnableHandover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.DataReady     	:= TRUE;
		_ReqDriveRun										:= FALSE;
		
		// Rollcontainer handover allowed
		HW_Outputs.LedReady := TRUE;
	END_IF
	
	//-- ACTION -----------------------------------------------------------------------------------------------------------------
	_TimeRemaining := _Settings.RecognisionDebounceTime - _InfeedState.P_TimeInState;
	
	//-- TRANSITION -------------------------------------------------------------------------------------------------------------
	//-- linked function is occupied
	IF _TransContrData.Occupied
	THEN
		_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.Occupied,
								mReason := 'Zone is occupied');
								
	//-- TuRecognition sensor is not occupied
	ELSIF NOT _Inputs.TuRecognition
	THEN
		_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.FeedInDebounce,
								mReason := 'No TU recognized anymore');
								
	//-- Debounce time expired
	ELSIF _InfeedState.P_TimeInState >= _Settings.RecognisionDebounceTime
	THEN
		_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.FeedForward,
								mReason := 'Debounce time expired');
	END_IF
    

		
		
(********************************************************************************************************************************)
(** Move TU forward until sensor gets free **************************************************************************************)
E_RcManualInfeed_States.FeedForward:
	//-- ENTRY ------------------------------------------------------------------------------------------------------------------
	IF _InfeedState.P_Entering
	THEN
		FunctionInterface.Out.AddOnOrders.Enable        	:= TRUE;
		FunctionInterface.Out.AddOnOrders.EnableTakeover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.EnableHandover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.DataReady     	:= TRUE;
		_ReqDriveRun										:= TRUE;
		
		// Rollcontainer handover allowed
		HW_Outputs.LedReady := FALSE;
	END_IF
	
	//-- ACTION -----------------------------------------------------------------------------------------------------------------
	
	//-- TRANSITION -------------------------------------------------------------------------------------------------------------
		//-- linked function is occupied
		IF _TransContrData.SensorOccupied
		THEN
			_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.Occupied,
									mReason := 'Zone is occupied');
									
		//-- TU left TuRecognition sensor
		ELSIF NOT _Inputs.TuRecognition
		THEN
			_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.ForwardDelay,
									mReason := 'TU left recognition sensor');

		END_IF



(********************************************************************************************************************************)
(** Move delay after sensore gets free ******************************************************************************************)
E_RcManualInfeed_States.ForwardDelay:
	//-- ENTRY ------------------------------------------------------------------------------------------------------------------
	IF _InfeedState.P_Entering
	THEN
		FunctionInterface.Out.AddOnOrders.Enable        	:= TRUE;
		FunctionInterface.Out.AddOnOrders.EnableTakeover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.EnableHandover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.DataReady     	:= TRUE;
		_ReqDriveRun										:= TRUE;
		
		// Rollcontainer handover allowed
		HW_Outputs.LedReady := FALSE;
	END_IF
	
	//-- ACTION -----------------------------------------------------------------------------------------------------------------
	_TimeRemaining := _Settings.ForwardDelayTime - _InfeedState.P_TimeInState;
	
	
	//-- TRANSITION -------------------------------------------------------------------------------------------------------------	
		//-- linked function is occupied
		IF _TransContrData.Occupied
		THEN
			_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.Occupied,
									mReason := 'Zone is occupied');
									
		//-- Forward delay time expired
		ELSIF _InfeedState.P_TimeInState >= _Settings.ForwardDelayTime
		THEN
			_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.IdleFeedIn,
									mReason := 'Forward delay time expired');

		END_IF


	
	
(********************************************************************************************************************************)
(** Zone is occupied and waits for handover *************************************************************************************)	
E_RcManualInfeed_States.Occupied:
	//-- ENTRY ------------------------------------------------------------------------------------------------------------------
	IF _InfeedState.P_Entering
	THEN
		FunctionInterface.Out.AddOnOrders.Enable        	:= TRUE;
		FunctionInterface.Out.AddOnOrders.EnableTakeover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.EnableHandover	:= TRUE;
		FunctionInterface.Out.AddOnOrders.DataReady     	:= FALSE;
		_ReqDriveRun										:= FALSE;
		
		// Rollcontainer handover allowed
		HW_Outputs.LedReady := FALSE;
	END_IF
	
	//-- ACTION -----------------------------------------------------------------------------------------------------------------
	
	//-- TRANSITION -------------------------------------------------------------------------------------------------------------
	// Function was stopped
	IF (FunctionInterface.In.Element.OperationState.eState <> E_PositionState.STARTED)
	THEN
		_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.INIT,
								mReason := 'Function was stopped');

	//-- linked function is not occupied
	ELSIF NOT _TransContrData.Occupied
	THEN
		_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.IdleFeedIn,
								mReason := 'Zone is free');
								
	//-- Transport Handover started
	ELSIF _TransContrData.eHandOverState = E_HandOverState.ACTIVE
	THEN
		_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.HandoverActive,
								mReason := 'Transport started');
    
	END_IF
	
	
(********************************************************************************************************************************)
(** Transport Control Handover is active ****************************************************************************************)
E_RcManualInfeed_States.HandoverActive:
	//-- ENTRY ------------------------------------------------------------------------------------------------------------------
	IF _InfeedState.P_Entering
	THEN
		FunctionInterface.Out.AddOnOrders.Enable        	:= TRUE;
		FunctionInterface.Out.AddOnOrders.EnableTakeover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.EnableHandover	:= FALSE;
		FunctionInterface.Out.AddOnOrders.DataReady     	:= FALSE;
		_ReqDriveRun										:= FALSE;
		
		// Rollcontainer handover allowed
		HW_Outputs.LedReady := FALSE;
	END_IF
	
	//-- ACTION -----------------------------------------------------------------------------------------------------------------
	
	//-- TRANSITION -------------------------------------------------------------------------------------------------------------
		//-- Transport Handover finished by TransportControl
		IF _TransContrData.eHandOverState <> E_HandOverState.ACTIVE
		THEN
			IF _TransContrData.Occupied
			THEN
				_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.Occupied,
										mReason := 'Zone is occupied again');
			ELSE
				_InfeedState.M_SetState(mState:= E_RcManualInfeed_States.IdleFeedIn,
										mReason := 'Zone is free');
			END_IF
		END_IF
	
	//-- Exiting ----------------------------------------------------------------------------------------------------------------
	IF _InfeedState.P_Exiting THEN
		_TuCounter := _TuCounter + 1;
	END_IF
	
	
(********************************************************************************************************************************)
(** invalid *********************************************************************************************************************)
ELSE
	_InfeedState.M_SetState1(mMsgType:= E_DebugMsgType.ERROR,
							mState := E_RcManualInfeed_States.Init,
							mReason:= 'invalid internal state');

							
							
END_CASE]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>