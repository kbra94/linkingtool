﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_FiredoorConveyor" Id="{8f68b7bc-77f9-4cb2-ab91-80db069c0f2c}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_FiredoorConveyor EXTENDS FB_FunctionConv IMPLEMENTS I_FiredoorConveyor
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 351950 $
 *	Revision date	:	$Date: 2015-07-03 14:25:19 +0200 (Fr, 03 Jul 2015) $
 *	Last changed by	:	$Author: b7bolm $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/tags/R0.5.8/01_Software/BoxControlFramework/BoxControlFramework/JumpStart/99_Templates/Template_FunctionBlock.TcPOU $
 *
 *	Purpose			:	Firedoor Function for conveyor
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				11.03.2021		jku					First Version
 *  
 *
 **************************************************************************************)
 
 (********************************************************************************************************************************)
VAR_STAT CONSTANT
    ERRORSET_AREANOTFREE  : BYTE                             := 5;           // FunctionData.ErrorData.ErrorDataSet[5] );	//FIRE DOOR AREA NOT FREE
END_VAR

(********************************************************************************************************************************)
VAR_INPUT
    SensorAreaFree        : BOOL;                                            // HW_Input, HI = door area free of TUs
    FD_ConvSTI            : I_FiredoorConveyorSTI;                              // if connected to master then = 0, if connected to FD_ConveyorSTI then <> 0
END_VAR

(********************************************************************************************************************************)
VAR
   InputAreaIsFree       : BOOL;
   TransportData         : ST_TransportControlData;  
   WatchdogAreaFree      : TON;
   FD_ConveyorStates     : E_FD_ConveyorStates;
   OpenRequest           : BOOL;
   EnableTransport       : BOOL;
   Idx                   : INT;
END_VAR
VAR CONSTANT
   AREAFREE_TIMEOUT      : TIME                             := T#15S;      // wait time after close command for area to become free
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Input mapping
***************************************************************************************************************)
A_InputMapping();

(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
  Call Interface
***************************************************************************************************************)
A_ExternalSTI();

(**************************************************************************************************************
   State control
***************************************************************************************************************)
A_StateControl();

(**************************************************************************************************************
  Firedoor Conveyor 
***************************************************************************************************************)
M_FireDoorConveyor();

(**************************************************************************************************************
   Error handler
***************************************************************************************************************)
A_ErrorHandler();

(**************************************************************************************************************
   Process AddOns
***************************************************************************************************************)
M_ProcessAddOns();

(**************************************************************************************************************
   Message handler zone
***************************************************************************************************************)
M_MsgHandlerZone(mZoneNumber := 1);

(**************************************************************************************************************
   ITC Zone output
***************************************************************************************************************)
M_ITC_ProcessOutZone(mZoneNumber := 1);

(**************************************************************************************************************
   Interface handler out
***************************************************************************************************************)
A_InterfaceHandlerOut();

(**************************************************************************************************************
   Output mapping
***************************************************************************************************************)
M_OutputMapping();
]]></ST>
    </Implementation>
    <Action Name="A_ExternalSTI" Id="{a15f4f88-0715-49f4-bb8f-e6807dd20005}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_ExternalSTI
 * 	FUNCTION	External Subsystem transport interface
 **************************************************************************************)


(**************************************************************************************
   External Subsystem transport interfaces
***************************************************************************************)

F_ResetError(Reset := TRUE, ErrorData := FunctionData.ErrorData.ErrorDataSet[7]);

//Call interface if configured (firedoor conveyor STI)
IF FD_ConvSTI <> 0 THEN
    FD_ConvSTI.M_Diagnose(ErrorDataSet := FunctionData.ErrorData.ErrorDataSet[7]);
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_Init" Id="{018c0357-896c-4c80-ad79-3fd85d3aab5f}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize function
 **************************************************************************************)
 
// Update Registry
M_UpdateRegistry();
 
// Init ongoing - Wait until init is released (Sequencial startup)
IF NOT FunctionData.Init AND FunctionData.InitRunning THEN
	// When the subsystem is started, 
	// When its released to initialize
	IF FunctionData.OperationState.Info.SystemReady AND
	   SettingsFunction.eFunctionNumber < FunctionData.OperationState.InitRelease.eLimitFunctionNumber THEN
	   
	   IF M_InitFunction(FALSE) THEN
			// Function initialized - and initialization done
			FunctionData.Init := TRUE;
			FunctionData.InitRunning := FALSE;
			
			// Set reset cmd
			FunctionData.ErrorData.Error.ResetError := TRUE;
			
			DebugMsg := CONCAT('Initialization done. Function: ', ConfigFunction.FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
		END_IF
	END_IF
(*
If the function is not initialized,
clean all values and wait until init is released
*)
ELSIF NOT FunctionData.Init THEN
	// Reset internal variables
	FunctionData.OperationState := FunctionBase.PositionStateEmpty;
	
	// Load function specific configuration to settings
	//Settings := Config; 
	
	// Reset values and load config
	M_PreInit();
	
	// To start init a valid functionnumber and element link is needed
	FunctionData.InitRunning := SettingsFunction.eFunctionNumber > 0 AND
								SettingsFunction.eElementLink > 0;
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InputMapping" Id="{79869500-a2a1-44f1-a5fc-d5a5b3553456}">
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(********************************************************************************************************************************)

(**************************************************************************************
   Transport data of corresponding zone - extern information has been used, somewhere more than one rollerdrive is needed to be controlled
***************************************************************************************)
// Get transport state of corresponding conveyor to generate a proper trigger
TransportData := F_GetTransportControlData(
							eFunctionNumber	:= SettingsFunction.FunctionLink.eFunctionNumber,
							ZoneNumber 		:= SettingsFunction.FunctionLink.ZoneNumber);
    

InputAreaIsFree :=     SensorAreaFree;
          
(**************************************************************************************
   Element states
***************************************************************************************)
// Get status of the corresponding element
M_GetElementStates();

(**************************************************************************************
   AddOn states
***************************************************************************************)
// Get status of all corresponding AddOn functions and store it in FunctionInterface.In.AddOnOrder
M_GetAddOnOrder();

IF FD_ConvSTI <> 0 THEN
    EnableTransport := FD_ConvSTI.P_EnableTransport;
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InterfaceHandlerOut" Id="{f3d40bc3-bd16-4784-a128-7a91ecd49a05}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InterfaceHandlerOut
 * 	FUNCTION	Handles outgoing interfaces
 **************************************************************************************)
 
 
(**************************************************************************************************************
   ITC Handler Out
***************************************************************************************************************)
M_ITC_ProcessOut();]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_Diagnose" Id="{5b87e206-97b6-4912-9a1d-a90ac2c1f8de}">
      <Declaration><![CDATA[//Method to call interface
METHOD PUBLIC M_Diagnose : BOOL

VAR_IN_OUT
    	ErrorDataSet  : ST_ErrorDataSet;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// not used in FiredoorConveyor
M_Diagnose := TRUE;]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_FireDoorConveyor" Id="{18b3be43-7b45-4bb7-aedf-487e01961f1c}">
      <Declaration><![CDATA[METHOD M_FireDoorConveyor : BOOL

(********************************************************************************************************************************)
VAR
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
IF (FunctionData.ErrorData.Error.ErrorType = E_ErrorType.NO_ERROR_PENDING) THEN
    
    // Watchdog timer
    WatchdogAreaFree( IN := (InputAreaIsFree = FALSE) AND (EnableTransport = TRUE), 
                      PT := AREAFREE_TIMEOUT);
END_IF                 

IF WatchdogAreaFree.Q THEN
    //set error
   F_SetError(ErrorType   := E_ErrorType.ERROR,
              ErrorParam  := '',
              ErrorMsg    := E_ErrorProduct.DOORS_AREA_NOT_FREE,
              ErrorData   := FunctionData.ErrorData.ErrorDataSet[ERRORSET_AREANOTFREE]);                        
END_IF

IF InputAreaIsFree = TRUE THEN
    //reset error
    F_ResetError( Reset 	  := TRUE,
                  ErrorData := FunctionData.ErrorData.ErrorDataSet[ERRORSET_AREANOTFREE]);
END_IF


CASE FD_ConveyorStates OF
  
   E_FD_ConveyorStates.INACTIVE:

      FunctionInterface.Out.AddOnOrders.Enable        := TRUE;
      FunctionInterface.Out.AddOnOrders.EnableHandover:= TRUE;
      FunctionInterface.Out.AddOnOrders.EnableTakeover:= TRUE;

     IF TransportData.eTransportState = E_TransportState.CLEARING
        OR (TransportData.eTransportState = E_TransportState.IDLE AND TransportData.eTakeOverState = E_TakeoverState.IDLE) THEN

            FD_ConveyorStates := E_FD_ConveyorStates.READY;
     END_IF
            
   //Ready state, no ongoing transport through firedoor 
  E_FD_ConveyorStates.READY:
  
    IF TransportData.eTakeOverState = E_TakeoverState.ACTIVE 
    OR TransportData.eHandOverState = E_HandoverState.READY
    OR TransportData.Occupied = TRUE THEN
        
        (*-- disable movement ----------------------------------------------------------------*)
        FunctionInterface.Out.AddOnOrders.Enable        := TRUE;
        FunctionInterface.Out.AddOnOrders.EnableHandover:= FALSE;
        FunctionInterface.Out.AddOnOrders.EnableTakeover:= TRUE;
    
        FD_ConveyorStates := E_FD_ConveyorStates.OPEN_REQUEST;
    END_IF

   //conveyor at firedoor requests door open
  E_FD_ConveyorStates.OPEN_REQUEST:
  
        OpenRequest := TRUE;
       
        (*-- disable movement ----------------------------------------------------------------*)
        FunctionInterface.Out.AddOnOrders.Enable        := TRUE;
        FunctionInterface.Out.AddOnOrders.EnableHandover:= FALSE;
        FunctionInterface.Out.AddOnOrders.EnableTakeover:= TRUE;
        
        IF EnableTransport THEN
            
            FD_ConveyorStates := E_FD_ConveyorStates.ONGOING_TRANSPORT;
        
		    END_IF

  //door is open, transport is active
  E_FD_ConveyorStates.ONGOING_TRANSPORT:
  
    (*-- enable movement ----------------------------------------------------------------*)
    FunctionInterface.Out.AddOnOrders.Enable        := TRUE;
    FunctionInterface.Out.AddOnOrders.EnableHandover:= TRUE;
    FunctionInterface.Out.AddOnOrders.EnableTakeover:= TRUE;  
  
    IF TransportData.eHandOverState = E_HandoverState.IDLE 
      AND NOT TransportData.Occupied THEN
        
        OpenRequest := FALSE;
        FD_ConveyorStates := E_FD_ConveyorStates.READY; 
	  END_IF

END_CASE]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_OutputMapping" Id="{8847eb88-06e4-4c03-b24b-8391fe6e66bf}">
      <Declaration><![CDATA[METHOD PROTECTED M_OutputMapping : BOOL

(********************************************************************************************************************************)
VAR
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(** function interface **********************************************************************************************************)
FunctionInterface.Out.FunctionOrders.ReqDriveRun:= FALSE;  // Drive run not needed
FunctionInterface.Out.FunctionOrders.Enable     := TRUE;   // Allow element to drive
  
FunctionInterface.Out.AddOnOrders.DataReady := TRUE;


(********************************************************************************************************************************)
(** AddOn ***********************************************************************************************************************)

 // done in state machine




(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF FD_ConvSTI <> 0 THEN
    FD_ConvSTI.P_AreaFree := InputAreaIsFree;
    FD_ConvSTI.P_AreaFreeTimeout := WatchdogAreaFree.Q; 
    FD_ConvSTI.P_OpenRequest := OpenRequest;
END_IF]]></ST>
      </Implementation>
    </Method>
    <Property Name="P_AreaFree" Id="{7a39d541-cbdb-4c8d-b6d9-3ff5c31faeed}">
      <Declaration><![CDATA[//sensor controls area between firedoor and conveyor to control proper closing
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC P_AreaFree : BOOL
]]></Declaration>
      <Get Name="Get" Id="{81c9696a-9613-425a-8130-9db467b6a397}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_AreaFree := InputAreaIsFree;


]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="P_AreaFreeTimeout" Id="{c1acd67f-65bc-419c-9ccf-edd01b135693}">
      <Declaration><![CDATA[//timer controls blocking of area between firedoor and conveyor
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC P_AreaFreeTimeout : BOOL
]]></Declaration>
      <Get Name="Get" Id="{4da74509-feff-4a6e-b10a-2bc4edadd881}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_AreaFreeTimeout  := WatchdogAreaFree.Q; ]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="P_EnableTransport" Id="{c7f50c21-2f68-4bbc-907c-5fc4cfee9eb9}">
      <Declaration><![CDATA[//Firedoor master allows conveyor to drive through
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC P_EnableTransport : BOOL
]]></Declaration>
      <Get Name="Get" Id="{dcdcb7ed-15f0-4e2b-8d7c-7b51d4ada358}">
        <Declaration><![CDATA[
VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_EnableTransport := EnableTransport;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{d253bfc0-37c3-489e-a58d-1d20918f0600}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[EnableTransport := P_EnableTransport;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_OpenRequest" Id="{153911b8-8eb9-4edf-975f-15b97876755a}">
      <Declaration><![CDATA[//Request to open firedoor
{attribute 'monitoring' := 'call'}
PROPERTY PUBLIC P_OpenRequest : BOOL]]></Declaration>
      <Get Name="Get" Id="{c1a9b8d1-3761-42d3-95f0-17750064da11}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_OpenRequest := OpenRequest;]]></ST>
        </Implementation>
      </Get>
    </Property>
  </POU>
</TcPlcObject>