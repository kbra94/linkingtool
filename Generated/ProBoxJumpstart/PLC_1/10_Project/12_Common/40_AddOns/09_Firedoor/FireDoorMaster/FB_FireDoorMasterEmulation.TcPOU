﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_FireDoorMasterEmulation" Id="{3c362f19-3d88-4984-93d7-4335a71204e9}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK PUBLIC FB_FireDoorMasterEmulation EXTENDS FB_FunctionConvEmulation
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 354922 $
 *	Revision date	:	$Date: 2015-09-15 18:01:17 +0200 (Di, 15 Sep 2015) $
 *	Last changed by	:	$Author: d7mangc $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BoxControlFramework/BoxControlFramework/JumpStart/11_Library/11Emulation/12_ElementFunctions/FB_DummyFunctionEmulation.TcPOU $
 *
 *	Purpose			:	Firedoor emulation
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				22.08.2018		swl					Draft
 *
 **************************************************************************************)

(********************************************************************************************************************************)
VAR_STAT CONSTANT
	HEIGHT                : LREAL                            :=  1_000;              // door height in mm
  SPEED                 : LREAL                            :=    100;              // door speed in mm/s
END_VAR
 
(********************************************************************************************************************************)
VAR
  pFunction             : POINTER   TO FB_FireDoorMaster         := 0;                   // Address to function
  rFunction             : REFERENCE TO FB_FireDoorMaster         := 0;                   // Reference to function
  //------------------------------------------------------------------------------------------------------------------------------
  FireAlert             : BOOL                             := FALSE;               // HI = fire alert active
  DoorError             : BOOL                             := FALSE;               // HI = door control error active
  AreaFree              : BOOL                             := TRUE;                // HI = area being free
  //------------------------------------------------------------------------------------------------------------------------------
  _MoveTime             : TIME                             := T#0S;                // timestamp to create door speed
	_Position             : LREAL                            := HEIGHT / 2;          // initial position (middle)
END_VAR

]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();


(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();

(**************************************************************************************************************
   Emulation
***************************************************************************************************************)
// If emulation function is initialized
IF FunctionData.Init THEN
	
	// Update data of real object
	M_UpdateRealObjectData();
	
	// Overwrite inputs
	M_OverWriteInputs();
END_IF]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{736ec09b-8ca2-4e15-b0c1-4058fb827899}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated dummy function
 **************************************************************************************)

// If dummy function is not initialized
IF NOT FunctionData.Init Then

	// Check that linked dummy function number is valid
	IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
	   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN   
		
		// Check if the real module is initialized
		IF F_IsFunctionInitialized(eFunctionNumber) THEN
			// Module is initialized
			FunctionData.Init:= TRUE;
			
			// Send debug message
			DebugMsg := CONCAT('Initialization done. Element Function: ',  FunctionRegistry[eFunctionNumber].FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
			
			// Initialize real module
			F_InitializeFunction(eFunctionNumber := eFunctionNumber);
		END_IF
	END_IF
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{c73f02f5-6e74-4a75-a514-814b107aeb7b}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid function number
IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN
   
	// Is reference valid  
	IF __ISVALIDREF(FunctionRegistry[eFunctionNumber].rFunction) THEN
		// Build address to reference
		pFunction := ADR(FunctionRegistry[eFunctionNumber].rFunction);

		// If address is possible
		IF pFunction <> 0 THEN
			// Build reference to memory
			rFunction REF= pFunction^;
		ELSE
			// Reinitialize module
			FunctionData.Init:= FALSE;
		END_IF
	ELSE
		// Reinitialize module
		FunctionData.Init:= FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_OverWriteInputs" Id="{e70f6b38-6743-4f5e-b84e-4984cfe21fe7}">
      <Declaration><![CDATA[METHOD PROTECTED M_OverWriteInputs : BOOL

(********************************************************************************************************************************)
VAR
  TimeDiff              : TIME                             := T#0S;                // time difference (task cycle)
  MoveDiff              : LREAL                            := 0;                   // distance travelled during one plc cycle
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(********************************************************************************************************************************)
(** door position ***************************************************************************************************************)
(*-- fire alert acitve - close door-----------------------------------------------------------------------------------------------*)
IF (FireAlert = TRUE) AND (DoorError = FALSE)
THEN
  rFunction.HW_Inputs.FireAlert:= FALSE;             // HI = no alert
  rFunction.HW_Inputs.DoorError:= NOT(DoorError);    // HI = no alert
//  rFunction.HW_Inputs.AreaFree :=     AreaFree;      // HI = no alert
  (*-----------------------------------------------------------------*)
  TimeDiff := F_GetTimeMsDiff(_MoveTime);
  _MoveTime:= F_GetTimeMsTick();
  (*-----------------------------------------------------------------*)
  MoveDiff := ( TIME_TO_LREAL(TimeDiff) * SPEED / 1_000);
  _Position:= ( _Position - MoveDiff );

(*-- moving upwards (opening) --------------------------------------------------------------------------------------------------*)
ELSIF (   (rFunction.HW_Outputs.CommandOpen  = TRUE )
       AND(rFunction.HW_Outputs.CommandClose = FALSE)  )
THEN
  rFunction.HW_Inputs.FireAlert:= TRUE;              // HI = no alert
  rFunction.HW_Inputs.DoorError:= NOT(DoorError);    // HI = no alert
//  rFunction.HW_Inputs.AreaFree :=     AreaFree;     // HI = no alert
  (*-----------------------------------------------------------------*)
  TimeDiff := F_GetTimeMsDiff(_MoveTime);
  _MoveTime:= F_GetTimeMsTick();
  (*-----------------------------------------------------------------*)
  MoveDiff := ( TIME_TO_LREAL(TimeDiff) * SPEED / 1_000);
  _Position:= ( _Position + MoveDiff );


(*-- moving downwards (closing) ------------------------------------------------------------------------------------------------*)
ELSIF (   (rFunction.HW_Outputs.CommandOpen  = FALSE)
       AND(rFunction.HW_Outputs.CommandClose = TRUE )  )
THEN
  rFunction.HW_Inputs.FireAlert:= TRUE;              // HI = no alert
  rFunction.HW_Inputs.DoorError:= NOT(DoorError);    // HI = no alert
//  rFunction.HW_Inputs.AreaFree :=     AreaFree;      // HI = no alert
  (*-----------------------------------------------------------------*)
  TimeDiff := F_GetTimeMsDiff(_MoveTime);
  _MoveTime:= F_GetTimeMsTick();
  (*-----------------------------------------------------------------*)
  MoveDiff := ( TIME_TO_LREAL(TimeDiff) * SPEED / 1_000);
  _Position:= ( _Position - MoveDiff );


(*-- standstill ----------------------------------------------------------------------------------------------------------------*)
ELSE
  rFunction.HW_Inputs.FireAlert:= TRUE;              // HI = no alert
  rFunction.HW_Inputs.DoorError:= NOT(DoorError);    // HI = no alert
 // rFunction.HW_Inputs.AreaFree :=     AreaFree;      // HI = no alert
  (*-----------------------------------------------------------------*)
  _MoveTime:= F_GetTimeMsTick();
END_IF

_Position:= LIMIT(0, _Position, HEIGHT);




(********************************************************************************************************************************)
(** inputs for real fb **********************************************************************************************************)
rFunction.HW_Inputs.Open  := _Position >= HEIGHT;
rFunction.HW_Inputs.Closed:= _Position <= 0;




(********************************************************************************************************************************)
(********************************************************************************************************************************)
]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>