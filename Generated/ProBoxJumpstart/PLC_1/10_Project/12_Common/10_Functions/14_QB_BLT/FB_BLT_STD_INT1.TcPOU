﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_BLT_STD_INT1" Id="{dc39967e-e9fd-027b-0533-b15449350b0d}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_BLT_STD_INT1 EXTENDS FB_FunctionQB2
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 488065 $
 *	Revision date	:	$Date: 2021-10-13 13:42:49 +0200 (Mi, 13 Okt 2021) $
 *	Last changed by	:	$Author: g7nowan $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/QuickMove/02_Controls/QuickBox/20_Release/trunk/01_Software/QB2Jumpstart/JumpStart/20_Product/20_QuickBox/22_InterRoll/12_E_Functions/BLT/FB_BLT_STD_INT1.TcPOU $
 *
 *	Purpose			:	Function for Belt without Tracking
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				25.06.2018		dlu					Typical renamed
 *	0.2				30.01.2019		BvW					Implemented virtual encoder
 *
 **************************************************************************************)
VAR_INPUT
	Config					: ST_CFG_BLT_STD; // Configuration
	HW_Inputs				: ST_HW_InputsBLT_STD_FunctionWin; // Hardware inputs
END_VAR

VAR_OUTPUT
	//Outputs					: ST_OutputsBLT_STD_FunctionWin; // Function specific outputs
	HW_Outputs				: ST_HW_OutputsBLT_STD_FunctionWin; // Hardware outputs
END_VAR

VAR
	Inputs					: ST_HW_InputsBLT_STD_FunctionWin; // Function specific inputs

	(*ReqSpeed				: REAL; // Requested speed*)
	fbTransportControl		: FB_TransportControl; // Transport control 
	fbSpeedControl 			: FB_SpeedControl; // Speed control for each zone	
	TestDrive				: BOOL;					// Testdrive Rollerdrive	

	fbEncoder				: FB_Encoder;		// Virtual encoder

	EncoderValue			: UDINT;
	MaxEncoderValue			: UDINT;
	IntAccelDecel 			: REAL	:= 1.5;	// [m/s²]
END_VAR

VAR PERSISTENT
	Settings				: ST_CFG_BLT_STD; // Settings - activated configuration 
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Input mapping
***************************************************************************************************************)
A_InputMapping();

(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   State control
***************************************************************************************************************)
A_StateControl();

(**************************************************************************************************************
   Error handler
***************************************************************************************************************)
A_ErrorHandler();

(**************************************************************************************************************
   Process AddOns
***************************************************************************************************************)
M_ProcessAddOns();

(**************************************************************************************************************
   Manual control
***************************************************************************************************************)
A_ManualControl();


(**************************************************************************************************************
   External STI call TODO!
***************************************************************************************************************)
A_ExternalSTI(); {warning'warum wurde das im QB standard nicht aufgerufen?'}
(**********************************************************************************************************
   Transport control
***********************************************************************************************************)
fbSpeedControl(
	FastSpeed 	:= 	(ZoneData[1].TransportControlData.FastSpeedActive AND
					FunctionData.OperationState.eMode = E_PositionMode.AUTO_MODE) OR
					(ManualPartsAktiv.FastSpeed AND
					FunctionData.OperationState.eMode = E_PositionMode.MANUAL_MODE), 
	SetSpeed 	:= SettingsFunction.Zone[1].Speed, 
	SpeedSlow 	:= SettingsFunction.Zone[1].SpeedSlow, 
	SpeedFast 	:= SettingsFunction.Zone[1].SpeedFast);

(**********************************************************************************************************
   Assign trasnport parameter
***********************************************************************************************************)
A_AssignTransportParam();


(**********************************************************************************************************
   Transport control
***********************************************************************************************************)
fbTransportControl(
	Param 					:= M_GetTransportControlParam(mZoneNumber := 1), // Parameter of transport control
	PrevTransportData 		:= M_GetPreviousTransportData(mZoneNumber := 1), // Previous transport data, 
	NextTransportData 		:= M_GetNextTransportData(mZoneNumber := 1), // Next transport data, 
	TransportEnable			:= ZoneData[1].AddOn.Orders.Enable AND
							   FunctionData.ErrorData.Error.ErrorType <> E_ErrorType.ERROR AND
							   ZoneData[1].ErrorData.Error.ErrorType <> E_ErrorType.ERROR AND
							   FunctionInterface.In.Element.State.DriveReady, // Transport enable
	TransportEnableTakeOver := ZoneData[1].AddOn.Orders.EnableTakeover, // Transport enable takeover 
	TransportEnableHandOver := ZoneData[1].AddOn.Orders.EnableHandover
								AND FunctionInterface.In.FunctionEnable
								AND FunctionInterface.In.Element.State.DriveReady, // Transport enable handover
	EnableFastSpeed 		:= TRUE, // Enable fast speed
	DriveRunning			:= FunctionInterface.In.Element.State.DriveRunning, // Feedback drive is running
	CurrentSpeed			:= fbSpeedControl.CommandSpeed,	
	ZoneOccupied			:= Inputs.Occupied, 
	ZoneData				:= ZoneData[1], // Zone data,
	Acceleration			:= REAL_TO_INT(IntAccelDecel*1000), 
	EncoderValue			:= EncoderValue, 
	MaxEncoderValue			:= MaxEncoderValue, 
	SyncTracking			:= FALSE, 
	SyncTrackingPosition	:= 0, 
	StoppingDistance		:= 200, //F_StoppingDistance(fbTransportControl.CurrentSpeed), // Distance until the TU is stopped [mm]
	ErrorData				:= ZoneData[1].ErrorData.ErrorDataSet[1]); // Error data	

		
(**********************************************************************************************************
   Transport link
***********************************************************************************************************)
A_TransportLink();

(**********************************************************************************************************
   Message handler zone
***********************************************************************************************************)
M_MsgHandlerZone(mZoneNumber := 1);
	
(**********************************************************************************************************
   ITC Zone output
***********************************************************************************************************)
M_ITC_ProcessOutZone(mZoneNumber := 1);
	
(**************************************************************************************************************
   Interface handler out
***************************************************************************************************************)
A_InterfaceHandlerOut();

(**************************************************************************************************************
   Output mapping
***************************************************************************************************************)
A_OutputMapping();


]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{ead51e0d-d820-0418-2f74-ff9d0a815f64}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize function
 **************************************************************************************)

// Update Registry
M_UpdateRegistry();
 
// Init ongoing - Wait until init is released (Sequencial startup)
IF NOT FunctionData.Init AND FunctionData.InitRunning THEN
	// When the subsystem is started, 
	// and its released to initialize
	IF FunctionData.OperationState.Info.SystemReady AND
	   SettingsFunction.eFunctionNumber < FunctionData.OperationState.InitRelease.eLimitFunctionNumber THEN
	   
	   IF M_InitFunction(FALSE) THEN
			// Function initialized - and initialization done
			FunctionData.Init := TRUE;
			FunctionData.InitRunning := FALSE;
			
			// Set reset cmd
			FunctionData.ErrorData.Error.ResetError := TRUE;
			
			DebugMsg := CONCAT('Initialization done. Function: ', ConfigFunction.FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
		END_IF
	END_IF
(*
If the function is not initialized,
clean all values and wait until init is released
*)
ELSIF NOT FunctionData.Init THEN
	// Reset internal variables
	FunctionData.OperationState := FunctionBase.PositionStateEmpty;
	
	// Initiate initialization of all zones
	FOR ZoneIdx := 1 TO SettingsFunction.NumberOfZones DO
		ZoneData[ZoneIdx].Init := FALSE;
	END_FOR;
	
	// Load function specific configuration to settings
	Settings := Config;
	
	// Reset values and load config
	M_PreInit();
	
	// To start init a valid functionnumber and element link is needed
	FunctionData.InitRunning := SettingsFunction.eFunctionNumber > 0 AND
								SettingsFunction.eElementLink > 0;
END_IF

// Init zones
IF FunctionData.Init THEN
	// Set init flag after one cycle
	FOR ZoneIdx := 1 TO SettingsFunction.NumberOfZones DO
		// If a zone is not initialized, wait one cycle to finish init
		IF NOT ZoneData[ZoneIdx].Init THEN
			IF ZoneData[ZoneIdx].InitRunning THEN
				ZoneData[ZoneIdx].InitRunning := FALSE;
				ZoneData[ZoneIdx].Init := TRUE;
				ZoneData[ZoneIdx].ErrorData.Error.ResetError := TRUE;
			ELSE	
				ZoneData[ZoneIdx].InitRunning := TRUE;
			END_IF
		END_IF
	END_FOR;
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InputMapping" Id="{527a816f-4d82-0cff-3d9c-62333d772c29}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InputMapping
 * 	FUNCTION	Input mapping
 **************************************************************************************)
 
(**************************************************************************************
   Hardware Inputs
***************************************************************************************)
Inputs.DriveRunning  := HW_Inputs.DriveRunning	XOR  Settings.HW_InputInverted.DriveRunning;

//Inputs.StartTracking := FbTU_GapMask_StartTracking.OccupiedGapless;
Inputs.Occupied		 := HW_inputs.Occupied	XOR NOT Settings.HW_InputInverted.Occupied;

(**************************************************************************************
   Element states
***************************************************************************************)
// Get status of the corresponding element
M_GetElementStates();

(**********************************************************************************************************
   Encoder
***********************************************************************************************************)
IF Settings.iEncoderElement <> 0 THEN
	// Encoder element is configured
	EncoderValue	:= Settings.iEncoderElement.P_EncoderData.EncoderValueInc;
	MaxEncoderValue	:= Settings.iEncoderElement.P_EncoderData.MaxEncoderValueInc;
ELSE
	// No encoder element available
	fbEncoder(	Run				:= 	FunctionInterface.In.Element.State.DriveRunning,
				Speed			:=	FunctionInterface.In.Element.State.Speed / SettingsFunction.Zone[1].SpeedRatio,
				Resolution		:=	1);
	EncoderValue	:= fbEncoder.EncoderValue;
	MaxEncoderValue	:= 65535;
END_IF

(**************************************************************************************
   AddOn states
***************************************************************************************)
// Get status of all corresponding AddOn functions and store it in FunctionInterface.In.AddOnOrder
M_GetAddOnOrder();]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OutputMapping" Id="{e28bd8ee-bb59-0d49-1bc9-e6d527d18ac1}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OutputMapping
 * 	FUNCTION	Output mapping
 **************************************************************************************)
 
 
(**************************************************************************************
   Hardware outputs
***************************************************************************************)
// No hardware outputs

(**************************************************************************************
   Order outputs (Commands to corresponding element)
***************************************************************************************)
// Drive run request
FunctionInterface.Out.FunctionOrders.ReqDriveRun := fbTransportControl.Outputs.DriveRun;

// Speed request
FunctionInterface.Out.FunctionOrders.ReqSpeed := REAL_TO_INT(fbSpeedControl.CommandSpeed * SettingsFunction.Zone[1].SpeedRatio);

// Check error states
FunctionInterface.Out.FunctionOrders.Enable := NOT FunctionData.ErrorData.ZoneErrorPending AND
										   FunctionData.ErrorData.Error.ErrorType <> E_ErrorType.ERROR;

(**************************************************************************************
   AddOn outputs
***************************************************************************************)
// This function provide no AddOn functionality
;]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_TransportLink" Id="{7eaabc56-f7ee-08e9-22ae-bf56c307e288}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_TransportLink
 * 	FUNCTION	Links the element functions for transportation
 **************************************************************************************)

// If previous function is not static use merging service
IF SettingsFunction.ePrevFunctionNumber = E_FunctionNumber.F_BEGIN_FUNCTION THEN
 
	// Previous element function assignement
	IF fbTransportControl.Outputs.DeletePrevTransportData THEN
		FunctionInterface.In.ePrevFunctionNumber := E_FunctionNumber.F_BEGIN_FUNCTION;
	END_IF
	
	IF fbTransportControl.Outputs.RequestPrevTransportData THEN
		// Call merging service
		fbMergingService(
			eFunctionNumber 	:= SettingsFunction.eFunctionNumber,
			ePrevFunctionNumber => FunctionInterface.In.ePrevFunctionNumber);
	END_IF	
ELSE
	// Use static link
	FunctionInterface.In.ePrevFunctionNumber := SettingsFunction.ePrevFunctionNumber;
END_IF


// If next function is not static use routing service
IF SettingsFunction.eNextFunctionNumber = E_FunctionNumber.F_BEGIN_FUNCTION THEN

	// Next element function assignement
	IF fbTransportControl.Outputs.DeleteNextTransportData THEN
		FunctionInterface.In.eNextFunctionNumber := E_FunctionNumber.F_BEGIN_FUNCTION;
	END_IF
	
	IF fbTransportControl.Outputs.RequestNextTransportData THEN
		// Call routing service
		fbRoutingService(
			eFunctionNumber 	:= SettingsFunction.eFunctionNumber,
			eNextFunctionNumber => FunctionInterface.In.eNextFunctionNumber);
	END_IF
ELSE
	// Use static link
	FunctionInterface.In.eNextFunctionNumber := SettingsFunction.eNextFunctionNumber;
END_IF]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_DeleteTU_DataIndex" Id="{1d842376-434c-011c-26c0-1059223c307a}">
      <Declaration><![CDATA[METHOD M_DeleteTU_DataIndex : BOOL
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 488065 $
 *	Revision date	:	$Date: 2021-10-13 13:42:49 +0200 (Mi, 13 Okt 2021) $
 *	Last changed by	:	$Author: g7nowan $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/QuickMove/02_Controls/QuickBox/20_Release/trunk/01_Software/QB2Jumpstart/JumpStart/20_Product/20_QuickBox/22_InterRoll/12_E_Functions/BLT/FB_BLT_STD_INT1.TcPOU $
 *
 *	Purpose			:	Delete TU Data index on position
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR_INPUT
	mZoneNumber		: INT; // Zone number
	mTU_DataIndex	: DINT; // Tu index to delete
	mInit			: BOOL; // Initialize position afterwards
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Initialize
M_DeleteTU_DataIndex := FALSE;

// Zone number valid
IF mZoneNumber > 0 AND mZoneNumber <= SettingsFunction.NumberOfZones THEN
	// TU Index is equal to the current
	IF mTU_DataIndex = ZoneData[mZoneNumber].TransportControlData.TU_DataIndex THEN
		// Clear
		ZoneData[mZoneNumber].TransportControlData.TU_DataIndex := 0;
		ZoneData[mZoneNumber].TransportControlData.DataPresent := FALSE;
		
		// Init requested
		IF mInit THEN
			ZoneData[mZoneNumber].Init := FALSE;
		END_IF
		
		// Delete successful
		M_DeleteTU_DataIndex := TRUE;
	END_IF
ELSIF mZoneNumber = 0 THEN
	// Delete internal TU Data storage (missing)
	;
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_FindTU_DataIndex" Id="{712cbb65-9fa7-0123-112d-f02363deeb1d}">
      <Declaration><![CDATA[METHOD M_FindTU_DataIndex : BOOL
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 488065 $
 *	Revision date	:	$Date: 2021-10-13 13:42:49 +0200 (Mi, 13 Okt 2021) $
 *	Last changed by	:	$Author: g7nowan $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/QuickMove/02_Controls/QuickBox/20_Release/trunk/01_Software/QB2Jumpstart/JumpStart/20_Product/20_QuickBox/22_InterRoll/12_E_Functions/BLT/FB_BLT_STD_INT1.TcPOU $
 *
 *	Purpose			:	Checks if a position consists of specific TU_DataIndex.
 *						mZoneNumber 0 = search in whole position
 *						This method can be overwritten for example in a Belt, to give 
 *						access to internal TU index structures.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR_INPUT
	mZoneNumber		: INT; // Check zone number, 0 = check all
	mTU_DataIndex	: DINT; // Searched TU index
END_VAR

VAR
	mIdx			: INT; // Iterator
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Initialize
M_FindTU_DataIndex := FALSE;

// If Zone index is in a specific range 
IF mZoneNumber > 0 AND mZoneNumber <= SettingsFunction.NumberOfZones THEN
	// Check for index
	M_FindTU_DataIndex := ZoneData[mZoneNumber].TransportControlData.TU_DataIndex = mTU_DataIndex;
ELSIF mZoneNumber = 0 THEN
	// Search for index in internal TU Data storage (missing)
	;
END_IF]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>