﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_Merging2TU_IntoSingleConveyorEmulation" Id="{2594ecab-7f51-4709-a8f6-5fc8c814d236}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_Merging2TU_IntoSingleConveyorEmulation EXTENDS FB_FunctionConvEmulation
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date:$
 *	Last changed by	:	$Author:  $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Emulation for: Merging up to 2 certain TUs (according configured TU-Type)
 *						into one pair on a single conveyor. Merged TUs are further 
 *						transported as one TU with one TU-Index and changed TU-Type: Pair
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				16.09.2021		dlu					First version
 *  0.2             19.10.2021      dlu					PostReview correction
 *
 **************************************************************************************)
 VAR
	pFunction						: POINTER TO FB_Merging2TU_IntoSingleConveyor; // Address to function
	rFunction						: REFERENCE TO FB_Merging2TU_IntoSingleConveyor; // Reference to function
	fbTransportControlEmulation		: FB_PB_TransportControlEmulation; // Transport control emulation
	fbEncoder						: FB_PB_Element_Encoder();
    Sensor                      	: BOOL; // Example sensor on element level
	ElementDriveRunning				: BOOL; // Feedback drive is running from element
	ElementSpeed					: INT; // Feedback speed from element	
	SensorFirst						: BOOL; //test
	SensorStop						: BOOL; //test
	EmuSensors						: ARRAY [1..NUMBER_OF_ZONES_PER_FUNCTION] OF BOOL;
	DebugSensors					: ARRAY [1..2] OF BOOL; //Slug / Stop sensor
	ZoneIdx: INT;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();

(**************************************************************************************************************
   Emulation
***************************************************************************************************************)
// If emulation function is initialized
IF FunctionData.Init THEN

	// Update data of real object
	M_UpdateRealObjectData();
	
	// Handle external Subsystem-Subsystem interfaces
	A_ExternalSTI();
	
	// Assign feedback from element
	IF rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_12 OR rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_21 THEN
		ElementDriveRunning := FunctionBase.rFunctionInterfaceReal.In.Element.State.DriveRunning AND FunctionBase.rFunctionInterfaceReal.Out.FunctionOrders.ReqDriveRun;
		ElementSpeed := FunctionBase.rFunctionInterfaceReal.In.Element.State.Speed;
	ELSIF rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_34 OR rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_43 THEN
		ElementDriveRunning := FunctionBase.rFunctionInterfaceReal.In.Element.State.DriveRunning_34 AND FunctionBase.rFunctionInterfaceReal.Out.FunctionOrders.ReqDriveRun_34;
		ElementSpeed := FunctionBase.rFunctionInterfaceReal.In.Element.State.Speed_34;
	ELSE
		ElementDriveRunning := FALSE;
		ElementSpeed := 0;
	END_IF
	
	//Run encoder positon, 
//TODO: Move to Element???
	fbEncoder(	Run:=ElementDriveRunning, 
				Speed:= ProBoxSystem.EMULATION_SPEED);
	
	// Call transport control 
	fbTransportControlEmulation(
		Param 					:= M_GetTransportControlParam(mZoneNumber := 1), // Parameter of transport control
		PrevTransportData 		:= M_GetPreviousTransportData(mZoneNumber := 1), // Previous transport data
		NextTransportData 		:= M_GetNextTransportData(mZoneNumber := 1), // Next transport data	
		TransportEnable			:= TRUE, // Transport enable	
		DriveRunning			:= ElementDriveRunning, // 
		EncoderValue			:= fbEncoder.EncoderValue, //FunctionBase.rFunctionInterfaceReal.In.Element.State.EncoderValue
		ZoneData 				:= ZoneData[1]); // Zone data


A_OverWriteInputs();	

END_IF]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{f6a171a1-f334-4011-ac21-875647b124a7}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated dummy function
 **************************************************************************************)

// If dummy function is not initialized
IF NOT FunctionData.Init Then
	// Check that linked dummy function number is valid
	IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
	   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN   
		
		// Check if the real module is initialized
		IF F_IsFunctionInitialized(eFunctionNumber := eFunctionNumber) THEN
			// Module is initialized
			FunctionData.Init:= TRUE;
			
			// Send debug message
			DebugMsg := CONCAT('Initialization done. Element Function: ', FunctionRegistry[eFunctionNumber].FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
			
			// Initialize real module
			F_InitializeFunction(eFunctionNumber := eFunctionNumber);
		END_IF
	END_IF
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs" Id="{1788f570-586a-4a37-be0f-318f7b4afdfc}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OverWriteInputs
 * 	FUNCTION	Overwrite inputs for emulation
 **************************************************************************************)
ZoneData[1].TransportControlData.TransportParam.ZoneLength := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1];	

EmuSensors[1] :=  fbTransportControlEmulation.M_TU_PresentState(
										mPosition := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[2],
										mIndex => FunctionBase.IndexToRemove[1],
										mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos);

//FWD drive direction
SensorStop := EmuSensors[1];

//Set HW outputs, Keep input if direction is changed.
rFunction.HW_Inputs.FullCapacitySensor := sensorStop AND(NOT FunctionBase.rSettingsFunctionReal.FlowDirectionREV OR rFunction.HW_Inputs.FullCapacitySensor);

rFunction.HW_Inputs.FullCapacitySensor := rFunction.HW_Inputs.FullCapacitySensor OR DebugSensors[2];

// Changed.mvs.14.09.2021
rFunction.HW_Inputs.FirstSequenceSensor := ZoneData[1].TransportControlData.TU_LeadingEdgePos > (FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] / 2) - FunctionBase.rSettingsFunctionReal.Zone[ZoneIdx].ZoneEndLengthToSide[2];

]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{c93c8038-a55f-4df8-b43c-6d58d0107cf5}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid function number
IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN

	// Is reference valid  
	IF __ISVALIDREF(FunctionRegistry[eFunctionNumber].rFunction) THEN
		// Build address to reference
		pFunction := ADR(FunctionRegistry[eFunctionNumber].rFunction);

		// If address is possible
		IF pFunction <> 0 THEN
			// Build reference to memory
			rFunction REF= pFunction^;
		ELSE
			// Reinitialize module
			FunctionData.Init:= FALSE;
		END_IF
	ELSE
		// Reinitialize module
		FunctionData.Init:= FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_GetTU_PresentState" Id="{b617c0e8-a9c8-4144-a8d6-4b551114d454}">
      <Declaration><![CDATA[METHOD M_GetTU_PresentState : BOOL
(**************************************************************************************
 * 	Application		:	ProBox
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date: $
 *	Last changed by	:	$Author: q8Hedls $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Get TU present state, overriden from base object
 *
 **************************************************************************************)
VAR_INPUT
	mZone	 : INT; // Zone number
	mPosition : UINT; // Position [mm]
END_VAR
VAR_OUTPUT
	mTU_DataIndex : DINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF(mZone>=1 AND mZone<= NUMBER_OF_ZONES_PER_FUNCTION) THEN
	M_GetTU_PresentState := fbTransportControlEmulation.M_TU_PresentState(mPosition := mPosition, mTU_DataIndex => mTU_DataIndex);
END_IF
]]></ST>
      </Implementation>
    </Method>
    <ObjectProperties>
      <XmlArchive>
        <Data>
          <o xml:space="preserve" t="UMLStereoTypeContainerObject">
            <v n="IsType" t="UMLType">BaseArea</v>
            <v n="Stereotype">""</v>
            <d n="Stereotypes" t="Hashtable" />
          </o>
        </Data>
        <TypeList>
          <Type n="Hashtable">System.Collections.Hashtable</Type>
          <Type n="String">System.String</Type>
          <Type n="UMLStereoTypeContainerObject">{30250973-b110-4e31-b562-c102e042dca4}</Type>
          <Type n="UMLType">{0197b136-405a-42ee-bb27-fd08b621d0cf}</Type>
        </TypeList>
      </XmlArchive>
    </ObjectProperties>
  </POU>
</TcPlcObject>