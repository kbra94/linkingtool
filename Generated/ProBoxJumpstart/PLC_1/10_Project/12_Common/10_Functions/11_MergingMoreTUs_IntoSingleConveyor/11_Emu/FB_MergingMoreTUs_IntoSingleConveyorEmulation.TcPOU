﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_MergingMoreTUs_IntoSingleConveyorEmulation" Id="{c023c77c-194c-4aa7-b024-01af30cbadba}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_MergingMoreTUs_IntoSingleConveyorEmulation EXTENDS FB_FunctionConvEmulation
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date:$
 *	Last changed by	:	$Author:  $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Emulation for: Merging more TUs (Rc oder Pallets)
 *						into one pair on a single conveyor. Merged TUs are further 
 *						transported as one TU with one TU-Index and changed TU-Type: Mixed
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				16.09.2021		dlu					First version
 *  0.2				19.10.2021		dlu					Post Review corrections
 *
 **************************************************************************************)
 VAR
	pFunction						: POINTER TO FB_MergingMoreTUs_IntoSingleConveyor; // Address to function
	rFunction						: REFERENCE TO FB_MergingMoreTUs_IntoSingleConveyor; // Reference to function
	fbTransportControlEmulation		: FB_PB_TransportControlEmulation; // Transport control emulation
	fbEncoder						: FB_PB_Element_Encoder();
    Sensor                      	: BOOL; // Example sensor on element level
	ElementDriveRunning				: BOOL; // Feedback drive is running from element
	ElementSpeed					: INT; // Feedback speed from element	
	SensorFirst						: BOOL; //test
	SensorStop						: BOOL; //test
	EmuSensors						: ARRAY [1..NUMBER_OF_ZONES_PER_FUNCTION] OF BOOL;
	DebugSensors					: ARRAY [1..2] OF BOOL; //Slug / Stop sensor
END_VAR

VAR CONSTANT
	POSITION_OF_RC_SLUG_SENSOR		: UINT := 700;  // Position is defined from start of the conveyor in mm
	POSITION_OF_PALLET_SLUG_SENSOR	: UINT := 1300; // Position is defined from start of the conveyor in mm
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();

(**************************************************************************************************************
   Emulation
***************************************************************************************************************)
// If emulation function is initialized
IF FunctionData.Init THEN

	// Update data of real object
	M_UpdateRealObjectData();
	
	// Handle external Subsystem-Subsystem interfaces
	A_ExternalSTI();
	
	// Assign feedback from element
	IF rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_12 OR rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_21 THEN
		ElementDriveRunning := FunctionBase.rFunctionInterfaceReal.In.Element.State.DriveRunning AND FunctionBase.rFunctionInterfaceReal.Out.FunctionOrders.ReqDriveRun;
		ElementSpeed := FunctionBase.rFunctionInterfaceReal.In.Element.State.Speed;
	ELSIF rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_34 OR rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_43 THEN
		ElementDriveRunning := FunctionBase.rFunctionInterfaceReal.In.Element.State.DriveRunning_34 AND FunctionBase.rFunctionInterfaceReal.Out.FunctionOrders.ReqDriveRun_34;
		ElementSpeed := FunctionBase.rFunctionInterfaceReal.In.Element.State.Speed_34;
	ELSE
		ElementDriveRunning := FALSE;
		ElementSpeed := 0;
	END_IF
	
	//Run encoder positon, 
//TODO: Move to Element???
	fbEncoder(	Run:=ElementDriveRunning, 
				Speed:= ProBoxSystem.EMULATION_SPEED);

	// Call transport control 
	fbTransportControlEmulation(
		Param 					:= M_GetTransportControlParam(mZoneNumber := 1), // Parameter of transport control
		PrevTransportData 		:= M_GetPreviousTransportData(mZoneNumber := 1), // Previous transport data
		NextTransportData 		:= M_GetNextTransportData(mZoneNumber := 1), // Next transport data	
		TransportEnable			:= TRUE, // Transport enable	
		DriveRunning			:= ElementDriveRunning, // 
		EncoderValue			:= fbEncoder.EncoderValue, //FunctionBase.rFunctionInterfaceReal.In.Element.State.EncoderValue
		ZoneData 				:= ZoneData[1]); // Zone data


A_OverWriteInputs();	

END_IF]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{acc4b78b-c406-4cea-89df-02a77e60a7b1}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated dummy function
 **************************************************************************************)

// If dummy function is not initialized
IF NOT FunctionData.Init Then
	// Check that linked dummy function number is valid
	IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
	   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN   
		
		// Check if the real module is initialized
		IF F_IsFunctionInitialized(eFunctionNumber := eFunctionNumber) THEN
			// Module is initialized
			FunctionData.Init:= TRUE;
			
			// Send debug message
			DebugMsg := CONCAT('Initialization done. Element Function: ', FunctionRegistry[eFunctionNumber].FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
			
			// Initialize real module
			F_InitializeFunction(eFunctionNumber := eFunctionNumber);
		END_IF
	END_IF
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs" Id="{6bf06350-68a8-4a6b-9271-bee2eb540ee2}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OverWriteInputs
 * 	FUNCTION	Overwrite inputs for emulation
 **************************************************************************************)

ZoneData[1].TransportControlData.TransportParam.ZoneLength := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1];	

EmuSensors[1] :=  fbTransportControlEmulation.M_TU_PresentState(
										mPosition := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[2],
										mIndex => FunctionBase.IndexToRemove[1],
										mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos);

//Set sensor at last zone
IF(FunctionBase.rSettingsFunctionReal.FlowDirectionREV) THEN
	//RWD drive direction
	SensorStop := EmuSensors[1];
ELSE
	//FWD drive direction
	SensorStop := EmuSensors[rFunction.SettingsFunction.NumberOfZones];
END_IF


//Set HW outputs, Keep input if direction is changed.
rFunction.HW_Inputs.FullCapacitySensor := sensorStop AND(NOT FunctionBase.rSettingsFunctionReal.FlowDirectionREV OR rFunction.HW_Inputs.FullCapacitySensor);

rFunction.HW_Inputs.FullCapacitySensor := rFunction.HW_Inputs.FullCapacitySensor OR DebugSensors[2];

// Simulate Slug sensors
rFunction.HW_Inputs.RcSlugSensor 		:= ZoneData[1].TransportControlData.TU_LeadingEdgePos > POSITION_OF_RC_SLUG_SENSOR;
rFunction.HW_Inputs.PalletSlugSensor 	:= ZoneData[1].TransportControlData.TU_LeadingEdgePos > POSITION_OF_PALLET_SLUG_SENSOR;

]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{2fad1986-28fe-4a48-aca2-e377044e80b1}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid function number
IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN

	// Is reference valid  
	IF __ISVALIDREF(FunctionRegistry[eFunctionNumber].rFunction) THEN
		// Build address to reference
		pFunction := ADR(FunctionRegistry[eFunctionNumber].rFunction);

		// If address is possible
		IF pFunction <> 0 THEN
			// Build reference to memory
			rFunction REF= pFunction^;
		ELSE
			// Reinitialize module
			FunctionData.Init:= FALSE;
		END_IF
	ELSE
		// Reinitialize module
		FunctionData.Init:= FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_GetTU_PresentState" Id="{062f6dc9-11c0-459d-a002-9ad0424c7cda}">
      <Declaration><![CDATA[METHOD M_GetTU_PresentState : BOOL
(**************************************************************************************
 * 	Application		:	ProBox
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date: $
 *	Last changed by	:	$Author: q8Hedls $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Get TU present state, overriden from base object
 *
 **************************************************************************************)
VAR_INPUT
	mZone	 : INT; // Zone number
	mPosition : UINT; // Position [mm]
END_VAR
VAR_OUTPUT
	mTU_DataIndex : DINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF(mZone>=1 AND mZone<= NUMBER_OF_ZONES_PER_FUNCTION) THEN
	M_GetTU_PresentState := fbTransportControlEmulation.M_TU_PresentState(mPosition := mPosition, mTU_DataIndex => mTU_DataIndex);
END_IF
]]></ST>
      </Implementation>
    </Method>
    <ObjectProperties>
      <XmlArchive>
        <Data>
          <o xml:space="preserve" t="UMLStereoTypeContainerObject">
            <v n="IsType" t="UMLType">BaseArea</v>
            <v n="Stereotype">""</v>
            <d n="Stereotypes" t="Hashtable" />
          </o>
        </Data>
        <TypeList>
          <Type n="Hashtable">System.Collections.Hashtable</Type>
          <Type n="String">System.String</Type>
          <Type n="UMLStereoTypeContainerObject">{30250973-b110-4e31-b562-c102e042dca4}</Type>
          <Type n="UMLType">{0197b136-405a-42ee-bb27-fd08b621d0cf}</Type>
        </TypeList>
      </XmlArchive>
    </ObjectProperties>
  </POU>
</TcPlcObject>