﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_ShuttleLHD_OLD" Id="{1eab062a-0914-4096-9590-e907c46366a9}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_ShuttleLHD_OLD EXTENDS FB_FunctionConv;
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 417197 $
 *	Revision date	:	$Date: 2018-11-15 16:09:16 +0100 (tor, 15 nov 2018) $
 *	Last changed by	:	$Author: q8aberh $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/ProMove/02_Controls/ProBox/03_Implementation/01_DevelopmentProject/01_Software/ProBox_COE/ProBox_DevProj_v1.0/PLC_1/20_Product/10_App/12_E_Functions/11_ProBox/11_StraightTransport/10_App/FB_ProBoxS $
 *
 *	Purpose			:	LHD function - This function contains an own motor and connection to higher function.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				02.09.2020		MSe					First version of a LHD function. It can be roller drive or chain conveyor
 *
 **************************************************************************************)
VAR_INPUT
	Config					: ST_CFG_ShuttleLHD; // Configuration
	HW_Inputs		 		: ST_HW_InputsShuttleLHD; // Hardware inputs
END_VAR

VAR
	Inputs					: ST_InputsShuttleLHD; // Function specific inputs	
	Idx						: INT; // Iterator
	ZoneIdx					: INT; // Iterator
	Outputs					: ST_OutputsShuttleLHD; // Function specific outputs
	fbTransportControl 		: FB_ProBoxTransportControl; // Transport control for each zone	
	fbProBoxSpeedControl	: FB_ProBoxSpeedControl; // Speed control for each zone
	ZoneOutputs				: ST_OutputsProBoxStraightTransport; // Output commands for each zone	
	EmptyManualCommands		: ST_ITC_ManualMotion; // Empty manual commands
	CreepSpeed				: BOOL; // Change to creep speed	
	ResetSpeed				: BOOL;	// Reset speed
	Speed					: E_ProBoxMotorSpeed; // Feedback speed from element
	SensorOccupied			: BOOL;	// Assigned occupied sensor	
	ElementEnableTransport	: BOOL; // Feedback enable transport from element
	ElementDriveRunning		: BOOL; // Feedback drive is running from element
	InterlockMover			: ST_Interlock;	
	TransportEnable			: BOOL; // Transport enable
	TransportEnableTakeOver : BOOL; // Transport enable takeover 
	TransportEnableHandOver : BOOL; // Transport enable handover
	fb_ProBoxToFunctionOrders : FB_ProBoxToFunctionOrders;
END_VAR

VAR PERSISTENT
	Settings				: ST_CFG_ShuttleLHD; // Settings - activated configuration 
END_VAR

VAR CONSTANT
	ERROR_TRANSCTRL			: INT := 1;
	ERROR_INTERLOCK			: INT := 2;
	ERROR_OVERHANG			: INT := 3;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Input mapping
***************************************************************************************************************)
A_InputMapping();

(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   External Subsystem transport interface
***************************************************************************************************************)
A_ExternalSTI();

(**************************************************************************************************************
   State control
***************************************************************************************************************)
A_StateControl();

(**************************************************************************************************************
   Error handler
***************************************************************************************************************)
A_ErrorHandler();

(**************************************************************************************************************
   Process AddOns
***************************************************************************************************************)
M_ProcessAddOns();


(**************************************************************************************************************
   Common Errors
***************************************************************************************************************)
A_Errors();


(**********************************************************************************************************
   Speed control
***********************************************************************************************************)

CreepSpeed := (Outputs.Drive_Run_12 AND Inputs.Sensor_Creep_12) OR
			  (Outputs.Drive_Run_21 AND Inputs.Sensor_Creep_21) OR				   
			  NOT ZoneData[ZoneIdx].TransportControlData.FastSpeedActive;	

fbProBoxSpeedControl(
		InTakeOver		:= (ZoneData[ZoneIdx].TransportControlData.eTransportState = E_TransportState.TAKEOVER OR 
						   (NOT ZoneData[ZoneIdx].TransportControlData.FastSpeedActive AND 
						   ZoneData[ZoneIdx].TransportControlData.eHandOverState = E_HandOverState.ACTIVE)), 
		CreepSpeed		:= CreepSpeed, 
		MotorRunning	:= fbTransportControl.DriveRunning, 
		ResetSpeed		:= ResetSpeed);

(**********************************************************************************************************
   Assign transport data
***********************************************************************************************************)
A_AssignTransportParam();

(**********************************************************************************************************
   LHD states
***********************************************************************************************************)
//A_LHD_States();


(**********************************************************************************************************
   Transport control
***********************************************************************************************************)

// Assign sensor occpupied

// Initialize
SensorOccupied	:= FALSE;	

// If previous function is avaiable
IF FunctionInterface.In.ePrevFunctionNumber <> 0 THEN			
	// If previous function is on side 1
	IF FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[1] THEN
		SensorOccupied := Inputs.Sensor_Stop_12;			
	// If previous function is on side 2
	ELSIF FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[2] THEN
		SensorOccupied := Inputs.Sensor_Stop_21;
	END_IF;			
ELSE
	SensorOccupied := Inputs.Sensor_Stop_12 OR Inputs.Sensor_Stop_21;				
END_IF;


// Assign feedback from element

// Initialize
ElementEnableTransport := FALSE;
ElementDriveRunning := FALSE;
Speed := E_ProBoxMotorSpeed.NORMAL;	

// Feedback from element
ElementEnableTransport := FunctionInterface.In.Element.State.Enable_12;
ElementDriveRunning := FunctionInterface.In.Element.State.DriveRunning_12;
IF FunctionInterface.In.Element.State.Speed_12 > 50 THEN
	Speed := E_ProBoxMotorSpeed.NORMAL;
ELSIF FunctionInterface.In.Element.State.Speed_12 > 20 AND Speed <= 50 THEN
	Speed := E_ProBoxMotorSpeed.LOW_SPEED;
ELSE
	Speed := E_ProBoxMotorSpeed.CREEP_SPEED;
END_IF	


// Check direction of the conveyor
IF FunctionInterface.In.eNextFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[2] THEN
	SettingsFunction.FlowDirectionREV := FALSE;
ELSIF FunctionInterface.In.eNextFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[1] THEN
	SettingsFunction.FlowDirectionREV := TRUE;
ELSE
	SettingsFunction.FlowDirectionREV := FALSE;
END_IF

// Assign transport enable
TransportEnable := ZoneData[ZoneIdx].AddOn.Orders.Enable  // Addon enables transport
					AND ZoneData[ZoneIdx].ErrorData.ErrorState = E_ErrorState.OPERATIONAL  // Zone has no error or warning	
					AND FunctionData.ErrorData.ErrorState = E_ErrorState.OPERATIONAL  // Function has no error or warning
					AND NOT FunctionInterface.In.Element.State.Error  // Element has no error
					AND ElementEnableTransport
					AND InterlockMover.Enable; // Enable from Mover when it is in State Hold Position
				   
TransportEnableTakeOver := ZoneData[ZoneIdx].AddOn.Orders.EnableTakeover  // Addon enables takeover
						   AND ElementEnableTransport
						   {warning: Prüfen ob Enable oder EnableZone}
						   AND ZoneInterface[ZoneIdx].In.ZoneEnable; // specific enable LHDs seperatlly by mover;
							
TransportEnableHandOver := ZoneData[ZoneIdx].AddOn.Orders.EnableHandover  // Addon enables handover
						   AND ElementEnableTransport
						   {warning: Prüfen ob Enable oder EnableZone}
						   AND ZoneInterface[ZoneIdx].In.ZoneEnable; // specific enable LHDs seperatlly by mover;

		
// Reset FunctionNumber after transport complete		
{warning 'löschen falls nicht benötigt'}		   
//IF ZoneData[ZoneIdx].TransportControlData.eHandOverState = E_HandOverState.COMPLETE
//OR ZoneData[ZoneIdx].TransportControlData.eTakeOverState = E_TakeOverState.COMPLETE
//THEN
//	FunctionInterface.In.ePrevFunctionNumber	:= E_FunctionNumber.F_BEGIN_FUNCTION;
//	FunctionInterface.In.eNextFunctionNumber	:= E_FunctionNumber.F_BEGIN_FUNCTION;
//END_IF

fbTransportControl(
	Param 					:= M_GetTransportControlParam(mZoneNumber := ZoneIdx), // Parameter of transport control
	ParamFirst				:= M_GetTransportControlParam(mZoneNumber := 1),
	ParamLast				:= M_GetTransportControlParam(mZoneNumber := SettingsFunction.NumberOfZones),
	PrevTransportData 		:= M_GetPreviousTransportData(mzoneNumber := ZoneIdx), // Previous transport data
	NextTransportData 		:= M_GetNextTransportData(mZoneNumber := ZoneIdx), // Next transport data        
	TransportEnable			:= TransportEnable, // Transport enable
	TransportEnableTakeOver := TransportEnableTakeOver, // Transport enable takeover 
	TransportEnableHandOver := TransportEnableHandOver, // Transport enable handover
	SensorOccupiedFirst		:= SensorOccupied, // Sensor position occupied
	SensorOccupiedLast		:= FALSE, // Sensor position occupied
	DriveRunning			:= ElementDriveRunning, // Feedback drive is running	
	ZoneData 				:= ZoneData[ZoneIdx], // Zone data
	ErrorData				:= ZoneData[ZoneIdx].ErrorData.ErrorDataSet[ERROR_TRANSCTRL],// Error data
	Speed					:= Speed,		
	TwoSensorFunction	 	:= FALSE,
	TimeOutTake				:= Settings.TimeOutTakeOver);   	

// Output axis 1-2
IF (fbTransportControl.Outputs.DriveTakeoverRun AND FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[1]) OR
   (fbTransportControl.Outputs.DriveHandoverRun AND FunctionInterface.In.eNextFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[2]) THEN
	Outputs.Drive_Run_12 := TRUE;
ELSIF(fbTransportControl.Outputs.DriveTakeoverRun AND FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[2]) OR
	 (fbTransportControl.Outputs.DriveHandoverRun AND FunctionInterface.In.eNextFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[1]) THEN			  
	Outputs.Drive_Run_21 := TRUE;
ELSE
	Outputs.Drive_Run_12 := FALSE;
	Outputs.Drive_Run_21 := FALSE;		
END_IF;

(**********************************************************************************************************
   Message handler zone
***********************************************************************************************************)
M_MsgHandlerZone(mZoneNumber := ZoneIdx);

(**********************************************************************************************************
   ITC Zone output
***********************************************************************************************************)
M_ITC_ProcessOutZone(mZoneNumber := ZoneIdx);

(**************************************************************************************************************
   Interface handler out
***************************************************************************************************************)
M_ITC_ProcessOut();

(**************************************************************************************************************
   Output mapping
***************************************************************************************************************)
A_OutputMapping();	]]></ST>
    </Implementation>
    <Method Name="_M_GetConvDirection" Id="{48ef662a-ce60-43ab-bbc4-4428357ec1be}">
      <Declaration><![CDATA[METHOD PRIVATE _M_GetConvDirection : E_PickupDepositSide
VAR_INPUT
	PDs				: REFERENCE TO ARRAY[1..GVL_ShuttleRewe.MAXDEST] OF ST_CFG_DEST;
	FunctionNumber	: E_FunctionNumber;
END_VAR
VAR
	i:INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[_M_GetConvDirection:= E_PickupDepositSide.Unknown;

IF(FunctionNumber = E_FunctionNumber.F_BEGIN_FUNCTION) THEN 
	RETURN; 
END_IF

FOR i:=1 TO GVL_ShuttleRewe.MAXDEST DO
	IF (PDs[i].eFunctionNumber = FunctionNumber) THEN
		_M_GetConvDirection:= PDs[i].Side;
		RETURN;
	END_IF		
END_FOR]]></ST>
      </Implementation>
    </Method>
    <Action Name="A_AssignTransportParam" Id="{949b2f4d-8761-4a4d-8a6c-792e2bd4d8bb}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_AssignTransportParam
 * 	FUNCTION	Assign the variable transport parameter based to the current side
 **************************************************************************************)
 
(**********************************************************************************************************
	 Get current valid transport parameters
***********************************************************************************************************)
// Init var 
ZoneData[1].TransportControlData.TransportParam.ZoneEndLength := 0;
ZoneData[1].TransportControlData.TransportParam.StopDelayDistance := 0;

// Set zone length
ZoneData[1].TransportControlData.TransportParam.ZoneLength := SettingsFunction.Zone[1].Length[1];

//SettingsFunction.eAdjacentFunctionNumberSide[1] := E_FunctionNumber.F_BEGIN_FUNCTION;
//SettingsFunction.eAdjacentFunctionNumberSide[2]	:= E_FunctionNumber.F_BEGIN_FUNCTION;

// Process for sides 1 and 2 
FOR Idx := 1 TO 2 DO
	// If a previous function number is existing
	IF FunctionInterface.In.ePrevFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION 
		AND FunctionInterface.In.ePrevFunctionNumber < E_FunctionNumber.F_END_FUNCTION
	OR FunctionInterface.In.ePrevFunctionNumber > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION 
		AND FunctionInterface.In.ePrevFunctionNumber < E_FunctionNumber.F_END_EXTERN_FUNCTION
	THEN
		//Set adjacent sides on shuttle conveyor	
		IF _M_GetConvDirection(PDs := Settings.PDs, FunctionNumber := FunctionInterface.In.ePrevFunctionNumber) = E_PickupDepositSide.LeftSide 
		THEN
			//Pickup on right side -> drive Forward 
			SettingsFunction.eAdjacentFunctionNumberSide[1] := FunctionInterface.In.ePrevFunctionNumber;
			SettingsFunction.eAdjacentFunctionNumberSide[2] := E_FunctionNumber.F_BEGIN_FUNCTION;	
		ELSE                 
			//Pickup on left side -> drive Reverse
			SettingsFunction.eAdjacentFunctionNumberSide[1] := E_FunctionNumber.F_BEGIN_FUNCTION;
			SettingsFunction.eAdjacentFunctionNumberSide[2] := FunctionInterface.In.ePrevFunctionNumber;			
		END_IF
		
		// Update stop delay distance based on previous function number
		IF FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[Idx] THEN
			ZoneData[1].TransportControlData.TransportParam.StopDelayDistance := SettingsFunction.Zone[1].StopDelayDistanceFromSide[Idx];
		END_IF
	END_IF

	// If the zone is occupied	
	IF ZoneData[1].TransportControlData.Occupied THEN
		// If a next function number is existing
		IF FunctionInterface.In.eNextFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION 
			AND FunctionInterface.In.eNextFunctionNumber < E_FunctionNumber.F_END_FUNCTION 
		OR FunctionInterface.In.eNextFunctionNumber > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION 
			AND FunctionInterface.In.eNextFunctionNumber < E_FunctionNumber.F_END_EXTERN_FUNCTION 
		THEN
			//Set adjacent sides on shuttle conveyor	
			IF _M_GetConvDirection(PDs := Settings.PDs, FunctionNumber := FunctionInterface.In.eNextFunctionNumber) = E_PickupDepositSide.LeftSide 
			THEN
				//Pickup on right side -> drive Forward 
				SettingsFunction.eAdjacentFunctionNumberSide[1] := FunctionInterface.In.eNextFunctionNumber;
				SettingsFunction.eAdjacentFunctionNumberSide[2] := E_FunctionNumber.F_BEGIN_FUNCTION;	
			ELSE                 
				//Pickup on left side -> drive Reverse
				SettingsFunction.eAdjacentFunctionNumberSide[1] := E_FunctionNumber.F_BEGIN_FUNCTION;
				SettingsFunction.eAdjacentFunctionNumberSide[2] := FunctionInterface.In.eNextFunctionNumber;			
			END_IF
			
			// Update zone end length based on next function number
			IF FunctionInterface.In.eNextFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[Idx] THEN
				ZoneData[1].TransportControlData.TransportParam.ZoneEndLength := SettingsFunction.Zone[1].ZoneEndLengthToSide[Idx];
			END_IF
		END_IF
	END_IF
END_FOR

// If the zone is not occupied	
IF NOT ZoneData[1].TransportControlData.Occupied THEN
	// If a previous function number is existing
	IF FunctionInterface.In.ePrevFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION 
		AND FunctionInterface.In.ePrevFunctionNumber < E_FunctionNumber.F_END_FUNCTION 
	OR FunctionInterface.In.ePrevFunctionNumber > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION 
		AND FunctionInterface.In.ePrevFunctionNumber < E_FunctionNumber.F_END_EXTERN_FUNCTION
	THEN
		// Update zone end length based on previous function number
		IF FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[1] THEN
			ZoneData[1].TransportControlData.TransportParam.ZoneEndLength := SettingsFunction.Zone[1].ZoneEndLengthToSide[2];
		ELSIF FunctionInterface.In.ePrevFunctionNumber = SettingsFunction.eAdjacentFunctionNumberSide[2] THEN
			ZoneData[1].TransportControlData.TransportParam.ZoneEndLength := SettingsFunction.Zone[1].ZoneEndLengthToSide[1];
		END_IF
	END_IF
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_Errors" Id="{88b5b3d2-8480-4ed5-823e-1b8ac2ae0791}">
      <Implementation>
        <ST><![CDATA[

F_ResetError(Reset:= FALSE, ErrorData:= FunctionData.ErrorData.ErrorDataSet[ERROR_TRANSCTRL]);
F_ResetError(Reset:= FALSE, ErrorData:= FunctionData.ErrorData.ErrorDataSet[ERROR_INTERLOCK]);
F_ResetError(Reset:= FALSE, ErrorData:= FunctionData.ErrorData.ErrorDataSet[ERROR_OVERHANG]);


(***********************************************************************)
// Check reference is valid
IF SettingsFunction.FunctionLink.eFunctionNumber <> E_FunctionNumber.F_BEGIN_FUNCTION
AND __ISVALIDREF(FunctionRegistry[SettingsFunction.FunctionLink.eFunctionNumber].rFunctionInterface) 
THEN
	InterlockMover := F_GetFunctionInterlock(eFunctionNumber:= SettingsFunction.FunctionLink.eFunctionNumber);
ELSE
	F_SetError(
			ErrorType 	:= E_ErrorType.ERROR,
			ErrorParam 	:= 'Interlock function not reachable',
			ErrorMsg 	:= E_ErrorConv.CONNECTION_ERROR,
			ErrorData	:= FunctionData.ErrorData.ErrorDataSet[ERROR_INTERLOCK]); 
END_IF



// Check overhang sensors
IF NOT Inputs.OverhangOK 
AND NOT InterlockMover.Enable
THEN
	F_SetError(
		ErrorType 	:= E_ErrorType.ERROR,
		ErrorParam 	:= 'Gap check sensors not free',
		ErrorMsg 	:= E_ErrorConv.GAP_CHECK_ACTIVE,
		ErrorData	:= FunctionData.ErrorData.ErrorDataSet[ERROR_OVERHANG]); 
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_ExternalSTI" Id="{c4e3d419-3816-40e9-b063-f355e7981c48}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_ExternalSTI
 * 	FUNCTION	External Subsystem transport interface
 **************************************************************************************)


(**************************************************************************************
   External Subsystem transport interfaces
***************************************************************************************)
FOR Idx := 1 TO NUMBER_OF_STI DO
	// Is an external Subsystem transport interface configured
	IF SettingsFunction.eExternalSTI_FunctionNumber[Idx] > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION AND
	   SettingsFunction.eExternalSTI_FunctionNumber[Idx] < E_FunctionNumber.F_END_EXTERN_FUNCTION THEN
	   
		// When interface is valid
		IF __QUERYINTERFACE(fbSTI_Channels[SettingsFunction.eExternalSTI_FunctionNumber[Idx]].Instance,FunctionBase.STI_Instance) THEN
			// Call external inbound interface
			fbSTI_Channels[SettingsFunction.eExternalSTI_FunctionNumber[Idx]].Instance.M_CallInterface(
				mErrorData				:= FunctionData.ErrorData.ErrorDataSet[1]);
		END_IF
	END_IF
END_FOR
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_Init" Id="{18785b49-2767-40ce-bceb-1e545a98cce4}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize function
 **************************************************************************************)
 
// Update Registry
M_UpdateRegistry();
 

// Init ongoing - Wait until init is released (Sequencial startup)
IF NOT FunctionData.Init AND FunctionData.InitRunning THEN
	// When the subsystem is started, 
	// When its released to initialize
	IF FunctionData.OperationState.Info.SystemReady AND
	   SettingsFunction.eFunctionNumber < FunctionData.OperationState.InitRelease.eLimitFunctionNumber THEN
	   
	   IF M_InitFunction(FALSE) THEN
			// Function initialized - and initialization done
			FunctionData.Init := TRUE;
			FunctionData.InitRunning := FALSE;
			
			// Set reset cmd
			FunctionData.ErrorData.Error.ResetError := TRUE;
			
			DebugMsg := CONCAT('Initialization done. Function: ', ConfigFunction.FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
		END_IF
	END_IF
(*
If the function is not initialized,
clean all values and wait until init is released
*)
ELSIF NOT FunctionData.Init THEN
	// Reset internal variables
	FunctionData.OperationState := FunctionBase.PositionStateEmpty;

	// Initiate initialization of all zones
	FOR ZoneIdx := 1 TO SettingsFunction.NumberOfZones DO
		ZoneData[ZoneIdx].Init := FALSE;
	END_FOR;
	
	// Load function specific configuration to settings
	Settings := Config;
		
	// the LHD needs a functionlink to a mover/lift
	IF SettingsFunction.FunctionLink.eFunctionNumber = E_FunctionNumber.F_BEGIN_FUNCTION
	THEN
		DebugMsg := CONCAT('FunctionLink is missing. FunctionNumber: ', SettingsFunction.FunctionName);
		fbDebugMsg.M_SendInfoMsg(DebugMsg);
	END_IF
	
	// Reset values and load config
	M_PreInit();
	
	// To start init a valid functionnumber and element link is needed
	FunctionData.InitRunning := SettingsFunction.eFunctionNumber > 0 AND
								SettingsFunction.eElementLink > 0;
END_IF

// Init zones
IF FunctionData.Init THEN
	// Set init flag after one cycle
	FOR ZoneIdx := 1 TO SettingsFunction.NumberOfZones DO
		// If a zone is not initialized, wait one cycle to finish init
		IF NOT ZoneData[ZoneIdx].Init THEN
			IF ZoneData[ZoneIdx].InitRunning THEN
				ZoneData[ZoneIdx].InitRunning := FALSE;
				ZoneData[ZoneIdx].Init := TRUE;
				ZoneData[ZoneIdx].ErrorData.Error.ResetError := TRUE;
			ELSE	
				ZoneData[ZoneIdx].InitRunning := TRUE;
			END_IF
		END_IF
	END_FOR;
END_IF

ZoneIdx := 1;]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InputMapping" Id="{3924a612-25e3-4382-a5b8-444a9ead331b}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InputMapping
 * 	FUNCTION	Input mapping
 **************************************************************************************)
 
(**************************************************************************************
   Hardware Inputs
***************************************************************************************)
// Adapt input logic state high/low
Inputs.Sensor_Stop_12		:= Settings.HW_InputInverted.Sensor_Stop_12 XOR HW_Inputs.Sensor_Stop_12;
Inputs.Sensor_First_12		:= Settings.HW_InputInverted.Sensor_First_12 XOR HW_Inputs.Sensor_First_12;
Inputs.Sensor_Slow_12		:= Settings.HW_InputInverted.Sensor_Slow_12 XOR HW_Inputs.Sensor_Slow_12;
Inputs.Sensor_Creep_12		:= Settings.HW_InputInverted.Sensor_Creep_12 XOR HW_Inputs.Sensor_Creep_12;
Inputs.Sensor_Stop_21		:= Settings.HW_InputInverted.Sensor_Stop_21 XOR HW_Inputs.Sensor_Stop_21;
Inputs.Sensor_First_21		:= Settings.HW_InputInverted.Sensor_First_21 XOR HW_Inputs.Sensor_First_21;
Inputs.Sensor_Slow_21 		:= Settings.HW_InputInverted.Sensor_Slow_21 XOR HW_Inputs.Sensor_Slow_21;
Inputs.Sensor_Creep_21		:= Settings.HW_InputInverted.Sensor_Creep_21 XOR HW_Inputs.Sensor_Creep_21;
Inputs.OverhangOK			:= HW_Inputs.Sensor_Overhang_12 AND HW_Inputs.Sensor_Overhang_21;

(**************************************************************************************
   Element states
***************************************************************************************)
// Get status of the corresponding element
M_GetElementStates(); 

(**************************************************************************************
   AddOn states
***************************************************************************************)
// Get status of all corresponding AddOn functions and store it in FunctionInterface.In.AddOnOrder
M_GetAddOnOrder();

]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OutputMapping" Id="{9b1010d5-3f36-4020-91ab-ce28b9ba3a20}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OutputMapping
 * 	FUNCTION	Output mapping
 **************************************************************************************)
 
(**************************************************************************************
   Hardware outputs
***************************************************************************************)


(**************************************************************************************
   Order outputs (Commands to corresponding element)
***************************************************************************************)

fb_ProBoxToFunctionOrders(
	CreepSpeed_12 := (fbProBoxSpeedControl.Speed = E_ProBoxMotorSpeed.CREEP_SPEED), 
	LowSpeed_12 := (fbProBoxSpeedControl.Speed = E_ProBoxMotorSpeed.LOW_SPEED), 
	Forward_12 := Outputs.Drive_Run_12, 
	Reverse_12 := Outputs.Drive_Run_21, 
	CreepSpeed_34 := FALSE, 
	LowSpeed_34 := FALSE, 
	Forward_34 := FALSE, 
	Reverse_34 := FALSE);
	
// Initialize variables
FunctionInterface.Out.FunctionOrders.Enable := TRUE;
FunctionInterface.Out.FunctionOrders.ReqDriveRun := FALSE;
FunctionInterface.Out.FunctionOrders.ReqDriveDirection := FALSE;
FunctionInterface.Out.FunctionOrders.ReqSpeed := 0;
FunctionInterface.Out.FunctionOrders.Enable_34 := TRUE;
FunctionInterface.Out.FunctionOrders.ReqDriveRun_34 := FALSE;
FunctionInterface.Out.FunctionOrders.ReqDriveDirection_34 := FALSE;
FunctionInterface.Out.FunctionOrders.ReqSpeed_34 := 0;	


FunctionInterface.Out.FunctionOrders.Enable := FunctionData.ErrorData.Error.ErrorType = E_ErrorType.NO_ERROR_PENDING 
												AND NOT FunctionData.ErrorData.ZoneErrorPending;;
FunctionInterface.Out.FunctionOrders.ReqDriveRun := fb_ProBoxToFunctionOrders.FunctionOrders.ReqDriveRun;
FunctionInterface.Out.FunctionOrders.ReqSpeed := fb_ProBoxToFunctionOrders.FunctionOrders.ReqSpeed;
FunctionInterface.Out.FunctionOrders.ReqDriveDirection := fb_ProBoxToFunctionOrders.FunctionOrders.ReqDriveDirection;

//Manual mode
//IF(FunctionData.OperationState.eMode = E_PositionMode.MANUAL_MODE) THEN
//	FunctionInterface.Out.FunctionOrders.Enable_12	:=NOT _M_IsErrorActive();
//	FunctionInterface.Out.FunctionOrders.ReqDriveRun_12 := HW_Inputs.ShuttleAP100.ConvAFwd OR HW_Inputs.ShuttleAP100.ConvARev;
//	FunctionInterface.Out.FunctionOrders.ReqDriveDirection_12 := HW_Inputs.ShuttleAP100.ConvAFwd AND NOT HW_Inputs.ShuttleAP100.ConvARev;	// TRUE = FWD
//	FunctionInterface.Out.FunctionOrders.ReqSpeed_12 := 100; // normal speed E_ProBoxMotorSpeed.NORMAL
//	
//	FunctionInterface.Out.FunctionOrders.Enable_34	:=NOT _M_IsErrorActive();
//	FunctionInterface.Out.FunctionOrders.ReqDriveRun_34 := HW_Inputs.ShuttleAP100.ConvBFwd OR HW_Inputs.ShuttleAP100.ConvBRev;
//	FunctionInterface.Out.FunctionOrders.ReqDriveDirection_34 := HW_Inputs.ShuttleAP100.ConvBFwd AND NOT HW_Inputs.ShuttleAP100.ConvBRev;	// TRUE = FWD
//	FunctionInterface.Out.FunctionOrders.ReqSpeed_34 := 100; // normal speed E_ProBoxMotorSpeed.NORMAL
//END_IF

(**************************************************************************************
   AddOn outputs
***************************************************************************************)
// This function provide no AddOn functionality
;
]]></ST>
      </Implementation>
    </Action>
  </POU>
</TcPlcObject>