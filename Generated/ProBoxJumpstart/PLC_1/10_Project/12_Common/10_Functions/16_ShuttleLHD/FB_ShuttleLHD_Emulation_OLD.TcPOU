﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_ShuttleLHD_Emulation_OLD" Id="{c1280b1b-59ac-464a-84a4-e6401677f0c2}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_ShuttleLHD_Emulation_OLD EXTENDS FB_FunctionConvEmulation
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date: 2021-01-20 14:48:51 +0200 (Do, 09 Aug 2018) $
 *	Last changed by	:	$Author: g7seggm $
 *	URL				:	$$
 *
 *	Purpose			:	Emulation LHD function with one zone logic.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				20.01.2021		MSe					First Version of LHD emulation
 * 
 **************************************************************************************)
 VAR
	pFunction						: POINTER TO FB_ShuttleLHD_OLD; // Address to function
	rFunction						: REFERENCE TO FB_ShuttleLHD_OLD; // Reference to function
	fbTransportControlEmulation		: FB_PB_TransportControlEmulation; // Transport control emulation	
    Sensor                      	: BOOL; // Example sensor on element level
	ZoneIdx							: INT;
	ElementDriveRunning				: BOOL; // Feedback drive is running from element
	ElementSpeed					: INT; // Feedback speed from element	
	SensorFirst						: BOOL; //test
	SensorStop						: BOOL; //test
	EmuSensors						: BOOL;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();

(**************************************************************************************************************
   Emulation
***************************************************************************************************************)
// If emulation function is initialized
IF FunctionData.Init THEN

	// Update data of real object
	M_UpdateRealObjectData();
	
	// Handle external Subsystem-Subsystem interfaces
	A_ExternalSTI();
	
	// Assign feedback from element
	ElementDriveRunning := FunctionBase.rFunctionInterfaceReal.In.Element.State.DriveRunning_12 
							AND FunctionBase.rFunctionInterfaceReal.Out.FunctionOrders.ReqDriveRun_12;
	ElementSpeed 		:= FunctionBase.rFunctionInterfaceReal.In.Element.State.Speed_12;
	
	// Call transport control 
	fbTransportControlEmulation(
		Param 					:= M_GetTransportControlParam(mZoneNumber := 1), // Parameter of transport control
		PrevTransportData 		:= M_GetPreviousTransportData(mZoneNumber := 1), // Previous transport data
		NextTransportData 		:= M_GetNextTransportData(mZoneNumber := 1), // Next transport data	
		TransportEnable			:= TRUE, // Transport enable	
		DriveRunning			:= ElementDriveRunning, // rFunction.Outputs.Drive_Run_12 OR rFunction.Outputs.Drive_Run_21, // Feedback drive is running
		CurrentSpeed 			:= 300, //ElementSpeed, // Current speed [mm/s]
		ZoneData 				:= ZoneData[1]); // Zone data
			
A_OverWriteInputs();	

END_IF]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{332386d0-13de-4e97-8944-5e8ee43e905d}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated dummy function
 **************************************************************************************)

// If dummy function is not initialized
IF NOT FunctionData.Init Then
	// Check that linked dummy function number is valid
	IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
	   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN   
		
		// Check if the real module is initialized
		IF F_IsFunctionInitialized(eFunctionNumber := eFunctionNumber) THEN
			// Module is initialized
			FunctionData.Init:= TRUE;
			
			// Send debug message
			DebugMsg := CONCAT('Initialization done. Element Function: ', FunctionRegistry[eFunctionNumber].FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
			
			// Initialize real module
			F_InitializeFunction(eFunctionNumber := eFunctionNumber);
		END_IF
	END_IF
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs" Id="{29577b57-8e61-4fd7-b03b-34d0463aa8bd}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OverWriteInputs
 * 	FUNCTION	Overwrite inputs for emulation
 **************************************************************************************)
ZoneData[1].TransportControlData.TransportParam.ZoneLength := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1];	 
//ZoneData[rFunction.SettingsFunction.NumberOfZones].TransportControlData.TransportParam.ZoneLength := FunctionBase.rSettingsFunctionReal.Zone[rFunction.SettingsFunction.NumberOfZones].Length[1];


EmuSensors :=  fbTransportControlEmulation.M_TU_PresentState(
											mPosition := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[2],
											mIndex => FunctionBase.IndexToRemove[1],
											mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos);


IF NOT FunctionBase.rSettingsFunctionReal.FlowDirectionREV THEN

	sensorFirst := fbTransportControlEmulation.M_TU_PresentState(
												mPosition := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[2]-200,
												mIndex => FunctionBase.IndexToRemove[1],
												mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos);

								
	
	sensorStop := fbTransportControlEmulation.M_TU_PresentState(
													mPosition := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[2],
													mIndex => FunctionBase.IndexToRemove[1],
													mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos);
													

	//rFunction.HW_Inputs.V2V1_Occupied_21 := FALSE;
	//rFunction.HW_Inputs.SensorOccupied_21 := FALSE;
												
											
ELSE
	
		//reverse...

	sensorFirst := fbTransportControlEmulation.M_TU_PresentState(
												mPosition := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[1]-200,
												mIndex => FunctionBase.IndexToRemove[1],
												mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos);

	sensorStop := fbTransportControlEmulation.M_TU_PresentState(
												mPosition := FunctionBase.rSettingsFunctionReal.Zone[rFunction.SettingsFunction.NumberOfZones].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[rFunction.SettingsFunction.NumberOfZones].ZoneEndLengthToSide[1],
												mIndex => FunctionBase.IndexToRemove[1],
												mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos);
	
	//rFunction.HW_Inputs.V2V1_Occupied_12 := FALSE;
	//rFunction.HW_Inputs.SensorOccupied_12  := FALSE;
											
END_IF

//Set HW outputs, Keep input if direction is changed.
rFunction.HW_Inputs.Sensor_First_12 := sensorFirst AND(NOT FunctionBase.rSettingsFunctionReal.FlowDirectionREV OR rFunction.HW_Inputs.Sensor_First_12);
rFunction.HW_Inputs.Sensor_First_21 := sensorFirst AND(FunctionBase.rSettingsFunctionReal.FlowDirectionREV OR rFunction.HW_Inputs.Sensor_First_21);
rFunction.HW_Inputs.Sensor_Stop_12 := sensorStop AND(NOT FunctionBase.rSettingsFunctionReal.FlowDirectionREV OR rFunction.HW_Inputs.Sensor_Stop_12);
rFunction.HW_Inputs.Sensor_Stop_21 := sensorStop AND(FunctionBase.rSettingsFunctionReal.FlowDirectionREV OR rFunction.HW_Inputs.Sensor_Stop_21);
rFunction.HW_Inputs.Sensor_Overhang_12 := TRUE;
rFunction.HW_Inputs.Sensor_Overhang_21 := TRUE;]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{157bec91-e783-4244-9560-e0e84f24c120}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid function number
IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN

	// Is reference valid  
	IF __ISVALIDREF(FunctionRegistry[eFunctionNumber].rFunction) THEN
		// Build address to reference
		pFunction := ADR(FunctionRegistry[eFunctionNumber].rFunction);

		// If address is possible
		IF pFunction <> 0 THEN
			// Build reference to memory
			rFunction REF= pFunction^;
		ELSE
			// Reinitialize module
			FunctionData.Init:= FALSE;
		END_IF
	ELSE
		// Reinitialize module
		FunctionData.Init:= FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_GetTU_PresentState" Id="{04b614dd-0393-4691-a551-0ff04eb0f8d8}">
      <Declaration><![CDATA[METHOD M_GetTU_PresentState : BOOL
(**************************************************************************************
 * 	Application		:	ProBox
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date: $
 *	Last changed by	:	$Author: q8Hedls $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Get TU present state, overriden from base object
 *
 **************************************************************************************)
VAR_INPUT
	mZone	 : INT; // Zone number
	mPosition : UINT; // Position [mm]
END_VAR
VAR_OUTPUT
	mTU_DataIndex : DINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF(mZone>=1 AND mZone<= NUMBER_OF_ZONES_PER_FUNCTION) THEN
	M_GetTU_PresentState := fbTransportControlEmulation.M_TU_PresentState(mPosition := mPosition, mTU_DataIndex => mTU_DataIndex);
END_IF
]]></ST>
      </Implementation>
    </Method>
    <ObjectProperties>
      <XmlArchive>
        <Data>
          <o xml:space="preserve" t="UMLStereoTypeContainerObject">
            <v n="IsType" t="UMLType">BaseArea</v>
            <v n="Stereotype">""</v>
            <d n="Stereotypes" t="Hashtable" />
          </o>
        </Data>
        <TypeList>
          <Type n="Hashtable">System.Collections.Hashtable</Type>
          <Type n="String">System.String</Type>
          <Type n="UMLStereoTypeContainerObject">{30250973-b110-4e31-b562-c102e042dca4}</Type>
          <Type n="UMLType">{0197b136-405a-42ee-bb27-fd08b621d0cf}</Type>
        </TypeList>
      </XmlArchive>
    </ObjectProperties>
  </POU>
</TcPlcObject>