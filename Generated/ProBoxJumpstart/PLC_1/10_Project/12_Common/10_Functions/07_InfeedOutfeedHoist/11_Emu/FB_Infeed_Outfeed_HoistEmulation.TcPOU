﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_Infeed_Outfeed_HoistEmulation" Id="{d846a104-4a99-4fb5-bb7c-bdaf15a5d9c9}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_Infeed_Outfeed_HoistEmulation EXTENDS FB_FunctionConvEmulation
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 411985 $
 *	Revision date	:	$Date: 2018-08-09 14:48:51 +0200 (Do, 09 Aug 2018) $
 *	Last changed by	:	$Author: p8fiscp $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Emulation Infeed Outfeed Hoist function with one zone.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				19.04.2018		PkF					First Version of ProBox straight emulation
 *  0.2				14.11.2018		jko					names adapted according convention
 * 
 **************************************************************************************)
 VAR
	pFunction						: POINTER TO FB_Infeed_Outfeed_Hoist; // Address to function
	rFunction						: REFERENCE TO FB_Infeed_Outfeed_Hoist; // Reference to function
	fbTransportControlEmulation		: FB_TransportControlEmulation_CustomReverse; // Transport control emulation
	fbEncoder						: FB_PB_Element_Encoder();
	ElementDriveRunning				: BOOL; // Feedback drive is running from element
	ElementSpeed					: INT; // Feedback speed from element	
	SensorFirst						: BOOL; //test
	SensorStop						: BOOL; //test
	EmuSensors						: ARRAY [1..NUMBER_OF_ZONES_PER_FUNCTION] OF BOOL;
	DebugSensors					: ARRAY [1..2] OF BOOL; //Slug / Stop sensor
	
	ElementReverse					: BOOL; // True = Backwards, False = Forward
	
	fbHoistEmulation				: FB_EccentricHoistControlEmulation;
	force_Sensor_Stop_12		  	: BOOL; // Sensor stop first axis 1-2 forward
	force_TuRecognitionBottom     	: BOOL; // Tu recognition in lift position down
	force_TuRecognitionTop        	: BOOL; // Tu recognition in lift position up
	force_SwitchInfeed   			: BOOL; // Switch mode between Infeed and Outfeed manually
	force_SwitchOutfeed     		: BOOL; // Switch mode between Infeed and Outfeed manually
	force_OccupiedAndMaxGap       	: BOOL; // Infeed: Detect the max length of 2 Rollcontainers with gap Outfeed: Sensor_Stop
	force_Lower_Position_Reached  	: BOOL; // Lower position reached (0 mm)
	force_Upper_Position_Reached  	: BOOL; // Upper position reached (300 mm)
	force_ForkDetection           	: BOOL; // Detection of the fork to infeed or outfeed TU's
		
	mPosition             			: UINT  := 0; // temp for mapping
	
	allowLeadingEdgeReverse			: BOOL;
	rZoneDataReal					: REFERENCE TO ST_ZoneData;
	
	// For ATQ RTQ Buttons
	_ATQ            	: BOOL                              := FALSE;     // touch field
	_ATQ_Last       	: BOOL                              := FALSE;     // edge detection
	_RTQ            	: BOOL                              := FALSE;     // touch field
	_RTQ_Last       	: BOOL                              := FALSE;     // edge detection
	_Count_ATQ			: ULINT 							:= 0; // counts all send ATQs
	_Count_RTQ			: ULINT 							:= 0; // counts all send RTQs
	_FunctionName 		: STRING(NAME_LENGTH);
	_Position			: STRING(Conveyor.POSITION_STRINGLENGTH); // SIS position name MM-000-0000-000
	ATQData     		: ST_ITC_ATQ;
	RTQData     		: ST_ITC_RTQ;
	DestStart           : POINTER TO BYTE                  := 0;     // = ADR(MyString)                    -> start position to concat
  	DestEnd             : POINTER TO BYTE                  := 0;     // = ADR(MyString) + SIZEOF(MyString) -> buffer/string end
	
	Delme             			: BOOL; // temp for mapping
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();

(**************************************************************************************************************
   Emulation
***************************************************************************************************************)
// If emulation function is initialized
IF FunctionData.Init THEN

	// Update data of real object
	M_UpdateRealObjectData();
	
	// Handle external Subsystem-Subsystem interfaces
	A_ExternalSTI();
	
	// Emulation Hoist
	fbHoistEmulation.Drive_Hoist := rFunction.fbHoistControl.P_MotorRun;
	fbHoistEmulation();
	
	
	//_realTransportData := rFunction.M_GetTransportData(rFunction.ConfigFunction.eFunctionNumber);
	
	
	IF __ISVALIDREF(FunctionRegistry[eFunctionNumber].rFunction) THEN
		rZoneDataReal	REF= FunctionRegistry[eFunctionNumber].rFunction.ZoneData[1];
	ELSE
		rZoneDataReal REF= FunctionBase.ZoneDataEmpty[1];
	END_IF
	
	allowLeadingEdgeReverse :=rZoneDataReal.TransportControlData.eHandOverState <> E_HandOverState.ACTIVE 
							AND rZoneDataReal.TransportControlData.eTakeOverState <> E_TakeOverState.ACTIVE;	
				
	// Assign feedback from element
	IF rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_12 OR rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_21 THEN
		ElementDriveRunning := FunctionBase.rFunctionInterfaceReal.In.Element.State.DriveRunning AND FunctionBase.rFunctionInterfaceReal.Out.FunctionOrders.ReqDriveRun;
		ElementSpeed := FunctionBase.rFunctionInterfaceReal.In.Element.State.Speed;
		ElementReverse := NOT FunctionBase.rFunctionInterfaceReal.Out.FunctionOrders.ReqDriveDirection AND allowLeadingEdgeReverse;
	ELSIF rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_34 OR rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_43 THEN
		ElementDriveRunning := FunctionBase.rFunctionInterfaceReal.In.Element.State.DriveRunning_34 AND FunctionBase.rFunctionInterfaceReal.Out.FunctionOrders.ReqDriveRun_34;
		ElementSpeed := FunctionBase.rFunctionInterfaceReal.In.Element.State.Speed_34;
	ELSE
		ElementDriveRunning := FALSE;
		ElementSpeed := 0;
	END_IF
	
	//Run encoder positon, 
	fbEncoder(	Run:=ElementDriveRunning,
				Speed:= ProBoxSystem.EMULATION_SPEED);
			
	// Call transport control 
	fbTransportControlEmulation(
		Param 					:= M_GetTransportControlParam(mZoneNumber := 1), // Parameter of transport control
		PrevTransportData 		:= M_GetPreviousTransportData(mZoneNumber := 1), // Previous transport data
		NextTransportData 		:= M_GetNextTransportData(mZoneNumber := 1), // Next transport data	
		TransportEnable			:= TRUE, // Transport enable	
		DriveRunning			:= ElementDriveRunning,
		EncoderValue			:= fbEncoder.EncoderValue,
		ZoneData 				:= ZoneData[1],// Zone data
		ElementReverse 			:= ElementReverse); 
		


A_OverWriteInputs();	

END_IF]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{48e80a4d-62ba-4ae4-9480-df9d7e99dd04}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated dummy function
 **************************************************************************************)

// If dummy function is not initialized
IF NOT FunctionData.Init Then
	// Check that linked dummy function number is valid
	IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
	   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN   
		
		// Check if the real module is initialized
		IF F_IsFunctionInitialized(eFunctionNumber := eFunctionNumber) THEN
			// Module is initialized
			FunctionData.Init:= TRUE;
			
			// Send debug message
			DebugMsg := CONCAT('Initialization done. Element Function: ', FunctionRegistry[eFunctionNumber].FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
			
			// Initialize real module
			F_InitializeFunction(eFunctionNumber := eFunctionNumber);
		END_IF
	END_IF
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs" Id="{01e24c3f-ca71-428d-aa03-5af322d13cc3}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OverWriteInputs
 * 	FUNCTION	Overwrite inputs for emulation
 **************************************************************************************)

// ATQ
IF (    (_ATQ      = TRUE)
	   AND(_ATQ_Last = FALSE)  )
  THEN
	_FunctionName := F_GetFunctionName(rFunction.ConfigFunction.eFunctionNumber, 1);
	_FunctionName := DELETE(_FunctionName,1,16);
	_FunctionName := INSERT(_FunctionName,'1',15);
	_FunctionName := DELETE(_FunctionName,1,15);
	_FunctionName := DELETE(_FunctionName,1,12);
	_FunctionName := DELETE(_FunctionName,1,7);
	_FunctionName := DELETE(_FunctionName,1,3);
	_Position 	  := _FunctionName;
	
	ATQData.Position       := _Position;
	ATQData.Weight         := 125;
	ATQData.Length         := 500;
	ATQData.Width          := 722;
	ATQData.Height         := 1740;
	ATQData.LabelPresent   := 1;
	//------------------------------------------------
	DestStart:= ADR(ATQData.Label);
	DestEnd  := ADR(ATQData.Label) + SIZEOF(ATQData.Label);
	DestStart:= F_StringCopy     (DestStart:= DestStart,  DestEnd:= DestEnd,  Source:= ADR(_Position)  );
	DestStart:= F_StringCopy     (DestStart:= DestStart,  DestEnd:= DestEnd,  Source:= ADR('_')  );

	DestStart:= F_StringFromUlint(DestStart:= DestStart,  DestEnd:= DestEnd,  Value:= _Count_ATQ,  Length:= 5 );
	
	//------------------------------------------------
	fbITC_ConveyorEmulation.M_AddData( meChannelSrc:= 0,
									   mMsgType    := E_ITC_MsgTypeConv.E_ADD_TU_REQUEST,
									   mpData      := ADR   (ATQData),
									   mLen        := SIZEOF(ATQData));
	//------------------------------------------------
	_Count_ATQ:= _Count_ATQ + 1;
  END_IF
  
  _ATQ_Last:= _ATQ;
  
  // RTQ
IF (    (_RTQ      = TRUE)
	   AND(_RTQ_Last = FALSE)  )
  THEN
	_FunctionName := F_GetFunctionName(rFunction.ConfigFunction.eFunctionNumber, 1);
	_FunctionName := DELETE(_FunctionName,1,16);
	_FunctionName := INSERT(_FunctionName,'1',15);
	_FunctionName := DELETE(_FunctionName,1,15);
	_FunctionName := DELETE(_FunctionName,1,12);
	_FunctionName := DELETE(_FunctionName,1,7);
	_FunctionName := DELETE(_FunctionName,1,3);
	_Position 	  := _FunctionName;
	
	RTQData.Position       := _Position;
	ATQData.LabelPresent   := 1;
	//------------------------------------------------
	DestStart:= ADR(RTQData.Label);
	DestEnd  := ADR(RTQData.Label) + SIZEOF(RTQData.Label);
	DestStart:= F_StringCopy     (DestStart:= DestStart,  DestEnd:= DestEnd,  Source:= ADR(_Position)  );
	DestStart:= F_StringCopy     (DestStart:= DestStart,  DestEnd:= DestEnd,  Source:= ADR('_')  );

	DestStart:= F_StringFromUlint(DestStart:= DestStart,  DestEnd:= DestEnd,  Value:= _Count_RTQ,  Length:= 5 );
	
	//------------------------------------------------
	fbITC_ConveyorEmulation.M_AddData( meChannelSrc:= 0,
									   mMsgType    := E_ITC_MsgTypeConv.E_REMOVE_TU_REQUEST,
									   mpData      := ADR   (RTQData),
									   mLen        := SIZEOF(RTQData));
	//------------------------------------------------
	_Count_RTQ:= _Count_RTQ + 1;
  END_IF
  
  _RTQ_Last:= _RTQ;

 
//****************EMULATE INPUTS***************** 
 
ZoneData[1].TransportControlData.TransportParam.ZoneLength := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1];	

EmuSensors[1] :=  fbTransportControlEmulation.M_TU_PresentState(
											mPosition := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[2],
											mIndex => FunctionBase.IndexToRemove[1],
											mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos);

//Set Sensor at first zone
//IF(rFunction.SettingsFunction.NumberOfZones>1 AND rFunction.Settings.TwoSensorFunction) THEN
//	IF(FunctionBase.rSettingsFunctionReal.FlowDirectionREV) THEN
//		//RWD drive direction
//		SensorFirst:= EmuSensors[rFunction.SettingsFunction.NumberOfZones];
//	ELSE
//		//FWD drive direction
//		SensorFirst:= EmuSensors[1];
//	END_IF	
//END_IF
//Set sensor at last zone
IF(FunctionBase.rSettingsFunctionReal.FlowDirectionREV) THEN
	//RWD drive direction
	SensorStop := EmuSensors[1];
ELSE
	//FWD drive direction
	SensorStop := EmuSensors[rFunction.SettingsFunction.NumberOfZones];
END_IF

// Write Sensor Sensor_Stop_12  at 900 tu leading edge
IF rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.Infeed 
OR rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.Outfeed 
OR ((rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.InOutfeed) 
	AND rFunction.HW_Inputs.SwitchInfeed) 
THEN
	rFunction.HW_Inputs.Sensor_Stop_12 := rFunction.Config.HW_InputInverted.Sensor_Stop_12 XOR (sensorStop OR DebugSensors[2]	OR force_Sensor_Stop_12);
END_IF					

// Write Sensor_Stop_12 at 100 tu leading edge
IF  (rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.InOutfeed) 
AND rFunction.HW_Inputs.SwitchOutfeed 
THEN
	mPosition:= (  FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[1] );
				  
	rFunction.HW_Inputs.Sensor_Stop_12:= 	rFunction.Config.HW_InputInverted.Sensor_Stop_12
											XOR (fbTransportControlEmulation.M_TU_PresentState(
														mPosition := mPosition,
														mIndex => FunctionBase.IndexToRemove[1],
														mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos)
												  OR force_Sensor_Stop_12);		
END_IF		

									
// Write Sensor Occupiedand Maxgap for InfeedOutfeed in Outfeed at 900
IF  (rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.InOutfeed) AND rFunction.HW_Inputs.SwitchOutfeed THEN
	rFunction.HW_Inputs.OccupiedAndMaxGap := rFunction.Config.HW_InputInverted.OccupiedAndMaxGap XOR (sensorStop OR DebugSensors[2]	OR force_OccupiedAndMaxGap);		
END_IF		

// Write Sensor OccupiedAndMaxGap at 100
IF rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.Infeed OR ((rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.InOutfeed) AND rFunction.HW_Inputs.SwitchInfeed) THEN
	mPosition:= (  FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[1] );
				  
	rFunction.HW_Inputs.OccupiedAndMaxGap:= rFunction.Config.HW_InputInverted.OccupiedAndMaxGap
		XOR (fbTransportControlEmulation.M_TU_PresentState(
				mPosition := mPosition,
				mIndex => FunctionBase.IndexToRemove[1],
				mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos)
							OR force_OccupiedAndMaxGap);		
END_IF		



// Write SwitchInfeedOutfeed
rFunction.HW_Inputs.SwitchInfeed:=	force_SwitchInfeed;
rFunction.HW_Inputs.SwitchOutfeed	:=	force_SwitchOutfeed;

// Write Sensor Lower_Position_Reached		  
rFunction.HW_Inputs.Lower_Position_Reached:= rFunction.Config.HW_InputInverted.Lower_Position_Reached
                        XOR (fbHoistEmulation.Hoist_Pos_Down
						OR force_Lower_Position_Reached);					
								
// Write Sensor Upper_Position_Reached	  
rFunction.HW_Inputs.Upper_Position_Reached:= rFunction.Config.HW_InputInverted.Upper_Position_Reached
                        XOR (fbHoistEmulation.Hoist_Pos_Up
						OR force_Upper_Position_Reached);	
														
// Write Sensor TuRecognitionBottom



mPosition:= (FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - 400);

rFunction.HW_Inputs.TuRecognitionBottom:= rFunction.Config.HW_InputInverted.TuRecognitionBottom
	XOR ((fbTransportControlEmulation.M_TU_PresentState(
			mPosition := mPosition,
			mIndex => FunctionBase.IndexToRemove[1],
			mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos)
						AND fbHoistEmulation.Hoist_Pos_Down)
						OR force_TuRecognitionBottom);
								
// Write Sensor TuRecognitionTop
mPosition:= (FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - 400);
			  
rFunction.HW_Inputs.TuRecognitionTop:= rFunction.Config.HW_InputInverted.TuRecognitionTop
	XOR ((fbTransportControlEmulation.M_TU_PresentState(
			mPosition := mPosition,
			mIndex => FunctionBase.IndexToRemove[1],
			mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos)
						AND fbHoistEmulation.Hoist_Pos_Up)
						OR force_TuRecognitionTop);								
																	
								
// Write Sensor ForkDetection		  
rFunction.HW_Inputs.ForkDetection:= rFunction.Config.HW_InputInverted.ForkDetection
                        XOR ( force_ForkDetection);		]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs_1" Id="{aa6390e9-c6bf-0357-04fb-0c637736ab91}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OverWriteInputs
 * 	FUNCTION	Overwrite inputs for emulation
 **************************************************************************************)

// ATQ
IF (    (_ATQ      = TRUE)
	   AND(_ATQ_Last = FALSE)  )
  THEN
	_FunctionName := F_GetFunctionName(rFunction.ConfigFunction.eFunctionNumber, 1);
	_FunctionName := DELETE(_FunctionName,1,16);
	_FunctionName := INSERT(_FunctionName,'1',15);
	_FunctionName := DELETE(_FunctionName,1,15);
	_FunctionName := DELETE(_FunctionName,1,12);
	_FunctionName := DELETE(_FunctionName,1,7);
	_FunctionName := DELETE(_FunctionName,1,3);
	_Position 	  := _FunctionName;
	
	ATQData.Position       := _Position;
	ATQData.Weight         := 125;
	ATQData.Length         := 500;
	ATQData.Width          := 722;
	ATQData.Height         := 1740;
	ATQData.LabelPresent   := 1;
	//------------------------------------------------
	DestStart:= ADR(ATQData.Label);
	DestEnd  := ADR(ATQData.Label) + SIZEOF(ATQData.Label);
	DestStart:= F_StringCopy     (DestStart:= DestStart,  DestEnd:= DestEnd,  Source:= ADR(_Position)  );
	DestStart:= F_StringCopy     (DestStart:= DestStart,  DestEnd:= DestEnd,  Source:= ADR('_')  );

	DestStart:= F_StringFromUlint(DestStart:= DestStart,  DestEnd:= DestEnd,  Value:= _Count_ATQ,  Length:= 5 );
	
	//------------------------------------------------
	fbITC_ConveyorEmulation.M_AddData( meChannelSrc:= 0,
									   mMsgType    := E_ITC_MsgTypeConv.E_ADD_TU_REQUEST,
									   mpData      := ADR   (ATQData),
									   mLen        := SIZEOF(ATQData));
	//------------------------------------------------
	_Count_ATQ:= _Count_ATQ + 1;
  END_IF
  
  _ATQ_Last:= _ATQ;
  
  // RTQ
IF (    (_RTQ      = TRUE)
	   AND(_RTQ_Last = FALSE)  )
  THEN
	_FunctionName := F_GetFunctionName(rFunction.ConfigFunction.eFunctionNumber, 1);
	_FunctionName := DELETE(_FunctionName,1,16);
	_FunctionName := INSERT(_FunctionName,'1',15);
	_FunctionName := DELETE(_FunctionName,1,15);
	_FunctionName := DELETE(_FunctionName,1,12);
	_FunctionName := DELETE(_FunctionName,1,7);
	_FunctionName := DELETE(_FunctionName,1,3);
	_Position 	  := _FunctionName;
	
	RTQData.Position       := _Position;
	ATQData.LabelPresent   := 1;
	//------------------------------------------------
	DestStart:= ADR(RTQData.Label);
	DestEnd  := ADR(RTQData.Label) + SIZEOF(RTQData.Label);
	DestStart:= F_StringCopy     (DestStart:= DestStart,  DestEnd:= DestEnd,  Source:= ADR(_Position)  );
	DestStart:= F_StringCopy     (DestStart:= DestStart,  DestEnd:= DestEnd,  Source:= ADR('_')  );

	DestStart:= F_StringFromUlint(DestStart:= DestStart,  DestEnd:= DestEnd,  Value:= _Count_RTQ,  Length:= 5 );
	
	//------------------------------------------------
	fbITC_ConveyorEmulation.M_AddData( meChannelSrc:= 0,
									   mMsgType    := E_ITC_MsgTypeConv.E_REMOVE_TU_REQUEST,
									   mpData      := ADR   (RTQData),
									   mLen        := SIZEOF(RTQData));
	//------------------------------------------------
	_Count_RTQ:= _Count_RTQ + 1;
  END_IF
  
  _RTQ_Last:= _RTQ;

 
//****************EMULATE INPUTS***************** 
 
ZoneData[1].TransportControlData.TransportParam.ZoneLength := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1];	

EmuSensors[1] :=  fbTransportControlEmulation.M_TU_PresentState(
											mPosition := FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[2],
											mIndex => FunctionBase.IndexToRemove[1],
											mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos);

//Set Sensor at first zone
IF(rFunction.SettingsFunction.NumberOfZones>1 AND rFunction.Settings.TwoSensorFunction) THEN
	IF(FunctionBase.rSettingsFunctionReal.FlowDirectionREV) THEN
		//RWD drive direction
		SensorFirst:= EmuSensors[rFunction.SettingsFunction.NumberOfZones];
	ELSE
		//FWD drive direction
		SensorFirst:= EmuSensors[1];
	END_IF	
END_IF
//Set sensor at last zone
IF(FunctionBase.rSettingsFunctionReal.FlowDirectionREV) THEN
	//RWD drive direction
	SensorStop := EmuSensors[1];
ELSE
	//FWD drive direction
	SensorStop := EmuSensors[rFunction.SettingsFunction.NumberOfZones];
END_IF

// Write Sensor Sensor_Stop_12  at 900 tu leading edge
IF rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.Infeed OR rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.Outfeed OR ((rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.InOutfeed) AND rFunction.HW_Inputs.SwitchInfeed) THEN
rFunction.HW_Inputs.Sensor_Stop_12 := rFunction.Config.HW_InputInverted.Sensor_Stop_12 XOR (sensorStop OR DebugSensors[2]	OR force_Sensor_Stop_12);
END_IF					

// Write Sensor_Stop_12 at 100 tu leading edge
IF  (rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.InOutfeed) AND rFunction.HW_Inputs.SwitchOutfeed THEN
	mPosition:= (  FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[1] );
				  
	rFunction.HW_Inputs.Sensor_Stop_12:= rFunction.Config.HW_InputInverted.Sensor_Stop_12
		XOR (fbTransportControlEmulation.M_TU_PresentState(
				mPosition := mPosition,
				mIndex => FunctionBase.IndexToRemove[1],
				mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos)
							OR force_Sensor_Stop_12);		
END_IF		

									
// Write Sensor Occupiedand Maxgap for InfeedOutfeed in Outfeed at 900
IF  (rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.InOutfeed) AND rFunction.HW_Inputs.SwitchOutfeed THEN
	rFunction.HW_Inputs.OccupiedAndMaxGap := rFunction.Config.HW_InputInverted.OccupiedAndMaxGap XOR (sensorStop OR DebugSensors[2]	OR force_OccupiedAndMaxGap);		
END_IF		

// Write Sensor OccupiedAndMaxGap at 100
IF rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.Infeed OR ((rFunction.Config.Mode = E_Mode_Infeed_Outfeed_Hoist.InOutfeed) AND rFunction.HW_Inputs.SwitchInfeed) THEN
	mPosition:= (  FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[1] );
				  
	rFunction.HW_Inputs.OccupiedAndMaxGap:= rFunction.Config.HW_InputInverted.OccupiedAndMaxGap
		XOR (fbTransportControlEmulation.M_TU_PresentState(
				mPosition := mPosition,
				mIndex => FunctionBase.IndexToRemove[1],
				mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos)
							OR force_OccupiedAndMaxGap);		
END_IF		



// Write SwitchInfeedOutfeed
rFunction.HW_Inputs.SwitchInfeed:=	force_SwitchInfeed;
rFunction.HW_Inputs.SwitchOutfeed	:=	force_SwitchOutfeed;

// Write Sensor Lower_Position_Reached		  
rFunction.HW_Inputs.Lower_Position_Reached:= rFunction.Config.HW_InputInverted.Lower_Position_Reached
                        XOR (fbHoistEmulation.Hoist_Pos_Down
						OR force_Lower_Position_Reached);					
								
// Write Sensor Upper_Position_Reached	  
rFunction.HW_Inputs.Upper_Position_Reached:= rFunction.Config.HW_InputInverted.Upper_Position_Reached
                        XOR (fbHoistEmulation.Hoist_Pos_Up
						OR force_Upper_Position_Reached);	
														
// Write Sensor TuRecognitionBottom



mPosition:= (FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - 400);

rFunction.HW_Inputs.TuRecognitionBottom:= rFunction.Config.HW_InputInverted.TuRecognitionBottom
	XOR ((fbTransportControlEmulation.M_TU_PresentState(
			mPosition := mPosition,
			mIndex => FunctionBase.IndexToRemove[1],
			mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos)
						AND fbHoistEmulation.Hoist_Pos_Down)
						OR force_TuRecognitionBottom);
								
// Write Sensor TuRecognitionTop
mPosition:= (FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - 400);
			  
rFunction.HW_Inputs.TuRecognitionTop:= rFunction.Config.HW_InputInverted.TuRecognitionTop
	XOR ((fbTransportControlEmulation.M_TU_PresentState(
			mPosition := mPosition,
			mIndex => FunctionBase.IndexToRemove[1],
			mLeadingEdgePos => ZoneData[1].TransportControlData.TU_LeadingEdgePos)
						AND fbHoistEmulation.Hoist_Pos_Up)
						OR force_TuRecognitionTop);								
																	
								
// Write Sensor ForkDetection		  
rFunction.HW_Inputs.ForkDetection:= rFunction.Config.HW_InputInverted.ForkDetection
                        XOR ( force_ForkDetection);		]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{da20d7f4-680d-487b-ad96-aa8796034338}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid function number
IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN

	// Is reference valid  
	IF __ISVALIDREF(FunctionRegistry[eFunctionNumber].rFunction) THEN
		// Build address to reference
		pFunction := ADR(FunctionRegistry[eFunctionNumber].rFunction);

		// If address is possible
		IF pFunction <> 0 THEN
			// Build reference to memory
			rFunction REF= pFunction^;
		ELSE
			// Reinitialize module
			FunctionData.Init:= FALSE;
		END_IF
	ELSE
		// Reinitialize module
		FunctionData.Init:= FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_GetTU_PresentState" Id="{ba7b1674-ac57-4a89-b5e6-888638fe322a}">
      <Declaration><![CDATA[METHOD M_GetTU_PresentState : BOOL
(**************************************************************************************
 * 	Application		:	ProBox
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date: $
 *	Last changed by	:	$Author: q8Hedls $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Get TU present state, overriden from base object
 *
 **************************************************************************************)
VAR_INPUT
	mZone	 : INT; // Zone number
	mPosition : UINT; // Position [mm]
END_VAR
VAR_OUTPUT
	mTU_DataIndex : DINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF(mZone>=1 AND mZone<= NUMBER_OF_ZONES_PER_FUNCTION) THEN
	M_GetTU_PresentState := fbTransportControlEmulation.M_TU_PresentState(mPosition := mPosition, mTU_DataIndex => mTU_DataIndex);
END_IF
]]></ST>
      </Implementation>
    </Method>
    <ObjectProperties>
      <XmlArchive>
        <Data>
          <o xml:space="preserve" t="UMLStereoTypeContainerObject">
            <v n="IsType" t="UMLType">BaseArea</v>
            <v n="Stereotype">""</v>
            <d n="Stereotypes" t="Hashtable" />
          </o>
        </Data>
        <TypeList>
          <Type n="Hashtable">System.Collections.Hashtable</Type>
          <Type n="String">System.String</Type>
          <Type n="UMLStereoTypeContainerObject">{30250973-b110-4e31-b562-c102e042dca4}</Type>
          <Type n="UMLType">{0197b136-405a-42ee-bb27-fd08b621d0cf}</Type>
        </TypeList>
      </XmlArchive>
    </ObjectProperties>
  </POU>
</TcPlcObject>