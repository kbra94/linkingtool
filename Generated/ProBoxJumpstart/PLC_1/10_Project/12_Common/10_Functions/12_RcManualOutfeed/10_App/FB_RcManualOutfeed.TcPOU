﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_RcManualOutfeed" Id="{d877df37-bd04-454c-947f-f32da3e2f11a}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_RcManualOutfeed EXTENDS FB_StraightBase
	IMPLEMENTS I_StateMachineTextCallback
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date:$
 *	Last changed by	:	$Author:  $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Rc manual outfeed with several zones.
 *
 *	
 **************************************************************************************)
VAR_INPUT
	Config					: ST_CFG_RcManualOutfeed; // Configuration
	HW_Inputs		 		: ST_HW_InputsRcManualOutfeed; // Hardware inputs
END_VAR

VAR_OUTPUT
	HW_Outputs		 		: ST_HW_OutputsRcManualOutfeed; // Hardware inputs
END_VAR

VAR
	Inputs					: ST_InputsRcManualOutfeed; // Function specific inputs
	fbTransportControl 		: ARRAY[1..NUMBER_OF_ZONES_PER_FUNCTION] OF FB_ProBoxTransportControl;	
	Idx						: INT; // Iterator
	ZoneIdx					: INT; // Iterator
	EmptyManualCommands		: ST_ITC_ManualMotion; // Empty manual commands			
	eState					: E_RcManualOutfeed_States;
	fbStateMachine			: FB_StateMachine(mName:= 'RcManualOutfeed state',  mStateTextCallback:= THIS^);
	TimeRemaining			: TIME := T#0S;
	
	DisableTransport        : BOOL;	// Disable Transport while waiting after TU was removed
	RemovalAllowed			: BOOL;	// Remove of TUs is allowed
END_VAR

VAR PERSISTENT
	Settings				: ST_CFG_RcManualOutfeed; // Settings - activated configuration 
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Input mapping
***************************************************************************************************************)
A_InputMapping();

(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   External Subsystem transport interface
***************************************************************************************************************)
A_ExternalSTI();

(**************************************************************************************************************
   State control
***************************************************************************************************************)
A_StateControl();

(**************************************************************************************************************
   Error handler
***************************************************************************************************************)
A_ErrorHandler();

(**************************************************************************************************************
   Process AddOns
***************************************************************************************************************)
M_ProcessAddOns();

(**************************************************************************************************************
   Manual control
***************************************************************************************************************)
A_ManualControl();

(**********************************************************************************************************
	   Transport link
***********************************************************************************************************)
// Transport link should be updated before calling the transport controller.
_M_TransportLink();

(**************************************************************************************************************
   run rc manual outfeed statemachine
***************************************************************************************************************)
M_RunStates();
	
(**************************************************************************************************************
   Call TransportControl
***************************************************************************************************************)
_M_TransportControl();	

(**************************************************************************************************************
   Interface handler out
***************************************************************************************************************)
M_ITC_ProcessOut();

(**************************************************************************************************************
   Output mapping
***************************************************************************************************************)
A_OutputMapping();	]]></ST>
    </Implementation>
    <Method Name="_M_TransportControl" Id="{745757f8-e182-484d-94c8-42ff02b9b757}">
      <Declaration><![CDATA[METHOD PRIVATE _M_TransportControl : BOOL
VAR
	SlugSensorOccupied		: BOOL;	// Assigned occupied sensor first
	StopSensorOccupied		: BOOL;	// Assigned occupied sensor	last
	CreepSensor				: BOOL; // Creep sensor
	ZoneOccupied			: BOOL; // Zone physical occupied
	CtrlInputPbTc			: ST_CtrlInPbTc; //Control input for Probox transportControl
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//Reset errors
F_ResetError(FALSE,FunctionData.ErrorData.ErrorDataSet[1]);

// Check transport direction of the conveyor
SettingsFunction.FlowDirectionREV := _M_DefineTransportDirection(FunctionData.ErrorData.ErrorDataSet[1]);


// Assign sensor occpupied
_M_Check_HW_SensorInput(
					Sensor_Stop_12		:= Inputs.Sensor_Stop_12, 
					Sensor_Stop_21		:= FALSE, 
					Sensor_Slug_12		:= Inputs.Sensor_First_12, 
					Sensor_Slug_21		:= FALSE,
					Sensor_Creep_12		:= FALSE,
					Sensor_Creep_21		:= FALSE, 
					FlowDirectionREV	:= SettingsFunction.FlowDirectionREV, 
					HasSluggingSensor	:= Settings.TwoSensorFunction,
					ExtRunSynteticSensor:= FALSE, 
					StopSensorOccupied	=> StopSensorOccupied, 
					SlugSensorOccupied	=> SlugSensorOccupied,
					CreepSensor			=> CreepSensor);
	

// call Straight transport method of base class

CtrlInputPbTc.EnableSlugging 	:= FunctionInterface.In.eNextFunctionNumber = E_FunctionNumber.F_BEGIN_FUNCTION;
CtrlInputPbTc.SlugSensor		:= SlugSensorOccupied;
CtrlInputPbTc.SlugSensorExists	:= Settings.TwoSensorFunction;
CtrlInputPbTc.StopSensor		:= StopSensorOccupied;

FOR ZoneIdx := 1 TO SettingsFunction.NumberOfZones DO
		
	M_StraightTransport(
		fbTransportControl		:=fbTransportControl[ZoneIdx],
		ZoneIx					:= ZoneIdx, 
		ExtDisableTakeover		:= FALSE, 
		ExtDisableHandover		:= FALSE, 
		ExtDisableTransport		:= DisableTransport,		
		CtrlInputPbTc			:= CtrlInputPbTc,
		CreepHwSensor			:= CreepSensor, 
		Orientation_Axis		:= Settings.Orientation_Axis_12);
END_FOR
]]></ST>
      </Implementation>
    </Method>
    <Action Name="A_ExternalSTI" Id="{224be558-e16f-4d0d-aea1-3be1cd821722}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_ExternalSTI
 * 	FUNCTION	External Subsystem transport interface
 **************************************************************************************)


(**************************************************************************************
   External Subsystem transport interfaces
***************************************************************************************)
FOR Idx := 1 TO NUMBER_OF_STI DO
	// Is an external Subsystem transport interface configured
	IF SettingsFunction.eExternalSTI_FunctionNumber[Idx] > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION AND
	   SettingsFunction.eExternalSTI_FunctionNumber[Idx] < E_FunctionNumber.F_END_EXTERN_FUNCTION THEN
	   
		// When interface is valid
		IF __QUERYINTERFACE(fbSTI_Channels[SettingsFunction.eExternalSTI_FunctionNumber[Idx]].Instance,FunctionBase.STI_Instance) THEN
			// Call external inbound interface
			fbSTI_Channels[SettingsFunction.eExternalSTI_FunctionNumber[Idx]].Instance.M_CallInterface(
				mErrorData				:= FunctionData.ErrorData.ErrorDataSet[1]);
		END_IF
	END_IF
END_FOR
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_Init" Id="{f56c8d9c-f16c-493a-b3f0-9a467257b80e}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize function
 **************************************************************************************)
 
// Update Registry
M_UpdateRegistry();
 

// Init ongoing - Wait until init is released (Sequencial startup)
IF NOT FunctionData.Init AND FunctionData.InitRunning THEN
	// When the subsystem is started, 
	// When its released to initialize
	IF FunctionData.OperationState.Info.SystemReady AND
	   SettingsFunction.eFunctionNumber < FunctionData.OperationState.InitRelease.eLimitFunctionNumber THEN
	   
	   IF M_InitFunction(FALSE) THEN
			// Function initialized - and initialization done
			FunctionData.Init := TRUE;
			FunctionData.InitRunning := FALSE;
			
			// Set reset cmd
			FunctionData.ErrorData.Error.ResetError := TRUE;
			
			DebugMsg := CONCAT('Initialization done. Function: ', ConfigFunction.FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
		END_IF
	END_IF
(*
If the function is not initialized,
clean all values and wait until init is released
*)
ELSIF NOT FunctionData.Init THEN
	// Reset internal variables
	FunctionData.OperationState := FunctionBase.PositionStateEmpty;

	// Initiate initialization of all zones
	FOR ZoneIdx := 1 TO SettingsFunction.NumberOfZones DO
		ZoneData[ZoneIdx].Init := FALSE;
	END_FOR;
	
	// Load function specific configuration to settings
	Settings := Config;
	
	// Reset state machine
	fbStateMachine.M_SetState(mState:= E_RcManualOutfeed_States.Init,
										mReason := 'function initialize');
	
	// Reset values and load config
	M_PreInit();
	
	// To start init a valid functionnumber and element link is needed
	FunctionData.InitRunning := SettingsFunction.eFunctionNumber > 0 AND
								SettingsFunction.eElementLink > 0;
END_IF

// Init zones
IF FunctionData.Init THEN
	// Set init flag after one cycle
	FOR ZoneIdx := 1 TO SettingsFunction.NumberOfZones DO
		// If a zone is not initialized, wait one cycle to finish init
		IF NOT ZoneData[ZoneIdx].Init THEN
			IF ZoneData[ZoneIdx].InitRunning THEN
				ZoneData[ZoneIdx].InitRunning := FALSE;
				ZoneData[ZoneIdx].Init := TRUE;
				ZoneData[ZoneIdx].ErrorData.Error.ResetError := TRUE;
			ELSE	
				ZoneData[ZoneIdx].InitRunning := TRUE;
			END_IF
		END_IF
	END_FOR;
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InputMapping" Id="{85db0d83-2ba0-486e-b60f-c988171a9f86}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InputMapping
 * 	FUNCTION	Input mapping
 **************************************************************************************)
 
(**************************************************************************************
   Hardware Inputs
***************************************************************************************)
// Adapt input logic state high/low
Inputs.Sensor_Stop_12		:= Settings.Invert_Sensor_Stop_12 XOR HW_Inputs.Sensor_Stop_12;
Inputs.Sensor_First_12		:= Settings.Invert_Sensor_First_12 XOR HW_Inputs.Sensor_First_12;

(**************************************************************************************
   Element states
***************************************************************************************)
// Get status of the corresponding element
M_GetElementStates(); 

(**************************************************************************************
   AddOn states
***************************************************************************************)
// Get status of all corresponding AddOn functions and store it in FunctionInterface.In.AddOnOrder
M_GetAddOnOrder();

]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_ManualControl" Id="{50c96652-561f-4946-88ff-261404465251}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_ManualControl
 * 	FUNCTION	Control manual movement
 **************************************************************************************)

 
(**************************************************************************************
   Reset manual commands
***************************************************************************************)

// If function is not in manual mode
IF FunctionData.OperationState.eMode <> E_PositionMode.MANUAL_MODE THEN
	// Reset manual commands
	ITC.ManualMotion := EmptyManualCommands;
END_IF

(**************************************************************************************
   Enable available axis, buttons, features
***************************************************************************************)
// Example:
//	ITC.ManualMotion.Axis[1].Forward.Enable := TRUE;


]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OutputMapping" Id="{9a892053-5750-4333-879c-4feb5d019a1b}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OutputMapping
 * 	FUNCTION	Output mapping
 **************************************************************************************)
 
 (**************************************************************************************
   Hardware outputs
***************************************************************************************)
 HW_Outputs.RemovalAllowed	:= RemovalAllowed;
 
(**************************************************************************************
   Order outputs (Commands to corresponding element)
***************************************************************************************)
THIS^.M_OutputMapping(Settings.Orientation_Axis_12,FALSE);
]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_GetStateText" Id="{d699445d-4636-4951-9e9b-0e1754643e59}">
      <Declaration><![CDATA[// forwards state text to FB_StateMachine
METHOD PUBLIC M_GetStateText : A_StateText

(********************************************************************************************************************************)
VAR_INPUT
	StateMachine    : POINTER TO FB_StateMachine;		// requesting state machine instance
	StateNbr		: UINT;		// state number which text is requested
END_VAR

(********************************************************************************************************************************)
VAR
	Enum			: E_RcManualOutfeed_States		:= E_RcManualOutfeed_States.Init;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[(********************************************************************************************************************************)
(********************************************************************************************************************************)
IF (StateMachine = ADR(eState))
THEN
	Enum := StateNbr;
	M_GetStateText:= TO_STRING(Enum);
	RETURN;
END_IF


(********************************************************************************************************************************)
(********************************************************************************************************************************)
M_GetStateText:= 'unknown state machine';]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_OutputMapping" Id="{a48dcda4-bd50-4449-a51e-d0fd8e1bedd2}">
      <Declaration><![CDATA[METHOD PROTECTED M_OutputMapping : BOOL
VAR_INPUT
	Orientation_Axis		: E_Axis_Orientation := E_Axis_Orientation.Element_12;
	Ext_Disable				: BOOL; //Extern Disable. TRUE = override transport control and set FunctionOrders.Enable to FALSE	
END_VAR

VAR
	fb_ProBoxToFunctionOrders : FB_ProBoxToFunctionOrders;
	Outputs					: ST_StraightBaseZoneOrders;
	
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
//Get the summary of transport control output of all zones 
_M_GetZoneOrderSummary(SummaryFunctionOrder=>Outputs);


fb_ProBoxToFunctionOrders(
	CreepSpeed_12	:= Outputs.Speed = E_ProBoxMotorSpeed.CREEP_SPEED, 
	LowSpeed_12		:= Outputs.Speed = E_ProBoxMotorSpeed.LOW_SPEED, 
	Forward_12		:= Outputs.Drive_Run, 
	Reverse_12		:= Outputs.Drive_RunRev,
	Enable_12		:= Outputs.Enable,	 
	CreepSpeed_34	:= FALSE, 
	LowSpeed_34		:= FALSE, 
	Forward_34		:= FALSE, 
	Reverse_34		:= FALSE,
	Enable_34 		:= FALSE);
	
// Initialize variables
FunctionInterface.Out.FunctionOrders.Enable := TRUE;
FunctionInterface.Out.FunctionOrders.ReqDriveRun := FALSE;
FunctionInterface.Out.FunctionOrders.ReqDriveDirection := FALSE;
FunctionInterface.Out.FunctionOrders.ReqSpeed := 0;
FunctionInterface.Out.FunctionOrders.Enable_34 := TRUE;
FunctionInterface.Out.FunctionOrders.ReqDriveRun_34 := FALSE;
FunctionInterface.Out.FunctionOrders.ReqDriveDirection_34 := FALSE;
FunctionInterface.Out.FunctionOrders.ReqSpeed_34 := 0;	


// If axis 1-2 is requested by the element axis 1-2 in the any direction
IF Orientation_Axis = E_Axis_Orientation.Element_12 OR Orientation_Axis = E_Axis_Orientation.Element_21 THEN
	FunctionInterface.Out.FunctionOrders.Enable 			:= fb_ProBoxToFunctionOrders.FunctionOrders.Enable AND NOT Ext_Disable;
	FunctionInterface.Out.FunctionOrders.ReqDriveRun 		:= fb_ProBoxToFunctionOrders.FunctionOrders.ReqDriveRun;
	FunctionInterface.Out.FunctionOrders.ReqSpeed 			:= fb_ProBoxToFunctionOrders.FunctionOrders.ReqSpeed;
	FunctionInterface.Out.FunctionOrders.ReqDriveDirection 	:= fb_ProBoxToFunctionOrders.FunctionOrders.ReqDriveDirection;
		
// If axis 1-2 is requested by the element axis 3-4 in any direction
ELSIF Orientation_Axis = E_Axis_Orientation.Element_34 OR Orientation_Axis = E_Axis_Orientation.Element_43 THEN
	FunctionInterface.Out.FunctionOrders.Enable_34 				:= fb_ProBoxToFunctionOrders.FunctionOrders.Enable AND NOT Ext_Disable;
	FunctionInterface.Out.FunctionOrders.ReqDriveRun_34 		:= fb_ProBoxToFunctionOrders.FunctionOrders.ReqDriveRun;
	FunctionInterface.Out.FunctionOrders.ReqSpeed_34 			:= fb_ProBoxToFunctionOrders.FunctionOrders.ReqSpeed;
	FunctionInterface.Out.FunctionOrders.ReqDriveDirection_34 	:= fb_ProBoxToFunctionOrders.FunctionOrders.ReqDriveDirection;	
END_IF;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_RunStates" Id="{f18877ba-54ec-4df4-8afd-7393129bc396}">
      <Declaration><![CDATA[METHOD PRIVATE M_RunStates

]]></Declaration>
      <Implementation>
        <ST><![CDATA[

eState := fbStateMachine.P_State;
CASE eState OF
	

E_RcManualOutfeed_States.INIT:			
		//-- ENTRY ------------------------------------------------------------------------------------------------------------------
		fbStateMachine.P_SisNbr:= F_AddDividerToSisString(ConfigFunction.FunctionName);
	
		//-- set hardware outputs --------------------------------------------------------------
	
		//-- set transport movement ------------------------------------------------------------
		DisableTransport    := FALSE;
		RemovalAllowed		:= FALSE;
	
		//-- ACTION -----------------------------------------------------------------------------------------------------------------
	
		//-- TRANSITION -------------------------------------------------------------------------------------------------------------
		IF FunctionData.Init
		AND (FunctionInterface.In.Element.OperationState.eState = E_PositionState.STARTED)
		AND (FunctionData.ErrorData.Error.ErrorType <> E_ErrorType.ERROR)
		THEN
			//-- last zone is occupied
			IF ZoneData[SettingsFunction.NumberOfZones].TransportControlData.Occupied = TRUE
			THEN
				fbStateMachine.M_SetState(mState:= E_RcManualOutfeed_States.ReadyToRemove,
											mReason := 'function started and last zone is occupied');
			//-- last zone is not occupied
			ELSIF ZoneData[SettingsFunction.NumberOfZones].TransportControlData.Occupied = FALSE
			THEN
				fbStateMachine.M_SetState(mState:= E_RcManualOutfeed_States.BuiltTrain,
											mReason := 'function started and last zone is not occupied');
			END_IF
		END_IF
		
(********************************************************************************************************************************)
(** Wait for new TUs ************************************************************************************************************)
E_RcManualOutfeed_States.BuiltTrain:
		//-- ENTRY ------------------------------------------------------------------------------------------------------------------
		IF fbStateMachine.P_Entering
		THEN
			DisableTransport    := FALSE;
			RemovalAllowed		:= FALSE;
		END_IF
		
		//-- ACTION -----------------------------------------------------------------------------------------------------------------
		FunctionInterface.In.ePrevFunctionNumber := SettingsFunction.eAdjacentFunctionNumberSide[1];
		FunctionInterface.In.eNextFunctionNumber := E_FunctionNumber.F_BEGIN_FUNCTION;
		
		//-- TRANSITION -------------------------------------------------------------------------------------------------------------
		//-- last zone is occupied
		IF ZoneData[SettingsFunction.NumberOfZones].TransportControlData.Occupied = TRUE
		THEN
			fbStateMachine.M_SetState(mState:= E_RcManualOutfeed_States.ReadyToRemove,
										mReason := 'function started and last zone is occupied');
		END_IF
				
(********************************************************************************************************************************)
(** Wait for a TU is removed ************************************************************************************************************)
E_RcManualOutfeed_States.ReadyToRemove:
		//-- ENTRY ------------------------------------------------------------------------------------------------------------------
		IF fbStateMachine.P_Entering
		THEN
			DisableTransport    := FALSE;
			RemovalAllowed		:= TRUE;
		END_IF
		
		//-- ACTION -----------------------------------------------------------------------------------------------------------------
			FunctionInterface.In.eNextFunctionNumber := E_FunctionNumber.F_END_FUNCTION;
		
		//-- TRANSITION -------------------------------------------------------------------------------------------------------------
		//-- function is empty, go to build train
		IF F_CheckAllFree(SettingsFunction.eFunctionNumber) = TRUE
		THEN
			fbStateMachine.M_SetState(mState:= E_RcManualOutfeed_States.BuiltTrain,
											mReason := 'function is empty');
		//-- last zone is empty, wait to move forward
		ELSIF ZoneData[SettingsFunction.NumberOfZones].TransportControlData.Occupied = FALSE
		THEN
			fbStateMachine.M_SetState(mState:= E_RcManualOutfeed_States.ForwardDelay,
											mReason := 'last tu removed');
		END_IF
	
		
(********************************************************************************************************************************)
(** forward delay ************************************************************************************************************)
E_RcManualOutfeed_States.ForwardDelay:
		//-- ENTRY ------------------------------------------------------------------------------------------------------------------
		IF fbStateMachine.P_Entering
		THEN
			DisableTransport    := TRUE;
			RemovalAllowed		:= FALSE;
		END_IF
		
		//-- ACTION -----------------------------------------------------------------------------------------------------------------
		TimeRemaining := Settings.MoveForwardDelay - fbStateMachine.P_TimeInState;
		
		//-- TRANSITION -------------------------------------------------------------------------------------------------------------
		//-- function is empty, go to build train
		IF fbStateMachine.P_TimeInState >= Settings.MoveForwardDelay
		THEN
			fbStateMachine.M_SetState(mState:= E_RcManualOutfeed_States.MoveForward,
											mReason := 'waiting time expired');
		END_IF
		
(********************************************************************************************************************************)
(** Move TUs forward ************************************************************************************************************)
E_RcManualOutfeed_States.MoveForward:
		//-- ENTRY ------------------------------------------------------------------------------------------------------------------
		IF fbStateMachine.P_Entering
		THEN
			DisableTransport    := FALSE;
			RemovalAllowed		:= FALSE;
		END_IF
		
		//-- ACTION -----------------------------------------------------------------------------------------------------------------
		FunctionInterface.In.eNextFunctionNumber := E_FunctionNumber.F_END_FUNCTION;
		
		//-- TRANSITION -------------------------------------------------------------------------------------------------------------
		//-- function is empty, go to build train
		IF ZoneData[SettingsFunction.NumberOfZones].TransportControlData.Occupied = TRUE
		THEN
			fbStateMachine.M_SetState(mState:= E_RcManualOutfeed_States.ReadyToRemove,
											mReason := 'last zone is occupied');
		END_IF
END_CASE ]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>