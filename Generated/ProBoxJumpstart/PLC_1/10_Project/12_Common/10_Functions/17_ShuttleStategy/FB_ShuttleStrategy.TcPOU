﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_ShuttleStrategy" Id="{f01b20d4-4fa0-0adf-1172-b0a5ddef502b}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_ShuttleStrategy IMPLEMENTS I_ShuttleStrategy
VAR
	_Buffer			: ARRAY[1..GVL_ShuttleRewe.NUMBER_OF_SHUTTLES] OF ARRAY[ELEMENTFIRST..ELEMENTLAST] OF ST_StrategyBuffer;
	_EmptyBuffer	: ST_StrategyBuffer := (eFunctionNumber := 0, eDestination := 0, TU_Type := 0, Sequence := 0, Priority := 0);
	//_EmptyArea		: ARRAY[ELEMENTFIRST..ELEMENTLAST] OF ST_StrategyBuffer;
	_Dest			: ARRAY[1..GVL_ShuttleRewe.MAXDEST] OF ST_PositionIdent;
	_TransData		: ARRAY[1..GVL_ShuttleRewe.MAXDEST] OF ST_TransportControlData;
	_TU_DataIndex	: DINT;
	idx				: INT;
	BufferTransData : ARRAY[1..GVL_ShuttleRewe.MAXDEST] OF ST_TransportControlData;
	
	i,j : INT;
	
	_PalletStackPickup		: E_FunctionNumber;
	_PalletStackDeposit		: E_FunctionNumber;
	_PalletStackArea		: INT;
	
	ActiveWorkarea	: ARRAY [1..GVL_ShuttleRewe.NUMBER_OF_SHUTTLES] OF INT;
	_Counter		: ARRAY [1..GVL_ShuttleRewe.NUMBER_OF_SHUTTLES] OF INT;		// counter for orders in a workarea
	_CounterMax		: INT := 2; // Change workarea if this value is reached
	
	_config 			: ST_CFG_ShuttleStrategy;
	
END_VAR

VAR CONSTANT
	ELEMENTFIRST	: INT := 1;
    ELEMENTLAST 	: INT := 3;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[// Body never called]]></ST>
    </Implementation>
    <Method Name="_GetNextActiveWorkarea" Id="{0e63d53e-687d-06ca-156a-faf81c4ec705}">
      <Declaration><![CDATA[METHOD _GetNextActiveWorkarea : INT
VAR_INPUT
	_ShuttleNumber  		: INT;
	_NumberOfActiveAreas 	: INT;
	_ActiveWorkArea 		: INT; 
	_WorkAreas				: ARRAY [1..GVL_ShuttleRewe.NUMBER_OF_SHUTTLES] OF BOOL;
END_VAR
VAR
	_i, _j : INT;
	nextAreaPossible : BOOL := FALSE;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[// get work area that is the next to the acitve work area
FOR _j := (_ActiveWorkArea + 1) TO GVL_ShuttleRewe.NUMBER_OF_SHUTTLES DO 
	IF _WorkAreas[_j] THEN 
		nextAreaPossible := TRUE;		
	END_IF
END_FOR
IF nextAreaPossible THEN 	
	FOR _i := 1 TO GVL_ShuttleRewe.NUMBER_OF_SHUTTLES DO 
		IF _WorkAreas[_i] AND _i > _ActiveWorkArea THEN 
			_GetNextActiveWorkarea := _i;
			RETURN;
		END_IF
	END_FOR
ELSE // first work area that is enabled
	FOR _i := 1 TO GVL_ShuttleRewe.NUMBER_OF_SHUTTLES DO 
		IF _WorkAreas[_i] THEN 
			_GetNextActiveWorkarea := _i;
			RETURN;
		END_IF
	END_FOR
END_IF
]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_1RcReady" Id="{a0786f9f-f963-098f-0318-2c26ec212444}">
      <Declaration><![CDATA[METHOD PRIVATE _M_1RcReady : BOOL
VAR_INPUT
	ShuttleNo : INT;
END_VAR
VAR_OUTPUT
	LHD_1	: INT;
	LHD_2	: INT;
END_VAR

VAR
	idxRC		: INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[_M_1RcReady := FALSE;

// Search for RC
FOR idxRC := ELEMENTFIRST TO ELEMENTLAST DO
	IF _Buffer[ActiveWorkarea[ShuttleNo]][idxRC].TU_Type = ProjectTuTypes.RC_SINGLE
	THEN
		// if RC was found but has higher sequence then PAL
		// or depositposition is not ready => return
		IF NOT _M_CheckForLowerSequence(ShuttleNo, idxRC, ProjectTuTypes.PALLET_SINGLE_EUR)
		AND NOT _M_CheckForLowerSequence(ShuttleNo, idxRC, ProjectTuTypes.RC_SINGLE)
		AND _M_DepositReady(_M_GetDepositFuncNumber(_Buffer[ActiveWorkarea[ShuttleNo]][idxRC].eDestination), FALSE) 
		THEN
			IF _M_Assign1Rc(ShuttleNo, idxRC) THEN
				LHD_1 := idxRC;
			ELSE
				LHD_2 := idxRC;
			END_IF
			
			_M_1RcReady := TRUE;
			EXIT;
		END_IF
	END_IF
END_FOR


]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_2RcReady" Id="{e1b4080e-bec1-0171-0749-c9c7776e8b72}">
      <Declaration><![CDATA[METHOD PRIVATE _M_2RcReady : BOOL
VAR_INPUT
	ShuttleNo : INT;
END_VAR
VAR_OUTPUT
	LHD_1	: INT;
	LHD_2	: INT;
END_VAR

VAR
	idxRC1		: INT;
	RC1_Found	: BOOL;
	idxRC2		: INT;
	RC2_Found	: BOOL;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[_M_2RcReady := FALSE;

// Search for first RC
FOR idxRC1 := ELEMENTFIRST TO ELEMENTLAST DO
	IF _Buffer[ActiveWorkarea[ShuttleNo]][idxRC1].TU_Type = ProjectTuTypes.RC_SINGLE
	THEN
		// if RC was found but has higher sequence then PAL
		// or depositposition is not ready => return
		IF NOT _M_CheckForLowerSequence(ShuttleNo, idxRC1, ProjectTuTypes.PALLET_SINGLE_EUR)
		AND _M_DepositReady(_M_GetDepositFuncNumber(_Buffer[ActiveWorkarea[ShuttleNo]][idxRC1].eDestination), FALSE)
		THEN
			RC1_Found := TRUE;
			EXIT;
		ELSE
			RETURN;
		END_IF
	END_IF
END_FOR

// Search for second RC
FOR idxRC2 := ELEMENTFIRST TO ELEMENTLAST DO
	IF idxRC1 <> idxRC2
	AND _Buffer[ActiveWorkarea[ShuttleNo]][idxRC2].TU_Type = ProjectTuTypes.RC_SINGLE
	THEN
		// if RC was found but has higher sequence then PAL
		// or depositposition is not ready => return
		IF _M_DepositReady(_M_GetDepositFuncNumber(_Buffer[ActiveWorkarea[ShuttleNo]][idxRC2].eDestination), FALSE) 
		AND NOT _M_CheckForLowerSequence(ShuttleNo, idxRC2, ProjectTuTypes.PALLET_SINGLE_EUR)
		THEN
			RC2_Found := TRUE;
			EXIT;
		ELSE
			RETURN;
		END_IF
	END_IF
END_FOR

IF RC1_Found
AND RC2_Found
THEN
	// Check which assignment allows a doubleplay
	IF _M_Assign2Rc(ShuttleNo, idxRC1, idxRC2)
	THEN
		LHD_1	:= idxRC1;
		LHD_2	:= idxRC2;
	ELSE
		LHD_1	:= idxRC2;
		LHD_2	:= idxRC1;
	END_IF
	_M_2RcReady := TRUE;
END_IF
]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_AddItem" Id="{99f99f8f-cd6d-0df8-1e3d-c346765c553f}">
      <Declaration><![CDATA[METHOD PRIVATE _M_AddItem : BOOL
VAR_INPUT
	Index			: INT;
	FunctionNumber  : E_FunctionNumber; // Pickup position
	TU_Destination	: E_FunctionNumber;	// Destination of TU => to find Deposit
	TU_DataIndex	: DINT;
END_VAR

VAR	
	myBufferIx		: INT:=0;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[//search for function in buffer, get myBufferIx
FOR myBufferIx := ELEMENTFIRST TO ELEMENTLAST BY 1 DO
	IF(_Buffer[Index][myBufferIx].eFunctionNumber=FunctionNumber) THEN EXIT; END_IF
END_FOR

//if entry was not already in buffer -> search for first empty slot
IF(myBufferIx>ELEMENTLAST) THEN // no item found	
	FOR myBufferIx := ELEMENTFIRST TO ELEMENTLAST BY 1 DO
		IF(_Buffer[Index][myBufferIx].eFunctionNumber=0) THEN EXIT;	END_IF
	END_FOR
END_IF

//if no emtpy slot was found, then buffer full, so return.
IF(myBufferIx>ELEMENTLAST) THEN RETURN; END_IF

//------------------------------------------------------------------------
// By now, the Buffer Ix is determined
// Add function number to the buffer list
//------------------------------------------------------------------------
_Buffer[Index][myBufferIx].eFunctionNumber	:= FunctionNumber;
_Buffer[Index][myBufferIx].eDestination 	:= TU_Destination; 
_Buffer[Index][myBufferIx].TU_Type			:= TU_DataTable[TU_DataIndex].DataSet.TU_Type;	// RC = 1; PAL = 3 from SynQ Interface
_Buffer[Index][myBufferIx].Sequence 		:= TU_DataTable[TU_DataIndex].DataSet.SubSequence;

_M_SortFiFo(Index);

_M_AddItem := TRUE;]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_Assign1Rc" Id="{6c5eca58-e94d-0e62-1dd5-05225c6d5ea8}">
      <Declaration><![CDATA[METHOD PRIVATE _M_Assign1Rc : BOOL
VAR_INPUT
	ShuttleNo : INT;
	idxRC	: INT;
END_VAR

VAR
	idx		: INT;
	PDidx	: INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
// Find PD index
FOR idx := 1 TO GVL_ShuttleRewe.MAXDEST DO
	IF _config.PDs[idx].eFunctionNumber = _Buffer[ActiveWorkarea[ShuttleNo]][idxRC].eFunctionNumber
	THEN
		PDidx := idx;
		EXIT;
	END_IF
END_FOR


// Search for other PD with the same Position
FOR idx := 1 TO GVL_ShuttleRewe.MAXDEST DO
	IF _config.PDs[idx].eDestination[1] = F_BEGIN_FUNCTION
	AND _config.PDs[PDidx].Position[1] = _config.PDs[idx].Position[2]
	THEN
		_M_Assign1Rc := TRUE;
		EXIT;

	ELSIF _config.PDs[idx].eDestination[1] = F_BEGIN_FUNCTION
	AND _config.PDs[PDidx].Position[2] = _config.PDs[idx].Position[1]
	THEN
		_M_Assign1Rc := FALSE;
		EXIT;
	END_IF
END_FOR

]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_Assign2Rc" Id="{dcdfabb9-1a92-083b-0ef6-cceda4a6b4b8}">
      <Declaration><![CDATA[METHOD PRIVATE _M_Assign2Rc : BOOL
VAR_INPUT
	ShuttleNo : INT;
	idxRC1	: INT;
    idxRC2  : INT;
END_VAR

VAR
	idx		: INT;
	PDidx1	: INT;
	PDidx2	: INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[

FOR idx := 1 TO GVL_ShuttleRewe.MAXDEST DO
	IF _config.PDs[idx].eFunctionNumber = _Buffer[ActiveWorkarea[ShuttleNo]][idxRC1].eFunctionNumber
	THEN
		PDidx1 := idx;
	END_IF
	
	IF _config.PDs[idx].eFunctionNumber = _Buffer[ActiveWorkarea[ShuttleNo]][idxRC2].eFunctionNumber
	THEN
		PDidx2 := idx;
	END_IF

	IF PDidx1 <> 0
	AND PDidx2 <> 0
	THEN
		IF _config.PDs[PDidx1].Position[1] = _config.PDs[PDidx2].Position[2]
		THEN
			_M_Assign2Rc := TRUE;
		ELSIF _config.PDs[PDidx1].Position[2] = _config.PDs[PDidx2].Position[1]
		THEN
			_M_Assign2Rc := FALSE;
		END_IF
		EXIT;
	END_IF
END_FOR




]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_CheckForLowerSequence" Id="{83409b4b-fabe-08e8-27f3-16c0f9f5db0d}">
      <Declaration><![CDATA[// TRUE if lower sequence of TuType was found
METHOD PRIVATE _M_CheckForLowerSequence : BOOL
VAR_INPUT
	ShuttleNo : INT;
	Index : INT;	// Index of Buffer[] to compare with
	TuType : INT;	// TU_Type to compare with
END_VAR
VAR
	idx : INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[_M_CheckForLowerSequence := FALSE;

// Check if lower sequence was found
FOR idx := ELEMENTFIRST TO ELEMENTLAST DO
	IF _Buffer[ActiveWorkarea[ShuttleNo]][Index].eDestination = _Buffer[ActiveWorkarea[ShuttleNo]][idx].eDestination
	AND _Buffer[ActiveWorkarea[ShuttleNo]][Index].Sequence > _Buffer[ActiveWorkarea[ShuttleNo]][idx].Sequence
	AND _Buffer[ActiveWorkarea[ShuttleNo]][idx].TU_Type = TuType
	THEN
		_M_CheckForLowerSequence := TRUE;
		RETURN;
	END_IF
END_FOR]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_DeleteBuffer" Id="{2f713696-730d-0c4a-0553-efbad32ff795}">
      <Declaration><![CDATA[METHOD PRIVATE _M_DeleteBuffer : BOOL
VAR_INPUT
	Index : INT;
END_VAR

VAR
	idx : INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
FOR idx := ELEMENTFIRST TO ELEMENTLAST DO
	_Buffer[Index][idx] := _EmptyBuffer;
END_FOR]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_DepositReady" Id="{26e54758-acf4-082a-3f27-94988f323015}">
      <Declaration><![CDATA[METHOD PRIVATE _M_DepositReady : BOOL
VAR_INPUT
	FunctionNumber 	: E_FunctionNumber; 
	IsPallet		: BOOL;
END_VAR

VAR
	pShuttleMoverInstance : POINTER TO ITF_ShuttleMoverGiveTransport;
	rZoneData		:REFERENCE TO ST_ZoneData;
	rZoneTrspEnable :REFERENCE TO ST_TransportEnable;
	i:INT;
	ElementNumber	: E_ElementNumber;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[_M_DepositReady :=FALSE;

_M_DepositReady :=FALSE;
IF FunctionNumber > F_BEGIN_FUNCTION AND FunctionNumber < F_END_FUNCTION THEN 
	IF( NOT __ISVALIDREF(FunctionRegistry[FunctionNumber].rFunction)) THEN RETURN; END_IF
	IF( NOT __ISVALIDREF(FunctionRegistry[FunctionNumber].rFunctionInterface)) THEN RETURN; END_IF
	IF( NOT __ISVALIDREF(FunctionRegistry[FunctionNumber].rFunctionData)) THEN RETURN; END_IF
	IF( NOT __ISVALIDREF(FunctionRegistry[FunctionNumber].rSettingsFunction)) THEN RETURN; END_IF
	
	//check that there are no active errors in element, functon or zones
	
	ElementNumber := FunctionRegistry[FunctionNumber].rSettingsFunction.eElementLink;
	IF(__ISVALIDREF(ElementRegistry[ElementNumber].rElementData))THEN
		IF( NOT ElementRegistry[ElementNumber].rElementData.ErrorData.FunctionErrorPending AND 
			ElementRegistry[ElementNumber].rElementData.ErrorData.ErrorState = E_ErrorState.OPERATIONAL) THEN
			_M_DepositReady :=TRUE;
		END_IF	
	END_IF	
	
	
	
	// check if element/function is started
	_M_DepositReady := _M_DepositReady AND FunctionRegistry[FunctionNumber].rFunctionInterface.In.Element.OperationState.eState = E_PositionState.STARTED;								
	//check if any zone/Function/element is disabled
	_M_DepositReady := _M_DepositReady AND F_CheckAllEnabled(FunctionNumber);								
	//check if zone is occupied
	IF IsPallet THEN// pal: 
		IF FunctionRegistry[FunctionNumber].rSettingsFunction.NumberOfZones = 2 THEN 
			_M_DepositReady := _M_DepositReady AND NOT FunctionRegistry[FunctionNumber].Zone[1].refZoneData.TransportControlData.Occupied
												AND NOT FunctionRegistry[FunctionNumber].Zone[2].refZoneData.TransportControlData.Occupied;
		ELSE
			_M_DepositReady := _M_DepositReady AND NOT FunctionRegistry[FunctionNumber].Zone[1].refZoneData.TransportControlData.Occupied;
		END_IF
	ELSE// rc: 
		_M_DepositReady := _M_DepositReady AND NOT FunctionRegistry[FunctionNumber].Zone[2].refZoneData.TransportControlData.Occupied;
		
	END_IF
ELSIF FunctionNumber > F_BEGIN_EXTERN_FUNCTION AND FunctionNumber < F_END_EXTERN_FUNCTION THEN 
	IF IsPallet THEN 		
		_M_DepositReady := _M_GetStiFunction(FunctionNumber).P_DepositReadyForPallet;
	ELSE
		_M_DepositReady := _M_GetStiFunction(FunctionNumber).P_DepositReadyForRc;	
	END_IF
	
END_IF



]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_DepositReady_OLD" Id="{c6e97eaf-35ec-088f-084e-f3494ec1212d}">
      <Declaration><![CDATA[METHOD PRIVATE _M_DepositReady_OLD : BOOL
VAR_INPUT
	FunctionNumber 	: E_FunctionNumber; 
	IsPallet		: BOOL;
END_VAR
VAR
	rZoneData		:REFERENCE TO ST_ZoneData;
	rZoneTrspEnable :REFERENCE TO ST_TransportEnable;
	i:INT;
	ElementNumber	: E_ElementNumber;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_M_DepositReady :=FALSE;
IF FunctionNumber > F_BEGIN_FUNCTION AND FunctionNumber < F_END_FUNCTION THEN 
	IF( NOT __ISVALIDREF(FunctionRegistry[FunctionNumber].rFunction)) THEN RETURN; END_IF
	IF( NOT __ISVALIDREF(FunctionRegistry[FunctionNumber].rFunctionInterface)) THEN RETURN; END_IF
	IF( NOT __ISVALIDREF(FunctionRegistry[FunctionNumber].rFunctionData)) THEN RETURN; END_IF
	IF( NOT __ISVALIDREF(FunctionRegistry[FunctionNumber].rSettingsFunction)) THEN RETURN; END_IF
	
	//check that there are no active errors in element, functon or zones
	
	ElementNumber := FunctionRegistry[FunctionNumber].rSettingsFunction.eElementLink;
	IF(__ISVALIDREF(ElementRegistry[ElementNumber].rElementData))THEN
		IF( NOT ElementRegistry[ElementNumber].rElementData.ErrorData.FunctionErrorPending AND 
			ElementRegistry[ElementNumber].rElementData.ErrorData.ErrorState = E_ErrorState.OPERATIONAL) THEN
			_M_DepositReady :=TRUE;
		END_IF	
	END_IF	
	
	
	
	// check if element/function is started
	_M_DepositReady := _M_DepositReady AND FunctionRegistry[FunctionNumber].rFunctionInterface.In.Element.OperationState.eState = E_PositionState.STARTED;								
	//check if any zone/Function/element is disabled
	_M_DepositReady := _M_DepositReady AND F_CheckAllEnabled(FunctionNumber);								
	//check if zone is occupied
	IF IsPallet THEN// pal: 
		IF FunctionRegistry[FunctionNumber].rSettingsFunction.NumberOfZones = 2 THEN 
			_M_DepositReady := _M_DepositReady AND NOT FunctionRegistry[FunctionNumber].Zone[1].refZoneData.TransportControlData.Occupied
												AND NOT FunctionRegistry[FunctionNumber].Zone[2].refZoneData.TransportControlData.Occupied;
		ELSE
			_M_DepositReady := _M_DepositReady AND NOT FunctionRegistry[FunctionNumber].Zone[1].refZoneData.TransportControlData.Occupied;
		END_IF
	ELSE// rc: 
		_M_DepositReady := _M_DepositReady AND NOT FunctionRegistry[FunctionNumber].Zone[2].refZoneData.TransportControlData.Occupied;
		
	END_IF
ELSIF FunctionNumber > F_BEGIN_EXTERN_FUNCTION AND FunctionNumber < F_END_EXTERN_FUNCTION THEN 
	_M_DepositReady := TRUE; 
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_GetDepositFuncNumber" Id="{8876fda6-6bfd-0fb4-2188-98c7b5db8c6b}">
      <Declaration><![CDATA[// Get NextFunctionNumber of the TU destination
METHOD PRIVATE _M_GetDepositFuncNumber : E_FunctionNumber
VAR_INPUT
	FunctionNumber			: E_FunctionNumber; 
END_VAR
VAR
	i,
	j :INT;
	
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[_M_GetDepositFuncNumber := F_BEGIN_FUNCTION;

IF(FunctionNumber = E_FunctionNumber.F_BEGIN_FUNCTION) THEN RETURN; END_IF

FOR i :=1 TO GVL_ShuttleRewe.MAXDEST DO
	{warning 'die array größe möglicher destinations parametrierbar machen'}
	FOR j :=1 TO 2 DO
		IF(_config.PDs[i].eDestination[j] = FunctionNumber) THEN
			_M_GetDepositFuncNumber:= _config.PDs[i].eFunctionNumber;
			EXIT;
		END_IF
	END_FOR
END_FOR]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_GetStiFunction" Id="{448ef2e0-9220-0c34-3654-2e6acdd77f39}">
      <Declaration><![CDATA[METHOD _M_GetStiFunction : ITF_ShuttleMoverGiveTransport
VAR_INPUT
	FunctionNumber: E_FunctionNumber;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[FOR i := 1 TO GVL_ShuttleRewe.MAXPOSITIONS DO 
	IF _config.PDs[i].eFunctionNumber = FunctionNumber THEN 
		_M_GetStiFunction := _config.PDs[i].iFunctionSTI;
	END_IF
END_FOR
]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_PalReady" Id="{0a59394f-a0f1-0db1-357a-e298d0242763}">
      <Declaration><![CDATA[METHOD PRIVATE _M_PalReady : BOOL
VAR_INPUT
	ShuttleNo : INT;
END_VAR
VAR_OUTPUT
	LHD	: INT;
END_VAR

VAR
	idx : INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[_M_PalReady := FALSE;

// Search for PAL index
FOR idx := ELEMENTFIRST TO ELEMENTLAST DO
	IF _Buffer[ActiveWorkarea[ShuttleNo]][idx].TU_Type = ProjectTuTypes.PALLET_SINGLE_EUR
	THEN
		IF NOT _M_CheckForLowerSequence(ShuttleNo, idx, ProjectTuTypes.RC_SINGLE)
		AND _M_DepositReady(_M_GetDepositFuncNumber(_Buffer[ActiveWorkarea[ShuttleNo]][idx].eDestination), TRUE)
		THEN
			LHD := idx;
			_M_PalReady := TRUE;
			EXIT;
		END_IF
	END_IF
END_FOR


]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_RemoveItem" Id="{f4c242b5-eb6c-018f-319b-5db58190558e}">
      <Declaration><![CDATA[METHOD _M_RemoveItem : BOOL
VAR_INPUT
	Index			: INT;
	//eFunctionNumber : E_FunctionNumber;
END_VAR

VAR
	idx : INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
FOR idx := ELEMENTFIRST TO ELEMENTLAST DO
	IF _Buffer[Index][idx].eFunctionNumber <> F_BEGIN_FUNCTION
	THEN
		_TransData[idx] := F_GetTransportControlData(eFunctionNumber:= _Buffer[Index][idx].eFunctionNumber, ZoneNumber:= 1);
		
		IF NOT _TransData[idx].Occupied
		AND NOT _TransData[idx].SensorOccupied
		AND _TransData[idx].NumberOfPresentTUs < 1
		THEN
			_Buffer[Index][idx] := _EmptyBuffer;
			_M_SortFiFo(Index);
		END_IF
	END_IF
END_FOR








]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_SortFiFo" Id="{f188492c-9168-0c64-335a-8a6abcd93817}">
      <Declaration><![CDATA[METHOD PRIVATE _M_SortFiFo : BOOL
VAR_INPUT
	Index : INT;
END_VAR
VAR
	i,j:INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[FOR i:=ELEMENTFIRST TO ELEMENTLAST-1 BY 1 DO	
	IF(_Buffer[Index][i].eFunctionNumber=0) THEN
		FOR j:=i+1 TO ELEMENTLAST BY 1 DO
			IF(_Buffer[Index][j].eFunctionNumber <> 0) THEN
				_Buffer[Index][i] := _Buffer[Index][j];
				_Buffer[Index][j] := _EmptyBuffer;				
				EXIT;
			END_IF
		END_FOR
		EXIT;	//No need iterate over entire buffer. this will be covered next cycle.
	END_IF	
END_FOR]]></ST>
      </Implementation>
    </Method>
    <Method Name="_M_SortSequenceInBuffer" Id="{02857158-e454-0691-1cd4-9713ba938b78}">
      <Declaration><![CDATA[METHOD PRIVATE _M_SortSequenceInBuffer : BOOL
VAR_INPUT
	Index	: INT; // index of buffer
END_VAR

VAR
	i, j	: INT;
	k		: INT := 0;
	_BufferSort	: ARRAY[ELEMENTFIRST..ELEMENTLAST] OF ST_StrategyBuffer;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
FOR i := ELEMENTFIRST TO ELEMENTLAST DO
	
	IF _Buffer[Index][i].eFunctionNumber <> F_BEGIN_FUNCTION
	THEN
		IF i < ELEMENTLAST
		THEN
				// Search for other index with the same destination
				FOR j := i + 1 TO ELEMENTLAST DO
					// if a other index with the same destination was found
					IF _Buffer[Index][i].eDestination = _Buffer[Index][j].eDestination
					THEN
						IF _Buffer[Index][i].Sequence > _Buffer[Index][j].Sequence
						THEN
							k := k + 1;
							_BufferSort[k] := _Buffer[Index][j];
							_Buffer[Index][j] := _EmptyBuffer;
							k := k + 1;
							_BufferSort[k] := _Buffer[Index][i];
							_Buffer[Index][i] := _EmptyBuffer;
							EXIT;
						ELSE
							k := k + 1;
							_BufferSort[k] := _Buffer[Index][i];
							_Buffer[Index][i] := _EmptyBuffer;
							EXIT;
						END_IF
					END_IF
					
					// if no other index with the same destination was found, copy index to _BufferSort
					IF j = ELEMENTLAST
					THEN
						k := k + 1;
						_BufferSort[k] := _Buffer[Index][i];	
						_Buffer[Index][i] := _EmptyBuffer;
					END_IF
			END_FOR
		
		
		// Copy last buffer index without searching
		ELSIF i = ELEMENTLAST 
		THEN
			k := k + 1;
			_BufferSort[k] := _Buffer[Index][i];	
			_Buffer[Index][i] := _EmptyBuffer;
		END_IF
	END_IF
END_FOR


// Return sorted buffer
_Buffer[Index] := _BufferSort;]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_AddToBuffer" Id="{79c66578-67e4-0c7b-2457-2f56a8f6f47d}">
      <Declaration><![CDATA[METHOD M_AddToBuffer : BOOL
VAR_INPUT
//	Enable			: BOOL;
//	ShuttleNo		: INT;
//	WorkAreas		: ARRAY[1..GVL_ShuttleRewe.NUMBER_OF_SHUTTLES] OF BOOL; // number of areas the strategy has to check
//	PDs				: ARRAY[1..GVL_ShuttleRewe.MAXDEST] OF ST_CFG_DEST;
	WorkArea		: INT;
	FunctionNumber	: INT;
END_VAR
VAR
	i : INT;
	_destination : ST_PositionIdent;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
//IF Enable THEN

//_Buffer[WorkArea] := 


// Assignment of stacked pallets to Shuttle Number 
// handle sequence for one or more active areas
//_M_HandleWorkareas();

_destination := F_GetDestination(eFunctionNumber:= FunctionNumber, ZoneNumber:= 1, DestinationIdx:= 1);
_TU_DataIndex := F_GetTU_DataIndex(eFunctionNumber:= FunctionNumber, ZoneNumber:= 1);
// Add items to Buffer 1,2 or 3
IF _TU_DataIndex <> 0 THEN 
	_M_AddItem(WorkArea, FunctionNumber, _destination.eFunctionNumber, _TU_DataIndex);
END_IF
(*
FOR idx := 1 TO GVL_ShuttleRewe.MAXDEST DO
	
	IF _config.PDs[idx].eFunctionNumber = FunctionNumber THEN 
		_destination := F_GetDestination(eFunctionNumber:= FunctionNumber, ZoneNumber:= 1, DestinationIdx:= 1);
		
		IF _destination.eFunctionNumber <> F_BEGIN_FUNCTION
		THEN
			_TU_DataIndex := F_GetTU_DataIndex(eFunctionNumber:= FunctionNumber, ZoneNumber:= 1);
			
			IF _TU_DataIndex > 0
			THEN
				IF WorkArea = _config.PDs[idx].Area THEN 
					_M_AddItem(WorkArea, _config.PDs[idx].eFunctionNumber, _Dest[idx].eFunctionNumber, _TU_DataIndex);
				END_IF
			END_IF			
		END_IF
	END_IF 
END_FOR*)
//END_IF

]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_CheckMultTakeover" Id="{99d40004-7644-07bd-3f99-7a1da418afc7}">
      <Declaration><![CDATA[METHOD M_CheckMultTakeover : BOOL
VAR_INPUT
	ShuttleNo : INT;
END_VAR
VAR_IN_OUT
	LHDs : ARRAY [1..GVL_ShuttleRewe.NUMBER_OF_LHDs] OF ST_Shuttle_LHDs;
END_VAR
VAR
	
	i,j  : INT;
	
	_TU_DataIndex : DINT;
	LHD_NO			: INT;
	
	idxLHD_1	: INT;
	idxLHD_2	: INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[M_CheckMultTakeover := FALSE;
IF (LHDs[1].ePickupFuncNumber = F_BEGIN_FUNCTION) XOR (LHDs[2].ePickupFuncNumber = F_BEGIN_FUNCTION) THEN 
	
	IF LHDs[1].ePickupFuncNumber <> F_BEGIN_FUNCTION THEN 
		_TU_DataIndex := F_GetTU_DataIndex(eFunctionNumber:= LHDs[1].ePickupFuncNumber, ZoneNumber:= 1);
		LHD_NO := 1;
	ELSE
		_TU_DataIndex := F_GetTU_DataIndex(eFunctionNumber:= LHDs[2].ePickupFuncNumber, ZoneNumber:= 1);
		LHD_NO := 2;
	END_IF
		

	IF TU_DataTable[_TU_DataIndex].DataSet.TU_Type = ProjectTuTypes.RC_SINGLE THEN 
		
		IF _M_2RcReady(ShuttleNo:= ShuttleNo, LHD_1 => idxLHD_1, LHD_2 => idxLHD_2) THEN
			LHDs[1].ePickupFuncNumber := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eFunctionNumber;
			LHDs[2].ePickupFuncNumber := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_2].eFunctionNumber;
			M_CheckMultTakeover := TRUE;
			RETURN;			
		END_IF	
	
	
		
	END_IF

END_IF
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_CheckPositionReachable" Id="{9fa83e64-ec8a-0bd7-12db-bc96d6f9aad1}">
      <Declaration><![CDATA[METHOD M_CheckPositionReachable : BOOL
VAR_INPUT
	ShuttleNo			: INT;
	DesiredPos			: DINT; // 1 - 99 [Anzufahrende positionen]
	Workarea			: INT;
END_VAR
VAR_OUTPUT
	DesiredPosInMM		: DINT; // 1 - 150000 [mm]
END_VAR
VAR
	idx : INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[							
M_CheckPositionReachable := FALSE;

FOR idx := 1 TO GVL_ShuttleRewe.MAXDEST DO
	IF (_config.PDs[idx].Position[1] = GVL_ShuttleRewe.SemiPositions[DesiredPos]
	OR _config.PDs[idx].Position[2] = GVL_ShuttleRewe.SemiPositions[DesiredPos])
	THEN
		IF ShuttleNo = 1 THEN
			IF Workarea = 1
			AND _config.PDs[idx].Area = 1
			THEN
				DesiredPosInMM := GVL_ShuttleRewe.SemiPositions[DesiredPos];
				M_CheckPositionReachable := TRUE;
				EXIT;
			
			ELSIF Workarea = 2
			AND (_config.PDs[idx].Area = 1
			OR _config.PDs[idx].Area = 2)
			THEN
				DesiredPosInMM := GVL_ShuttleRewe.SemiPositions[DesiredPos];
				M_CheckPositionReachable := TRUE;
				EXIT;
				
			ELSIF Workarea = 3
			THEN
				DesiredPosInMM := GVL_ShuttleRewe.SemiPositions[DesiredPos];
				M_CheckPositionReachable := TRUE;
				EXIT;
			END_IF
		
		ELSIF ShuttleNo = 2 THEN
			IF Workarea = 1
			AND _config.PDs[idx].Area = 2
			THEN
				DesiredPosInMM := GVL_ShuttleRewe.SemiPositions[DesiredPos];
				M_CheckPositionReachable := TRUE;
				EXIT;
			
			ELSIF Workarea = 2
			AND (_config.PDs[idx].Area = 1
			OR _config.PDs[idx].Area = 2)
			THEN
				DesiredPosInMM := GVL_ShuttleRewe.SemiPositions[DesiredPos];
				M_CheckPositionReachable := TRUE;
				EXIT;
				
			ELSIF Workarea = 2
			AND (_config.PDs[idx].Area = 2
			OR _config.PDs[idx].Area = 3)
			THEN
				DesiredPosInMM := GVL_ShuttleRewe.SemiPositions[DesiredPos];
				M_CheckPositionReachable := TRUE;
				EXIT;
				
			ELSIF Workarea = 3
			THEN
				DesiredPosInMM := GVL_ShuttleRewe.SemiPositions[DesiredPos];
				M_CheckPositionReachable := TRUE;
				EXIT;
			END_IF
		
		ELSIF ShuttleNo = 3 THEN
			IF Workarea = 1
			AND _config.PDs[idx].Area = 3
			THEN
				DesiredPosInMM := GVL_ShuttleRewe.SemiPositions[DesiredPos];
				M_CheckPositionReachable := TRUE;
				EXIT;
			
			ELSIF Workarea = 2
			AND (_config.PDs[idx].Area = 2
			OR _config.PDs[idx].Area = 3)
			THEN
				DesiredPosInMM := GVL_ShuttleRewe.SemiPositions[DesiredPos];
				M_CheckPositionReachable := TRUE;
				EXIT;
				
			ELSIF Workarea = 3
			THEN
				DesiredPosInMM := GVL_ShuttleRewe.SemiPositions[DesiredPos];
				M_CheckPositionReachable := TRUE;
				EXIT;
			END_IF
		END_IF
	END_IF
END_FOR

]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_DeleteOrder" Id="{012f49ce-cf9c-0846-086f-85bf0776b0d3}">
      <Declaration><![CDATA[METHOD M_DeleteOrder : BOOL
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[FOR i := 1 TO GVL_ShuttleRewe.NUMBER_OF_SHUTTLES DO 
	// Delete from _Buffer if element is not occupied anymore depending on active workareas	

	_M_RemoveItem(i);
	_M_SortSequenceInBuffer(i);	
END_FOR]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_GetNextOrder" Id="{c6e3d8e1-8e38-06f0-337c-1132dfe9e34d}">
      <Declaration><![CDATA[METHOD M_GetNextOrder : BOOL;
VAR_INPUT
	ShuttleNo		: INT;
	WorkAreas		: ARRAY[1..GVL_ShuttleRewe.NUMBER_OF_SHUTTLES] OF BOOL; // number of areas the strategy has to check
	LHD_1_FunctionNumber : E_FunctionNumber;
	LHD_2_FunctionNumber : E_FunctionNumber;
	// als input vllt. den wirkbereich angeben, 
	// damit hier nicht nach wirkbereich unterschieden werden muss
END_VAR

VAR_OUTPUT
	LHD_1	: E_FunctionNumber;
	LHD_2	: E_FunctionNumber;
END_VAR

VAR
	i : INT;
	j : INT;
	idxLHD_1	: INT;
	idxLHD_2	: INT;
	
	NumberOfActiveAreas : INT;
	
	DepositFuncNumber : E_FunctionNumber;
	
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[

M_GetNextOrder := FALSE;
LHD_1 := F_BEGIN_FUNCTION;
LHD_2 := F_BEGIN_FUNCTION;

FOR i := 1 TO GVL_ShuttleRewe.NUMBER_OF_SHUTTLES DO 
	// Delete from _Buffer if element is not occupied anymore depending on active workareas	

	_M_RemoveItem(i);
	_M_SortSequenceInBuffer(i);	
END_FOR

NumberOfActiveAreas := 0; 
FOR i := 1 TO GVL_ShuttleRewe.NUMBER_OF_SHUTTLES DO 
	IF WorkAreas[i] THEN 
		NumberOfActiveAreas := NumberOfActiveAreas + 1; 
	END_IF
END_FOR


// Count number of orders to switch areas after max number of orders are reached
IF NumberOfActiveAreas > 1 THEN 
	IF _Counter[ShuttleNo] < _CounterMax THEN 
		
		IF ActiveWorkarea[ShuttleNo] = 0 THEN 
			ActiveWorkarea[ShuttleNo] := ShuttleNo;
		END_IF
	ELSE
		_Counter[ShuttleNo] := 0;
		ActiveWorkarea[ShuttleNo] := _GetNextActiveWorkarea(ShuttleNo,
															NumberOfActiveAreas, 
															ActiveWorkArea[ShuttleNo], 
															WorkAreas) ;
	END_IF
ELSE
	_Counter[ShuttleNo] := 0;
	ActiveWorkarea[ShuttleNo] := ShuttleNo;
END_IF


IF ActiveWorkarea[ShuttleNo] = _PalletStackArea
AND _PalletStackPickup <> F_BEGIN_FUNCTION
AND _M_DepositReady(_PalletStackDeposit, TRUE)
//and _M_PalletStackPickup 
THEN
	LHD_1 := _PalletStackPickup;
		
	_config.PDs[i].iFunctionSTI.M_SetAdjecentSide(eFunctionNumber := LHD_1_FunctionNumber, eFunctionName := FunctionRegistry[LHD_1_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
	RETURN;
	
ELSE
		// if first element is a pallet
		IF _Buffer[ActiveWorkarea[ShuttleNo]][1].TU_Type = ProjectTuTypes.PALLET_SINGLE_EUR
		THEN
			IF _M_PalReady(ShuttleNo:= ShuttleNo, LHD => idxLHD_1) THEN
				LHD_1 := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eFunctionNumber;
				M_GetNextOrder := TRUE;
				_Counter[ShuttleNo] := _Counter[ShuttleNo] + 1;
				DepositFuncNumber := _M_GetDepositFuncNumber( _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eDestination);
				 _M_GetStiFunction(DepositFuncNumber).M_SetAdjecentSide(eFunctionNumber := LHD_1_FunctionNumber, eFunctionName := FunctionRegistry[LHD_1_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
				
				RETURN;
				
			ELSE
				IF _M_2RcReady(ShuttleNo:= ShuttleNo, LHD_1 => idxLHD_1, LHD_2 => idxLHD_2) THEN
					LHD_1 := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eFunctionNumber;
					LHD_2 := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_2].eFunctionNumber;
					M_GetNextOrder := TRUE;
					_Counter[ShuttleNo] := _Counter[ShuttleNo] + 1;
					DepositFuncNumber := _M_GetDepositFuncNumber( _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eDestination);
					_M_GetStiFunction(DepositFuncNumber).M_SetAdjecentSide(eFunctionNumber := LHD_1_FunctionNumber, eFunctionName := FunctionRegistry[LHD_1_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
					DepositFuncNumber := _M_GetDepositFuncNumber( _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_2].eDestination);
					_M_GetStiFunction(DepositFuncNumber).M_SetAdjecentSide(eFunctionNumber := LHD_2_FunctionNumber, eFunctionName := FunctionRegistry[LHD_2_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
				
					RETURN;
					
				ELSIF _M_1RcReady(ShuttleNo:= ShuttleNo, LHD_1 => idxLHD_1, LHD_2 => idxLHD_2) THEN
					IF idxLHD_1 <> 0 THEN
						LHD_1 := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eFunctionNumber;
						DepositFuncNumber := _M_GetDepositFuncNumber( _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eDestination);
						_M_GetStiFunction(DepositFuncNumber).M_SetAdjecentSide(eFunctionNumber := LHD_1_FunctionNumber, eFunctionName := FunctionRegistry[LHD_1_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
					ELSIF idxLHD_2 <> 0 THEN
						LHD_2 := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_2].eFunctionNumber;
						DepositFuncNumber := _M_GetDepositFuncNumber( _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_2].eDestination);
						_M_GetStiFunction(DepositFuncNumber).M_SetAdjecentSide(eFunctionNumber := LHD_2_FunctionNumber, eFunctionName := FunctionRegistry[LHD_2_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
						
					END_IF
					M_GetNextOrder := TRUE;
					_Counter[ShuttleNo] := _Counter[ShuttleNo] + 1;
					
					RETURN;
					
				END_IF
			END_IF
			
			
		// if first element is not a pallet	
		ELSE 
			IF _M_2RcReady(ShuttleNo:= ShuttleNo, LHD_1 => idxLHD_1, LHD_2 => idxLHD_2) THEN
				LHD_1 := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eFunctionNumber;
				LHD_2 := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_2].eFunctionNumber;
				M_GetNextOrder := TRUE;
				_Counter[ShuttleNo] := _Counter[ShuttleNo] + 1;
				DepositFuncNumber := _M_GetDepositFuncNumber( _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eDestination);
				_M_GetStiFunction(DepositFuncNumber).M_SetAdjecentSide(eFunctionNumber := LHD_1_FunctionNumber, eFunctionName := FunctionRegistry[LHD_1_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
				DepositFuncNumber := _M_GetDepositFuncNumber( _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_2].eDestination);
				_M_GetStiFunction(DepositFuncNumber).M_SetAdjecentSide(eFunctionNumber := LHD_2_FunctionNumber, eFunctionName := FunctionRegistry[LHD_2_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
				
				RETURN;
				
			ELSIF _M_PalReady(ShuttleNo:= ShuttleNo, LHD => idxLHD_1) THEN
				LHD_1 := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eFunctionNumber;
				M_GetNextOrder := TRUE;
				_Counter[ShuttleNo] := _Counter[ShuttleNo] + 1;
				DepositFuncNumber := _M_GetDepositFuncNumber( _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eDestination);
				_M_GetStiFunction(DepositFuncNumber).M_SetAdjecentSide(eFunctionNumber := LHD_1_FunctionNumber, eFunctionName := FunctionRegistry[LHD_1_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
				
				RETURN;
			
			ELSIF _M_1RcReady(ShuttleNo:= ShuttleNo, LHD_1 => idxLHD_1, LHD_2 => idxLHD_2) THEN
				
				IF idxLHD_1 <> 0 THEN
					LHD_1 := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eFunctionNumber;
					DepositFuncNumber := _M_GetDepositFuncNumber( _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_1].eDestination);
					_M_GetStiFunction(DepositFuncNumber).M_SetAdjecentSide(eFunctionNumber := LHD_1_FunctionNumber, eFunctionName := FunctionRegistry[LHD_1_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
				ELSIF idxLHD_2 <> 0 THEN
					LHD_2 := _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_2].eFunctionNumber;
					DepositFuncNumber := _M_GetDepositFuncNumber( _Buffer[ActiveWorkarea[ShuttleNo]][idxLHD_2].eDestination);
					_M_GetStiFunction(DepositFuncNumber).M_SetAdjecentSide(eFunctionNumber := LHD_2_FunctionNumber, eFunctionName := FunctionRegistry[LHD_2_FunctionNumber].Zone[1].ZoneName ,ZoneNumber := 1);
				END_IF
				
				M_GetNextOrder := TRUE;
				_Counter[ShuttleNo] := _Counter[ShuttleNo] + 1;
				
				
				RETURN;
				
			END_IF
		END_IF
END_IF

//no new orders for that area
_Counter[ShuttleNo] := _CounterMax;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_SetPD" Id="{e7a42773-e873-021a-0ff6-11c5ac283ad3}">
      <Declaration><![CDATA[METHOD M_SetPD : BOOL
VAR_INPUT
	PDs : ARRAY[1..GVL_ShuttleRewe.MAXDEST] OF ST_CFG_DEST;;
END_VAR
VAR_IN_OUT
	//ErrorData : ST_ErrorDataSet;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[_config.PDs := PDs;

FOR i := 1 TO GVL_ShuttleRewe.MAXPOSITIONS DO 
	IF _config.PDs[i].QVW_PositionType = E_QVW_PositionType.PalletStackDeposit THEN 
		_PalletStackPickup := _config.PDs[i].eFunctionNumber;
	ELSIF _config.PDs[i].QVW_PositionType = E_QVW_PositionType.PalletStackPickup THEN  
		_PalletStackDeposit := _config.PDs[i].eFunctionNumber;
		_PalletStackArea := _config.PDs[i].Area;
	END_IF
END_FOR
]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>