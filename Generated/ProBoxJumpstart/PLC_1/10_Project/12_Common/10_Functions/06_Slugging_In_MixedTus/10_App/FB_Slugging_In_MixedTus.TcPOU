﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_Slugging_In_MixedTus" Id="{10a0296b-f18b-47cc-8bfb-9bc4c7ebc0ad}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_Slugging_In_MixedTus EXTENDS FB_StraightBase;
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date:$
 *	Last changed by	:	$Author:  $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Kombi Slugging straight transport for several zones. The length of the zones 
 *						may be little bit shorter than the length of the TUs. By longer TUs
 *						stop sensor will be delayed set to occupied, in order to taken over whole
 *						longer TU.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				09.11.2021		dlu					First version
 *
 **************************************************************************************)
VAR_INPUT
	Config					: ST_CFG_Slugging_In_MixedTus; // Configuration
	HW_Inputs		 		: ST_HW_InputsProBoxStraightTransport; // Hardware inputs
END_VAR

VAR_OUTPUT
END_VAR

VAR
	Inputs					: ST_InputsProBoxStraightTransport; // Function specific inputs
	fbTransportControl 		: ARRAY[1..NUMBER_OF_ZONES_PER_FUNCTION] OF FB_ProBoxTransportControl;	
	Idx						: INT; // Iterator
	ZoneIdx					: INT; // Iterator
	EmptyManualCommands		: ST_ITC_ManualMotion; // Empty manual commands				
	fb_ExtendedRun			: FB_ExtendedRun;
	fb_DelayedSensorActive  : FB_DelayedSensorActive; // Observe and set delaying sensor after certain distance
END_VAR

VAR PERSISTENT
	Settings				: ST_CFG_Slugging_In_MixedTus; // Settings - activated configuration 
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Input mapping
***************************************************************************************************************)
A_InputMapping();

(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   External Subsystem transport interface
***************************************************************************************************************)
A_ExternalSTI();

(**************************************************************************************************************
   State control
***************************************************************************************************************)
A_StateControl();

(**************************************************************************************************************
   Error handler
***************************************************************************************************************)
A_ErrorHandler();

(**************************************************************************************************************
   Process AddOns
***************************************************************************************************************)
M_ProcessAddOns();

(**************************************************************************************************************
   Manual control
***************************************************************************************************************)
A_ManualControl();

(**********************************************************************************************************
	   Transport link
***********************************************************************************************************)
// Transport link should be updated before calling the transport controller.
_M_TransportLink();
	
(**************************************************************************************************************
   Extended run
***************************************************************************************************************)
fb_ExtendedRun(
	HostFunction:= SettingsFunction.eFunctionNumber,	
	HWSensorStop:= Inputs.Sensor_StopExtRun, 
	Enable:= TRUE, 
	Config:= Settings.ExtendedRun,	 
	ErrorData:= ZoneData[Settings.ExtendedRun.FunctionZone].ErrorData.ErrorDataSet[2]);


(**************************************************************************************************************
   Call TransportControl
***************************************************************************************************************)
_M_TransportControl();	

(**************************************************************************************************************
   Interface handler out
***************************************************************************************************************)
M_ITC_ProcessOut();

(**************************************************************************************************************
   Output mapping
***************************************************************************************************************)
A_OutputMapping();	]]></ST>
    </Implementation>
    <Method Name="_M_TransportControl" Id="{02c93f92-435d-4304-9aff-136880edb54a}">
      <Declaration><![CDATA[METHOD PRIVATE _M_TransportControl : BOOL
VAR
	SlugSensorOccupied		: BOOL;	// Assigned occupied sensor first
	StopSensorOccupied		: BOOL;	// Assigned occupied sensor	last
	CreepSensor				: BOOL; // Creep sensor
	ZoneOccupied			: BOOL; // Zone physical occupied
	CtrlInputPbTc			: ST_CtrlInPbTc; //Control input for Probox transportControl
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//Reset errors
F_ResetError(FALSE,FunctionData.ErrorData.ErrorDataSet[1]);

// Check transport direction of the conveyor
SettingsFunction.FlowDirectionREV := _M_DefineTransportDirection(FunctionData.ErrorData.ErrorDataSet[1]);


// Assign sensor occpupied
_M_Check_HW_SensorInput(
					Sensor_Stop_12		:= Inputs.Sensor_Stop_12, 
					Sensor_Stop_21		:= Inputs.Sensor_Stop_21, 
					Sensor_Slug_12		:= Inputs.Sensor_First_12, 
					Sensor_Slug_21		:= Inputs.Sensor_First_21,
					Sensor_Creep_12		:= Inputs.Sensor_Creep_12,
					Sensor_Creep_21		:= Inputs.Sensor_Creep_21, 
					FlowDirectionREV	:= SettingsFunction.FlowDirectionREV, 
					HasSluggingSensor	:= Settings.TwoSensorFunction,
					ExtRunSynteticSensor:= fb_ExtendedRun.ExtRunSynteticSensor, 
					StopSensorOccupied	=> StopSensorOccupied, 
					SlugSensorOccupied	=> SlugSensorOccupied,
					CreepSensor			=> CreepSensor);
	

// call Straight transport method of base class

CtrlInputPbTc.EnableSlugging 	:= FunctionInterface.In.eNextFunctionNumber = E_FunctionNumber.F_BEGIN_FUNCTION;
CtrlInputPbTc.SlugSensor		:= SlugSensorOccupied;
CtrlInputPbTc.SlugSensorExists	:= Settings.TwoSensorFunction;
CtrlInputPbTc.StopSensor		:= StopSensorOccupied;

FOR ZoneIdx := 1 TO SettingsFunction.NumberOfZones DO
		
	M_StraightTransport(
		fbTransportControl		:=fbTransportControl[ZoneIdx],
		ZoneIx					:= ZoneIdx, 
		ExtDisableTakeover		:= FALSE, 
		ExtDisableHandover		:= fb_ExtendedRun.ExtRunStarted, 
		ExtDisableTransport		:= FALSE,		
		CtrlInputPbTc			:= CtrlInputPbTc,
		CreepHwSensor			:= CreepSensor, 
		Orientation_Axis		:= Settings.Orientation_Axis_12);
END_FOR
]]></ST>
      </Implementation>
    </Method>
    <Action Name="A_ExternalSTI" Id="{737b2425-2b73-4c69-b978-289abcd2f6a6}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_ExternalSTI
 * 	FUNCTION	External Subsystem transport interface
 **************************************************************************************)


(**************************************************************************************
   External Subsystem transport interfaces
***************************************************************************************)
FOR Idx := 1 TO NUMBER_OF_STI DO
	// Is an external Subsystem transport interface configured
	IF SettingsFunction.eExternalSTI_FunctionNumber[Idx] > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION AND
	   SettingsFunction.eExternalSTI_FunctionNumber[Idx] < E_FunctionNumber.F_END_EXTERN_FUNCTION THEN
	   
		// When interface is valid
		IF __QUERYINTERFACE(fbSTI_Channels[SettingsFunction.eExternalSTI_FunctionNumber[Idx]].Instance,FunctionBase.STI_Instance) THEN
			// Call external inbound interface
			fbSTI_Channels[SettingsFunction.eExternalSTI_FunctionNumber[Idx]].Instance.M_CallInterface(
				mErrorData				:= FunctionData.ErrorData.ErrorDataSet[1]);
		END_IF
	END_IF
END_FOR
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_Init" Id="{aab5e826-3d98-4440-b3cd-05d6957be3c6}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize function
 **************************************************************************************)
 
// Update Registry
M_UpdateRegistry();
 

// Init ongoing - Wait until init is released (Sequencial startup)
IF NOT FunctionData.Init AND FunctionData.InitRunning THEN
	// When the subsystem is started, 
	// When its released to initialize
	IF FunctionData.OperationState.Info.SystemReady AND
	   SettingsFunction.eFunctionNumber < FunctionData.OperationState.InitRelease.eLimitFunctionNumber THEN
	   
	   IF M_InitFunction(FALSE) THEN
			// Function initialized - and initialization done
			FunctionData.Init := TRUE;
			FunctionData.InitRunning := FALSE;
			
			// Set reset cmd
			FunctionData.ErrorData.Error.ResetError := TRUE;
			
			DebugMsg := CONCAT('Initialization done. Function: ', ConfigFunction.FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
		END_IF
	END_IF
(*
If the function is not initialized,
clean all values and wait until init is released
*)
ELSIF NOT FunctionData.Init THEN
	// Reset internal variables
	FunctionData.OperationState := FunctionBase.PositionStateEmpty;

	// Initiate initialization of all zones
	FOR ZoneIdx := 1 TO SettingsFunction.NumberOfZones DO
		ZoneData[ZoneIdx].Init := FALSE;
	END_FOR;
	
	// Load function specific configuration to settings
	Settings := Config;
	
	// Reset values and load config
	M_PreInit();
	
	// To start init a valid functionnumber and element link is needed
	FunctionData.InitRunning := SettingsFunction.eFunctionNumber > 0 AND
								SettingsFunction.eElementLink > 0;
END_IF

// Init zones
IF FunctionData.Init THEN
	// Set init flag after one cycle
	FOR ZoneIdx := 1 TO SettingsFunction.NumberOfZones DO
		// If a zone is not initialized, wait one cycle to finish init
		IF NOT ZoneData[ZoneIdx].Init THEN
			IF ZoneData[ZoneIdx].InitRunning THEN
				ZoneData[ZoneIdx].InitRunning := FALSE;
				ZoneData[ZoneIdx].Init := TRUE;
				ZoneData[ZoneIdx].ErrorData.Error.ResetError := TRUE;
			ELSE	
				ZoneData[ZoneIdx].InitRunning := TRUE;
			END_IF
		END_IF
	END_FOR;
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InputMapping" Id="{0c68a13d-a52a-4d7a-a1d3-7aa580e5cafb}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InputMapping
 * 	FUNCTION	Input mapping
 **************************************************************************************)
 
(**************************************************************************************
   Hardware Inputs
***************************************************************************************)
// Adapt input logic state high/low
Inputs.Sensor_Stop_12		:= Settings.HW_InputInverted.Sensor_Stop_12 XOR HW_Inputs.Sensor_Stop_12;
//Inputs.Sensor_First_12		:= Settings.HW_InputInverted.Sensor_First_12 XOR HW_Inputs.Sensor_First_12;
Inputs.Sensor_Slow_12		:= Settings.HW_InputInverted.Sensor_Slow_12 XOR HW_Inputs.Sensor_Slow_12;
Inputs.Sensor_Creep_12		:= Settings.HW_InputInverted.Sensor_Creep_12 XOR HW_Inputs.Sensor_Creep_12;
Inputs.Sensor_Stop_21		:= Settings.HW_InputInverted.Sensor_Stop_21 XOR HW_Inputs.Sensor_Stop_21;
Inputs.Sensor_First_21		:= Settings.HW_InputInverted.Sensor_First_21 XOR HW_Inputs.Sensor_First_21;
Inputs.Sensor_Slow_21 		:= Settings.HW_InputInverted.Sensor_Slow_21 XOR HW_Inputs.Sensor_Slow_21;
Inputs.Sensor_Creep_21		:= Settings.HW_InputInverted.Sensor_Creep_21 XOR HW_Inputs.Sensor_Creep_21;
Inputs.Sensor_StopExtRun	:= Settings.HW_InputInverted.Sensor_StopExtRun XOR HW_Inputs.Sensor_StopExtRun;

(**   Function Custom Part     ****************************************************************)
// Slug Sensor Sensor_First_12 will be delaeyd triggered for certain TUs
fb_DelayedSensorActive(EnableDelaySensor :=	M_EnableSensorDeactivation(), // Enabled delaying of sensor
					   ObservingSignal := Settings.HW_InputInverted.Sensor_First_12 XOR HW_Inputs.Sensor_First_12,
					   DelayedDistance := M_GetTUInactiveSensorDistance(),
					   EncoderValueInc := FunctionInterface.In.Element.State.EncoderValue);

Inputs.Sensor_First_12 := fb_DelayedSensorActive.Q;

(**************************************************************************************
   Element states
***************************************************************************************)
// Get status of the corresponding element
M_GetElementStates(); 

(**************************************************************************************
   AddOn states
***************************************************************************************)
// Get status of all corresponding AddOn functions and store it in FunctionInterface.In.AddOnOrder
M_GetAddOnOrder();

]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_ManualControl" Id="{9bdddb17-4ff4-4f4c-8541-299fe5fc8883}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_ManualControl
 * 	FUNCTION	Control manual movement
 **************************************************************************************)

 
(**************************************************************************************
   Reset manual commands
***************************************************************************************)

// If function is not in manual mode
IF FunctionData.OperationState.eMode <> E_PositionMode.MANUAL_MODE THEN
	// Reset manual commands
	ITC.ManualMotion := EmptyManualCommands;
END_IF

(**************************************************************************************
   Enable available axis, buttons, features
***************************************************************************************)
// Example:
//	ITC.ManualMotion.Axis[1].Forward.Enable := TRUE;


]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OutputMapping" Id="{84a74620-048e-466b-9784-fbe0a202ecaf}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OutputMapping
 * 	FUNCTION	Output mapping
 **************************************************************************************)
 
(**************************************************************************************
   Order outputs (Commands to corresponding element)
***************************************************************************************)
THIS^.M_OutputMapping(Settings.Orientation_Axis_12,FALSE);

(**************************************************************************************************************
   Extended run
***************************************************************************************************************)

fb_ExtendedRun.M_SetProBoxFunctionOrder(FunctionOrders:= FunctionInterface.Out.FunctionOrders);
// ---------------------------------------------------
//Inhibit stopping for the function when ExtendedRun is driving
THIS^.InhibitStopping := fb_ExtendedRun.ExtRunStarted;]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_EnableSensorDeactivation" Id="{2cbb6eee-912b-4c83-914d-1a84f9f519cd}">
      <Declaration><![CDATA[METHOD PRIVATE M_EnableSensorDeactivation : BOOL
]]></Declaration>
      <Implementation>
        <ST><![CDATA[M_EnableSensorDeactivation := FALSE;

// TU_Index exists on 1 Zone
IF ZoneData[1].TransportControlData.TU_DataIndex > 0 THEN
	
	M_EnableSensorDeactivation := TRUE;
END_IF]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_GetTUInactiveSensorDistance" Id="{7c41e236-bd0a-4e79-84f8-1bae54cd50fc}">
      <Declaration><![CDATA[METHOD PRIVATE M_GetTUInactiveSensorDistance : DINT

VAR
	TU_DataIndex				: DINT;
	ShortTUInactiveSlugSensorDistance: INT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[M_GetTUInactiveSensorDistance := 0;

// TU_Index exists on 1 Zone
IF ZoneData[1].TransportControlData.TU_DataIndex > 0 THEN
	
	TU_DataIndex := ZoneData[1].TransportControlData.TU_DataIndex;
	
	// There is a short TU
	IF (TU_Table.TU_DataTable[TU_DataIndex].DataSet.TU_Type = Settings.SmallTransportUnit_TU_Type_1) OR 
		(TU_Table.TU_DataTable[TU_DataIndex].DataSet.TU_Type = Settings.SmallTransportUnit_TU_Type_2) OR
		(TU_Table.TU_DataTable[TU_DataIndex].DataSet.TU_Type = Settings.SmallTransportUnit_TU_Type_3) THEN
		M_GetTUInactiveSensorDistance := Settings.ShortTUInactiveSlugSensorDistance;
	// there is a long TU
	ELSE
		M_GetTUInactiveSensorDistance := Settings.LongTUInactiveSlugSensorDistance;
	END_IF
END_IF]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>