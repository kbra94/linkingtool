﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_Slugging_In_MixedTusEmulation" Id="{88e87ae4-fffb-4dd1-8ed2-2638f7445e2f}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_Slugging_In_MixedTusEmulation EXTENDS FB_FunctionConvEmulation
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date:$
 *	Last changed by	:	$Author:  $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Emulation of straight transport function with zone logic for TUs which may be longer
 *						than length of the zones.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				09.11.2021		dlu					First version
 * 
 **************************************************************************************)
 VAR
	pFunction						: POINTER TO FB_Slugging_In_MixedTus; // Address to function
	rFunction						: REFERENCE TO FB_Slugging_In_MixedTus; // Reference to function
	fbTransportControlEmulation		: ARRAY[1..NUMBER_OF_ZONES_PER_FUNCTION] OF FB_PB_TransportControlEmulation; // Transport control emulation
	fbEncoder						: FB_PB_Element_Encoder();
	ZoneIdx							: INT;
	ElementDriveRunning				: BOOL; // Feedback drive is running from element
	ElementSpeed					: INT; // Feedback speed from element	
	SensorFirst						: BOOL; //test
	SensorStop						: BOOL; //test
	EmuSensors						: ARRAY [1..NUMBER_OF_ZONES_PER_FUNCTION] OF BOOL;
	DebugSensors					: ARRAY [1..2] OF BOOL; //Slug / Stop sensor
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();

(**************************************************************************************************************
   Emulation
***************************************************************************************************************)
// If emulation function is initialized
IF FunctionData.Init THEN

	// Update data of real object
	M_UpdateRealObjectData();
	
	// Handle external Subsystem-Subsystem interfaces
	A_ExternalSTI();
	
	// Assign feedback from element
	IF rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_12 OR rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_21 THEN
		ElementDriveRunning := FunctionBase.rFunctionInterfaceReal.In.Element.State.DriveRunning AND FunctionBase.rFunctionInterfaceReal.Out.FunctionOrders.ReqDriveRun;
		ElementSpeed := FunctionBase.rFunctionInterfaceReal.In.Element.State.Speed;
	ELSIF rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_34 OR rFunction.Config.Orientation_Axis_12 = E_Axis_Orientation.Element_43 THEN
		ElementDriveRunning := FunctionBase.rFunctionInterfaceReal.In.Element.State.DriveRunning_34 AND FunctionBase.rFunctionInterfaceReal.Out.FunctionOrders.ReqDriveRun_34;
		ElementSpeed := FunctionBase.rFunctionInterfaceReal.In.Element.State.Speed_34;
	ELSE
		ElementDriveRunning := FALSE;
		ElementSpeed := 0;
	END_IF
	
	//Run encoder positon, 
//TODO: Move to Element???
	fbEncoder(	Run:=ElementDriveRunning, 
				Speed:= ProBoxSystem.EMULATION_SPEED);
	
	FOR ZoneIdx := 1 TO rFunction.SettingsFunction.NumberOfZones DO	
		
		// Call transport control 
		fbTransportControlEmulation[ZoneIdx](
			Param 					:= M_GetTransportControlParam(mZoneNumber := ZoneIdx), // Parameter of transport control
			PrevTransportData 		:= M_GetPreviousTransportData(mZoneNumber := ZoneIdx), // Previous transport data
			NextTransportData 		:= M_GetNextTransportData(mZoneNumber := ZoneIdx), // Next transport data	
			TransportEnable			:= TRUE, // Transport enable	
			DriveRunning			:= ElementDriveRunning, // 
			EncoderValue			:= fbEncoder.EncoderValue, //FunctionBase.rFunctionInterfaceReal.In.Element.State.EncoderValue
			ZoneData 				:= ZoneData[ZoneIdx]); // Zone data
			
	END_FOR

A_OverWriteInputs();	

END_IF]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{a4fea9a0-c35a-484e-a809-63a4a66dba93}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated dummy function
 **************************************************************************************)

// If dummy function is not initialized
IF NOT FunctionData.Init Then
	// Check that linked dummy function number is valid
	IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
	   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN   
		
		// Check if the real module is initialized
		IF F_IsFunctionInitialized(eFunctionNumber := eFunctionNumber) THEN
			// Module is initialized
			FunctionData.Init:= TRUE;
			
			// Send debug message
			DebugMsg := CONCAT('Initialization done. Element Function: ', FunctionRegistry[eFunctionNumber].FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
			
			// Initialize real module
			F_InitializeFunction(eFunctionNumber := eFunctionNumber);
		END_IF
	END_IF
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs" Id="{ff6fe4e6-a390-4b45-b6be-6c54ec3e5005}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OverWriteInputs
 * 	FUNCTION	Overwrite inputs for emulation
 **************************************************************************************)
FOR ZoneIdx:=1 TO rFunction.SettingsFunction.NumberOfZones DO
	ZoneData[ZoneIdx].TransportControlData.TransportParam.ZoneLength := FunctionBase.rSettingsFunctionReal.Zone[ZoneIdx].Length[1];	
END_FOR

FOR ZoneIdx:=1 TO rFunction.SettingsFunction.NumberOfZones DO
	EmuSensors[ZoneIdx] :=  fbTransportControlEmulation[ZoneIdx].M_TU_PresentState(
											mPosition := FunctionBase.rSettingsFunctionReal.Zone[ZoneIdx].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[ZoneIdx].ZoneEndLengthToSide[2],
											mIndex => FunctionBase.IndexToRemove[ZoneIdx],
											mLeadingEdgePos => ZoneData[ZoneIdx].TransportControlData.TU_LeadingEdgePos);
END_FOR

//Set Sensor at first zone
IF(rFunction.SettingsFunction.NumberOfZones>1 AND rFunction.Settings.TwoSensorFunction) THEN
	IF(FunctionBase.rSettingsFunctionReal.FlowDirectionREV) THEN
		//RWD drive direction
		SensorFirst:= EmuSensors[rFunction.SettingsFunction.NumberOfZones];
	ELSE
		//FWD drive direction
		SensorFirst:= EmuSensors[1];
	END_IF	
END_IF
//Set sensor at last zone
IF(FunctionBase.rSettingsFunctionReal.FlowDirectionREV) THEN
	//RWD drive direction
	SensorStop := EmuSensors[1];
ELSE
	//FWD drive direction
	SensorStop := EmuSensors[rFunction.SettingsFunction.NumberOfZones];
END_IF


//Set HW outputs, Keep input if direction is changed.
rFunction.HW_Inputs.Sensor_First_12 := sensorFirst AND(NOT FunctionBase.rSettingsFunctionReal.FlowDirectionREV OR rFunction.HW_Inputs.Sensor_First_12);
rFunction.HW_Inputs.Sensor_First_21 := sensorFirst AND(FunctionBase.rSettingsFunctionReal.FlowDirectionREV OR rFunction.HW_Inputs.Sensor_First_21);
rFunction.HW_Inputs.Sensor_Stop_12 := sensorStop AND(NOT FunctionBase.rSettingsFunctionReal.FlowDirectionREV OR rFunction.HW_Inputs.Sensor_Stop_12);
rFunction.HW_Inputs.Sensor_Stop_21 := sensorStop AND(FunctionBase.rSettingsFunctionReal.FlowDirectionREV OR rFunction.HW_Inputs.Sensor_Stop_21);

rFunction.HW_Inputs.Sensor_First_12 := (rFunction.HW_Inputs.Sensor_First_12 OR DebugSensors[1]) AND rFunction.Config.TwoSensorFunction;
rFunction.HW_Inputs.Sensor_Stop_12 := rFunction.HW_Inputs.Sensor_Stop_12 OR DebugSensors[2];
													
//Set extended run stop sensor 10 mm after the normal function stop sensor.
IF(rFunction.Config.ExtendedRun.Activated) THEN
	ZoneIdx:=rFunction.Config.ExtendedRun.FunctionZone;
	IF(ZoneIdx>0 AND ZoneIdx<=NUMBER_OF_ZONES_PER_FUNCTION) THEN
		rFunction.HW_Inputs.Sensor_StopExtRun := ZoneData[ZoneIdx].TransportControlData.TU_LeadingEdgePos > FunctionBase.rSettingsFunctionReal.Zone[ZoneIdx].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[ZoneIdx].ZoneEndLengthToSide[2] + 50;
	END_IF
END_IF

rFunction.HW_Inputs.Sensor_Creep_12 := ZoneData[1].TransportControlData.TU_LeadingEdgePos > FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[2] - 100;
rFunction.HW_Inputs.Sensor_Slow_12 := ZoneData[1].TransportControlData.TU_LeadingEdgePos > FunctionBase.rSettingsFunctionReal.Zone[1].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[1].ZoneEndLengthToSide[2] - 200;

]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{e80fb7fe-36ea-4453-bdcd-24f05463bd60}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid function number
IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN

	// Is reference valid  
	IF __ISVALIDREF(FunctionRegistry[eFunctionNumber].rFunction) THEN
		// Build address to reference
		pFunction := ADR(FunctionRegistry[eFunctionNumber].rFunction);

		// If address is possible
		IF pFunction <> 0 THEN
			// Build reference to memory
			rFunction REF= pFunction^;
		ELSE
			// Reinitialize module
			FunctionData.Init:= FALSE;
		END_IF
	ELSE
		// Reinitialize module
		FunctionData.Init:= FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_GetTU_PresentState" Id="{853ffd95-3b28-4619-b4f1-04aa5075a9c5}">
      <Declaration><![CDATA[METHOD M_GetTU_PresentState : BOOL
(**************************************************************************************
 * 	Application		:	ProBox
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date: $
 *	Last changed by	:	$Author: q8Hedls $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Get TU present state, overriden from base object
 *
 **************************************************************************************)
VAR_INPUT
	mZone	 : INT; // Zone number
	mPosition : UINT; // Position [mm]
END_VAR
VAR_OUTPUT
	mTU_DataIndex : DINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF(mZone>=1 AND mZone<= NUMBER_OF_ZONES_PER_FUNCTION) THEN
	M_GetTU_PresentState := fbTransportControlEmulation[mZone].M_TU_PresentState(mPosition := mPosition, mTU_DataIndex => mTU_DataIndex);
END_IF
]]></ST>
      </Implementation>
    </Method>
    <ObjectProperties>
      <XmlArchive>
        <Data>
          <o xml:space="preserve" t="UMLStereoTypeContainerObject">
            <v n="IsType" t="UMLType">BaseArea</v>
            <v n="Stereotype">""</v>
            <d n="Stereotypes" t="Hashtable" />
          </o>
        </Data>
        <TypeList>
          <Type n="Hashtable">System.Collections.Hashtable</Type>
          <Type n="String">System.String</Type>
          <Type n="UMLStereoTypeContainerObject">{30250973-b110-4e31-b562-c102e042dca4}</Type>
          <Type n="UMLType">{0197b136-405a-42ee-bb27-fd08b621d0cf}</Type>
        </TypeList>
      </XmlArchive>
    </ObjectProperties>
  </POU>
</TcPlcObject>