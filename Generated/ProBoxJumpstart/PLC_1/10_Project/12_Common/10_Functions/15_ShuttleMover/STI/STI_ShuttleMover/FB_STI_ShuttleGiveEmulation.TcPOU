﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_STI_ShuttleGiveEmulation" Id="{5bfa4849-8aae-0b99-08de-5b2f6d41a45f}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_STI_ShuttleGiveEmulation IMPLEMENTS ITF_SubsystemTransportEmulation
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 412555 $
 *	Revision date	:	$Date: 2018-08-20 13:18:08 +0200 (Mo., 20 Aug 2018) $
 *	Last changed by	:	$Author: d7herza $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BCF/BCF/JumpStart/20_Product/11_Emu/11_Com/10_Lib/10_Conv/13_Sub-Sub/FB_STI_ConveyorEmulation.TcPOU $
 *
 *	Purpose			:	Subsystem transport interface: Conveyor emulation
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * VERSION DATE         INITIALS     DESCRIPTION
 * 0.0     DD.MM.YYYY   (name)       (explain customized modifications)
 *
 **************************************************************************************)
VAR_INPUT
	Config						: ST_CFG_STI_ShuttleGiveEmulation; // Config
END_VAR
VAR
	Init						: BOOL; // Initialize
	fbDebugMsg					: FB_DebugMsg; // Debug instance
	fbTCP_Driver				: FB_TCP_Driver;
	
	fbWatchdogTimer				: TON; // Monitors watchdog signal
	fbReinitConnection			: TON; // Timer connection retry
	fbResend					: TON; // Timer for resending last message3
	fbHeartBeatTimer			: TON; // Timer for generating heart beat of watchdog
	RecvByte					: ARRAY[1..45] OF BYTE; // Receive as bytes (alignment)
	SendByte					: ARRAY[1..45] OF BYTE; // Send as bytes (alignment)
	RecvData					: ST_STI_ShuttleGiveDataEmulation; // Incoming conveyor data
	SendData					: ST_STI_ShuttleGiveDataEmulation; // Outgoing conveyor data
	LastSendData				: ST_STI_ShuttleGiveDataEmulation; // Outgoing conveyor data
	FlagWatchdog				: BOOL; // Flag, to save last watchdog state
	
	TU_Index					: DINT; // TU Index
	ExternalTU_DataIndex		: DINT; // Index created based on external TU info
	RecvCounter					: USINT; // Counts received messages
	SendCounter					: USINT; // Counts send messages
	AdjTransportData			: ST_TransportDataEmulation; // Transport data of adjacent function
	TransportControlData		: ST_TransportControlDataEmulation;
	TransportData				: ST_TransportDataEmulation;
	LastHandoverTU_DataIndex	: DINT;
	rTransportData				: REFERENCE TO ST_TransportData;
	TU_DataAllocated			: BOOL; // Flag, TU-data was allocated
	TU_DataMoved				: BOOL; // Flag, TU-data was moved
	tHeartBeat					: TIME; // Time for generating watchdog heart beat
END_VAR
VAR PERSISTENT
	Settings					: ST_CFG_STI_ShuttleGiveEmulation;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Body never called!
***************************************************************************************************************)
;]]></ST>
    </Implementation>
    <Action Name="A_AllocateDataIndex" Id="{3b746816-9a65-055a-2c3c-3098bb71be61}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_AllocateDataIndex
 * 	FUNCTION	Allocate a data index
 **************************************************************************************)

 (**************************************************************************************
   Delete old
***************************************************************************************)
// If an old index is still available
IF ExternalTU_DataIndex <> 0 THEN
	// Nobody took the index - clear it!
	MEMSET(ADR(TU_DataTableEmulation[ExternalTU_DataIndex]) , 0, SIZEOF(TU_DataTableEmulation[ExternalTU_DataIndex]));
	fbDebugMsg.M_SendWarningMsg(mMessage := 'Index unused - Cleared!');
	
	// Clear only index
	ExternalTU_DataIndex := 0;
END_IF
 
 (**************************************************************************************
   Allocate new index
***************************************************************************************)
// Create a new set
ExternalTU_DataIndex := F_InsertTU_DataEmulation(
							eFunctionNumber := Config.eExternalFunctionNumber,
							ZoneNumber 		:= 0);

// If a index could be created
IF ExternalTU_DataIndex > 0 THEN
	TU_DataTableEmulation[ExternalTU_DataIndex].Label.ASCII := RecvData.Label;
	TU_DataTableEmulation[ExternalTU_DataIndex].Label.HashCode := F_CreateHashCode(RecvData.Label);
	TU_DataTableEmulation[ExternalTU_DataIndex].DataSet.Dimension.Weight := RecvData.Weight;
	TU_DataTableEmulation[ExternalTU_DataIndex].DataSet.Dimension.Length := RecvData.Length;
	TU_DataTableEmulation[ExternalTU_DataIndex].DataSet.Dimension.Height := RecvData.Height;
	TU_DataTableEmulation[ExternalTU_DataIndex].DataSet.Dimension.Width := RecvData.Width;
	TU_DataTableEmulation[ExternalTU_DataIndex].DataSet.LongSideLeading := RecvData.LongSideLeading;
	TU_DataTableEmulation[ExternalTU_DataIndex].DataSet.DataPresent := TRUE;
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_MonitorCommunication" Id="{1c7cceb5-25fd-0b3e-219c-d3a8cec0e1eb}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_MonitorCommunication
 * 	FUNCTION	Monitors TCP Communication
 **************************************************************************************)

(**************************************************************************************
   Watchdog
***************************************************************************************) 
fbWatchdogTimer(IN := TRUE, PT := Settings.tWatchdog);
 
// If connection is server
IF Settings.TCP_Driver.Server THEN
	// Watchdog signals
	IF RecvData.WatchdogToggle = SendData.WatchdogToggle THEN
		// Reset timer
		fbWatchdogTimer(IN := FALSE);
		fbHeartBeatTimer(IN := TRUE, PT := tHeartBeat);
	END_IF
	
	fbHeartBeatTimer();
	IF fbHeartBeatTimer.Q THEN
		fbHeartBeatTimer(IN := FALSE);
		// Return watchdog signal
		SendData.WatchdogToggle := NOT RecvData.WatchdogToggle;
	END_IF

// Otherwise, client
ELSE
	// Watchdog signals
	IF RecvData.WatchdogToggle XOR SendData.WatchdogToggle THEN
		// Reset timer
		fbWatchdogTimer(IN := FALSE);
	END_IF
 	// Return watchdog signal
 	SendData.WatchdogToggle := RecvData.WatchdogToggle;
END_IF

(**************************************************************************************
   Reestablish connection
***************************************************************************************)
IF Settings.tRetryConnection <> T#0S THEN
	// Reestablish connection after a certain time
	fbReinitConnection(IN := fbWatchdogTimer.Q, PT := Settings.tRetryConnection);
	
	// Restart
	IF fbReinitConnection.Q THEN
		fbTCP_Driver.M_RestartCom();
		// Reset
		fbReinitConnection(IN := FALSE);
	END_IF
ELSE
	// Reset
	fbReinitConnection(IN := FALSE);
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OutputMapping" Id="{97fb4fa0-95a2-0062-0a01-426f1caf4e5f}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OutputMapping
 * 	FUNCTION	Map output variables
 **************************************************************************************)

// Get transport data of adjacent function
AdjTransportData := F_GetAdjacentTransportDataEmulation(
						eFunctionNumber 		:= Settings.AdjFunctionNumber.eFunctionNumber,
						eSourceFunctionNumber 	:= Settings.eExternalFunctionNumber);
						
	 
SendData.eHandOverState := AdjTransportData.rTransportControlData.eHandOverState;
SendData.eTakeOverState := AdjTransportData.rTransportControlData.eTakeOverState;

SendData.TU_LeadingEdgePos := AdjTransportData.rTransportControlData.TU_LeadingEdgePos;
SendData.ZoneLength := AdjTransportData.rTransportControlData.TransportParam.ZoneLength;

// If interface is configured as inbound

TU_Index := AdjTransportData.rTransportControlData.TU_DataIndex;

IF TU_Index > 0 THEN	
	SendData.LongSideLeading := TU_DataTableEmulation[TU_Index].DataSet.LongSideLeading;
	SendData.Length := TU_DataTableEmulation[TU_Index].DataSet.Dimension.Length;
	SendData.Width := TU_DataTableEmulation[TU_Index].DataSet.Dimension.Width;
	SendData.Height := TU_DataTableEmulation[TU_Index].DataSet.Dimension.Height;
	SendData.Weight := TU_DataTableEmulation[TU_Index].DataSet.Dimension.Weight;
	SendData.Label := TU_DataTableEmulation[TU_Index].Label.ASCII;
ELSE
	SendData.LongSideLeading := FALSE;
	SendData.Length := 0;
	SendData.Width := 0;
	SendData.Height := 0;
	SendData.Weight := 0;
	SendData.Label := '';
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_TakeOverStates" Id="{88ad9a94-2fd9-0d48-2486-3b56e1ba1727}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_TakeOverInterface
 * 	FUNCTION	TakeOver interface - Take over from external conveyor
 **************************************************************************************)


(**********************************************************************************
   Move TU Data
***********************************************************************************)
IF NOT TU_DataMoved THEN 
	IF  RecvData.eTakeOverState = E_TakeOverState.ACTIVE  THEN
		// External conveyor took index over - remove on giving position
		LastHandoverTU_DataIndex := F_MoveTU_DataIndexEmulation(
										eFunctionNumber := Settings.AdjFunctionNumber.eFunctionNumber,
										mZoneNumber     := Settings.AdjFunctionNumber.ZoneNumber);
		TU_DataMoved := TRUE;
	END_IF

(**********************************************************************************
   Delete TU Data
***********************************************************************************)
ELSIF AdjTransportData.rTransportControlData.eHandOverState = E_HandoverState.COMPLETE THEN
	IF LastHandoverTU_DataIndex = 0 THEN
		fbDebugMsg.M_SendErrorMsg('No emulation index moved.');
	ELSE
		// Delete index - TU has left the system
		MEMSET(	destAddr:= ADR(TU_DataTableEmulation[LastHandoverTU_DataIndex]),
				fillByte:= 0,
				n       := SIZEOF(TU_DataTableEmulation[LastHandoverTU_DataIndex]));
		
		LastHandoverTU_DataIndex:= 0;
	END_IF
	
	TU_DataMoved := FALSE;
END_IF
 
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_TCP_Communication" Id="{8434ca92-926e-09f6-3853-d9a3586c7cbd}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_TCP_Communication
 * 	FUNCTION	Handles TCP Communication to external interface
 **************************************************************************************)

 // Call driver
 fbTCP_Driver(
	Config 			:= Settings.TCP_Driver,
	pReceiveData 	:= ADR(RecvByte),
	ReceiveDataLen 	:= SIZEOF(RecvByte));
 
(**************************************************************************************
   Receive data
***************************************************************************************)
// Check TCP/IP channel for new data
IF fbTCP_Driver.NewData THEN
	// Check border counter
	IF RecvCounter > 254 THEN
		// Reset
		RecvCounter := 0;
	END_IF
	
	// Update counter
	RecvCounter := RecvCounter + 1;
	
	// Map values - (alignment to ensure compatibility to other systems)
	RecvData.WatchdogToggle := RecvByte[1].0;
	RecvData.LongSideLeading := RecvByte[1].1;
	
	MEMCPY(destAddr := ADR(RecvData.eTakeOverState), srcAddr := ADR(RecvByte[3]), n := 2);
	MEMCPY(destAddr := ADR(RecvData.eHandOverState), srcAddr := ADR(RecvByte[5]), n := 2);
	MEMCPY(destAddr := ADR(RecvData.TU_LeadingEdgePos), srcAddr := ADR(RecvByte[7]), n := 2);
	MEMCPY(destAddr := ADR(RecvData.ZoneLength), srcAddr := ADR(RecvByte[9]), n := 2);
	MEMCPY(destAddr := ADR(RecvData.Length), srcAddr := ADR(RecvByte[11]), n := 2);
	MEMCPY(destAddr := ADR(RecvData.Width), srcAddr := ADR(RecvByte[13]), n := 2);
	MEMCPY(destAddr := ADR(RecvData.Height), srcAddr := ADR(RecvByte[15]), n := 2);
	MEMCPY(destAddr := ADR(RecvData.Weight), srcAddr := ADR(RecvByte[17]), n := 4);
	MEMCPY(destAddr := ADR(RecvData.Label), srcAddr := ADR(RecvByte[21]), n := 25);

	// Data processed
	fbTCP_Driver.M_DataProcessed();
END_IF

(**************************************************************************************
   Send data
***************************************************************************************)
// If the connection is established
IF fbTCP_Driver.Connected THEN
	// Call message resend timer
	fbResend(IN := Settings.tResendMsg <> T#0S, PT := Settings.tResendMsg);
	
	// If data have changed or resend is requested
	IF (MEMCMP(
		pBuf1 := ADR(LastSendData),
		pBuf2 := ADR(SendData),
		n	  := SIZEOF(SendData)) <> 0 OR fbResend.Q) AND NOT fbTCP_Driver.SendBusy THEN
	
		LastSendData := SendData;
		
		// Reset timer
		fbResend(IN := FALSE);
		
		// Map values - (alignment to ensure compatibility to other systems)
		SendByte[1].0 := SendData.WatchdogToggle;
		SendByte[1].1 := SendData.LongSideLeading;
		MEMCPY(destAddr := ADR(SendByte[3]), srcAddr := ADR(SendData.eTakeOverState), n := 2);
		MEMCPY(destAddr := ADR(SendByte[5]), srcAddr := ADR(SendData.eHandOverState), n := 2);
		MEMCPY(destAddr := ADR(SendByte[7]), srcAddr := ADR(SendData.TU_LeadingEdgePos), n := 2);
		MEMCPY(destAddr := ADR(SendByte[9]), srcAddr := ADR(SendData.ZoneLength), n := 2);
		MEMCPY(destAddr := ADR(SendByte[11]), srcAddr := ADR(SendData.Length), n := 2);
		MEMCPY(destAddr := ADR(SendByte[13]), srcAddr := ADR(SendData.Width), n := 2);
		MEMCPY(destAddr := ADR(SendByte[15]), srcAddr := ADR(SendData.Height), n := 2);
		MEMCPY(destAddr := ADR(SendByte[17]), srcAddr := ADR(SendData.Weight), n := 4);
		MEMCPY(destAddr := ADR(SendByte[21]), srcAddr := ADR(SendData.Label), n := 25);
		
		// Check border counter
		IF SendCounter > 254 THEN
			// Reset
			SendCounter := 0;
		END_IF
			
		// Update message counter
		SendCounter := SendCounter + 1;
	
		// Forward data to TCP channel
		fbTCP_Driver.M_Send(
			mpSendData := ADR(SendByte),
			mSendDataLen := SIZEOF(SendByte));
	END_IF
ELSE
	// Reset
	fbResend(IN := FALSE);
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_CallInterface" Id="{fd6aba6c-5672-021c-22f8-4a8b09717c7c}">
      <Declaration><![CDATA[METHOD M_CallInterface : BOOL
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 412555 $
 *	Revision date	:	$Date: 2018-08-20 13:18:08 +0200 (Mo., 20 Aug 2018) $
 *	Last changed by	:	$Author: d7herza $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BCF/BCF/JumpStart/20_Product/11_Emu/11_Com/10_Lib/10_Conv/13_Sub-Sub/FB_STI_ConveyorEmulation.TcPOU $
 *
 *	Purpose			:	Call main interface
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR
	FunctionNumber : E_FunctionNumber;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
(**************************************************************************************
	Init
 **************************************************************************************)
M_Init(mInit := FALSE);

(***********************************************************************************
Monitor TCP/IP communication
***********************************************************************************)
A_MonitorCommunication();

(***********************************************************************************
TCP/IP communication
***********************************************************************************)
A_TCP_Communication();

(**************************************************************************************
	Transport interface - Handles external conveyor position
 **************************************************************************************)
// Outgoing - Handle take over states (View of own system)
//			--->
//  HandOver  |	   TakeOver
// |-------|  |   |--------|
//	 INTERN	  |		EXTERN

FOR FunctionNumber := E_FunctionNumber.F_BEGIN_FUNCTION TO E_FunctionNumber.F_END_FUNCTION DO 
	IF __ISVALIDREF(FunctionRegistry[FunctionNumber].rSettingsFunction) THEN 
		IF FunctionRegistry[FunctionNumber].rSettingsFunction.eExternalSTI_FunctionNumber[1] = Settings.eExternalFunctionNumber THEN 
			Settings.AdjFunctionNumber.eFunctionNumber := FunctionNumber;
		END_IF
	END_IF 
END_FOR

IF Settings.AdjFunctionNumber.eFunctionNumber <> F_BEGIN_FUNCTION THEN 
	A_TakeOverStates();
	
	
	(**************************************************************************************
		Output mapping
	 **************************************************************************************)
	A_OutputMapping();
END_IF
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_GetTransportData" Id="{79c1944f-02be-0f08-275b-cbb39c3b9630}">
      <Declaration><![CDATA[METHOD M_GetTransportData : REFERENCE TO ST_TransportDataEmulation
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 412555 $
 *	Revision date	:	$Date: 2018-08-20 13:18:08 +0200 (Mo., 20 Aug 2018) $
 *	Last changed by	:	$Author: d7herza $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BCF/BCF/JumpStart/20_Product/11_Emu/11_Com/10_Lib/10_Conv/13_Sub-Sub/FB_STI_ConveyorEmulation.TcPOU $
 *
 *	Purpose			:	Return transport data
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR
	mSTI_Instance				: ITF_SubsystemTransport; // Instance to compare valid interfaces
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Prepare transport control data
TransportControlData.eHandOverState := RecvData.eHandOverState;
TransportControlData.eTakeOverState := RecvData.eTakeOverState;
TransportControlData.TU_DataIndex := ExternalTU_DataIndex;
TransportControlData.TU_LeadingEdgePos := RecvData.TU_LeadingEdgePos;
TransportControlData.TransportParam.ZoneLength := RecvData.ZoneLength;

MEMSET(ADR(TransportData), 0, SIZEOF(TransportData));
TransportData.eFunctionNumber := Settings.eExternalFunctionNumber;
TransportData.rTransportControlData REF= TransportControlData;

IF Settings.eExternalFunctionNumber > E_FunctionNumber.F_BEGIN_EXTERN_FUNCTION AND Settings.eExternalFunctionNumber < E_FunctionNumber.F_END_EXTERN_FUNCTION THEN
	IF __QUERYINTERFACE(fbSTI_Channels[Settings.eExternalFunctionNumber].Instance, mSTI_Instance) THEN
		rTransportData REF= fbSTI_Channels[Settings.eExternalFunctionNumber].Instance.M_GetTransportData();
		TransportData.rRealObjTrspControlData REF= rTransportData.rTransportControlData;
	END_IF
END_IF

M_GetTransportData REF= TransportData;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Init" Id="{5c1067fb-5de9-0583-21a7-bce568020db2}">
      <Declaration><![CDATA[METHOD M_Init : BOOL
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 412555 $
 *	Revision date	:	$Date: 2018-08-20 13:18:08 +0200 (Mo., 20 Aug 2018) $
 *	Last changed by	:	$Author: d7herza $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BCF/BCF/JumpStart/20_Product/11_Emu/11_Com/10_Lib/10_Conv/13_Sub-Sub/FB_STI_ConveyorEmulation.TcPOU $
 *
 *	Purpose			:	Initialize interface
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR_INPUT
	mInit			: BOOL; // Force init
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF NOT Init THEN
	
	Settings := Config;

	// Set default value if not configured
	IF Settings.tHeartBeat = T#0S THEN
		tHeartBeat := T#1S;
	ELSE
		tHeartBeat := Settings.tHeartBeat;
	END_IF
	
	Init := TRUE;
END_IF
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_MoveTU_DataIndex" Id="{65643f1e-2b16-00b6-037b-59f7d5729543}">
      <Declaration><![CDATA[METHOD M_MoveTU_DataIndex : DINT
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 412555 $
 *	Revision date	:	$Date: 2018-08-20 13:18:08 +0200 (Mo., 20 Aug 2018) $
 *	Last changed by	:	$Author: d7herza $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BCF/BCF/JumpStart/20_Product/11_Emu/11_Com/10_Lib/10_Conv/13_Sub-Sub/FB_STI_ConveyorEmulation.TcPOU $
 *
 *	Purpose			:	Initialize interface
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Return TU Data Index
M_MoveTU_DataIndex := ExternalTU_DataIndex;

// Clear internally
ExternalTU_DataIndex := 0;
]]></ST>
      </Implementation>
    </Method>
    <ObjectProperties>
      <XmlArchive>
        <Data>
          <o xml:space="preserve" t="UMLStereoTypeContainerObject">
            <v n="IsType" t="UMLType">BaseArea</v>
            <v n="Stereotype">""</v>
            <d n="Stereotypes" t="Hashtable" />
          </o>
        </Data>
        <TypeList>
          <Type n="Hashtable">System.Collections.Hashtable</Type>
          <Type n="String">System.String</Type>
          <Type n="UMLStereoTypeContainerObject">{30250973-b110-4e31-b562-c102e042dca4}</Type>
          <Type n="UMLType">{0197b136-405a-42ee-bb27-fd08b621d0cf}</Type>
        </TypeList>
      </XmlArchive>
    </ObjectProperties>
  </POU>
</TcPlcObject>