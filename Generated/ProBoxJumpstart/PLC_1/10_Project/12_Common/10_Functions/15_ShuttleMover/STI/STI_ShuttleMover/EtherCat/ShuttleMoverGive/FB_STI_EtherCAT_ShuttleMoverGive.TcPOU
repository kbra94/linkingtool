﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_STI_EtherCAT_ShuttleMoverGive" Id="{83f5461f-9a11-066d-35f0-b43bbbdd6b76}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_STI_EtherCAT_ShuttleMoverGive IMPLEMENTS ITF_ShuttleMoverGiveTransport
 (**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 355211 $
 *	Revision date	:	$Date: 2015-09-22 11:14:21 +0200 (Tue, 22 Sep 2015) $
 *	Last changed by	:	$Author: d7mangc $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BoxControlFramework/BoxControlFramework/JumpStart/11_Library/10_Application/11_Communication/12_MFCS/11_SIS/10_Base/FB_SIS_BaseDriver.TcPOU $
 *
 *	Purpose			:	Subsystem transport interface over EtherCAT: Conveyor
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 *  VERSION DATE         INITIALS     DESCRIPTION
 *  0.0     DD.MM.YYYY   (name)       (explain customized modifications)
 *
 **************************************************************************************)
VAR_INPUT
	Config					: ST_CFG_STI_EtherCAT_ShuttleMoverGive; // Config
	HW_Inputs				: ST_HW_InputsEtherCAT_ConveyorInterface_ShuttleMoverGive;
END_VAR
VAR_OUTPUT
	HW_Outputs				: ST_HW_OutputsEtherCAT_ShuttleMoverGive;
END_VAR
VAR
	Init					: BOOL; // Initialize
	Outputs					: ST_HW_OutSTI_EtherCAT_ShuttleMoverGive; // Outputs
	fbDebugMsg				: FB_DebugMsg; // Debug instance
	DebugMsg				: T_MAXSTRING; // Debug msg
	ErrorData				: ST_ErrorDataSet; // Error data
	DestinationPosition		: ST_PositionIdent; 
	Position				: ST_PositionIdent; 
	ExternalTU_DataIndex	: DINT; 
	TU_DataIndex			: DINT;
	TU_DataIndexAssignment	: DINT;
	LastHandoverTU_DataIndex: DINT;
	TransportControlData	: ST_TransportControlData; 
	Inputs					: ST_HW_InSTI_EtherCAT_ShuttleMoverGive; // Input structure
	AdjTransportData		: ST_TransportData; // Transport data of adjacent function
	fbWatchdogTimer			: TON; // Monitors watchdog signal
	fbHeartBeatTimer		: TON; // Timer for generating heart beat of watchdog
	TransportData			: ST_TransportData; // Transport data
	DataError				: BOOL; // Error on data allocation action
	TU_DataAllocated		: BOOL; // Flag, TU-data was allocated
	TU_DataMoved			: BOOL; // Flag, TU-data was moved
	FlagWatchdog			: BOOL; // Flag, to save last watchdog state
	tHeartBeat				: TIME; // Time for generating watchdog heart beat
	TU_ID_Present			: BOOL; // TU ID present
	AssignmentID_Present	: BOOL; // Assignment ID present
	NewIndexCreated			: BOOL;
END_VAR
VAR PERSISTENT
	Settings				: ST_CFG_STI_EtherCAT_ShuttleMoverGive;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Body never called!
***************************************************************************************************************)

;]]></ST>
    </Implementation>
    <Action Name="A_AllocateDataIndex" Id="{63873c3b-f5ed-0b31-1f95-4d8f6d6557ca}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_AllocateDataIndex
 * 	FUNCTION	Allocate a data index
 **************************************************************************************)

 (**************************************************************************************
   Delete old
***************************************************************************************)
 // If an old index is still available
 IF ExternalTU_DataIndex <> 0 THEN
	// Check if a position contains this index
	Position := F_SearchPositionOfTU_Index(
					TU_DataIndex 			:= ExternalTU_DataIndex,
					eStartFunctionNumber 	:= E_FunctionNumber.F_BEGIN_FUNCTION);
					
	// If index was found on a position
	IF Position.eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION THEN
		// debug message: '11-001-0000-00-0,  used TU_DataIndex deleted, position = 99-999-9999-99-9,  TU_DataIndex = 5,  TuID = 000012345,  AssignmentID = 999999999'
		DebugMsg := '';
		DebugMsg := CONCAT(DebugMsg, F_AddDividerToSisString(SubsystemRegistry.SubsystemName));
		DebugMsg := CONCAT(DebugMsg, ',  used TU_DataIndex deleted');
		DebugMsg := CONCAT(DebugMsg, DebugTextBlocks.POSITION);
		DebugMsg := CONCAT(DebugMsg, Position.PositionName);
		DebugMsg := CONCAT(DebugMsg, F_CreateTU_DataString(TU_DataIndex:= ExternalTU_DataIndex));
		fbDebugMsg.M_SendWarningMsg(mMessage := DebugMsg);
		// Clear only index
		ExternalTU_DataIndex := 0;
	ELSE
		// debug message: '11-000-0000-00-0,  unused TU_DataIndex deleted, external = 99-999-9999-99-9,  TU_DataIndex = 5,  TuID = 000012345,  AssignmentID = 999999999'
		DebugMsg := '';
		DebugMsg := CONCAT(DebugMsg, F_AddDividerToSisString(SubsystemRegistry.SubsystemName));
		DebugMsg := CONCAT(DebugMsg, ',  unused TU_DataIndex deleted');
		DebugMsg := CONCAT(DebugMsg, DebugTextBlocks.POSITION);
		DebugMsg := CONCAT(DebugMsg, TO_STRING(Settings.eExternalFunctionNumber));  // ###todo: how to retrieve sis number string for external?
		DebugMsg := CONCAT(DebugMsg, F_CreateTU_DataString(TU_DataIndex:= ExternalTU_DataIndex));
		fbDebugMsg.M_SendInfoMsg(DebugMsg);
		// Clear data set
		F_DeleteTU_Data(
			TU_DataIndex 	:= ExternalTU_DataIndex,
			NoTTC 			:= TRUE);
			
		// Clear index
		ExternalTU_DataIndex := 0;
	END_IF
 END_IF
 
 (**************************************************************************************
   Allocate new index
***************************************************************************************)
TU_ID_Present := Inputs.TU_ID <> '000000000000000000' AND
				 Inputs.TU_ID <> '                  ' AND
			 	 Inputs.TU_ID <> '';

AssignmentID_Present := Inputs.AssignmentID > 0;

// If giving conveyor provides a TU ID or an assignment ID
IF TU_ID_Present OR
   AssignmentID_Present THEN
   
   // Reset
   DataError := FALSE;
   
	// Search if index based on TU-ID is already available 
	TU_DataIndex := F_SearchTU_ByHashCode(TU_ID := Inputs.TU_ID);
	// Search if index based on Assignment is already available
	TU_DataIndexAssignment := F_SearchAssignmentId(Inputs.AssignmentID);

	// When both an index for the TU-ID AND the Assignment ID is available in the system 
	IF TU_DataIndex > 0 AND TU_DataIndexAssignment > 0 THEN
		// Check if they are equal, otherwise data set is ignored
		// This would lead to a data mismatch.
		IF TU_DataIndex <> TU_DataIndexAssignment THEN
		   DataError := TRUE;
		   
		   	// Debug - wrong index found
			DebugMsg := CONCAT('External interface: AssignmentID and TU-ID have not the same index. TU-ID index:', DINT_TO_STRING(TU_DataIndex));
			DebugMsg := CONCAT(DebugMsg, ', AssignmentID index: ');
			DebugMsg := CONCAT(DebugMsg, DINT_TO_STRING(TU_DataIndexAssignment));
			fbDebugMsg.M_SendWarningMsg(mMessage := DebugMsg);
		
		// Clear TU slot before taking over TU data from the Interface
		ELSIF Settings.DeleteTU_Data THEN
			MEMSET(ADR(TU_DataTable[TU_DataIndex]), 0, SIZEOF(TU_DataTable[TU_DataIndex]));	
		END_IF 
		
	// When index for TU-ID is available in the system
	ELSIF TU_DataIndex > 0 THEN
		IF Settings.DeleteTU_Data THEN
			// Clear TU slot before taking over TU data from the Interface
			MEMSET(ADR(TU_DataTable[TU_DataIndex]), 0, SIZEOF(TU_DataTable[TU_DataIndex]));	
		END_IF 
		
	// When index for Assignment ID is available in the system
	ELSIF TU_DataIndexAssignment > 0 THEN
		IF Settings.DeleteTU_Data THEN
			// Clear TU slot before taking over TU data from the Interface
			MEMSET(ADR(TU_DataTable[TU_DataIndexAssignment]), 0, SIZEOF(TU_DataTable[TU_DataIndexAssignment]));	
		END_IF 	
		TU_DataIndex := TU_DataIndexAssignment;
		
	ELSE
		// Allocate new one
	   TU_DataIndex := F_InsertTU_Data(
							eFunctionNumber := Settings.AdjFunctionNumber.eFunctionNumber,
							ZoneNumber := Settings.AdjFunctionNumber.ZoneNumber);
		
		NewIndexCreated := TU_DataIndex > 0;		
		
		// If creation was not succesfull
		IF NOT NewIndexCreated THEN
			DataError := TRUE;
		   
		   	// Debug - Image creation failed
			DebugMsg := 'External interface: AllocateDataIndex was not able to create free index';
			fbDebugMsg.M_SendWarningMsg(mMessage := DebugMsg);
			
			// Error no free TU Index
			IF ErrorData.ErrorType = E_ErrorType.NO_ERROR_PENDING THEN
				// Set communication error
				F_SetError(
					ErrorMsg	:= E_ErrorConv.NO_FREE_TU_DATA_SET,
					ErrorType 	:= E_ErrorType.ERROR,
					ErrorParam 	:= '',
					ErrorData	:= ErrorData);
			END_IF
		END_IF
	END_IF
   
	// When no error occured 
	IF NOT DataError AND 
		TU_DataIndex > 0 THEN
		
		// When new Tu data index was created or exisitng should be overwritten
		IF Settings.OverwriteTU_Data OR NewIndexCreated THEN
			// Takeover data from interface
			
			// Set data present
			TU_DataTable[TU_DataIndex].DataSet.DataPresent := TRUE;
			
			// Update data
			IF TU_ID_Present THEN
				TU_DataTable[TU_DataIndex].TU_ID.ASCII := Inputs.TU_ID;
				TU_DataTable[TU_DataIndex].DataSet.TU_Type := Inputs.TU_Type; // Added 09.08.2021 to add TU Type
				TU_DataTable[TU_DataIndex].TU_ID.HashCode := F_CreateHashCode(Inputs.TU_ID);
			END_IF
			
			IF AssignmentID_Present THEN
				TU_DataTable[TU_DataIndex].AssignmentID := Inputs.AssignmentID;
			END_IF
	
			IF Inputs.Destination <> '' THEN
				// Search function number of corresponding string
				DestinationPosition := F_SearchFunctionPosition(Position := Inputs.Destination);
			
				// Destination was found 
				IF DestinationPosition.eFunctionNumber <> 0 THEN
					TU_DataTable[TU_DataIndex].Destination[1].eFunctionNumber 	:= DestinationPosition.eFunctionNumber;
					TU_DataTable[TU_DataIndex].Destination[1].ZoneNumber 		:= DestinationPosition.ZoneNumber;
					TU_DataTable[TU_DataIndex].Destination[1].PositionName 		:= DestinationPosition.PositionName;
					TU_DataTable[TU_DataIndex].DataSet.JobPresent := TRUE;
					TU_DataTable[TU_DataIndex].DataSet.NumberOfDest := 1;
				ELSE
					// Debug - Destination not found
					DebugMsg := CONCAT('External interface: Destination not found(', Inputs.Destination);
					DebugMsg := CONCAT(DebugMsg, ')');
					
					fbDebugMsg.M_SendWarningMsg(mMessage := DebugMsg);
				END_IF
			END_IF			
		END_IF
		// Set to external TU Data Index
		ExternalTU_DataIndex := TU_DataIndex;
	END_IF
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InputMapping" Id="{a4663249-4b2f-0347-0eef-3fc15c73e17c}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InputMapping
 * 	FUNCTION	Map input variables
 **************************************************************************************)

(**************************************************************************************
   Map interface inputs
***************************************************************************************)
Inputs.Watchdog := HW_Inputs.STI.Watchdog; 

Inputs.TakeOverState := HW_Inputs.STI.TakeOverState;

IF Settings.OnlyFastSpeed THEN
	Inputs.FastSpeedActive := TRUE;
ELSE
	Inputs.FastSpeedActive := HW_Inputs.STI.FastSpeedActive;
END_IF

Inputs.TransportActive := HW_Inputs.STI.TransportActive;

// TU data
Inputs.TU_ID := HW_Inputs.STI.TU_ID;
Inputs.TU_Type := HW_Inputs.STI.TU_Type; //Added 09.08.2021 mhu
Inputs.AssignmentID := HW_Inputs.STI.AssignmentID;
Inputs.Destination := HW_Inputs.STI.Destination;


(**************************************************************************************
   Get external data
***************************************************************************************)
// Get transport data of adjacent function
AdjTransportData := F_GetAdjacentTransportData(
						eFunctionNumber 		:= Settings.AdjFunctionNumber.eFunctionNumber,
						eSourceFunctionNumber 	:= Settings.eExternalFunctionNumber);

			]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_MonitorCommunication" Id="{ebfe69bd-c76f-01e9-06d4-6e7334f1788a}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_MonitorCommunication
 * 	FUNCTION	Monitors TCP Communication
 **************************************************************************************)

(**************************************************************************************
   Watchdog
***************************************************************************************) 
fbWatchdogTimer(IN := TRUE, PT := Settings.tWatchdog);

// Watchdog signals
IF HW_Inputs.STI.Watchdog <> FlagWatchdog THEN
	// Reset timer
	fbWatchdogTimer(IN := FALSE);
	FlagWatchdog := HW_Inputs.STI.Watchdog;
END_IF

fbHeartBeatTimer(IN := TRUE, PT := tHeartBeat);
IF fbHeartBeatTimer.Q THEN
	fbHeartBeatTimer(IN := FALSE);
	// Toggle watchdog
	Outputs.Watchdog := NOT Outputs.Watchdog;
END_IF


(**************************************************************************************
   Communication error
***************************************************************************************)
// Communication timeout
IF fbWatchdogTimer.Q AND
 	ErrorData.ErrorType = E_ErrorType.NO_ERROR_PENDING THEN
	// Set communication error
	F_SetError(
		ErrorMsg	:= E_ErrorConv.CONNECTION_ERROR_TO_EXTERNAL_INTERFACE,
		ErrorType 	:= E_ErrorType.ERROR,
		ErrorParam 	:= 'Timeout watchdog',
		ErrorData	:= ErrorData);
		
ELSIF HW_Inputs.DeviceState <> EC_DEVICE_STATE_OP THEN
	// Set communication error
	F_SetError(
		ErrorMsg	:= E_ErrorConv.CONNECTION_ERROR_TO_EXTERNAL_INTERFACE,
		ErrorType 	:= E_ErrorType.ERROR,
		ErrorParam 	:= 'EtherCAT Bridge not in OP',
		ErrorData	:= ErrorData);
		
ELSIF HW_Inputs.ExternalDeviceNotInOP THEN
	// Set communication error
	F_SetError(
		ErrorMsg	:= E_ErrorConv.CONNECTION_ERROR_TO_EXTERNAL_INTERFACE,
		ErrorType 	:= E_ErrorType.ERROR,
		ErrorParam 	:= 'External EtherCAT Bridge not in OP',
		ErrorData	:= ErrorData);
		
// Communication reestablished
ELSIF NOT fbWatchdogTimer.Q AND ErrorData.ErrorCode.Conv = E_ErrorConv.CONNECTION_ERROR_TO_EXTERNAL_INTERFACE THEN
	// Auto reset
	F_ResetError(
		Reset		:= TRUE,
		ErrorData 	:= ErrorData);
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OutputMapping" Id="{595b3b15-5258-0230-1561-df16590e23b1}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OutputMapping
 * 	FUNCTION	Map output variables
 **************************************************************************************)
HW_Outputs.STI.Watchdog := Outputs.Watchdog;

// If interface is configured as inbound
HW_Outputs.STI.HandOverState := Outputs.HandOverState;

HW_Outputs.STI.FastSpeedActive := Outputs.FastSpeedActive;
HW_Outputs.STI.TransportActive := Outputs.TransportActive;

// TU data
HW_Outputs.STI.TU_ID := Outputs.TU_ID;
HW_Outputs.STI.TU_Type := Outputs.TU_Type;
HW_Outputs.STI.AssignmentID := Outputs.AssignmentID;
HW_Outputs.STI.Destination := Outputs.Destination;

;]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_TakeOverStates" Id="{320335df-8586-0c80-0bc3-278b7363e209}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_TakeOverInterface
 * 	FUNCTION	TakeOver interface - Take over from external conveyor
 **************************************************************************************)

 (**************************************************************************************
	Update outputs for external conveyor position
 **************************************************************************************)
Outputs.eHandOverState := AdjTransportData.rTransportControlData.eHandOverState;
Outputs.TransportActive := AdjTransportData.rTransportControlData.TransportActive;
Outputs.FastSpeedActive := AdjTransportData.rTransportControlData.FastSpeedActive;

TU_DataIndex := AdjTransportData.rTransportControlData.TU_DataIndex;
	
IF TU_DataIndex > 0 THEN				
	Outputs.Destination := TU_DataTable[TU_DataIndex].Destination[1].PositionName;
ELSE
	Outputs.Destination := '';
END_IF	

Outputs.TU_ID := TU_DataTable[TU_DataIndex].TU_ID.ASCII;
Outputs.AssignmentID := TU_DataTable[TU_DataIndex].AssignmentID;						
	
(**********************************************************************************
   Delete TU data
***********************************************************************************)

// Delete existing TU data if the handover starts and if it is configured
// The TU data are taken over from the next position during their previous E_TakeOverState.MOVE_TU_DATA state
IF AdjTransportData.rTransportControlData.eHandOverState = E_HandOverState.READY AND
   Inputs.TakeOverState = E_TakeOverState.ACTIVE AND NOT TU_DataMoved AND Settings.DeleteTU_Data THEN

	// External conveyor took index over - remove on giving position
	LastHandoverTU_DataIndex := F_MoveTU_DataIndex(
									eFunctionNumber := Settings.AdjFunctionNumber.eFunctionNumber,
									mZoneNumber 	:= Settings.AdjFunctionNumber.ZoneNumber);
							
	// If a index was found
	IF LastHandoverTU_DataIndex <> 0  THEN
		// debug message: '11-001-0000-00-0,  handed over TU_DataIndex deleted,  position = 99-999-9999-99-9,  TU_DataIndex = 5,  TuID = 000012345,  AssignmentID = 999999999'
		DebugMsg := '';
		DebugMsg := CONCAT(DebugMsg, F_AddDividerToSisString(SubsystemRegistry.SubsystemName));
		DebugMsg := CONCAT(DebugMsg, ',  handed over TU_DataIndex deleted');
		DebugMsg := CONCAT(DebugMsg, DebugTextBlocks.POSITION);
		DebugMsg := CONCAT(DebugMsg, TO_STRING(Settings.eExternalFunctionNumber));  // ###todo: how to retrieve sis number string for external?
		DebugMsg := CONCAT(DebugMsg, F_CreateTU_DataString(TU_DataIndex:= LastHandoverTU_DataIndex));
		fbDebugMsg.M_SendInfoMsg(DebugMsg);
		// Delete TU data in own system
		F_DeleteTU_Data(
			TU_DataIndex 	:= LastHandoverTU_DataIndex,
			NoTTC			:= TRUE);
	END_IF

	TU_DataMoved := TRUE;
	
ELSIF Inputs.TakeOverState <> E_TakeOverSTate.ACTIVE THEN
	TU_DataMoved := FALSE;
END_IF								
]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_CallInterface" Id="{c0d8a335-1704-0e50-1ad7-fda363698e3b}">
      <Declaration><![CDATA[METHOD M_CallInterface : BOOL
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 355136 $
 *	Revision date	:	$Date: 2015-09-21 07:38:56 +0200 (Mo, 21 Sep 2015) $
 *	Last changed by	:	$Author: d7mangc $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BoxControlFramework/BoxControlFramework/JumpStart/11_Library/10_Application/12_ElementFunctions/10_Functions/FB_TransportControl.TcPOU $
 *
 *	Purpose			:	Call main interface
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR_IN_OUT
	mErrorData				: ST_ErrorDataSet; // Error data
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
// Store inputs in instance
ErrorData := mErrorData;

(**************************************************************************************
	Init
 **************************************************************************************)
M_Init(mInit := FALSE);

(**************************************************************************************
	General error reset
 **************************************************************************************)
F_ResetError(
	Reset		:= FALSE,
	ErrorData 	:= ErrorData);

(***********************************************************************************
Monitor TCP/IP communication
***********************************************************************************)
A_MonitorCommunication();

(**************************************************************************************
	Input mapping
 **************************************************************************************)
A_InputMapping();

(**************************************************************************************
	Transport interface - Handles external conveyor position
 **************************************************************************************)
// Outgoing - Handle take over states
//			--->
//  HandOver  |	   TakeOver
// |-------|  |   |--------|
//	 INTERN	  |		EXTERN
A_TakeOverStates();


(**************************************************************************************
	Output mapping
 **************************************************************************************)
A_OutputMapping();

// Update error structure
mErrorData := ErrorData;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_GetTransportData" Id="{e14bf32c-d037-0159-0261-d3e632103d14}">
      <Declaration><![CDATA[METHOD M_GetTransportData : REFERENCE TO ST_TransportData
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 355211 $
 *	Revision date	:	$Date: 2015-09-22 11:14:21 +0200 (Tue, 22 Sep 2015) $
 *	Last changed by	:	$Author: d7mangc $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BoxControlFramework/BoxControlFramework/JumpStart/11_Library/10_Application/11_Communication/12_MFCS/11_SIS/10_Base/FB_SIS_BaseDriver.TcPOU $
 *
 *	Purpose			:	Return transport data
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)

]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Prepare transport control data
TransportControlData.eTakeOverState := Inputs.TakeOverState;
TransportControlData.FastSpeedActive := Inputs.FastSpeedActive;
TransportControlData.TransportActive := Inputs.TransportActive;

// Return
MEMSET(ADR(TransportData), 0, SIZEOF(TransportData));
TransportData.eFunctionNumber := Settings.eExternalFunctionNumber;
TransportData.rTransportControlData REF= TransportControlData;
M_GetTransportData REF= TransportData;
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Init" Id="{5396e8fe-e873-00a8-1155-5e1ae89a205e}">
      <Declaration><![CDATA[METHOD M_Init : BOOL
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 355211 $
 *	Revision date	:	$Date: 2015-09-22 11:14:21 +0200 (Tue, 22 Sep 2015) $
 *	Last changed by	:	$Author: b7bolm $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BoxControlFramework/BoxControlFramework/JumpStart/11_Library/10_Application/11_Communication/12_MFCS/11_SIS/10_Base/FB_SIS_BaseDriver.TcPOU $
 *
 *	Purpose			:	Initialize interface
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR_INPUT
	mInit			: BOOL; // Force init
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
IF NOT Init THEN
	Settings := Config;
	
	// Set default value if not configured
	IF Settings.tHeartBeat = T#0S THEN
		tHeartBeat := T#1S;
	ELSE
		tHeartBeat := Settings.tHeartBeat;
	END_IF
	
	Init := TRUE;
END_IF
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_MoveTU_DataIndex" Id="{e7ed5e25-a9f2-0d0f-340a-40b53d83b487}">
      <Declaration><![CDATA[METHOD M_MoveTU_DataIndex : DINT
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 355211 $
 *	Revision date	:	$Date: 2015-09-22 11:14:21 +0200 (Tue, 22 Sep 2015) $
 *	Last changed by	:	$Author: b7bolm $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/BoxControlFramework/20_Release/trunk/01_Software/BoxControlFramework/BoxControlFramework/JumpStart/11_Library/10_Application/11_Communication/12_MFCS/11_SIS/10_Base/FB_SIS_BaseDriver.TcPOU $
 *
 *	Purpose			:	Initialize interface
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Return TU Data Index
M_MoveTU_DataIndex := ExternalTU_DataIndex;

// Clear internally
ExternalTU_DataIndex := 0;
]]></ST>
      </Implementation>
    </Method>
    <Property Name="P_AdjecentSide" Id="{57ec9404-979f-0246-17a7-b37d64f32075}">
      <Declaration><![CDATA[PROPERTY P_AdjecentSide : BOOL]]></Declaration>
      <Get Name="Get" Id="{c97c1842-d5db-062f-03bc-7f473764f659}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_AdjecentSide := settings.AdjFunctionNumber;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{d82f957b-9a1e-019d-370c-a46f80c9ed23}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Settings.AdjFunctionNumber := P_AdjecentSide;]]></ST>
        </Implementation>
      </Set>
    </Property>
  </POU>
</TcPlcObject>