﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_MultiLayerDeStacker_2_SCHW_MoviMotEmulation" Id="{3d42f4a0-915f-4c5f-88e2-0da03ea6bd69}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_MultiLayerDeStacker_2_SCHW_MoviMotEmulation EXTENDS FB_FunctionConvEmulation
(**************************************************************************************
 * 	Application		:	QuickBox2.0
 *	Revision		:	$Revision: 488065 $
 *	Revision date	:	$Date: 2021-10-13 13:42:49 +0200 (Mi, 13 Okt 2021) $
 *	Last changed by	:	$Author: g7nowan $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/QuickMove/02_Controls/QuickBox/20_Release/trunk/01_Software/QB2Jumpstart/JumpStart/20_Product/20_QuickBox/22_InterRoll/12_E_Functions/Stacker/MultiLayerStacker_2_SCHW_INT1/FB_MultiLayerStacker_2_SC $
 *
 *	Purpose			:	Function Rollerdrive Straight Emulation
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				25.06.2018		dlu					Typical renamed
  *	0.2				09.01.2019		mbo					Updated encoder functionality and transportcontrol
 *
 **************************************************************************************)
 VAR
	pFunction						: POINTER TO FB_MultiLayerDeStacker_2_SCHW_MoviMot; // Address to function
	rFunction						: REFERENCE TO FB_MultiLayerDeStacker_2_SCHW_MoviMot; // Reference to function
	ZoneIdx							: INT; //ZoneIdx	
	EncoderMoviMot					: ARRAY [1..2] OF UDINT;	//encoder value
	Encoder_helper					: ARRAY [1..2] OF REAL;	//needed to not lose value <1
	EncoderValueAdd					: ARRAY [1..2] OF USINT;	//value added to Encoder in one Cycle
	fbTransportControl 				: ARRAY [1..2] OF FB_TransportControlEmulation; // Transport control for each zone	
	fbEncoder						: ARRAY [1..2] OF FB_Encoder;
	fbSpeedEmulator					: ARRAY [1..2] OF FB_SpeedEmulator; // Speed emulation for each zone
	
	(*Stopper*)
	ValveStopperPosition 			: INT := 0;
	ValveStopperUpperPosition		: INT := 60;
	ValveStopperLowerPosition		: INT := 0;
	StopAtBlade						: ARRAY [1..2] OF BOOL;
	(*Clips*)
	ClipClosedPosition 				: INT := 0;
	ClipOpenPosition				: INT := 20;
	ClipPosition					: INT := 0;
    
	(*Safety variables*)
	SafetyLightCurtains 			: BOOL;
	FanModul1_OK 					: BOOL;
	FanModul2_OK 					: BOOL;
	FanModul3_OK 					: BOOL;
	FanModul4_OK 					: BOOL;
	MachineStop_OK 					: BOOL;
	EmergencyStop_OK				: BOOL;
	V24DC_OK 						: BOOL;
	V400AC_OK	 					: BOOL;
	DoorOpen 						: BOOL;
	
	LimitSwitchTop					: BOOL;
	LimitSwitchBottom				: BOOL;
	
	(*MoviDrive*)
	Encoder							: DINT;	//encoder value
	EncoderInClipCS					: DINT;
	LastEncoderInClipCS				: DINT;
	cycletimeIn_ms					: UINT;
	MaxRpmMotor						: UINT := 1435; //Max rpm of motor 
	CW								: UINT := 86; // Gear ratio motor-shaft
	EncoderIPR						: UINT := 4096; // Encoder IncrementsPerRound
	IncPerCylce						: REAL;
	mmPerCycle						: REAL;
	TargetPosition					: DINT; // Target position      	
	UpperPosition					: UINT := 1000;
	AutoMode						: BOOL;
	ManualMode						: BOOL;
	
	(*Emulated Stack ID*)
	StackEmulationId				: ARRAY [1.. 2] OF  UINT;
	
	EmulationTuID 					: DINT;
	LayerIdx						: ARRAY [1.. 2] OF INT ;
	helper_Idx 						: INT;
	helper_Idx2						: INT;
	MoveUpDone						: ARRAY [1.. 2] OF BOOL ;
	MoveDownDone 					: ARRAY [1.. 2] OF BOOL ;
	DriveOutDone					: BOOL;
	(*Test variables*)
	StopClipMovement				: BOOL;
	StopLiftMovement				: BOOL;
	StopStopperMovement				: BOOL;
	delayTimer						: TON;
	startTimer						: BOOL;
	
	SingleTrayLayerHandling			: BOOL := TRUE;
END_VAR

VAR CONSTANT
	MAX_TRAYS_ON_LIFT 					: INT := 4; //Number of tray layers, that will trigger the lighteye if lift is up.
	MAX_STACK_SIZE						: INT := 18;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();

(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();


(**************************************************************************************************************
   Emulation
***************************************************************************************************************)
// If emulation function is initialized
IF FunctionData.Init THEN
	
	// Update data of real object
	M_UpdateRealObjectData();
	
 	// Handle external Subsystem-Subsystem interfaces
	A_ExternalSTI();
	IF rFunction.FunctionData.Init THEN 
		StopAtBlade[rFunction.NumberofZones] :=	ZoneData[rFunction.NumberofZones].TransportControlData.TU_LeadingEdgePos > (FunctionBase.rSettingsFunctionReal.Zone[rFunction.NumberofZones].Length[1]- 50)
												AND rFunction.HW_Inputs.StopperUp;
		IF rFunction.NumberofZones > 1 THEN
			FOR ZoneIdx:=1 TO rFunction.NumberofZones-1 DO
				StopAtBlade[ZoneIdx] :=	ZoneData[ZoneIdx].TransportControlData.TU_LeadingEdgePos > (FunctionBase.rSettingsFunctionReal.Zone[ZoneIdx].Length[1]- 50)
									AND ZoneData[ZoneIdx+1].TransportControlData.Occupied;
			END_FOR
		END_IF			
		
		
		FOR ZoneIdx:=1 TO rFunction.NumberofZones DO	
			rFunction.HW_Inputs.MoviMot[ZoneIdx].State				:=8;
			rFunction.HW_Inputs.MoviMot[ZoneIdx].PI01_StatusWord1.2	:=TRUE;
			rFunction.HW_Inputs.MoviMot[ZoneIdx].PI01_StatusWord1.1	:=TRUE;
			rFunction.HW_Inputs.MoviMot[ZoneIdx].PI03_StatusWord2.9 := TRUE;
			
			rFunction.HW_Inputs.MoviMot[ZoneIdx].PI01_StatusWord1.0:= rFunction.HW_Outputs.MoviMot[ZoneIdx].PO1_ControlWord.1;
			IF rFunction.HW_Inputs.MoviMot[ZoneIdx].PI01_StatusWord1.0 THEN
				
				cycletimeIn_ms:=UDINT_TO_UINT(_TaskInfo[1].CycleTime / 10000);
			
				IncPerCylce := 	LREAL_TO_REAL(	(rFunction.fbSpeedControl[ZoneIdx].CommandSpeed)
												* (UINT_TO_REAL(cycletimeIn_ms)/1000)
												* rFunction.Settings.Movimot[ZoneIdx].EncoderIncrementsPerRound
												* rFunction.Settings.Movimot[ZoneIdx].NominalGearRatio
												* rFunction.Settings.Movimot[ZoneIdx].MechanicsGearRatio
												/ (	rFunction.Settings.Movimot[ZoneIdx].DiameterBeltAxis
													* 1000
													* PI
												  )
											  );
				
				Encoder_helper[ZoneIdx]:=Encoder_helper[ZoneIdx]+IncPerCylce;
				EncoderValueAdd[ZoneIdx]:=REAL_TO_USINT(Encoder_helper[ZoneIdx]);
				Encoder_helper[ZoneIdx]:=Encoder_helper[ZoneIdx]-USINT_TO_REAL(EncoderValueAdd[ZoneIdx]);
				
				IF rFunction.Settings.Movimot[ZoneIdx].DriveDirection THEN
					EncoderMoviMot[ZoneIdx]:=EncoderMoviMot[ZoneIdx]+EncoderValueAdd[ZoneIdx];
				ELSE
					EncoderMoviMot[ZoneIdx]:=EncoderMoviMot[ZoneIdx]-EncoderValueAdd[ZoneIdx];
				END_IF
				
					
				rFunction.HW_Inputs.MoviMot[ZoneIdx].PI08_Encoder_Low:=UDINT_TO_UINT(EncoderMoviMot[ZoneIdx]);
				rFunction.HW_Inputs.MoviMot[ZoneIdx].PI07_Encoder_High:=UDINT_TO_UINT(EncoderMoviMot[ZoneIdx]/65536);
			END_IF
			
			
			// Emulate actual speed
			fbSpeedEmulator[ZoneIdx](	CommandRun		:= rFunction.fbTransportControl[ZoneIdx].Outputs.DriveRun,
								CommandFastStop	:= FALSE,
								CommandSpeed	:= REAL_TO_INT(rFunction.fbSpeedControl[ZoneIdx].CommandSpeed * rFunction.SettingsFunction.Zone[1].SpeedRatio),
								CommandRamp		:= INT_TO_UINT(rFunction.fbTransportControl[ZoneIdx].Acceleration));
								
			// emulate position
			fbEncoder[ZoneIdx](
				Run	  := fbSpeedEmulator[ZoneIdx].DriveRunning,
				Speed := fbSpeedEmulator[ZoneIdx].ActualSpeed);	
			
			// Call transport control 
			fbTransportControl[ZoneIdx](
				Param 					:= M_GetTransportControlParam(mZoneNumber := ZoneIdx), // Parameter of transport control
				PrevTransportData 		:= M_GetPreviousTransportData(mZoneNumber := ZoneIdx), // Previous transport data
				NextTransportData 		:= M_GetNextTransportData(mZoneNumber := ZoneIdx), // Next transport data	
				TransportEnable			:= TRUE, // Transport enable		
				DriveRunning			:= fbSpeedEmulator[ZoneIdx].DriveRunning,
				EncoderValue			:= fbEncoder[ZoneIdx].EncoderValue,
				ZoneData 				:= ZoneData[ZoneIdx]); // Zone data
		END_FOR
		
		// Overwrite inputs
		A_OverWriteInputs();
	END_IF 
END_IF]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{55260be8-e40a-4beb-a9a6-4216c250ae45}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated dummy function
 **************************************************************************************)

// If dummy function is not initialized
IF NOT FunctionData.Init Then
	// Check that linked dummy function number is valid
	IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
	   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN   
		(*******************************************************
		 * Custom initialization steps
		*******************************************************)
	   	FOR ZoneIdx:=1 TO 2 DO
			LayerIdx[ZoneIdx] := 0;
			StackEmulationId[ZoneIdx] := 0;
		END_FOR	
		(******************************************************)

		// Check if the real module is initialized
		IF F_IsFunctionInitialized(eFunctionNumber := eFunctionNumber) THEN
			// Module is initialized
			FunctionData.Init:= TRUE;
			
			// Send debug message
			DebugMsg := CONCAT('Initialization done. Element Function: ', FunctionRegistry[eFunctionNumber].FunctionName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
			
			// Initialize real module
			F_InitializeFunction(eFunctionNumber := eFunctionNumber);
		END_IF
	END_IF
END_IF
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs" Id="{0669aa35-68c9-449f-99cc-2f3583133bc5}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OverWriteInputs
 * 	FUNCTION	Overwrite inputs for emulation
 **************************************************************************************)
(***************************************************************************************
 * Static feedback of state  
 **************************************************************************************)
SafetyLightCurtains 	:= TRUE;
FanModul1_OK 			:= TRUE;
FanModul2_OK 			:= TRUE;
FanModul3_OK 			:= TRUE;
FanModul4_OK 			:= TRUE;
MachineStop_OK			:= TRUE;
EmergencyStop_OK		:= True; 
V24DC_OK 				:= TRUE;
V400AC_OK	 			:= TRUE;
DoorOpen 				:= FALSE;

LimitSwitchBottom		:= TRUE;
LimitSwitchTop			:= TRUE;

//rFunction.HW_Inputs.HW_InputsStackerCabinet.ManualButton_ManualModeOn := TRUE;
//rFunction.HW_Inputs.HW_InputsStackerCabinet.ManualButton_RequestTray  := TRUE;


(***************************************************************************************
 * Test environment  
 **************************************************************************************)
// StartTimer
delayTimer(IN := rFunction.eStackerStates = E_StackerStates.MoveDown , PT:= T#100MS);
IF delayTimer.Q and startTimer THEN 
	MachineStop_OK := FALSE;
END_IF

(**************************************************************************************
 * Rollerdrives
 **************************************************************************************)

FOR ZoneIdx:=1 TO rFunction.NumberofZones DO
	ZoneData[ZoneIdx].TransportControlData.TransportParam.ZoneLength := FunctionBase.rSettingsFunctionReal.Zone[ZoneIdx].Length[1];	 
	
			rFunction.HW_Inputs.Sensor[ZoneIdx]:= NOT rFunction.Settings.InvertOccupiedSensor[ZoneIdx] XOR
										   fbTransportControl[ZoneIdx].M_TU_PresentState(
												mPosition := FunctionBase.rSettingsFunctionReal.Zone[ZoneIdx].Length[1] - FunctionBase.rSettingsFunctionReal.Zone[ZoneIdx].ZoneEndLengthToSide[2],
												mIndex => FunctionBase.IndexToRemove[ZoneIdx],
												mLeadingEdgePos => ZoneData[ZoneIdx].TransportControlData.TU_LeadingEdgePos);
					
END_FOR

(**************************************************************************************
 * Sensor blade stop
 * Note:
 *		Stopper is 75mm behind the occupied sensor signal occupied 
 **************************************************************************************)

rFunction.HW_Inputs.StopperFree :=  NOT rFunction.Settings.InvertOccupiedSensor[rFunction.NumberofZones] XOR
										fbTransportControl[rFunction.NumberofZones].M_TU_PresentState(
												mPosition := 	FunctionBase.rSettingsFunctionReal.Zone[rFunction.NumberofZones].Length[1] 
																- FunctionBase.rSettingsFunctionReal.Zone[rFunction.NumberofZones].ZoneEndLengthToSide[2]
																+ 75, // Blade stop free sensor is 75mm behind sensor Occupied (TODO Config?)
												mIndex => FunctionBase.IndexToRemove[rFunction.NumberofZones],
												mLeadingEdgePos => ZoneData[rFunction.NumberofZones].TransportControlData.TU_LeadingEdgePos);


(**************************************************************************************
 * Valve blade stop
 * Note:
 *		Valve of blade stop is triggered by the signals "ValveStopperDown"
 *      and "ValveStopperUp". Doe to mechanics and delay times, it needs 
 * 		time from closed to open which is represented with the counter and positions
 *		"ValveStopperLowerPosition"/ ValveStopperUpperPosition.
 **************************************************************************************)

IF NOT StopStopperMovement THEN // Set to 'TRUE' to emulate Stopper error
	IF rFunction.HW_Outputs.ValveStopperDown THEN
		// start move down position
		IF ValveStopperPosition > ValveStopperLowerPosition THEN  
			ValveStopperPosition := ValveStopperPosition - TO_INT(cycletimeIn_ms);
		END_IF
	END_IF	
	IF rFunction.HW_Outputs.ValveStopperUp THEN  
		// start move up position
		IF ValveStopperPosition < ValveStopperUpperPosition THEN  
			ValveStopperPosition := ValveStopperPosition + TO_INT(cycletimeIn_ms);// start move down position
		END_IF
		
	END_IF
END_IF

IF ValveStopperPosition <= ValveStopperLowerPosition THEN 
	rFunction.HW_Inputs.StopperDown := NOT rFunction.Settings.HW_InputInverted.StopperDown;
ELSIF ValveStopperPosition >= ValveStopperUpperPosition THEN 
	rFunction.HW_Inputs.StopperUp := NOT rFunction.Settings.HW_InputInverted.StopperUp;
ELSE
	rFunction.HW_Inputs.StopperUp := rFunction.Settings.HW_InputInverted.StopperUp;
	rFunction.HW_Inputs.StopperDown := rFunction.Settings.HW_InputInverted.StopperDown;
END_IF

(**************************************************************************************
 * Valve clip
 * Note:
 *		Valve of clip is triggered by the signals "ValveClipOpen"
 *     	Due to mechanics and delay times, it needs 
 * 		time from closed to open which is represented with the counter and positions
 *		"ClipClosedPosition"/ "ClipOpenPosition".
 **************************************************************************************)

IF NOT StopClipMovement THEN // Set to 'TRUE' to emulate Clipper error
	IF rFunction.HW_Outputs.ValveClipOpen THEN
		// open clips
		IF ClipPosition < ClipOpenPosition THEN  
			ClipPosition := ClipPosition + TO_INT(cycletimeIn_ms);
		END_IF
	END_IF
		
	IF rFunction.HW_Outputs.ValveClipClose THEN
		// close clips
		IF ClipPosition > ClipClosedPosition THEN  
			ClipPosition := ClipPosition - TO_INT(cycletimeIn_ms);
		END_IF
	END_IF
END_IF 

IF ClipPosition >= ClipOpenPosition THEN 
	// clip is open 
	rFunction.HW_Inputs.ClipIsOpenRight := NOT rFunction.Settings.HW_InputInverted.ClipIsOpenRight;
	rFunction.HW_Inputs.ClipIsOpenLeft := NOT rFunction.Settings.HW_InputInverted.ClipIsOpenLeft;
ELSIF ClipPosition <= ClipClosedPosition THEN 
	// clip is closed
	rFunction.HW_Inputs.ClipIsClosedRight := NOT rFunction.Settings.HW_InputInverted.ClipIsClosedRight;
	rFunction.HW_Inputs.ClipIsClosedLeft := NOT rFunction.Settings.HW_InputInverted.ClipIsClosedLeft;
ELSE
	// clip is on the way 
	rFunction.HW_Inputs.ClipIsOpenRight := rFunction.Settings.HW_InputInverted.ClipIsOpenRight;
	rFunction.HW_Inputs.ClipIsOpenLeft := rFunction.Settings.HW_InputInverted.ClipIsOpenLeft;
	rFunction.HW_Inputs.ClipIsClosedRight := rFunction.Settings.HW_InputInverted.ClipIsClosedRight;
	rFunction.HW_Inputs.ClipIsClosedLeft := rFunction.Settings.HW_InputInverted.ClipIsClosedLeft;
END_IF


(**************************************************************************************
 * SaftyLightCurtains
 * Note:
 *		
 * 		
 *      
 **************************************************************************************)
rFunction.HW_Inputs.SafetyLightCurtain := SafetyLightCurtains;

(**************************************************************************************
 * MoviDrive
 * Note:
 *		
 * 		
 *      
 **************************************************************************************)
rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.1 := TRUE; // InverterReady
rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.2 := TRUE ; // IposReferenced
rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.4 := FALSE; // BreakOpen
rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.5 := FALSE; // Fault
rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.6 := FALSE; // LimitSwitchRight
rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.7 := FALSE; // LimitSwitchLeft

rFunction.HW_Inputs.MoviDrive.EtherCatState := 16#8;
cycletimeIn_ms := UDINT_TO_UINT(_TaskInfo[1].CycleTime / 10000);


TargetPosition := (UINT_TO_DINT(rFunction.HW_Outputs.MoviDrive.PO2_TargetPositionHigh)*65536) + UINT_TO_DINT(rFunction.HW_Outputs.MoviDrive.PO3_TargetPositionLow);
IF ABS(DINT_TO_LREAL(TargetPosition - Encoder)) < (2*mmPerCycle) THEN
	rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.3 := TRUE;//Targetposition reached
ELSE 
	rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.3 := FALSE;//Targetposition reached
END_IF 


rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.6 := FALSE ;
rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.7 := FALSE ;	


AutoMode := rFunction.HW_Outputs.MoviDrive.PO1_ControlWord.11 AND rFunction.HW_Outputs.MoviDrive.PO1_ControlWord.12;
ManualMode := rFunction.HW_Outputs.MoviDrive.PO1_ControlWord.11 AND NOT rFunction.HW_Outputs.MoviDrive.PO1_ControlWord.12;

IF rFunction.HW_Outputs.MoviDrive.PO1_ControlWord.8 AND NOT rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.3 THEN 

	
	IncPerCylce:=  	 (MaxRpmMotor	/ 60.0)		// rounds/s
					* EncoderIPR 				// imp / round
					* cycletimeIn_ms/1000.0;									
	mmPerCycle := LREAL_TO_REAL(	(MaxRpmMotor	/ 60.0)		// rounds/s
								/ CW						// Gear ratio
								* 2.0*PI*50.0				// circumference of gearwheel in mm
								* cycletimeIn_ms/1000.0);	
	IF rFunction.HW_Outputs.MoviDrive.PO1_ControlWord.8 THEN//Outputs.ControlWord.Start
		rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.0 := TRUE ; // DriveRun
		IF (AutoMode AND TargetPosition > (Encoder+REAL_TO_USINT(mmPerCycle))) OR (ManualMode AND rFunction.HW_Outputs.MoviDrive.PO1_ControlWord.9) THEN //ManualFwd
			Encoder:=Encoder+REAL_TO_USINT(mmPerCycle);
		ELSIF (AutoMode AND TargetPosition <  (Encoder+REAL_TO_USINT(mmPerCycle))) OR (ManualMode AND rFunction.HW_Outputs.MoviDrive.PO1_ControlWord.10) THEN //ManualFwd
			Encoder:=Encoder-REAL_TO_USINT(mmPerCycle);
		END_IF 
	ELSE 
		rFunction.HW_Inputs.MoviDrive.PI01_StatusWord1.0 := FALSE ; // DriveRun
	END_IF

	rFunction.HW_Inputs.MoviDrive.PI03_Encoder_Low :=DINT_TO_UINT(Encoder);
	rFunction.HW_Inputs.MoviDrive.PI02_Encoder_High :=DINT_TO_UINT(SHR(Encoder,16)); //65536	
	
	
END_IF

EncoderInClipCS := Encoder - rFunction.Settings.UpperEdgeToClipInMM + rFunction.Settings.RefIniPositionInMM; 

(**************************************************************************************
 * Stacker Full Sensor
 * Note: The LayerIdx represents the actual stack size. If one of them is greater than 
 *		 the MAX_STACK_SIZE, is the sensor triggered. 
 * 		 Since it is an photoelectric sensor is the sensor signal TRUE if no tray is 
 *       inbetween. If a tray interrupt the light beam, is the sensor signal False. 
 **************************************************************************************)
(*
IF LayerIdx[1] > 1 OR LayerIdx[2] > 1
OR ((LayerIdx[1] = 1 OR LayerIdx[2] = 1) AND (ABS(EncoderInClipCS - rFunction.Settings.ClipPositions.PickPos) < 10)
AND rFunction.HW_Inputs.ClipIsClosedLeft AND rFunction.HW_Inputs.ClipIsClosedRight) THEN 
	rFunction.HW_Inputs.StackerFull	:= NOT rFunction.Settings.HW_InputInverted.StackerFull;
ELSE
	rFunction.HW_Inputs.StackerFull	:= rFunction.Settings.HW_InputInverted.StackerFull;
END_IF
*)
IF LayerIdx[1] > 1 OR LayerIdx[2] > 1
OR ((LayerIdx[1] = 1 OR LayerIdx[2] = 1)
AND (EncoderInClipCS >= rFunction.Settings.ClipPositions.PickPos)
AND (EncoderInClipCS <= rFunction.Settings.ClipPositions.Up)
AND rFunction.HW_Inputs.ClipIsClosedLeft AND rFunction.HW_Inputs.ClipIsClosedRight) THEN 
	rFunction.HW_Inputs.StackerFull	:= NOT rFunction.Settings.HW_InputInverted.StackerFull;
ELSE
	rFunction.HW_Inputs.StackerFull	:= rFunction.Settings.HW_InputInverted.StackerFull;
END_IF


(**************************************************************************************
 * Lift reference position sensor
 * Note: Sets the LiftReferencePosition INI, if the motor shaft is between 0 and 45 
 * 		 degree compared to the bottom dead center.	
 **************************************************************************************)
IF ABS(Encoder) < 5 THEN 
	rFunction.HW_Inputs.LiftReferencePosition := NOT rFunction.Settings.HW_InputInverted.LiftReferencePosition;
ELSE 
	rFunction.HW_Inputs.LiftReferencePosition := rFunction.Settings.HW_InputInverted.LiftReferencePosition;
END_IF



FOR ZoneIdx:=1 TO rFunction.NumberofZones DO
	
	IF EncoderInClipCS >= (rFunction.Settings.ClipPositions.Up-5) THEN 
		MoveUpDone[ZoneIdx] := FALSE;
	END_IF
	
	IF EncoderInClipCS <= (rFunction.Settings.ClipPositions.Down+5) THEN 
		MoveDownDone[ZoneIdx] := FALSE;
	END_IF
	
	(**************************************************************************************
	 * Get layer index of stack when driving onto the zone (takeover).
	 * Note: 
	 **************************************************************************************)
	IF rFunction.ZoneData[ZoneIdx].TransportControlData.eTakeOverState = E_TakeoverState.ACTIVE THEN
	
		F_GetTU_PresentStateEmulation(
				eFunctionNumber := rFunction.SettingsFunction.eFunctionNumber , ZoneNumber:= ZoneIdx, 
				Position		:= rFunction.SettingsFunction.Zone[ZoneIdx].Length[1] - 2 * rFunction.SettingsFunction.Zone[ZoneIdx].ZoneEndLengthToSide[2], 
				TU_DataIndex	=> EmulationTuID);
			
		StackEmulationId[ZoneIdx] := TO_UINT(TU_DataTableEmulation[EmulationTuID].DataSet.NestedIndex);
		FOR helper_Idx := 1 TO MAX_TRAYS_ON_LIFT DO
			IF StacksEmulation.Stacks[TO_UINT(TU_DataTableEmulation[EmulationTuID].DataSet.NestedIndex)].Tray[helper_Idx].TU_DataIndex <> 0 THEN 
				LayerIdx[ZoneIdx] := helper_Idx;
			END_IF
		END_FOR	
		
	END_IF
	
	(**************************************************************************************
	 * Move up phase.
	 * Note: The lift, lifts the trays up. The informations are stored in an GVL Array "StackEmulationId"
	 *       and the TransportControlEmulation FB is forced to an empty state. 
	 **************************************************************************************)
	IF 	rFunction.ZoneData[ZoneIdx].TransportControlData.Occupied
	AND	(rFunction.HW_Inputs.ClipIsClosedLeft AND rFunction.HW_Inputs.ClipIsClosedRight)
	AND	EncoderInClipCS > rFunction.Settings.ClipPositions.PickPos
	AND	EncoderInClipCS < (rFunction.Settings.ClipPositions.Up-10)
	AND EncoderInClipCS > LastEncoderInClipCS 
	AND	NOT MoveUpDone[ZoneIdx] THEN
		
		
		// Help flag for one cycle execution
		MoveUpDone[ZoneIdx] := TRUE;
		
		// Get TU ID from emulated position
		F_GetTU_PresentStateEmulation(
			eFunctionNumber := rFunction.SettingsFunction.eFunctionNumber , ZoneNumber:= ZoneIdx, 
			Position		:= rFunction.SettingsFunction.Zone[ZoneIdx].Length[1] - 2 * rFunction.SettingsFunction.Zone[ZoneIdx].ZoneEndLengthToSide[2], 
			TU_DataIndex	=> EmulationTuID);
		
		StackEmulationId[ZoneIdx] := TO_UINT(TU_DataTableEmulation[EmulationTuID].DataSet.NestedIndex);
				
		TU_DataTableEmulation[StacksEmulation.Stacks[StackEmulationId[ZoneIdx]].Tray[1].TU_DataIndex].DataSet.NestedIndex := 0;
		
		FOR helper_Idx := 1 TO MAX_STACK_SIZE - 1 DO
			StacksEmulation.Stacks[StackEmulationId[ZoneIdx]].Tray[helper_Idx] :=  
			StacksEmulation.Stacks[StackEmulationId[ZoneIdx]].Tray[helper_Idx + 1];
			IF StacksEmulation.Stacks[StackEmulationId[ZoneIdx]].Tray[helper_Idx].TU_DataIndex = 0 THEN
				EXIT;
			END_IF
		END_FOR
		
		FOR helper_Idx := 1 TO MAX_TRAYS_ON_LIFT DO
			IF StacksEmulation.Stacks[StackEmulationId[ZoneIdx]].Tray[helper_Idx].TU_DataIndex <> 0 THEN 
				LayerIdx[ZoneIdx] := helper_Idx;
			END_IF
		END_FOR
	
	END_IF
	
	
	
	(**************************************************************************************
	 * Move down phase.
	 * Note: The lift moves the trays down. The informations are taken from the lowest tray 
	 * 		 in an GVL Array "StackEmulationId.Stacks[x].Tray[1]" and pass to the 
	 * 		 TransportControlEmulation FB. The TransportControlEmulation is triggered to 
	 *		 add this TU with the TU_DataIndex which was taken from the GVL Array. 
	 **************************************************************************************)
	IF NOT rFunction.ZoneData[ZoneIdx].TransportControlData.Occupied
	AND	EncoderInClipCS > (rFunction.Settings.ClipPositions.Down+10)
	AND	EncoderInClipCS < (rFunction.Settings.ClipPositions.PickPos-10)
	AND EncoderInClipCS < LastEncoderInClipCS
	AND NOT MoveDownDone[ZoneIdx] THEN
			
		MoveDownDone[ZoneIdx] := TRUE;
		
		ZoneData[ZoneIdx].ITC.Commands.AddTU := TRUE;
		ZoneData[ZoneIdx].ITC.Commands.TU_DataIndexToAdd := StacksEmulation.Stacks[StackEmulationId[ZoneIdx]].Tray[1].TU_DataIndex;
		
		IF NOT rFunction.Inputs.StackerFull THEN
			TU_DataTableEmulation[StacksEmulation.Stacks[StackEmulationId[ZoneIdx]].Tray[1].TU_DataIndex].DataSet.NestedIndex := 0;
			StacksEmulation.Stacks[StackEmulationId[ZoneIdx]].Tray[1] := StacksEmulation.Stacks[StackEmulationId[ZoneIdx]].Tray[2];
			LayerIdx[ZoneIdx] := 0;
		END_IF
			
	END_IF
END_FOR


(**************************************************************************************
 * Possible error signals 
 * Note: 
 **************************************************************************************)
rFunction.HW_Inputs.StackerCabinet.MachineStop_OK := MachineStop_OK;
rFunction.HW_Inputs.V24DC_OK := V24DC_OK;
rFunction.HW_Inputs.V400AC_Break_OK := V400AC_OK;
rFunction.HW_Inputs.FAN_OK := V400AC_OK;
rFunction.HW_Inputs.DoorsClosed := NOT DoorOpen;

rFunction.HW_Inputs.LimitSwitchBottom := LimitSwitchBottom;
rFunction.HW_Inputs.LimitSwitchTop := LimitSwitchTop;

LastEncoderInClipCS := EncoderInClipCS;
]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{38629e4d-4adb-47d4-bbf2-361aa6166af7}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid function number
IF eFunctionNumber > E_FunctionNumber.F_BEGIN_FUNCTION AND
   eFunctionNumber < E_FunctionNumber.F_END_FUNCTION THEN

	// Is reference valid  
	IF __ISVALIDREF(FunctionRegistry[eFunctionNumber].rFunction) THEN
		// Build address to reference
		pFunction := ADR(FunctionRegistry[eFunctionNumber].rFunction);

		// If address is possible
		IF pFunction <> 0 THEN
			// Build reference to memory
			rFunction REF= pFunction^;
		ELSE
			// Reinitialize module
			FunctionData.Init:= FALSE;
		END_IF
	ELSE
		// Reinitialize module
		FunctionData.Init:= FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_GetTU_PresentState" Id="{f2395a00-2ab4-4f72-82ca-c491c3ed9fa5}">
      <Declaration><![CDATA[METHOD M_GetTU_PresentState : BOOL
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 488065 $
 *	Revision date	:	$Date: 2021-10-13 13:42:49 +0200 (Mi, 13 Okt 2021) $
 *	Last changed by	:	$Author: g7nowan $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/QuickMove/02_Controls/QuickBox/20_Release/trunk/01_Software/QB2Jumpstart/JumpStart/20_Product/20_QuickBox/22_InterRoll/12_E_Functions/Stacker/MultiLayerStacker_2_SCHW_INT1/FB_MultiLayerStacker_2_SC $
 *
 *	Purpose			:	Get TU present state, overriden rom base object
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ----------------------------------------------------------------- -------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR_INPUT
	mZone	 : INT; // Zone number
	mPosition : UINT; // Position [mm]
END_VAR
VAR_OUTPUT
	mTU_DataIndex : DINT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF mZone = 0 THEN
	mZone := 1;
ELSIF mZone > NUMBER_OF_ZONES_PER_FUNCTION THEN
	mZone := NUMBER_OF_ZONES_PER_FUNCTION;
END_IF

fbTransportControl[mZone].M_TU_PresentState(mPosition := mPosition, mTU_DataIndex => mTU_DataIndex);
]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>