﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.9">
  <POU Name="FB_ProBOXSGCustomEmulationQVW" Id="{b2a3e26a-7529-0507-0b31-3d8cd06c3875}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_ProBOXSGCustomEmulationQVW EXTENDS FB_GroupConvEmulation
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision: 417092 $
 *	Revision date	:	$Date: 2018-11-14 12:58:58 +0100 (Mi., 14 Nov 2018) $
 *	Last changed by	:	$Author: q8aberh $
 *	URL				:	$URL: http://almscdc.swisslog.com/repo/SWPD/Development/ProMove/02_Controls/ProBox/03_Implementation/01_DevelopmentProject/01_Software/ProBox_COE/ProBox_DevProj_v1.0/PLC_1/20_Product/11_Emu/14_Groups/10_Lib/10_Conv/FB_DummySG_Emulation.TcPOU $
 *
 *	Purpose			:	Emulation switching group.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *	0.1				01.01.2001		swl					Example
 *
 **************************************************************************************)
VAR_INPUT
	Config						: ST_CFG_ProBoxSGCustomEmulation; // Configuration emulation
END_VAR
 
VAR
	pGroup						: POINTER TO FB_ProBOXSGCustomQVW; // Address to Group
	rGroup						: REFERENCE TO FB_ProBOXSGCustomQVW; // Reference to real instance
	DebugResetButton			: BOOL; //for debug purpose or visualisation
	DebugStartButton			: BOOL; //for debug purpose or visualisation
	DebugManualMode				: BOOL; //for debug purpose or visualisation
	DebugAutoMode				: BOOL; //for debug purpose or visualisation
	DebugSemiAutoMode			: BOOL := TRUE; //for debug purpose or visualisation
END_VAR

VAR PERSISTENT
	Settings					: ST_CFG_ProBoxSGCustomEmulation; // Settings 
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[
(**************************************************************************************************************
   Initialization
***************************************************************************************************************)
A_Init();


(**************************************************************************************************************
   Update reference
***************************************************************************************************************)
A_UpdateReference();


(**************************************************************************************************************
   Emulation
***************************************************************************************************************)
// If emulation group is initialized
IF GroupData.Init THEN
	
	// Update real data
	M_UpdateRealObjectData();	

	// Overwrite inputs
	A_OverWriteInputs();
END_IF
]]></ST>
    </Implementation>
    <Action Name="A_Init" Id="{a238840a-4bc4-07d4-1fbc-04ad9a78baf3}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_Init
 * 	FUNCTION	Initialize emulated group
 **************************************************************************************)
 
// If group is not initialized
IF NOT GroupData.Init THEN

	// Check that linked group number is valid
	IF eGroupNumber > E_GroupNumber.G_BEGIN_GROUP AND
	   eGroupNumber < E_GroupNumber.G_END_GROUP THEN

		// Load emulation settings	
		Settings := Config;
		
		// Check that the real module is initialized
		IF F_IsGroupInitialized(eGroupNumber := eGroupNumber) THEN
			// Module is initialized
			GroupData.Init := TRUE;
			
			// Send debug message
			DebugMsg := CONCAT('Initialization done. Group: ', GroupRegistry[eGroupNumber].GroupName);
			fbDebugMsg.M_SendInfoMsg(DebugMsg);
			
			// Initialize real module
			F_InitializeGroup(eGroupNumber := eGroupNumber);
		END_IF
	END_IF
END_IF



]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OverWriteInputs" Id="{b3c87fc7-6006-0611-2cfc-ec4f7f5eb286}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OverWriteInputs
 * 	FUNCTION	Overwrite inputs for emulation
 **************************************************************************************)

 // Modes
IF DebugAutoMode THEN
	DebugManualMode							:= FALSE;
	DebugSemiAutoMode                       := FALSE;
	rGroup.HW_InputsQVW.ManSW				:= FALSE;
	rGroup.HW_InputsQVW.AutoSW				:= TRUE;
	rGroup.HW_InputsQVW.SemiAutoSW			:= FALSE;
ELSIF DebugManualMode THEN
	DebugAutoMode							:= FALSE;
	DebugSemiAutoMode                       := FALSE;
	rGroup.HW_InputsQVW.ManSW				:= TRUE;
	rGroup.HW_InputsQVW.AutoSW				:= FALSE;
	rGroup.HW_InputsQVW.SemiAutoSW			:= FALSE;
ELSIF DebugSemiAutoMode THEN
	DebugAutoMode							:= FALSE;
	DebugManualMode                         := FALSE;
	rGroup.HW_InputsQVW.ManSW				:= FALSE;
	rGroup.HW_InputsQVW.AutoSW				:= FALSE;
	rGroup.HW_InputsQVW.SemiAutoSW			:= TRUE;
END_IF 

// Commands
rGroup.HW_InputsQVW.ResetErrorPB					:= DebugResetButton;
rGroup.HW_InputsQVW.AutoOnPB						:= (Settings.Autostart AND NOT GroupRegistry[rGroup.ConfigGroup.eGroupNumber].rGroupData.Init) OR DebugStartButton;
rGroup.HW_InputsQVW.MainContactorOFF				:= TRUE; //NC Push button
// Device input states
rGroup.HW_InputsQVW.EmergencyCircuitOK				:= TRUE;
rGroup.HW_InputsQVW.FeedBackMainContactorOFF[1]	:= FALSE;
rGroup.HW_InputsQVW.FeedBackMainContactorOFF[2]	:= FALSE;
rGroup.HW_InputsQVW.FeedBackMainContactorOFF[3]	:= FALSE;
rGroup.HW_InputsQVW.FeedBackMainContactorOFF[4]	:= FALSE;
rGroup.HW_InputsQVW.FeedBackMainContactorOFF[5]	:= FALSE;
rGroup.HW_InputsQVW.FeedBackMainContactorOFF[6]	:= FALSE;
rGroup.HW_InputsQVW.FireAlarm						:= TRUE;
                
rGroup.HW_InputsQVW.Fuse_Fan_Carbinet				:= TRUE;
rGroup.HW_InputsQVW.Fuse24VDC						:= TRUE;
rGroup.HW_InputsQVW.Fuse24VDC_Alarm90				:= FALSE;
rGroup.HW_InputsQVW.Fuse24VDC_Fiedlbuscomponent	:= TRUE;
rGroup.HW_InputsQVW.Fuse400VAC						:= TRUE;
rGroup.HW_InputsQVW.Overcurrent					:= FALSE;

DebugResetButton:=FALSE;


]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_UpdateReference" Id="{6e1803f4-627a-0081-22b6-ebe5d58d12e1}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_UpdateReference
 * 	FUNCTION	Update reference to real object
 **************************************************************************************)

// Check valid group number
IF eGroupNumber > E_GroupNumber.G_BEGIN_GROUP AND
   eGroupNumber < E_GroupNumber.G_END_GROUP THEN

	// Is reference valid  
	IF __ISVALIDREF(GroupRegistry[eGroupNumber].rGroup) THEN
		// Build address to reference
		pGroup := ADR(GroupRegistry[eGroupNumber].rGroup);

		// If address is possible
		IF pGroup <> 0 THEN
			// Build reference to memory
			rGroup REF= pGroup^;
		ELSE
			// Reinitialize module
			GroupData.Init := FALSE;
		END_IF
	ELSE
		// Reinitialize module
		GroupData.Init := FALSE;
	END_IF
END_IF
		]]></ST>
      </Implementation>
    </Action>
    <ObjectProperties>
      <XmlArchive>
        <Data>
          <o xml:space="preserve" t="UMLStereoTypeContainerObject">
            <v n="IsType" t="UMLType">BaseArea</v>
            <v n="Stereotype">""</v>
            <d n="Stereotypes" t="Hashtable" />
          </o>
        </Data>
        <TypeList>
          <Type n="Hashtable">System.Collections.Hashtable</Type>
          <Type n="String">System.String</Type>
          <Type n="UMLStereoTypeContainerObject">{30250973-b110-4e31-b562-c102e042dca4}</Type>
          <Type n="UMLType">{0197b136-405a-42ee-bb27-fd08b621d0cf}</Type>
        </TypeList>
      </XmlArchive>
    </ObjectProperties>
  </POU>
</TcPlcObject>