﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_MoviDriveIPOS" Id="{e486f371-94bb-4817-9503-0771ab84c09e}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_MoviDriveIPOS
VAR_INPUT
	/// Enable
	Enable: BOOL;
	/// Order Drive Run
	Run: BOOL;
	// Automode else Manual Tap Mode
	AutoModeOn :BOOL;
	// Button TapMode Forward
	BtnMaunalFWD : BOOL;
	// Button TapMode Backward
	BtnMaunalBWD : BOOL;
	/// Target Position [mm]
	TargetPosition: DINT;
	// Start Reference Manual
	ReferenceManual		: BOOL;
	/// HW Inputs
	HW_Inputs: ST_HW_InputsMoviDriveIPOS;
	/// Config from Moveimot
	Settings: ST_CFG_MoviDriveIPOS;
END_VAR
VAR_OUTPUT
	/// HW_Outputs
	HW_Outputs: ST_HW_OutputsMoviDriveIPOS;
	/// Drive Run
	DriveRunning: BOOL;
	/// Actuel Position from Drive in [mm]
	Position: DINT;
	// Target position is reached
	TargetPosReached	: BOOL;
	/// Init the Drive
	Init: BOOL;
	/// Drive is Ready to Control
	DriveReady: BOOL;
	// MoviDrive is Referenced
	IPOS_Referenced : BOOL;
END_VAR
VAR_IN_OUT
	ErrorDataSet: ST_ErrorDataSet;
END_VAR
VAR
	/// Debug instance
	fbDebugMsg: FB_DebugMsg;
	/// Debug msg
	DebugMsg: T_MAXSTRING;
	/// Max Speed from Moviemot [m/s]
	MaxSpeed: REAL;
	/// Ramp calculation with the Constant  [m/s]
	ReferencRamp: REAL;
	/// Speed [%]
	PercentToDrive: INT;
	/// Programm Inputs
	Inputs: ST_InputsMoviDriveIPOS;
	/// Programm Outputs	
	Outputs: ST_OutputsMoviDriveIPOS;
	/// State 
	eState: E_MoviDriveStateIPOS;
	/// Laststate 
	eLastState: E_MoviDriveStateIPOS;
//	Values: ARRAY[0..1] OF UINT;
//	pValue: POINTER TO UDINT;
	
	// Timer to leave reference point
	TofLeaveRef	: TOF;
	
	Merker: BOOL;
	RisEdgeReferenceSignal: R_TRIG;
	RefDone: BOOL;
	WaitForRef: BOOL;
	RisEdgeReferenceStart: R_TRIG;
END_VAR
VAR CONSTANT
	/// Deceleration time in 1s Need for Calculation
	ReferencRPM: INT := 3000;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[(************************************************************************
  Hardware Input
*************************************************************************)
A_InputMapping();

(************************************************************************
  Error handling
*************************************************************************)
A_Errorhandling();

(************************************************************************
 Init
*************************************************************************)
M_Init(mInit := FALSE);


(************************************************************************
 States
*************************************************************************)
A_States();


(************************************************************************
  Hardware Output
*************************************************************************)
A_OutputMapping();]]></ST>
    </Implementation>
    <Action Name="A_ErrorHandling" Id="{8bd7153c-8361-4bbc-873e-da8e64398ad6}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_ErrorHandling
 * 	FUNCTION	Control Error
 **************************************************************************************)
 
// Reset error  
F_ResetError(
	Reset := FALSE, 
	ErrorData := ErrorDataSet);
 
(************************************************************************
Bus Error
*************************************************************************)
F_CheckEthercatSlave(
	EthercatState:= Inputs.EtherCatState, 
	ErrorDataSet := ErrorDataSet);

(************************************************************************
Check Settings
*************************************************************************)
IF (Settings.Acceleration = 0
OR Settings.Deceleration = 0
OR Settings.SpeedAuto = 0
OR Settings.SpeedManual = 0)
AND 
   ErrorDataSet.ErrorType <> E_ErrorType.ERROR
THEN
	F_SetError(	
			ErrorType	:= E_ErrorType.ERROR,
			ErrorParam	:= '',
			ErrorMsg	:= E_ErrorConv.CONFIGURATION_ERROR,
			ErrorData	:= ErrorDataSet); 
END_IF
	
(************************************************************************
 Drive Errors
*************************************************************************)

IF Inputs.StatusWord1.Fault AND 
   ErrorDataSet.ErrorType <> E_ErrorType.ERROR THEN
	// Faultnumbers	
	F_SetError(	
		ErrorType	:= E_ErrorType.ERROR,
		ErrorParam	:= BOOL_TO_STRING(Inputs.StatusWord1.Fault),
		ErrorMsg	:= E_ErrorConv.DRIVE_ERROR,
		ErrorData	:= ErrorDataSet);
END_IF	

	




]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_InputMapping" Id="{ff745f24-f7c1-483a-9dd1-7fc42ebd6b2b}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_InputMapping
 * 	FUNCTION	Input mapping
 **************************************************************************************)
 
(**************************************************************************************************************
   Hardware Inputs
***************************************************************************************************************)

// Feedback from Drive
Inputs.StatusWord1.DriveRun			:= HW_Inputs.PI01_StatusWord1.0;
Inputs.StatusWord1.InverterReady	:= HW_Inputs.PI01_StatusWord1.1;
Inputs.StatusWord1.IPOS_Referenced	:= HW_Inputs.PI01_StatusWord1.2;
Inputs.StatusWord1.TargetPosReached	:= HW_Inputs.PI01_StatusWord1.3;
Inputs.StatusWord1.BrakeOpen		:= HW_Inputs.PI01_StatusWord1.4;
Inputs.StatusWord1.Fault			:= HW_Inputs.PI01_StatusWord1.5;
Inputs.StatusWord1.LimitSwitchRight	:= HW_Inputs.PI01_StatusWord1.6;
Inputs.StatusWord1.LimitSwitchLeft	:= HW_Inputs.PI01_StatusWord1.7;

Inputs.ReferenceSignal	:= HW_Inputs.ReferenceSignal;

//Encoder
Inputs.ActualPosition	:= (UINT_TO_DINT(HW_Inputs.PI02_Encoder_High)*65536) + UINT_TO_DINT(HW_Inputs.PI03_Encoder_Low);

// Speed
Inputs.ActualSpeed		:= HW_Inputs.PI04_ActualSpeed;

//Current
Inputs.CurrentLoad		:= HW_Inputs.PI05_CurrentLoad;

// Device Load
Inputs.DeviceLoad		:= HW_Inputs.PI06_DeviceLoad;

// Ethercat State
Inputs.EtherCatState	:= HW_Inputs.EtherCatState;


]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_OutputMapping" Id="{ff053e56-dc30-478c-97ce-501e6a130bc5}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_OutputMapping
 * 	FUNCTION	Output mapping
 **************************************************************************************)

Outputs.Controlword.Reset			:= ErrorDataSet.ResetError;
 
HW_Outputs.PO1_ControlWord.0	:= Outputs.ControlWord.InverterLock;
HW_Outputs.PO1_ControlWord.1	:= Outputs.Controlword.FastStop;
HW_Outputs.PO1_ControlWord.2	:= Outputs.Controlword.StopInverter;
HW_Outputs.PO1_ControlWord.3	:= Outputs.Controlword.HoldPosition;
HW_Outputs.PO1_ControlWord.4	:= Outputs.Controlword.SwitchIntegrator;
HW_Outputs.PO1_ControlWord.5	:= Outputs.Controlword.SwitchParameter;
HW_Outputs.PO1_ControlWord.6	:= Outputs.Controlword.Reset;
HW_Outputs.PO1_ControlWord.7	:= Outputs.Controlword.Reserve1;
HW_Outputs.PO1_ControlWord.8	:= Outputs.Controlword.Start;
HW_Outputs.PO1_ControlWord.9	:= Outputs.Controlword.ManualFwd;
HW_Outputs.PO1_ControlWord.10	:= Outputs.Controlword.ManualBwd;
HW_Outputs.PO1_ControlWord.11	:= Outputs.Controlword.SelectMode1;
HW_Outputs.PO1_ControlWord.12	:= Outputs.Controlword.SelectMode2;
HW_Outputs.PO1_ControlWord.13	:= Outputs.Controlword.Reserve2;
HW_Outputs.PO1_ControlWord.14	:= Outputs.Controlword.Ramp;
HW_Outputs.PO1_ControlWord.15	:= Outputs.Controlword.SoftLimitSwitch;

IF AutoModeOn AND (eState <> E_MoviDriveStateIPOS.RUN_REF) THEN
	HW_Outputs.PO4_Speed 				:= Settings.SpeedAuto;
ELSE
	HW_Outputs.PO4_Speed 				:= Settings.SpeedManual;
END_IF

TargetPosReached 			:= Inputs.StatusWord1.TargetPosReached;
Position 					:= Inputs.ActualPosition; 
HW_Outputs.InverterLock		:= NOT Outputs.ControlWord.InverterLock;
HW_Outputs.Enable			:= Enable;

HW_Outputs.PO2_TargetPositionHigh		:= DINT_TO_UINT(SHR(TargetPosition,16));//DINT_TO_UINT(TRUNC(TargetPosition/65535));
HW_Outputs.PO3_TargetPositionLow		:= DINT_TO_UINT(TargetPosition MOD 65535);
HW_Outputs.PO5_AccRamp					:= Settings.Acceleration;
HW_Outputs.PO6_DecRamp					:= Settings.Deceleration;


 
DriveReady	:= Inputs.StatusWord1.InverterReady;

IPOS_Referenced := Inputs.StatusWord1.IPOS_Referenced;]]></ST>
      </Implementation>
    </Action>
    <Action Name="A_States" Id="{6af3281c-4ece-411f-ab8e-6219feef38d1}">
      <Implementation>
        <ST><![CDATA[(**************************************************************************************
 * 	NAME		A_States
 * 	FUNCTION	State Control
 **************************************************************************************)
 
CASE eState OF
(*************************************************************************
  Init
*************************************************************************)
	E_MoviDriveStateIPOS.INIT:
		
		Outputs.Controlword.SelectMode1 	:= FALSE;
		Outputs.Controlword.SelectMode2 	:= FALSE;
		// Reset
		DriveRunning						:= FALSE;
					
		// Deaktivat Inverter	
		DriveRunning						:= FALSE;
		Outputs.ControlWord.StopInverter	:= FALSE;
		Outputs.ControlWord.FastStop		:= FALSE;
		Outputs.ControlWord.Start			:= FALSE;

				
		IF ErrorDataSet.ErrorType <> E_ErrorType.ERROR THEN
			// Drive Switch on	
			eLastState 	:= eState;
			eState 		:= E_MoviDriveStateIPOS.INVERTER_OFF;
		ELSE
			eLastState 	:= eState;
			eState		:= E_MoviDriveStateIPOS.FAULT;
		END_IF

(*************************************************************************
 Motor is Idle now Start the Inverter
*************************************************************************)
	E_MoviDriveStateIPOS.INVERTER_OFF:
	
		Outputs.Controlword.SelectMode1 		:= FALSE;
		Outputs.Controlword.SelectMode2 		:= FALSE;
			
		// Reset	
		DriveRunning							:= FALSE;
		Outputs.ControlWord.StopInverter		:= FALSE;
		Outputs.ControlWord.FastStop			:= FALSE;
		Outputs.ControlWord.Start				:= FALSE;

		// Check Error
		IF ErrorDataSet.ErrorType = E_ErrorType.ERROR THEN	
			eLastState 	:= eState;
			eState 		:= E_MoviDriveStateIPOS.FAULT;
		// Inverter is Activated
		ELSIF Inputs.StatusWord1.InverterReady THEN
			eLastState 	:= eState;
			eState 		:= E_MoviDriveStateIPOS.INVERTER_ON;	
		END_IF	
				
(*************************************************************************
  Inverter is Switched On
*************************************************************************)
	E_MoviDriveStateIPOS.INVERTER_ON:
	
		Outputs.Controlword.SelectMode1 := FALSE;
		Outputs.Controlword.SelectMode2 := FALSE;
		
		// Reset	
		DriveRunning					:= FALSE;
		Outputs.ControlWord.StopInverter:= FALSE;
		Outputs.ControlWord.Start		:= FALSE;
		Outputs.ControlWord.FastStop	:= TRUE;
		Outputs.Controlword.ManualFwd	:= FALSE;
		Outputs.Controlword.ManualBWD	:= FALSE;

			// Check Error
		IF ErrorDataSet.ErrorType = E_ErrorType.ERROR THEN	
				eLastState 	:= eState;
				eState 			:= E_MoviDriveStateIPOS.FAULT;
		// Drive is not referenced
		ELSIF (NOT Inputs.StatusWord1.IPOS_Referenced AND AutoModeOn) OR (RisEdgeReferenceStart.Q AND NOT AutoModeOn)THEN
				eLastState 	:= eState;
				eState 			:= E_MoviDriveStateIPOS.RUN_REF;
		// Order Drive Manual
		ELSIF NOT AutoModeOn AND RUN THEN
				eLastState 	:= eState;
				eState 			:= E_MoviDriveStateIPOS.RUN_MANUAL;
		// Order drive Run
		ELSIF AutoModeOn AND RUN AND NOT Inputs.StatusWord1.TargetPosReached THEN
				eLastState 	:= eState;
				eState 		:= E_MoviDriveStateIPOS.RUN_AUTO;	
		END_IF

(*************************************************************************
  Drive Reference Encoder		 
*************************************************************************)
	E_MoviDriveStateIPOS.RUN_REF:
		//Change to Manual Mode
		Outputs.Controlword.SelectMode1 := TRUE;
		Outputs.Controlword.SelectMode2 := FALSE;
		RefDone := FALSE;
		
		IF RisEdgeReferenceSignal.Q THEN
			WaitForRef							:= TRUE;	
		END_IF
		
		IF WaitForRef THEN
			Merker 								:= FALSE;
			Outputs.ControlWord.StopInverter	:= TRUE;
			Outputs.Controlword.ManualFwd		:= FALSE;
			Outputs.Controlword.ManualBwd		:= FALSE;
			// Change to Ref Mode
			Outputs.Controlword.SelectMode1 	:= FALSE;
			Outputs.Controlword.SelectMode2 	:= TRUE;
			Outputs.Controlword.Start			:= TRUE;
			RefDone								:= TRUE;
		ELSIF Inputs.ReferenceSignal AND NOT Merker THEN
			Merker 								:= TRUE;
			Outputs.ControlWord.StopInverter	:= TRUE;
			IF Settings.ReferenceToFwd THEN 
				Outputs.Controlword.ManualBwd		:= TRUE;
				Outputs.Controlword.ManualFwd		:= FALSE;
			ELSE
				Outputs.Controlword.ManualFwd		:= TRUE;
				Outputs.Controlword.ManualBwd		:= FALSE;
			END_IF
			
		ELSIF (NOT Inputs.ReferenceSignal AND NOT Merker) 
		OR (Merker AND TofLeaveRef.Q AND NOT Inputs.ReferenceSignal)THEN
		 	Outputs.ControlWord.StopInverter	:= TRUE;
			IF Settings.ReferenceToFwd THEN 
				Outputs.Controlword.ManualBwd		:= FALSE;
				Outputs.Controlword.ManualFwd		:= TRUE;
			ELSE
				Outputs.Controlword.ManualBwd		:= TRUE;
				Outputs.Controlword.ManualFwd		:= FALSE;
			END_IF
		END_IF
		
		// Check Error
		IF ErrorDataSet.ErrorType = E_ErrorType.ERROR THEN	
			Outputs.Controlword.Start			:= FALSE;
			Outputs.ControlWord.StopInverter	:= FALSE;
			WaitForRef							:= FALSE;
			eLastState 							:= eState;
			eState 								:= E_MoviDriveStateIPOS.FAULT;
		ELSIF Inputs.StatusWord1.IPOS_Referenced AND RefDone AND (Inputs.ActualPosition < 5) AND (Inputs.ActualPosition > -5) THEN
			Outputs.Controlword.Start			:= FALSE;
			Outputs.ControlWord.StopInverter	:= FALSE;
			WaitForRef							:= FALSE;
			eLastState 	:= eState;
			eState 		:= E_MoviDriveStateIPOS.INVERTER_ON;	
		END_IF


(*************************************************************************
  Drive in Tapmode		 
*************************************************************************)
	E_MoviDriveStateIPOS.RUN_MANUAL:
	
		Outputs.Controlword.SelectMode1 := TRUE;
		Outputs.Controlword.SelectMode2 := FALSE;
		//Outputs.ControlWord.FastStop	:= FALSE;
    	
		Outputs.ControlWord.ManualFwd			:= 	BtnMaunalFWD;
		Outputs.ControlWord.ManualBwd			:= 	BtnMaunalBWD;
		Outputs.ControlWord.StopInverter 		:= 	BtnMaunalFWD XOR BtnMaunalBWD;
		Outputs.ControlWord.Start 				:= 	BtnMaunalFWD XOR BtnMaunalBWD;
    	
		// Check Error
		IF ErrorDataSet.ErrorType = E_ErrorType.ERROR THEN
			Outputs.ControlWord.ManualFwd			:= FALSE;
			Outputs.ControlWord.ManualBwd			:= FALSE;
			eLastState 							:= eState;
			eState 								:= E_MoviDriveStateIPOS.FAULT;	
		ELSIF NOT BtnMaunalFWD AND NOT BtnMaunalBWD THEN
			Outputs.ControlWord.ManualFwd			:= FALSE;
			Outputs.ControlWord.ManualBwd			:= FALSE;
			Outputs.ControlWord.StopInverter	:= FALSE;
			Outputs.ControlWord.Start					:= FALSE;
			eLastState 												:= eState;
			eState 														:= E_MoviDriveStateIPOS.INVERTER_ON;
		END_IF
		
		

(*************************************************************************
  Drive Positioning		 
*************************************************************************)
	E_MoviDriveStateIPOS.RUN_AUTO:
	
		Outputs.Controlword.SelectMode1 := TRUE;
		Outputs.Controlword.SelectMode2 := TRUE;
	
		// Reset	
		DriveRunning						:= TRUE;
		Outputs.ControlWord.StopInverter	:= TRUE;
		Outputs.ControlWord.Start			:= TRUE;
		Outputs.ControlWord.FastStop		:= TRUE;
			
		
		// Check Error
		IF ErrorDataSet.ErrorType = E_ErrorType.ERROR THEN	
			eLastState 						:= eState;
			eState 								:= E_MoviDriveStateIPOS.FAULT;			
		// Drive normal Stop
		ELSIF NOT Run OR Inputs.StatusWord1.TargetPosReached THEN
			DriveRunning											:= FALSE;
			Outputs.ControlWord.StopInverter						:= FALSE;
			Outputs.ControlWord.Start								:= FALSE;
			eLastState 												:= eState;
			eState 													:= E_MoviDriveStateIPOS.INVERTER_ON;
		END_IF
		
			
(*************************************************************************
  Drive is in fault state
*************************************************************************)
	E_MoviDriveStateIPOS.FAULT:
	
		Outputs.Controlword.SelectMode1 := FALSE;
		Outputs.Controlword.SelectMode2 := FALSE;

		// All Outputs set False
		DriveRunning								:= FALSE;
		Outputs.ControlWord.StopInverter			:= FALSE;
		Outputs.ControlWord.Start					:= FALSE;
		Outputs.ControlWord.FastStop				:= FALSE;
		Outputs.Controlword.ManualFwd				:= FALSE;
		Outputs.Controlword.ManualBWD				:= FALSE;
	
		
		// when error is gone remove reset signal
		IF ErrorDataSet.ErrorType <> E_ErrorType.ERROR AND Inputs.StatusWord1.Fault = 0 THEN
			Outputs.ControlWord.Reset 		:= FALSE;		
			eLastState 						:= eState;
			eState 							:= E_MoviDriveStateIPOS.INIT;
		// send reset signal to Movimot
		ELSIF ErrorDataSet.ResetError THEN
			Outputs.ControlWord.Reset 	:= TRUE;
			F_ResetError(
				Reset := FALSE, 
				ErrorData := ErrorDataSet);
		END_IF
		
(*************************************************************************
  State error
*************************************************************************)
ELSE
	// Programming error
	DebugMsg := CONCAT(	'Programming error - Invalid state: ', 
					   	INT_TO_STRING(eState));
		
	fbDebugMsg.M_SendErrorMsg(DebugMsg);
END_CASE

// Call TON Reference
TofLeaveRef(IN:= Inputs.ReferenceSignal AND Merker AND (eState = E_MoviDriveStateIPOS.RUN_REF), PT:= T#1S, Q=> , ET=> );
RisEdgeReferenceSignal(CLK:= Inputs.ReferenceSignal, Q=> );
RisEdgeReferenceStart(CLK:= ReferenceManual, Q=> );


]]></ST>
      </Implementation>
    </Action>
    <Method Name="M_Init" Id="{604323da-d500-4982-8dcd-2e0bf7484b5f}">
      <Declaration><![CDATA[METHOD M_Init : BOOL
VAR_INPUT
	/// Force Init
	mInit: BOOL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF mInit OR NOT Init THEN
	
	// Init ist Finish
	Init		:= TRUE;				
	eLastState 	:= eState;
	eState 		:= E_MoviDriveStateIPOS.INIT;

END_IF

]]></ST>
      </Implementation>
    </Method>
  </POU>
</TcPlcObject>