﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_ChainConveyorControl" Id="{d7cae3d8-f1a6-49da-add7-e8129698957c}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_ChainConveyorControl IMPLEMENTS I_StateMachineTextCallback
(**************************************************************************************
 * 	Application		:	BoxControl Framework
 *	Revision		:	$Revision:  $
 *	Revision date	:	$Date:$
 *	Last changed by	:	$Author:  $
 *	URL				:	$URL: $
 *
 *	Purpose			:	Chain conveyor control.
 *
 * ------------------------------------------------------------------------------------
 *	Copyright Swisslog [IP AG], Switzerland. All rights reserved.
 * 
 * ------------------------------------------------------------------------------------
 *	
 * 	Revision History: 
 *
 * 	VERSION			DATE			INITIALS			DESCRIPTION
 *  0.1				04.01.2022		dlu					First draft
 *
 **************************************************************************************)
VAR_INPUT
	Config					: ST_ChainConveyorConfig;
	AutomaticModeStarted	: BOOL;
	AutomaticEnable			: BOOL;
END_VAR

VAR_IN_OUT 
	ErrorDataSet 			: ST_ErrorDataSet;
END_VAR

VAR
	fbStateMachine			: FB_StateMachine(mName:= 'Chain conveyor state', mStateTextCallback:= THIS^);
	eState					: E_ChainConveyorStates	:= E_ChainConveyorStates.INIT;
	eLastState				: E_ChainConveyorStates	:= E_ChainConveyorStates.INIT;
	
	CmdDriveBackwards		: BOOL;
	CmdDriveForwards		: BOOL;
	
	SensorBackwards			: BOOL;
	SensorForwards			: BOOL;
	
	RunMotorExtern			: BOOL;
	MotorDir				: BOOL;
	MotorFastSpeedActivate  : BOOL;
	
	Init					: BOOL;
	ManualMode				: BOOL;
	SemiAutoMode			: BOOL;
	AutomaticMode			: BOOL;
	Error					: BOOL;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[M_RunStates(_ErrorDataSet:= ErrorDataSet);]]></ST>
    </Implementation>
    <Method Name="M_GetStateText" Id="{03657d30-0558-4d99-a821-561f4265b96a}">
      <Declaration><![CDATA[(* forwards state text to FB_StateMachine*)
METHOD M_GetStateText : A_StateText
VAR_INPUT
	(* requesting state machine instance*)
	StateMachine	: POINTER TO FB_StateMachine;
	(* state number which text is requested*)
	StateNbr	: UINT;
END_VAR
VAR
	eStateEnum	: E_ChainConveyorStates := E_ChainConveyorStates.INIT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[eStateEnum:= StateNbr;
M_GetStateText  := TO_STRING(eStateEnum);
]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_RunStates" Id="{8cb927d7-0389-4a33-84a3-2d35c90083d5}">
      <Declaration><![CDATA[METHOD PRIVATE M_RunStates
VAR_IN_OUT 
	_ErrorDataSet : ST_ErrorDataSet;
END_VAR
VAR
	Reason	: STRING[127];
    i       : INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
IF Error THEN 
	F_SetError(ErrorType	:= E_ErrorType.ERROR,
			   ErrorParam	:= 'set error by extern',
			   ErrorMsg		:= E_ErrorConv.DEVICE_ERROR ,
			   ErrorData	:= _ErrorDataSet);
					   
	fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.Error,
									mReason:= 'set error by extern');
									
END_IF

// Run state maschine
eState		:= fbStateMachine.P_State;

CASE eState OF
	E_ChainConveyorStates.INIT:
		IF fbStateMachine.P_Entering THEN 
			MotorDir			:= FALSE;
			CmdDriveBackwards	:= FALSE;
			CmdDriveForwards	:= FALSE;
			MotorFastSpeedActivate := FALSE;
			Error				:= FALSE;
			F_ResetError(Reset:= TRUE, ErrorData := _ErrorDataSet);
		END_IF

		Init := TRUE;
		fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.Idle,
								mReason:= 'initialisation done');

	
	E_ChainConveyorStates.Idle:
		IF fbStateMachine.P_Entering THEN 
			RunMotorExtern := FALSE;
			IF _ErrorDataSet.ErrorType = E_ErrorType.ERROR THEN 
				fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.Error,
						mReason:= 'idle with error pending');
			END_IF
		
		ELSIF CmdDriveBackwards AND AutomaticModeStarted THEN 
				fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.DriveBackwards,
								mReason:= 'by command');
								
		ELSIF CmdDriveForwards AND AutomaticModeStarted THEN 
				fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.DriveForwards,
								mReason:= 'by command');
		END_IF
		
	E_ChainConveyorStates.IsBackwardSensorOccupied:
		IF fbStateMachine.P_Entering THEN 
			CmdDriveBackwards 	:= FALSE;
			CmdDriveForwards	:= FALSE;
		END_IF
		
		IF CmdDriveForwards AND AutomaticModeStarted THEN 
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.DriveForwards,
							mReason:= 'by command');
		ELSIF NOT SensorBackwards AND AutomaticModeStarted THEN 
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.Idle,
										  mReason:= 'Not in Backwards position any more');		
		END_IF 
		
	E_ChainConveyorStates.IsForwardSensorOccupied:
		IF fbStateMachine.P_Entering THEN 
			CmdDriveBackwards 	:= FALSE;
			CmdDriveForwards	:= FALSE;
		END_IF
		
		IF CmdDriveBackwards AND AutomaticModeStarted THEN 
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.DriveBackwards,
							mReason:= 'by command');
		ELSIF NOT SensorForwards AND AutomaticModeStarted THEN 
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.Idle,
										  mReason:= 'Not in Forwards position any more');		
		END_IF 
		
	E_ChainConveyorStates.DriveBackwards:
		IF fbStateMachine.P_Entering THEN 
			CmdDriveBackwards := FALSE;
		END_IF 
		
		MotorDir := FALSE;
		MotorFastSpeedActivate := FALSE;
		
		// Automatic mode was forced to stop
		IF NOT AutomaticEnable THEN
			
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.AutomaticModeStopped,
									  mReason:= 'Automatic mode was forced to stopped');
		
		ELSIF SensorBackwards THEN
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.IsBackwardSensorOccupied,
						mReason:= 'sensor reahed');	
						
		ELSIF fbStateMachine.P_TimeInState > Config.Movement_Timeout THEN 
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'Timeout Movement backwards',
					   ErrorMsg		:= E_ErrorConv.TIMEOUT,
					   ErrorData	:= _ErrorDataSet);
					   
					 
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.Error,
						mReason:= 'timeout');	
		END_IF
		
	E_ChainConveyorStates.DriveForwards:
		IF fbStateMachine.P_Entering THEN 
			CmdDriveForwards := FALSE;
		END_IF 
		
		MotorDir := TRUE;
		MotorFastSpeedActivate := FALSE;
		
		// Automatic mode was forced to stop
		IF NOT AutomaticEnable THEN
			
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.AutomaticModeStopped,
									  mReason:= 'Automatic mode was forced to stopped');
		
		ELSIF SensorForwards THEN
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.IsForwardSensorOccupied,
						mReason:= 'sensor reahed');	
						
		ELSIF fbStateMachine.P_TimeInState > Config.Movement_Timeout THEN 
			F_SetError(ErrorType	:= E_ErrorType.ERROR,
					   ErrorParam	:= 'Timeout Movement forwards',
					   ErrorMsg		:= E_ErrorConv.TIMEOUT,
					   ErrorData	:= _ErrorDataSet);
					   
					 
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.Error,
						mReason:= 'timeout');	
		END_IF

	E_ChainConveyorStates.AutomaticModeStopped:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		
		// Automatic mode is started
		IF AutomaticModeStarted AND AutomaticEnable THEN
			
			fbStateMachine.M_SetState(mState:= eLastState,
									  mReason:= 'Automatic mode started again');
	  	END_IF
		
	E_ChainConveyorStates.Manual:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		
	E_ChainConveyorStates.SemiAuto:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		
	E_ChainConveyorStates.Error:
		IF fbStateMachine.P_Entering THEN 
			;
		END_IF
		IF _ErrorDataSet.ResetError OR _ErrorDataSet.ErrorType = E_ErrorType.NO_ERROR_PENDING THEN 
			fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.INIT,
						mReason:= 'reset by extern');	
		END_IF
	
	(********************************************************************************************************************************)
	(** invalid *********************************************************************************************************************)
	ELSE
		
		fbStateMachine.M_SetState1(mMsgType:= E_DebugMsgType.ERROR,
								mState := E_ChainConveyorStates.Init,
								mReason:= 'invalid internal state');	
			
END_CASE 

eLastState	:= fbStateMachine.P_LastState;

]]></ST>
      </Implementation>
    </Method>
    <Property Name="P_AutomaticMode" Id="{a363d9a1-5024-4d4e-a0b0-295a3e44fda0}">
      <Declaration><![CDATA[PROPERTY P_AutomaticMode : BOOL]]></Declaration>
      <Get Name="Get" Id="{a67e7943-ab4e-42a1-97f6-890e8f2f6fd2}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_AutomaticMode := NOT (eState = E_ChainConveyorStates.Manual) AND NOT (eState = E_ChainConveyorStates.SemiAuto);
]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{62bc58c2-2160-43f9-a384-8cfd39441284}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF AutomaticMode <> P_AutomaticMode THEN
	AutomaticMode := P_AutomaticMode;
	
	IF AutomaticMode THEN 
		ManualMode := FALSE;
		SemiAutoMode := FALSE;
		fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.Idle,
										mReason:= 'set automatic mode by extern');	
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_CmdDriveBackwards" Id="{c5f7bb66-5184-4930-8d04-59c0b1cd9404}">
      <Declaration><![CDATA[PROPERTY P_CmdDriveBackwards : BOOL]]></Declaration>
      <Set Name="Set" Id="{445ddeee-ef96-4b23-b037-934b833973ef}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF (P_CmdDriveBackwards AND eState <> E_ChainConveyorStates.IsBackwardSensorOccupied)THEN 
	CmdDriveForwards  := FALSE;
	CmdDriveBackwards := P_CmdDriveBackwards;
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_CmdDriveForwards" Id="{b337096b-c61f-410c-a7ea-54e64e74f8d5}">
      <Declaration><![CDATA[PROPERTY P_CmdDriveForwards : BOOL]]></Declaration>
      <Set Name="Set" Id="{b6c4544b-cc26-4da1-8e48-0a71652237ae}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF (P_CmdDriveForwards AND eState <> E_ChainConveyorStates.IsForwardSensorOccupied)THEN 
	CmdDriveBackwards := FALSE;
	CmdDriveForwards := P_CmdDriveForwards;
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_CurrentPosition" Id="{b50b558e-4ff5-4877-a2cd-7e1e60415378}">
      <Declaration><![CDATA[PROPERTY P_CurrentPosition : E_ChainConveyorPositions]]></Declaration>
      <Get Name="Get" Id="{829ba9e1-0533-4254-ac31-8e3a162eaa4f}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF eState = E_ChainConveyorStates.IsBackwardSensorOccupied THEN 
	P_CurrentPosition := E_ChainConveyorPositions.Backwards;
	
ELSIF eState = E_ChainConveyorStates.IsForwardSensorOccupied THEN 
	P_CurrentPosition := E_ChainConveyorPositions.Forwards;
	
ELSE
	P_CurrentPosition := E_ChainConveyorPositions.Undefined;
END_IF

 ]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="P_Error" Id="{7516505e-65c4-40e7-9782-bd2dabc6bb4d}">
      <Declaration><![CDATA[PROPERTY P_Error : BOOL]]></Declaration>
      <Get Name="Get" Id="{95547c4a-9715-4189-9600-b431e1640c2f}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_Error := (eState = E_ChainConveyorStates.Error);]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{8f09737e-ff9d-4915-a0f7-8132aaa20ce4}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[Error := P_Error;
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_Init" Id="{ff645890-59dd-41f1-b41b-528eeae92da8}">
      <Declaration><![CDATA[PROPERTY P_Init : BOOL]]></Declaration>
      <Get Name="Get" Id="{5e01250a-56a5-4d42-860b-15fe9826661c}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_Init := Init;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{f7d98cb9-8a7e-4911-b8ec-cb576d78e9c2}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF AutomaticMode THEN
	
	IF NOT P_Init THEN
		Init := P_Init;
		fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.Init,
								mReason:= 'set init by extern');
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_ManualMode" Id="{7226ec46-b83e-41a6-84dc-5958d81cb82b}">
      <Declaration><![CDATA[PROPERTY P_ManualMode : BOOL]]></Declaration>
      <Get Name="Get" Id="{da13f260-7cfa-4c80-8d0e-3d4c47afc72a}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_ManualMode := (eState = E_ChainConveyorStates.Manual);
]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{32eee1c4-1638-4715-b243-10f107c9cec3}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF ManualMode <> P_ManualMode THEN
	ManualMode := P_ManualMode;
	
	IF ManualMode THEN 
		AutomaticMode 	:= FALSE;
		SemiAutoMode 	:= FALSE;
		fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.Manual,
									mReason:= 'set manual mode by extern');
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_MotorDir" Id="{a55af63e-6f48-4e3a-88a6-b7be2230930d}">
      <Declaration><![CDATA[PROPERTY P_MotorDir : BOOL]]></Declaration>
      <Get Name="Get" Id="{f45606a9-6376-4f41-8234-2636f5fef82e}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_MotorDir := MotorDir;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{e0e3b861-913e-4251-af0d-078d04318b21}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF eState = E_ChainConveyorStates.Manual OR eState = E_ChainConveyorStates.SemiAuto THEN 
	MotorDir := P_MotorDir;
END_IF 
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_MotorFastSpeedActive" Id="{517b978b-a0f8-437c-9875-e2d65bf1a314}">
      <Declaration><![CDATA[PROPERTY P_MotorFastSpeedActive : BOOL]]></Declaration>
      <Get Name="Get" Id="{6bebe9c0-1858-48da-b2d5-84e8b1aea9f3}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_MotorFastSpeedActive :=  MotorFastSpeedActivate;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{aa5dc829-2284-431d-9b1a-669beb949a9e}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF eState = E_ChainConveyorStates.Manual OR eState = E_ChainConveyorStates.SemiAuto THEN 
	MotorFastSpeedActivate := P_MotorFastSpeedActive;
END_IF 
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_MotorRun" Id="{084e14a4-7357-40aa-bfa3-5542d6fdf9a9}">
      <Declaration><![CDATA[PROPERTY P_MotorRun : BOOL]]></Declaration>
      <Get Name="Get" Id="{50b2c6e6-dc38-4bb8-bbfc-3a6b0ae69002}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_MotorRun := 	(eState = E_ChainConveyorStates.DriveBackwards)
				OR (eState = E_ChainConveyorStates.DriveForwards)
				OR (eState = E_ChainConveyorStates.Manual AND RunMotorExtern)
				OR (eState = E_ChainConveyorStates.SemiAuto AND RunMotorExtern);]]></ST>
        </Implementation>
      </Get>
    </Property>
    <Property Name="P_RunMotorExtern" Id="{1c57b4e7-2273-4741-a2e7-bc826dbe3381}">
      <Declaration><![CDATA[PROPERTY P_RunMotorExtern : BOOL]]></Declaration>
      <Set Name="Set" Id="{5c3d0a1a-e127-4f76-9378-e4ec5fd00e0a}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF (eState = E_ChainConveyorStates.Manual OR eState = E_ChainConveyorStates.SemiAuto) THEN 
	RunMotorExtern := P_RunMotorExtern;
END_IF
]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_SemiAutoMode" Id="{bf66ef31-92d5-4afc-8cda-af96f21156be}">
      <Declaration><![CDATA[PROPERTY P_SemiAutoMode : BOOL]]></Declaration>
      <Get Name="Get" Id="{815a1d89-c44e-47a1-9884-c740c360c51c}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_SemiAutoMode := (eState = E_ChainConveyorStates.SemiAuto);
]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{1d3b9743-ed69-4e1a-a236-66de63697a8e}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[IF SemiAutoMode <> P_SemiAutoMode THEN
	SemiAutoMode := P_SemiAutoMode;
	
	IF SemiAutoMode THEN 
		AutomaticMode 	:= FALSE;
		ManualMode 		:= FALSE;
		fbStateMachine.M_SetState(mState:= E_ChainConveyorStates.SemiAuto,
									mReason:= 'set semi-auto mode by extern');
	END_IF
END_IF]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_SensorBackwards" Id="{e0efbae6-ac4a-491a-a86a-7364cef1ec39}">
      <Declaration><![CDATA[PROPERTY P_SensorBackwards : BOOL]]></Declaration>
      <Set Name="Set" Id="{44793cd9-b49f-4836-aa99-09a8f1b50325}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[SensorBackwards := P_SensorBackwards;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="P_SensorForwards" Id="{73696f2b-ed1a-427c-a7be-a15b989d2c2f}">
      <Declaration><![CDATA[PROPERTY P_SensorForwards : BOOL]]></Declaration>
      <Set Name="Set" Id="{ba69bd0a-bdf6-4341-8a5e-a4834bb1847d}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[SensorForwards := P_SensorForwards;]]></ST>
        </Implementation>
      </Set>
    </Property>
  </POU>
</TcPlcObject>