# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 07:37:18 2021

@author: g7brank
"""

import xml.etree.ElementTree as ET
import re as re

def remove_namespaces(tree):
     for el in tree.iter():
        match = re.match("^(?:\{.*?\})?(.*)$", el.tag)
        
        if match:
            el.tag = match.group(1)
 
            
def wrTmp(treeObject, filepath):
    xml_str = '<?xml version="1.0" encoding="utf-8"?>' + '\n' 
    xml_byte = xml_str.encode() + ET.tostring(treeObject, method='xml')
    with open(filepath, 'wb') as xml_file:
         xml_file.write(xml_byte)

def isChild(tree, childName):
    if childName[0] in tree.keys():
        if not childName[1::]:
            if tree[childName[0]] is None:
                return True
            else:
                return False
        else:
            return isChild(tree[childName[0]], childName[1::])
    else:
        return False
    
def addChild(tree,childName):
    if not childName:
        #print("child already defined")
        return 0
    elif childName[0] in tree.keys():
        return addChild(tree[childName[0]], childName[1::])
    else:
        if not childName[1::]:
            tree[childName[0]] = None
            return 1
        else:
            tree[childName[0]] = dict()
            return addChild(tree[childName[0]], childName[1::])

        
import sys
import shutil

import glob
import pathlib



try:
    search_path = str(pathlib.Path(__file__).parent.parent.parent.parent.resolve()).replace('\\',r'/')+"/*/*/*.plcproj"
    text_files = glob.glob(search_path, recursive = True)
except:
    sys.exit("No .plcproj file found")

i = 0
for each in text_files:
    if not (str(pathlib.Path().resolve()) in each):
        plcProjDataPath = each
        i = i + 1

if i > 1:
    sys.exit("Found more than one .plcproj file")
    
import os
try:
    os.mkdir(str(pathlib.Path().resolve())+ "\\Archive")
except OSError:
    print()#print ("Creation of the directory %s failed" % (str(pathlib.Path().resolve())+ "\\Archive"))
else:
    print ("Successfully created the directory %s " % (str(pathlib.Path().resolve())+ "\\Archive"))

from datetime import datetime
current_time = datetime.now().strftime("%H_%M_%S")
today = datetime.today()
d1 = today.strftime("%d_%m_%Y-")+ current_time

shutil.copyfile(plcProjDataPath, str(pathlib.Path().resolve()) + "\\Archive\\" + d1 + ".plcproj")


tree = ET.parse(plcProjDataPath)

root = tree.getroot()

searchTree = dict()
NumberOfChilds = 0
''' Delete all include files which starts with 20_Product'''

ns= {'xns':'http://schemas.microsoft.com/developer/msbuild/2003'}
for ItemGroupElem in root.findall("xns:ItemGroup",ns):
    for compileElem in ItemGroupElem.findall(".//xns:Compile",ns):
        if '20_Product' in compileElem.attrib['Include']:
            splittedName = compileElem.attrib['Include'].split('\\')
            NumberOfChilds += addChild(searchTree, splittedName)
            ItemGroupElem.remove(compileElem)
        if r'10_Project' in compileElem.attrib['Include']:
            splittedName = compileElem.attrib['Include'].split('\\')
            NumberOfChilds += addChild(searchTree, splittedName)
            ItemGroupElem.remove(compileElem)
     
    for folderElem in ItemGroupElem.findall(".//xns:Folder",ns):
        if '20_Product' in folderElem.attrib['Include']:
            ItemGroupElem.remove(folderElem)
        if r'10_Project' in folderElem.attrib['Include']: 
            ItemGroupElem.remove(folderElem)
         
                 

#print(ET.tostring(root).decode('utf-8'))
remove_namespaces(tree)
path = str(pathlib.Path(__file__).parent.parent.parent.parent.resolve())

beckhoffFileExtention = [".TcTLO",".TcPOU",".TcDUT",".TcGVL",".TcTTO",".TcVis",".TcVMO",".TcGTLO",".TcTLEO",".TCIO",".TcIPO"]

text_files = []
for each in beckhoffFileExtention:
    text_files += (glob.glob(path + "/**/20_Product/**/*"+each, recursive = True))

ElementsToAdd = set()
FoldersToAdd = set()

remPath = ""
#for i, each in enumerate(text_files[0].split("\\")):
for i, each in enumerate(text_files[0].split("\\")):
    if each == "20_Product":
        break
    if i == 0: 
        remPath = each
    else:
        remPath = remPath + "\\" + each
        

for each in text_files:
    if any(eacheach.lower() in each.lower() for eacheach in beckhoffFileExtention):
        ElementsToAdd.add(each[len(remPath)+1::])
        #print(each[len(remPath)+1::])
        partitioned_string = each[len(remPath)+1::].rpartition("\\")
        FoldersToAdd.add(partitioned_string[0])
    else:
        FoldersToAdd.add(each[len(remPath)+1::])
        

text_files = []
for each in beckhoffFileExtention:
    text_files += (glob.glob(path + "/**/10_Project/**/*"+each, recursive = True))

#search for empty files as well
text_files += (glob.glob(path + "/**/10_Project/**/*", recursive = True))
      
         
            
remPath = ""
#for i, each in enumerate(text_files[0].split("\\")):
for i, each in enumerate(text_files[0].split("\\")):
    if each == "10_Project":
        break
    if i == 0: 
        remPath = each
    else:
        remPath = remPath + "\\" + each
        

for each in text_files:
    if any(eacheach.lower() in each.lower() for eacheach in beckhoffFileExtention):
        ElementsToAdd.add(each[len(remPath)+1::])
        partitioned_string = each[len(remPath)+1::].rpartition('\\')
        FoldersToAdd.add(partitioned_string[0])
    else:
        FoldersToAdd.add(each[len(remPath)+1::])
    #print(each)


ElementsToAdd = list(ElementsToAdd)
FoldersToAdd = list(FoldersToAdd)
''' Add files that were found in 20_Product and 10_Project folder'''

FolderAdded = False
ElementsAdded = False

_isUpdateNecessary = False

for i, child in enumerate(root): 
    #print(i)
    if child.tag.endswith("ItemGroup"):
        for idx,subchild in enumerate(child): 
            #print(idx)
            if subchild.tag == "Folder" and not FolderAdded: 
                for FolderToAdd in FoldersToAdd:
                    if len(FolderToAdd)>0:
                        #print(FolderToAdd)
                        elem = ET.SubElement(root.findall(child.tag)[i-1], 'Folder', {"Include":FolderToAdd})
                FolderAdded = True
            if subchild.tag == "Compile" and not ElementsAdded:
                for ElementToAdd in ElementsToAdd:
                    splittedName = ElementToAdd.split('\\')
                    _isChild = isChild(searchTree, splittedName)
                    _isUpdateNecessary = _isUpdateNecessary or (not _isChild)
                    if not _isChild:
                        print("Add " + ElementToAdd + " to .plcProj file")
                    NumberOfChilds -= 1
                    elem = ET.SubElement(root.findall(child.tag)[i-1], 'Compile', {"Include":ElementToAdd})
                    elemContent = ET.SubElement(elem, 'SubCode')
                    elemContent.text = "Code"
                ElementsAdded = True
            
            
root.set("xmlns", "http://schemas.microsoft.com/developer/msbuild/2003")

filesWereDeleted = (NumberOfChilds > 0)

if filesWereDeleted:
    print(str(NumberOfChilds) + " File(s) were deleted")
if _isUpdateNecessary or filesWereDeleted:
    print("regeneration of plcProj done")
    wrTmp(root, plcProjDataPath)
else:
    print("no regeneration of plcProj needed")
    
