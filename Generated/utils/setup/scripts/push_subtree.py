from pathlib import Path
import os
from types import resolve_bases
import configparser
import asyncio
import re
import time
import sys
import asyncio




def get_active_branch_name():
    head_dir = Path(".") / ".git" / "HEAD"
    with head_dir.open("r") as f: content = f.read().splitlines()

    for line in content:
        if line[0:4] == "ref:":
            return line.partition("refs/heads/")[2]


def match_pattern(stdout, stderr):
    pattern_list = ['git push using:.*','Everything up-to-date','.*\]','https.*merge_requests.*', '.*']
    lines = stdout + stderr
    split_lines = str(lines).splitlines()
    #print(lines)
    for line in split_lines:  
        for pattern in pattern_list:     
        
            m = re.search(pattern, line)
            if m:
                print(m.group(0))




    
async def execute_cmd(cmd):
    proc = await asyncio.create_subprocess_shell(
            cmd,
            shell=True,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)
    stdout, stderr = await proc.communicate()
    match_pattern(stdout.decode(), stderr.decode())

async def loading():
    syms = ['Uploading to Swisslog .', 'Uploading to Swisslog ..', 'Uploading to Swisslog ...', 'Uploading to Swisslog ....']
    bs = '\b'
    for sym in syms:
        sys.stdout.write("\b%s\r" % sym)
        sys.stdout.flush()
        await asyncio.sleep(5)
    await loading()


async def push_commits(repository, branch, plcPath):
    status_git_cmd = "git status"
    push_git_cmd = "git subtree push --prefix=ProBoxJumpstart/PLC_1/"+ plcPath + " " + repository + " " + branch
    cmd_list = [push_git_cmd]
    
    task1 =  asyncio.create_task(loading())

   
    # execute git cmd
    for cmd in cmd_list:
        task2 =  await asyncio.create_task(execute_cmd(cmd))

    task1.cancel()

branch = get_active_branch_name()

config = configparser.ConfigParser()

config.read(str(Path(__file__).parent.parent.resolve())+'/configuration/global-config.ini')

commRepo = config['REPO CONFIGURATION']["Common-Repo"]
commBranch = config['REPO CONFIGURATION']["Common-Branch"]
commPlcPath = config['REPO CONFIGURATION']["Common-Plc-Path"]

prodRepo = config['REPO CONFIGURATION']["Product-Repo"]
prodBranch = config['REPO CONFIGURATION']["Product-Branch"]
prodPlcPath = config['REPO CONFIGURATION']["Product-Plc-Path"]

asyncio.run(push_commits(prodRepo,branch, prodPlcPath))
asyncio.run(push_commits(commRepo,branch, commPlcPath))
