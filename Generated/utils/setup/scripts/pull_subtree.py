from pathlib import Path
import os
from types import resolve_bases
import configparser
import asyncio
import re



def get_active_branch_name():
    head_dir = Path(".") / ".git" / "HEAD"
    with head_dir.open("r") as f: content = f.read().splitlines()

    for line in content:
        if line[0:4] == "ref:":
            return line.partition("refs/heads/")[2]
 
def match_pattern(stdout, stderr):
    pattern_list = ['.*']
    lines = stdout + stderr
    split_lines = str(lines).splitlines()
    #print(lines)
    patternMatched = []
    for line in split_lines:  
        for pattern in pattern_list:     
            m = re.search(pattern, line)
            if m:
                patternMatched.append(m.group(0))
    
    return patternMatched
     
def match_git_pattern(text):
    pattern_list = ['Working tree has modifications.  Cannot add.']
    
    failed_matched = []
    for line in text:  
        for pattern in pattern_list:     
            m = re.search(pattern, line)
            if m:
                failed_matched.append(m.group(0))
    
    if failed_matched:
        return "#############! failed pull !#############"
    else:
        return "############# finished pull #############"

async def execute_cmd(cmd):
    proc = await asyncio.create_subprocess_shell(
            cmd,
            shell=True,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)
    stdout, stderr = await proc.communicate()
    matched_patt = match_pattern(stdout.decode(), stderr.decode())
    return matched_patt
       
async def execute_proc(args):
    proc = await asyncio.create_subprocess_exec(
        'python', args,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)
        
    stdout, stderr = await proc.communicate()
    matched_patt = match_pattern(stdout.decode(), stderr.decode())
    return matched_patt

def pull_commits(repository, branch, plcPath):
    status_git_cmd = "git status"
    pull_git_cmd = "git subtree pull --squash --prefix=ProBoxJumpstart/PLC_1/" + plcPath + " " + repository + " " + branch
    cmd_list = [status_git_cmd, pull_git_cmd]
    
    std_git_list = []
    for cmd in cmd_list:
        std_git = asyncio.run(execute_cmd(cmd))
        std_git_list = std_git_list + std_git

    return std_git_list
    
def print_lines(text):
    for line in text:
        print(line)

# future check if git is installed

config = configparser.ConfigParser()

config.read(str(Path(__file__).parent.parent.resolve())+'/configuration/global-config.ini')

commRepo = config['REPO CONFIGURATION']["Common-Repo"]
commBranch = config['REPO CONFIGURATION']["Common-Branch"]
commPlcPath = config['REPO CONFIGURATION']["Common-Plc-Path"]

prodRepo = config['REPO CONFIGURATION']["Product-Repo"]
prodBranch = config['REPO CONFIGURATION']["Product-Branch"]
prodPlcPath = config['REPO CONFIGURATION']["Product-Plc-Path"]


# filter print lines
std_git_p = pull_commits(prodRepo,prodBranch, prodPlcPath)
std_git_c = pull_commits(commRepo,commBranch, commPlcPath)
git_list = std_git_c + std_git_p

print_lines(git_list)
failed_list = match_git_pattern(git_list)
print(failed_list)

# update plcproj file
cwd = os.getcwd()
updateFile = os.path.join(cwd, "utils", "setup", "scripts", "update_project.py")

std_py = asyncio.run(execute_proc(updateFile))

print_lines(std_py)