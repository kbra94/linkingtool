#Call by One-Click-Configuration.bat
import os
import configparser
import sys
import glob
import pathlib

try:
    #text_files =  glob.glob(".\\[!Archive]*\\**\\*.plcproj", recursive = True) 
    #print(Path(__file__).parent.parent.parent.parent.resolve())
    #print(str(Path(__file__).parent.parent.parent.parent.resolve()).replace('\\',r'/')+"/*/*/*.plcproj")
    search_path = str(pathlib.Path(__file__).parent.parent.parent.parent.resolve()).replace('\\',r'/')+"/*/*/*.plcproj"
    print(search_path)
    text_files = glob.glob(search_path, recursive = True)

except:
    sys.exit("No .plcproj file found")
i = 0
for each in text_files:
    if not (str(pathlib.Path().resolve()) in each):
        plcProjDataPath = each
        i = i + 1

path = pathlib.PurePath(plcProjDataPath)


config = configparser.ConfigParser()

config.read(str(pathlib.Path(__file__).parent.parent.resolve())+'/configuration/global-config.ini')

commRepo = config['REPO CONFIGURATION']["Common-Repo"]
commBranch = config['REPO CONFIGURATION']["Common-Branch"]
commPlcPath = config['REPO CONFIGURATION']["Common-Plc-Path"]

cmd = "git subtree add --prefix="+path.parent.parent.name+"/"+path.parent.name+"/"+commPlcPath +" "+ commRepo +" "+ commBranch
print(cmd)
value = os.system(cmd)

prodRepo = config['REPO CONFIGURATION']["Product-Repo"]
prodBranch = config['REPO CONFIGURATION']["Product-Branch"]
prodPlcPath = config['REPO CONFIGURATION']["Product-Plc-Path"]

cmd = "git subtree add --prefix="+path.parent.parent.name+"/"+path.parent.name+"/"+prodPlcPath +" "+ prodRepo +" "+ prodBranch
print(cmd)
value = os.system(cmd)